/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.task;

import com.je.bpm.model.task.payloads.TaskRebookPayload;
import com.je.bpm.model.task.result.TaskListResult;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;

import java.util.Date;
import java.util.List;

/**
 * 任务操作服务定义
 */
public interface TaskOperatorService {

    /**
     * 任务提交
     *
     * @param prod     产品
     * @param piid     流程实例ID
     * @param beanId   业务bean主键
     * @param taskId   任务ID
     * @param assignee 指派人
     * @param comment  审批意见
     * @return
     */
    TaskListResult submit(String prod, String piid, String beanId, String taskId, String assignee, String comment) throws PayloadValidErrorException;

    TaskListResult submit(String prod, String piid, String beanId, String taskId, String assignee, String comment, String target) throws PayloadValidErrorException;

    TaskListResult submit(String prod, String piid, String beanId, String taskId, String assignee, String comment, String target, String nodeId) throws PayloadValidErrorException;

    /**
     * 会签-通过
     *
     * @param prod
     * @param piid
     * @param beanId
     * @param taskId
     * @param comment
     * @return
     * @throws PayloadValidErrorException
     */
    TaskResult pass(String prod, String piid, String beanId, String taskId, String comment) throws PayloadValidErrorException;

    /**
     * 会签-否决
     *
     * @param prod
     * @param piid
     * @param beanId
     * @param taskId
     * @param comment
     * @return
     * @throws PayloadValidErrorException
     */
    TaskResult veto(String prod, String piid, String beanId, String taskId, String comment) throws PayloadValidErrorException;

    /**
     * 会签弃权
     *
     * @param prod
     * @param piid
     * @param beanId
     * @param taskId
     * @param comment
     * @return
     * @throws PayloadValidErrorException
     */
    TaskResult abstain(String prod, String piid, String beanId, String taskId, String comment) throws PayloadValidErrorException;

    /**
     * 会签-改签
     *
     * @param prod
     * @param piid
     * @param beanId
     * @param taskId
     * @param comment
     * @return
     * @throws PayloadValidErrorException
     */
    TaskResult rebook(String prod, String piid, String beanId, String taskId, String comment, TaskRebookPayload.RebookTypeEnum rebookTypeEnum) throws PayloadValidErrorException;

    /**
     * 会签加签
     *
     * @param prod
     * @param piid
     * @param beanId
     * @param taskId
     * @param assignee
     * @return
     * @throws PayloadValidErrorException
     */
    TaskResult countersignAddSignature(String prod, String piid, String beanId, String taskId, String assignee) throws PayloadValidErrorException;

    /**
     * 会签人员调整
     *
     * @param prod
     * @param piid
     * @param beanId
     * @param taskId
     * @return
     */
    TaskResult personnelAdjustments(String prod, String piid, String beanId, String taskId, String assignees) throws PayloadValidErrorException;

    /**
     * 会签减签
     *
     * @param prod
     * @param piid
     * @param beanId
     * @param taskId
     * @param assignee
     * @return
     * @throws PayloadValidErrorException
     */
    TaskResult countersignReduction(String prod, String piid, String beanId, String taskId, String assignee) throws PayloadValidErrorException;

    /**
     * 任务委托
     *
     * @param prod     产品
     * @param piid     流程实例id
     * @param beanId   业务bean主键
     * @param taskId   任务ID
     * @param assignee 指派人
     * @param comment  审批意见
     * @return
     */
    TaskResult delegate(String prod, String piid, String beanId, String taskId, String assignee, String comment) throws PayloadValidErrorException;

    /**
     * 取消委托
     *
     * @param prod   产品
     * @param piid   流程实例Id
     * @param beanId 业务bean主键
     * @param taskId 任务ID
     * @return
     */
    TaskResult cancelDelegate(String prod, String piid, String beanId, String taskId) throws PayloadValidErrorException;

    /**
     * 更换负责人
     *
     * @param prod     产品
     * @param piid     流程实例id
     * @param beanId   业务bean主键
     * @param taskId   任务ID
     * @param assignee 指派人
     * @return
     */
    TaskResult changeAssignee(String prod, String piid, String beanId, String taskId, String assignee) throws PayloadValidErrorException;

    /**
     * 任务领取
     *
     * @param prod   产品
     * @param piid   流程实例id
     * @param beanId 业务bean主键
     * @param taskId 任务ID
     * @return
     */
    TaskResult claim(String prod, String piid, String beanId, String taskId) throws PayloadValidErrorException;

    /**
     * 任务延迟
     *
     * @param prod   产品
     * @param piid   流程实例id
     * @param beanId 业务bean主键
     * @param taskId 任务id
     * @return
     */
    TaskResult delay(String prod, String piid, String beanId, String taskId) throws PayloadValidErrorException;

    /**
     * 任务延迟
     *
     * @param prod    产品
     * @param piid    流程实例id
     * @param beanId  业务bean主键
     * @param taskId  任务id
     * @param dueDate 截止日期
     * @return
     */
    TaskResult delay(String prod, String piid, String beanId, String taskId, Date dueDate) throws PayloadValidErrorException;

    /**
     * 直送
     *
     * @param prod   产品
     * @param piid   流程实例id
     * @param beanId 业务bean主键
     * @param taskId 任务id
     * @return
     */
    TaskResult taskDirectSend(String prod, String piid, String beanId, String taskId) throws PayloadValidErrorException;

    /**
     * 驳回
     *
     * @param prod                产品
     * @param piid                流程实例id
     * @param beanId              业务bean主键
     * @param taskId              任务id
     * @param targetDefinitionKey 目标流程定义key
     * @param comment             操作意见
     * @return
     */
    TaskResult dismiss(String prod, String piid, String beanId, String taskId, String targetDefinitionKey, String comment) throws PayloadValidErrorException;

    /**
     * 退回
     *
     * @param prod    产品
     * @param piid    流程实例id
     * @param beanId  业务bean主键
     * @param taskId  任务id
     * @param comment 操作意见
     * @return
     */
    TaskResult goback(String prod, String piid, String beanId, String taskId, String comment) throws PayloadValidErrorException;

    /**
     * 传阅
     *
     * @param prod         产品
     * @param piid         流程实例id
     * @param beanId       业务bean主键
     * @param taskId       任务id
     * @param assigneeList 传阅人集合
     * @return
     */
    TaskResult passround(String prod, String piid, String beanId, String taskId, List<String> assigneeList) throws PayloadValidErrorException;

    /**
     * 传阅已读
     *
     * @param prod   产品
     * @param piid   流程实例id
     * @param beanId 业务bean主键
     * @param taskId 任务id
     * @return
     */
    TaskResult passroundRead(String prod, String piid, String beanId, String taskId) throws PayloadValidErrorException;

    /**
     * 传阅已读
     *
     * @param prod     产品
     * @param piid     流程实例id
     * @param beanId   业务bean主键
     * @param taskId   任务id
     * @param assignee 指派人
     * @return
     */
    TaskResult passroundRead(String prod, String piid, String beanId, String taskId, String assignee) throws PayloadValidErrorException;

    /**
     * 取回
     *
     * @param prod   产品
     * @param piid   流程实例id
     * @param beanId 业务bean主键
     * @param taskId 任务id
     * @return
     */
    TaskResult retrieve(String prod, String piid, String beanId, String taskId) throws PayloadValidErrorException;

    /**
     * 转办
     *
     * @param prod     产品
     * @param piid     流程实例id
     * @param beanId   业务bean主键
     * @param taskId   任务id
     * @param assignee 指派人
     * @return
     */
    TaskResult transfer(String prod, String piid, String beanId, String taskId, String assignee) throws PayloadValidErrorException;


    TaskResult taskAdjustingPersonnel(String prod, String piid, String beanId, String taskId, String assignee) throws PayloadValidErrorException;

}
