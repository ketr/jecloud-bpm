/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.process;

import com.je.bpm.model.process.results.*;
import com.je.bpm.model.shared.Result;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;

import java.util.Map;

/**
 * 流程操作服务
 */
public interface ProcessOperatorService {

    /**
     * 校验operate是否存在
     *
     * @param operatorId
     * @return
     */
    boolean checkOperateExits(String operatorId);

    /**
     * 操作
     *
     * @param operationId
     * @param params
     * @return
     */
    Result operate(String operationId, Map<String, Object> params) throws PayloadValidErrorException;

    /**
     * 获取按钮
     *
     * @param funcCode 功能code
     * @param userId   bean创建用户id
     * @param beanId   beanId
     * @param bean     bean信息
     * @return
     * @throws PayloadValidErrorException
     */
    ProcessButtonListResult getButton(String funcCode, String userId, String beanId, Map<String, Object> bean) throws PayloadValidErrorException;

    ProcessButtonListResult getButton(String funcCode, String userId, String beanId, Map<String, Object> bean, String prod) throws PayloadValidErrorException;

    /**
     * 获取提交节点信息
     *
     * @param taskId 任务id
     * @param pdid   部署id
     * @param bean   bean信息
     * @return
     * @throws PayloadValidErrorException
     */
    public ProcessNextElementResult getSubmitOutGoingNode(String taskId, String pdid, String prod, Map<String, Object> bean, String beanId) throws PayloadValidErrorException;

    public ProcessNextElementAssigneeResult getSubmitOutGoingNodeAssignee(String taskId, String pdid, String prod, String nodeId, Map<String, Object> bean, String operationId, String adjust, String beanId) throws PayloadValidErrorException;

    default public ProcessNextElementAssigneeResult getSubmitOutGoingNodeAssignee(String taskId, String pdid, String prod, String nodeId, Map<String, Object> bean, String operationId, String beanId) throws PayloadValidErrorException {
        return getSubmitOutGoingNodeAssignee(taskId, pdid, prod, nodeId, bean, operationId, "", beanId);
    }

    public ProcessCirculatedElementResult getCirculatedInfo(String taskId, String prod, Map<String, Object> bean) throws PayloadValidErrorException;

    /**
     * 获取驳回节点信息
     *
     * @param piid   流程实例id
     * @param taskId 任务id
     * @param pdid   部署id
     * @param bean   bean信息
     * @return
     * @throws PayloadValidErrorException
     */
    public ProcessDismissElementResult getDismissOutGoingNode(String piid, String taskId, String pdid, Map<String, Object> bean) throws PayloadValidErrorException;

    /**
     * 获取退回节点名称
     *
     * @param piid   流程实例id
     * @param taskId 任务id
     * @return
     * @throws PayloadValidErrorException
     */
    public ProcessGobackElementResult getGobackNodeName(String piid, String taskId) throws PayloadValidErrorException;

    /**
     * 获取当前节点的驳回节点信息
     *
     * @param piid   流程实例id
     * @param taskId 任务id
     * @return
     * @throws PayloadValidErrorException
     */
    public ProcessGobackElementResult getDismissNodeName(String piid, String taskId) throws PayloadValidErrorException;

    /**
     * 获取取回节点名称
     *
     * @param piid
     * @param taskId
     * @return
     * @throws PayloadValidErrorException
     */
    public ProcessGobackElementResult getRetrieveNodeName(String piid, String taskId) throws PayloadValidErrorException;

    /**
     * 获取委托节点信息
     *
     * @param piid
     * @param taskId
     * @return
     * @throws PayloadValidErrorException
     */
    public ProcessDelegateElementResult getDelegateNode(String piid, String taskId) throws PayloadValidErrorException;

    /**
     * 启动
     *
     * @param prod   产品
     * @param key    流程定义key
     * @param beanId 业务bean主键
     * @return
     */
    ProcessInstanceResult start(String prod, String key, String beanId) throws PayloadValidErrorException;

    /**
     * 发起
     *
     * @param prod     产品
     * @param pdid     流程定义key
     * @param beanId   业务bean主键
     * @param assignee 指派人
     * @return
     */
    ProcessInstanceResult sponsor(String prod, String pdid, String beanId, String assignee) throws PayloadValidErrorException;


    /**
     * 挂起
     *
     * @param prod   产品
     * @param piid   流程定义key
     * @param beanId 业务bean主键
     * @return
     */
    ProcessInstanceResult hang(String prod, String piid, String beanId) throws PayloadValidErrorException;

    /**
     * 激活
     *
     * @param prod   产品
     * @param piid   流程定义key
     * @param beanId 业务bean主键
     * @return
     */
    ProcessInstanceResult active(String prod, String piid, String beanId) throws PayloadValidErrorException;

    /**
     * 撤销
     *
     * @param prod   产品
     * @param pdid   流程定义key
     * @param piid   流程实例id
     * @param beanId 业务bean主键
     * @return
     */
    ProcessDefinitionResult cancel(String prod, String pdid, String piid, String beanId) throws PayloadValidErrorException;

    /**
     * 作废
     *
     * @param prod   产品
     * @param pdid   流程定义key
     * @param piid   流程定义id
     * @param beanId 业务bean主键
     * @return
     */
    ProcessDefinitionResult invalid(String prod, String pdid, String piid, String beanId) throws PayloadValidErrorException;

    /**
     * 获取流程定义操作
     *
     * @param prod   产品
     * @param pdid   流程定义key
     * @param beanId 业务BeanId
     * @return
     */
    ProcessDefinitionResult getProcessDefinition(String prod, String pdid, String beanId) throws PayloadValidErrorException;

    ProcessModelResult saveModel(String modelId, String modelName, String modelKey,
                                 String runModeCode, String runModeName,
                                 String category, String funcCode, String funcName,
                                 String metaInfo) throws PayloadValidErrorException;

}
