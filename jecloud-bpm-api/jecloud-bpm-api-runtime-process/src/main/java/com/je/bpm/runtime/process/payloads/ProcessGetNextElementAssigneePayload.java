/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.process.payloads;

import com.google.common.base.Strings;
import com.je.bpm.model.shared.Payload;

import java.util.Map;
import java.util.UUID;

/**
 * 获取提交节点信息处理人
 */
public class ProcessGetNextElementAssigneePayload implements Payload {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一标识
     */
    private String id;
    /**
     * 任务id
     */
    private String taskId;
    /**
     * 流程部署ID
     */
    private String pdid;

    private String prod;

    private String target;

    private Map<String, Object> bean;

    private String operationId;

    private String adjust = "0";

    private String businessKey;

    public static ProcessGetNextElementAssigneePayload build() {
        return new ProcessGetNextElementAssigneePayload();
    }

    public ProcessGetNextElementAssigneePayload() {
        this.id = UUID.randomUUID().toString();
    }

    public ProcessGetNextElementAssigneePayload(String piid) {
        this();
        this.taskId = taskId;
    }

    @Override
    public String getId() {
        return id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTaskId() {
        return taskId;
    }

    public ProcessGetNextElementAssigneePayload setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPdid() {
        return pdid;
    }

    public ProcessGetNextElementAssigneePayload setPdid(String pdid) {
        this.pdid = pdid;
        return this;
    }

    public String getProd() {
        return prod;
    }

    public ProcessGetNextElementAssigneePayload setProd(String prod) {
        this.prod = prod;
        return this;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public ProcessGetNextElementAssigneePayload setBean(Map<String, Object> bean) {
        this.bean = bean;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public ProcessGetNextElementAssigneePayload setTarget(String target) {
        this.target = target;
        return this;
    }

    public String getOperationId() {
        return operationId;
    }

    public ProcessGetNextElementAssigneePayload setOperationId(String operationId) {
        this.operationId = operationId;
        return this;
    }

    public String getAdjust() {
        if (Strings.isNullOrEmpty(adjust)) {
            return "0";
        }
        return adjust;
    }

    public ProcessGetNextElementAssigneePayload setAdjust(String adjust) {
        this.adjust = adjust;
        return this;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public ProcessGetNextElementAssigneePayload setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
        return this;
    }
}
