/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.payloads;

import com.je.bpm.model.shared.Payload;

import java.util.Date;
import java.util.UUID;

/**
 * 定时器
 */
public class TimerPayload implements Payload {

    /**
     * 唯一标识
     */
    private String id;
    /**
     * 到期时间
     */
    private Date dueDate;
    /**
     * 结束时间
     */
    private Date endDate;
    /**
     * 重试次数
     */
    private int retries;
    /**
     * 最大迭代次数
     */
    private int maxIterations;

    private String repeat;
    /**
     * 异常消息
     */
    private String exceptionMessage;

    public TimerPayload() {
        this.id = UUID.randomUUID().toString();
    }

    @Override
    public String getId() {
        return id;
    }

    public Date getDuedate() {
        return dueDate;
    }

    public TimerPayload setDuedate(Date dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public Date getEndDate() {
        return endDate;
    }

    public TimerPayload setEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public int getRetries() {
        return retries;
    }

    public TimerPayload setRetries(int retries) {
        this.retries = retries;
        return this;
    }

    public int getMaxIterations() {
        return maxIterations;
    }

    public TimerPayload setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
        return this;
    }

    public String getRepeat() {
        return repeat;
    }

    public TimerPayload setRepeat(String repeat) {
        this.repeat = repeat;
        return this;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public TimerPayload setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
        return this;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dueDate == null) ? 0 : dueDate.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((exceptionMessage == null) ? 0 : exceptionMessage.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + maxIterations;
        result = prime * result + ((repeat == null) ? 0 : repeat.hashCode());
        result = prime * result + retries;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        TimerPayload other = (TimerPayload) obj;
        if (dueDate == null) {
            if (other.dueDate != null){
                return false;
            }
        } else if (!dueDate.equals(other.dueDate)){
            return false;
        }

        if (endDate == null) {
            if (other.endDate != null){
                return false;
            }
        } else if (!endDate.equals(other.endDate)){
            return false;
        }

        if (exceptionMessage == null) {
            if (other.exceptionMessage != null){
                return false;
            }
        } else if (!exceptionMessage.equals(other.exceptionMessage)){
            return false;
        }

        if (id == null) {
            if (other.id != null){
                return false;
            }
        } else if (!id.equals(other.id)){
            return false;
        }

        if (maxIterations != other.maxIterations){
            return false;
        }

        if (repeat == null) {
            if (other.repeat != null){
                return false;
            }
        } else if (!repeat.equals(other.repeat)){
            return false;
        }

        if (retries != other.retries){
            return false;
        }

        return true;
    }

    public static TimerPayload build(){
        return new TimerPayload();
    }

}
