/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.events;

import com.je.bpm.model.process.model.BPMNTimer;
import com.je.bpm.model.shared.event.RuntimeEvent;

/**
 * 定时器事件，此事件的实体是BPMNTimer
 * @see com.je.bpm.model.process.model.BPMNTimer
 */
public interface BPMNTimerEvent extends RuntimeEvent<BPMNTimer, BPMNTimerEvent.TimerEvents> {

    /**
     * 定时器事件枚举定义
     */
    enum TimerEvents {
        /**
         * 定时器已被调度
         */
        TIMER_SCHEDULED,
        /**
         * 定时器已触发
         */
        TIMER_FIRED,
        /**
         * 定时器已取消
         */
        TIMER_CANCELLED,
        /**
         * 定时器已执行
         */
        TIMER_EXECUTED,
        /**
         * 定时器已失败
         */
        TIMER_FAILED,
        /**
         * 定时器重试
         */
        TIMER_RETRIES_DECREMENTED

    }
}
