/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.payloads;

import com.je.bpm.model.shared.Payload;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 获取流程定义model
 */
public class ProcessGetRunFormConfigPayload implements Payload {

    private String id;
    /**
     * 业务bean
     */
    private Map<String, Object> bean = new HashMap<>();
    /**
     * 主键
     */
    private String pkValue = "";
    /**
     * 功能code
     */
    private String funcCode = "";
    /**
     * 登录用户id
     */
    private String logUserId = "";

    public ProcessGetRunFormConfigPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public ProcessGetRunFormConfigPayload(String funcCode, String pkValue, Map<String, Object> bean, String logUserId) {
        this.id = UUID.randomUUID().toString();
        this.bean = bean;
        this.pkValue = pkValue;
        this.funcCode = funcCode;
        this.logUserId = logUserId;
    }

    @Override
    public String getId() {
        return id;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public ProcessGetRunFormConfigPayload setBean(Map<String, Object> bean) {
        this.bean = bean;
        return this;
    }

    public String getPkValue() {
        return pkValue;
    }

    public ProcessGetRunFormConfigPayload setPkValue(String pkValue) {
        this.pkValue = pkValue;
        return this;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public ProcessGetRunFormConfigPayload setFuncCode(String funcCode) {
        this.funcCode = funcCode;
        return this;
    }

    public String getLogUserId() {
        return logUserId;
    }

    public ProcessGetRunFormConfigPayload setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        return this;
    }

    public static ProcessGetRunFormConfigPayload build() {
        return new ProcessGetRunFormConfigPayload();
    }

}
