/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.model;

import com.je.bpm.model.shared.model.ApplicationElement;
import java.util.Date;

/**
 * 流程实例
 */
public interface ProcessInstance extends ApplicationElement {

    /**
     * 流程实例桩体
     */
    enum ProcessInstanceStatus {
        /**
         * 创建
         */
        CREATED,
        /**
         * 运行
         */
        RUNNING,
        /**
         * 挂起
         */
        SUSPENDED,
        /**
         *
         */
        CANCELLED,
        /**
         * 完成
         */
        COMPLETED
    }

    /**
     * 获取流程实例ID
     * @return
     */
    String getId();

    /**
     * 获取流程名称
     * @return
     */
    String getName();

    /**
     * 获取启动时间
     * @return
     */
    Date getStartDate();

    /**
     * 获取完成时间
     * @return
     */
    Date getCompletedDate();

    /**
     * 获取启动人
     * @return
     */
    String getInitiator();

    /**
     * 获取业务键
     * @return
     */
    String getBusinessKey();

    /**
     * 获取流程状态
     * @return
     */
    ProcessInstanceStatus getStatus();

    /**
     * 获取流程定义ID
     * @return
     */
    String getProcessDefinitionId();

    /**
     * 获取流程定义KEY
     * @return
     */
    String getProcessDefinitionKey();

    /**
     * 获取父执行ID
     * @return
     */
    String getParentId();

    /**
     * 获取流程定义版本
     * @return
     */
    Integer getProcessDefinitionVersion();

    /**
     * 获取流程定义名称
     * @return
     */
    String getProcessDefinitionName();

}
