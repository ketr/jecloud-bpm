/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.model;

import java.util.Map;

/**
 * 集成上下文
 */
public interface IntegrationContext {

    /**
     * 获取上下文ID
     * @return
     */
    String getId();

    /**
     * 流程实例ID
     * @return
     */
    String getProcessInstanceId();

    /**
     * 获取根流程实例ID
     * @return
     */
    String getRootProcessInstanceId();

    /**
     * 获取父流程实例ID
     * @return
     */
    String getParentProcessInstanceId();

    /**
     * 获取当前执行ID
     * @return
     */
    String getExecutionId();

    /**
     * 获取流程定义ID
     * @return
     */
    String getProcessDefinitionId();

    /**
     * 获取流程定义KEY
     * @return
     */
    String getProcessDefinitionKey();

    /**
     * 获取流程定义版本
     * @return
     */
    Integer getProcessDefinitionVersion();

    /**
     * 获取业务键
     * @return
     */
    String getBusinessKey();

    /**
     * 获取连接类型
     * @return
     */
    String getConnectorType();

    String getAppVersion();

    String getClientId();

    String getClientName();

    String getClientType();

    /**
     * 获取输入变量
     * @return
     */
    Map<String, Object> getInBoundVariables();

    /**
     * 获取输出变量
     * @return
     */
    Map<String, Object> getOutBoundVariables();

    /**
     * 增加输出变量
     * @param name
     * @param value
     */
    void addOutBoundVariable(String name, Object value);

    /**
     * 增加输出变量
     * @param variables
     */
    void addOutBoundVariables(Map<String, Object> variables);

    /**
     * 获取输入变量
     * @param name
     * @param <T>
     * @return
     */
    <T> T getInBoundVariable(String name);

    /**
     * 获取输入变量
     * @param name
     * @param type
     * @param <T>
     * @return
     */
    <T> T getInBoundVariable(String name, Class<T> type);

    /**
     * 获取输出变量
     * @param name
     * @param <T>
     * @return
     */
    <T> T getOutBoundVariable(String name);

    /**
     * 获取输出变量
     * @param name
     * @param type
     * @param <T>
     * @return
     */
    <T> T getOutBoundVariable(String name, Class<T> type);
}
