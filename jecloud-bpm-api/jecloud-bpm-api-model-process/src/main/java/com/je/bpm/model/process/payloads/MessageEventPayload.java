/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.payloads;

import com.je.bpm.model.shared.Payload;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * 消息事件
 */
public class MessageEventPayload implements Payload {

    /**
     * 唯一标识
     */
    private String id;
    /**
     * 名称
     */
    private String name;
    /**
     * 相关键
     */
    private String correlationKey;
    /**
     * 业务键
     */
    private String businessKey;
    /**
     * 变量集合
     */
    private Map<String, Object> variables;

    public MessageEventPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public MessageEventPayload(String name, String correlationKey, String businessKey, Map<String, Object> variables) {
        this();
        this.name = name;
        this.businessKey = businessKey;
        this.correlationKey = correlationKey;
        this.variables = variables;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public String getCorrelationKey() {
        return correlationKey;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public MessageEventPayload setName(String name) {
        this.name = name;
        return this;
    }

    public MessageEventPayload setCorrelationKey(String correlationKey) {
        this.correlationKey = correlationKey;
        return this;
    }

    public MessageEventPayload setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
        return this;
    }

    public MessageEventPayload setVariables(Map<String, Object> variables) {
        this.variables = variables;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MessageEventPayload [id=");
        builder.append(id);
        builder.append(", messageName=");
        builder.append(name);
        builder.append(", correlationKey=");
        builder.append(correlationKey);
        builder.append(", businessKey=");
        builder.append(businessKey);
        builder.append(", variables=");
        builder.append(variables);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(businessKey, correlationKey, id, name, variables);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        MessageEventPayload other = (MessageEventPayload) obj;
        return Objects.equals(businessKey, other.businessKey)
                && Objects.equals(correlationKey, other.correlationKey)
                && Objects.equals(id, other.id)
                && Objects.equals(name, other.name)
                && Objects.equals(variables, other.variables);
    }

    public static MessageEventPayload build(){
        return new MessageEventPayload();
    }

}
