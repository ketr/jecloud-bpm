/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.results;

import com.je.bpm.model.process.model.ProcessButton;
import com.je.bpm.model.process.model.ProcessConfig;
import com.je.bpm.model.process.model.ProcessDefinition;
import com.je.bpm.model.shared.Payload;
import com.je.bpm.model.shared.Result;
import java.util.List;

/**
 * 流程定义结果集
 */
public class ProcessDefinitionResult extends Result<ProcessDefinition> {

    private static final String TYPE = "processDefinitionResult";

    /**
     * 按钮集合
     */
    private List<ProcessButton> buttons;
    /**
     * 流程基础配置
     */
    private ProcessConfig processBasicConfig;

    public ProcessDefinitionResult() {
        super(TYPE);
    }

    public ProcessDefinitionResult(Payload payload, ProcessDefinition entity) {
        super(TYPE,payload, entity);
    }

    public ProcessDefinitionResult(Payload payload, ProcessDefinition entity, List<ProcessButton> buttons) {
        super(TYPE,payload, entity);
        this.buttons = buttons;
    }

    public List<ProcessButton> getButtons() {
        return buttons;
    }

    public void setButtons(List<ProcessButton> buttons) {
        this.buttons = buttons;
    }

    public ProcessConfig getProcessBasicConfig() {
        return processBasicConfig;
    }

    public void setProcessBasicConfig(ProcessConfig processBasicConfig) {
        this.processBasicConfig = processBasicConfig;
    }

}
