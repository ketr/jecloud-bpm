/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.task.model;

import com.je.bpm.model.shared.model.ApplicationElement;
import java.util.Date;
import java.util.List;

/**
 * 任务
 */
public interface Task extends ApplicationElement {

    /**
     * task状态枚举定义
     */
    enum TaskStatus {
        /**
         * 创建
         */
        CREATED,
        /**
         * 设置了处理人
         */
        ASSIGNED,
        /**
         * 暂停/挂起
         */
        SUSPENDED,
        /**
         * 完成
         */
        COMPLETED,
        /**
         * 取消
         */
        CANCELLED,
        /**
         * 删除
         */
        DELETED
    }

    /**
     * 节点ID
     * @return
     */
    String getId();

    /**
     * 节点拥有者
     * @return
     */
    String getOwner();

    /**
     * 任务处理人
     * @return
     */
    String getAssignee();

    /**
     * 任务名称
     * @return
     */
    String getName();

    /**
     * 任务描述
     * @return
     */
    String getDescription();

    /**
     * 创建事件
     * @return
     */
    Date getCreatedDate();

    /**
     * 领取时间（候选）
     * @return
     */
    Date getClaimedDate();

    /**
     * 到期时间
     * @return
     */
    Date getDueDate();

    /**
     * 优先级
     * @return
     */
    int getPriority();

    /**
     * 流程定义ID
     * @return
     */
    String getProcessDefinitionId();

    /**
     * 流程实例ID
     * @return
     */
    String getProcessInstanceId();

    /**
     * 父任务ID
     * @return
     */
    String getParentTaskId();

    /**
     * 任务状态
     * @return
     */
    TaskStatus getStatus();

    /**
     * 表单key
     * @return
     */
    String getFormKey();

    /**
     * 完成日期
     * @return
     */
    Date getCompletedDate();

    /**
     * 持续时间
     * @return
     */
    Long getDuration();

    /**
     * 流程定义版本
     * @return
     */
    Integer getProcessDefinitionVersion();

    /**
     * 业务键
     * @return
     */
    String getBusinessKey();

    /**
     * todo 待补充
     * @return
     */
    boolean isStandalone();

    /**
     * 流程定义Key
     * @return
     */
    String getTaskDefinitionKey();

    /**
     * 获取候选人
     * @return
     */
    List<String> getCandidateUsers();

    /**
     * 获取候选组
     * @return
     */
    List<String> getCandidateGroups();

    /**
     * 获取完成人
     * @return
     */
    String getCompletedBy();

    /**
     * 是否需要设置指派人
     * @return
     */
    boolean isNeedAssignee();
}
