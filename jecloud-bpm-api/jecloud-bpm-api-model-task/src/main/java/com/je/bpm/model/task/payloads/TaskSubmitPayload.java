/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.task.payloads;

import java.util.Map;
import java.util.UUID;

/**
 * 任务提交参数定义
 */
public class TaskSubmitPayload extends AbstractTaskPayload  {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 产品
     */
    private String prod;
    /**
     * 流程实例ID
     */
    private String piid;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 业务Bean主键
     */
    private String beanId;
    /**
     * 出口
     */
    private String target;
    /**
     * 指派人
     */
    private String assignee;

    private String comment;

    private String nodeId;

    private Map<String, Object> bean;

    private String sequentials;

    /**
     * 是否是跳跃
     */
    private String isJump = "0";

    public TaskSubmitPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public TaskSubmitPayload(String prod, String piid, String taskId, String beanId, String assignee) {
        this();
        this.prod = prod;
        this.piid = piid;
        this.taskId = taskId;
        this.beanId = beanId;
        this.assignee = assignee;
    }

    public TaskSubmitPayload(String prod, String piid, String taskId, String beanId, String assignee, String target, String nodeId, Map<String, Object> bean, String isJump) {
        this(prod, piid, taskId, beanId, assignee);
        this.target = target;
        this.nodeId = nodeId;
        this.bean = bean;
        this.isJump = isJump;
    }

    @Override
    public String getId() {
        return id;
    }

    public TaskSubmitPayload setId(String id) {
        this.id = id;
        return this;
    }

    public String getProd() {
        return prod;
    }

    public TaskSubmitPayload setProd(String prod) {
        this.prod = prod;
        return this;
    }

    public String getPiid() {
        return piid;
    }

    public TaskSubmitPayload setPiid(String piid) {
        this.piid = piid;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public TaskSubmitPayload setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getBeanId() {
        return beanId;
    }

    public TaskSubmitPayload setBeanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public String getAssignee() {
        return assignee;
    }

    public TaskSubmitPayload setAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public TaskSubmitPayload setTarget(String target) {
        this.target = target;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public TaskSubmitPayload setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public String getNodeId() {
        return nodeId;
    }

    public TaskSubmitPayload setNodeId(String nodeId) {
        this.nodeId = nodeId;
        return this;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public TaskSubmitPayload setBean(Map<String, Object> bean) {
        this.bean = bean;
        return this;
    }

    public String getSequentials() {
        return sequentials;
    }

    public void setSequentials(String sequentials) {
        this.sequentials = sequentials;
    }

    public static TaskSubmitPayload build() {
        return new TaskSubmitPayload();
    }


    public String getIsJump() {
        return isJump;
    }

    public void setIsJump(String isJump) {
        this.isJump = isJump;
    }
}
