/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.task.payloads;

import java.util.Date;
import java.util.UUID;

/**
 * 延迟payload
 */
public class TaskDelayPayload extends AbstractTaskPayload {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 流程实例id
     */
    private String piid;
    /**
     * 产品ID
     */
    private String prod;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 业务对象主键
     */
    private String beanId;

    private Date dueDate;

    public TaskDelayPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public TaskDelayPayload(String piid, String prod, String taskId, String beanId, Date dueDate) {
        this();
        this.piid = piid;
        this.prod = prod;
        this.taskId = taskId;
        this.beanId = beanId;
        this.dueDate = dueDate;
    }

    @Override
    public TaskDelayPayload setComment(String comment) {
        this.comment = comment;
        return this;
    }


    @Override
    public String getId() {
        return id;
    }

    public String getPiid() {
        return piid;
    }

    public TaskDelayPayload setPiid(String piid) {
        this.piid = piid;
        return this;
    }

    public String getProd() {
        return prod;
    }

    public TaskDelayPayload setProd(String prod) {
        this.prod = prod;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public TaskDelayPayload setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getBeanId() {
        return beanId;
    }

    public TaskDelayPayload setBeanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public TaskDelayPayload setDueDate(Date dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public static TaskDelayPayload build(){
        return new TaskDelayPayload();
    }

}
