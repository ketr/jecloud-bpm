/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.shared.operator;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.bpm.model.shared.Payload;
import com.je.bpm.model.shared.Result;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;
import java.util.Map;

public abstract class AbstractOperator<BUTTON_PARAMS_PAYLOAD extends Payload, BUTTON_RESULT_ENTITY extends Result> implements Operator<BUTTON_PARAMS_PAYLOAD, BUTTON_RESULT_ENTITY> {

    public static final String PROD = "prod";
    public static final String BEAN = "bean";

    public AbstractOperator() {
        OperatorRegistry.put(getId(), this);
    }

    @Override
    public BUTTON_RESULT_ENTITY operate(Map<String, Object> params) throws PayloadValidErrorException {
        BUTTON_PARAMS_PAYLOAD payload = paramsParse(params);
        getValidator().valid(payload);
        return execute(payload);
    }

    @Override
    public BUTTON_RESULT_ENTITY operate(BUTTON_PARAMS_PAYLOAD payload) throws PayloadValidErrorException {
        getValidator().valid(payload);
        return execute(payload);
    }

    /**
     * 执行
     *
     * @param payload
     * @return
     */
    public abstract BUTTON_RESULT_ENTITY execute(BUTTON_PARAMS_PAYLOAD payload);

    public abstract OperatorPayloadParamsValidator<BUTTON_PARAMS_PAYLOAD> getValidator();

    public String getAssigneeUser(String assigneeUser) {
        JSONArray assigneeUsers = JSONArray.parseArray(assigneeUser);
        if (assigneeUsers != null) {
            for (Object user : assigneeUsers) {
                JSONObject userJson = (JSONObject) JSONObject.parse(String.valueOf(user));
                return userJson.getString("assignee");
            }
        }
        return "";
    }

}
