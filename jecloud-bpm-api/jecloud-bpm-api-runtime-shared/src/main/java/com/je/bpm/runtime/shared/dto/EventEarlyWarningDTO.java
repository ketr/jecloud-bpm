/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.shared.dto;

import com.je.bpm.core.model.BpmnModel;

public class EventEarlyWarningDTO {
    /**
     * 功能编码
     */
    private String funcCode;
    /**
     * 功能编码
     */
    private String tableCode;
    /**
     * pkValue
     */
    private String pkValue;
    /**
     * 流程名称
     */
    private String workflowName;
    /**
     * 流程key
     */
    private String workflowKey;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 任务名称
     */
    private String currentTaskName;
    /**
     * 处理人信息
     */
    private String assignee;

    public static EventEarlyWarningDTO build() {
        EventEarlyWarningDTO eventSubmitDTO = new EventEarlyWarningDTO();
        return eventSubmitDTO;
    }

    public static EventEarlyWarningDTO build(String taskId, String currentTaskName, String assignee, BpmnModel bpmnModel, String pkValue) {
        String workflowName = bpmnModel.getMainProcess().getName();
        String funcCode = bpmnModel.getMainProcess().getProcessConfig().getFuncCode();
        String tableCode = bpmnModel.getMainProcess().getProcessConfig().getTableCode();
        String workflowKey = bpmnModel.getMainProcess().getId();
        EventEarlyWarningDTO eventSubmitDTO = new EventEarlyWarningDTO();
        eventSubmitDTO.setTaskId(taskId).setCurrentTaskName(currentTaskName)
                .setAssignee(assignee).setFuncCode(funcCode).setWorkflowKey(workflowKey)
                .setTableCode(tableCode)
                .setPkValue(pkValue)
                .setWorkflowName(workflowName);
        return eventSubmitDTO;
    }

    public String getTaskId() {
        return taskId;
    }

    public EventEarlyWarningDTO setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getCurrentTaskName() {
        return currentTaskName;
    }

    public EventEarlyWarningDTO setCurrentTaskName(String currentTaskName) {
        this.currentTaskName = currentTaskName;
        return this;
    }

    public String getAssignee() {
        return assignee;
    }

    public EventEarlyWarningDTO setAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public EventEarlyWarningDTO setFuncCode(String funcCode) {
        this.funcCode = funcCode;
        return this;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public EventEarlyWarningDTO setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
        return this;
    }

    public String getWorkflowKey() {
        return workflowKey;
    }

    public EventEarlyWarningDTO setWorkflowKey(String workflowKey) {
        this.workflowKey = workflowKey;
        return this;
    }

    public String getTableCode() {
        return tableCode;
    }

    public EventEarlyWarningDTO setTableCode(String tableCode) {
        this.tableCode = tableCode;
        return this;
    }

    public String getPkValue() {
        return pkValue;
    }

    public EventEarlyWarningDTO setPkValue(String pkValue) {
        this.pkValue = pkValue;
        return this;
    }
}
