/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.shared.identity.BO;

import com.je.bpm.core.model.config.task.assignment.TaskAssigneeConfigImpl;
import com.je.bpm.core.model.task.KaiteBaseUserTask;

import java.util.Map;

public class ParserUserBo {

    /**
     * 当前节点
     */
    private KaiteBaseUserTask kaiteBaseUserTask;
    /**
     * 是否多选
     */
    private Boolean multiple;
    /**
     * 人员配置
     */
    private TaskAssigneeConfigImpl taskAssigneeConfig;
    /**
     * 当前参照用户id
     */
    private String userId;
    /**
     * 提交过来的节点id
     */
    private String directTask;
    /**
     * 业务bean
     */
    private Map<String, Object> bean;
    /**
     * 产品标识
     */
    private String prod;
    /**
     * 任务指派人id
     */
    private String taskAssigner;
    /**
     * 前置任务指派人id
     */
    private String frontTaskAssigner;
    /**
     * 默认添加自己
     */
    private Boolean addOwn = true;
    /**
     * 流程启动人
     */
    private String starterUser;
    /**
     * 操作标识
     */
    private String operationId;

    public static ParserUserBo build() {
        return new ParserUserBo();
    }

    public static ParserUserBo build(Boolean addOwn, KaiteBaseUserTask kaiteBaseUserTask, Boolean multiple, TaskAssigneeConfigImpl taskAssigneeConfig,
                                     String userId, String directTask, Map<String, Object> bean, String prod, String taskAssigner, String frontTaskAssigner
            , String starterUser, String operationId) {
        ParserUserBo bo = new ParserUserBo();
        bo.setAddOwn(addOwn).setKaiteBaseUserTask(kaiteBaseUserTask).setMultiple(multiple).setTaskAssigneeConfig(taskAssigneeConfig)
                .setUserId(userId).setDirectTask(directTask).setBean(bean).setProd(prod).setTaskAssigner(taskAssigner)
                .setFrontTaskAssigner(frontTaskAssigner).setStarterUser(starterUser).setOperationId(operationId);
        return bo;
    }

    public KaiteBaseUserTask getKaiteBaseUserTask() {
        return kaiteBaseUserTask;
    }

    public ParserUserBo setKaiteBaseUserTask(KaiteBaseUserTask kaiteBaseUserTask) {
        this.kaiteBaseUserTask = kaiteBaseUserTask;
        return this;
    }

    public Boolean getMultiple() {
        return multiple;
    }

    public ParserUserBo setMultiple(Boolean multiple) {
        this.multiple = multiple;
        return this;
    }

    public TaskAssigneeConfigImpl getTaskAssigneeConfig() {
        return taskAssigneeConfig;
    }

    public ParserUserBo setTaskAssigneeConfig(TaskAssigneeConfigImpl taskAssigneeConfig) {
        this.taskAssigneeConfig = taskAssigneeConfig;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public ParserUserBo setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getDirectTask() {
        return directTask;
    }

    public ParserUserBo setDirectTask(String directTask) {
        this.directTask = directTask;
        return this;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public ParserUserBo setBean(Map<String, Object> bean) {
        this.bean = bean;
        return this;
    }

    public String getProd() {
        return prod;
    }

    public ParserUserBo setProd(String prod) {
        this.prod = prod;
        return this;
    }

    public String getTaskAssigner() {
        return taskAssigner;
    }

    public ParserUserBo setTaskAssigner(String taskAssigner) {
        this.taskAssigner = taskAssigner;
        return this;
    }

    public String getFrontTaskAssigner() {
        return frontTaskAssigner;
    }

    public ParserUserBo setFrontTaskAssigner(String frontTaskAssigner) {
        this.frontTaskAssigner = frontTaskAssigner;
        return this;
    }

    public Boolean getAddOwn() {
        return addOwn;
    }

    public ParserUserBo setAddOwn(Boolean addOwn) {
        this.addOwn = addOwn;
        return this;
    }

    public String getStarterUser() {
        return starterUser;
    }

    public ParserUserBo setStarterUser(String starterUser) {
        this.starterUser = starterUser;
        return this;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }
}
