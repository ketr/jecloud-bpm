/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.shared.dto;

import com.je.bpm.common.operation.OperatorEnum;

import java.util.List;
import java.util.Map;

public class EventSubmitDTO {
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 任务名称
     */
    private String currentTaskName;
    /**
     * 目标任务
     */
    private String targetTaskName;
    /**
     * 目标路线
     */
    private String targetTransition;
    /**
     * 提交类型（通过或退回）
     */
    private OperatorEnum operatorEnum;
    /**
     * 提交意见
     */
    private String submitComment;
    /**
     * 处理人信息
     */
    private List<Map<String, String>> assignees;
    /**
     * 执行Bean
     */
    private Map<String, Object> dynaBean;

    private String funcCode;

    public static EventSubmitDTO build() {
        EventSubmitDTO eventSubmitDTO = new EventSubmitDTO();
        return eventSubmitDTO;
    }

    public static EventSubmitDTO build(String taskId, String currentTaskName, String targetTaskName, String targetTransition,
                                       OperatorEnum operatorEnum, String submitComment, List<Map<String, String>> assignees,
                                       Map<String, Object> dynaBean,String funcCode) {
        EventSubmitDTO eventSubmitDTO = new EventSubmitDTO();
        eventSubmitDTO.setTaskId(taskId).setCurrentTaskName(currentTaskName).setTargetTaskName(targetTaskName)
                .setTargetTransition(targetTransition).setOperatorEnum(operatorEnum).setSubmitComment(submitComment)
                .setAssignees(assignees).setDynaBean(dynaBean).setFuncCode(funcCode);
        return eventSubmitDTO;
    }

    public String getTaskId() {
        return taskId;
    }

    public EventSubmitDTO setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getCurrentTaskName() {
        return currentTaskName;
    }

    public EventSubmitDTO setCurrentTaskName(String currentTaskName) {
        this.currentTaskName = currentTaskName;
        return this;
    }

    public String getTargetTaskName() {
        return targetTaskName;
    }

    public EventSubmitDTO setTargetTaskName(String targetTaskName) {
        this.targetTaskName = targetTaskName;
        return this;
    }

    public String getTargetTransition() {
        return targetTransition;
    }

    public EventSubmitDTO setTargetTransition(String targetTransition) {
        this.targetTransition = targetTransition;
        return this;
    }

    public OperatorEnum getOperatorEnum() {
        return operatorEnum;
    }

    public EventSubmitDTO setOperatorEnum(OperatorEnum operatorEnum) {
        this.operatorEnum = operatorEnum;
        return this;
    }

    public String getSubmitComment() {
        return submitComment;
    }

    public EventSubmitDTO setSubmitComment(String submitComment) {
        this.submitComment = submitComment;
        return this;
    }

    public List<Map<String, String>> getAssignees() {
        return assignees;
    }

    public EventSubmitDTO setAssignees(List<Map<String, String>> assignees) {
        this.assignees = assignees;
        return this;
    }

    public Map<String, Object> getDynaBean() {
        return dynaBean;
    }

    public EventSubmitDTO setDynaBean(Map<String, Object> dynaBean) {
        this.dynaBean = dynaBean;
        return this;
    }


    public String getFuncCode() {
        return funcCode;
    }

    public EventSubmitDTO setFuncCode(String funcCode) {
        this.funcCode = funcCode;
        return this;
    }
}
