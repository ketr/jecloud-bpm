/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.process;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.spring.process.model.Extension;
import com.je.bpm.spring.process.model.ProcessExtensionModel;
import com.je.bpm.spring.process.model.VariableDefinition;
import com.je.bpm.spring.process.variable.types.VariableType;
import com.je.bpm.spring.resources.ResourceReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.function.Predicate;

public class ProcessExtensionResourceReader implements ResourceReader<ProcessExtensionModel> {

    private final ObjectMapper objectMapper;
    private final Map<String, VariableType> variableTypeMap;

    public ProcessExtensionResourceReader(ObjectMapper objectMapper, Map<String, VariableType> variableTypeMap) {
        this.objectMapper = objectMapper;
        this.variableTypeMap = variableTypeMap;
    }

    @Override
    public Predicate<String> getResourceNameSelector() {
        return resourceName -> resourceName.endsWith("-extensions.json");
    }

    @Override
    public ProcessExtensionModel read(InputStream inputStream) throws IOException {
        objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
        ProcessExtensionModel mappedModel = objectMapper.readValue(inputStream, ProcessExtensionModel.class);
        return convertJsonVariables(mappedModel);
    }

    /**
     * Json variables need to be represented as JsonNode for engine to handle as Json
     * Do this for any var marked as json or whose type is not recognised from the extension file
     */
    private ProcessExtensionModel convertJsonVariables(ProcessExtensionModel processExtensionModel) {
        if (processExtensionModel != null && processExtensionModel.getAllExtensions() != null && processExtensionModel.getAllExtensions().size() > 0) {
            for (Extension extension : processExtensionModel.getAllExtensions().values()) {
                if (extension.getProperties() != null) {
                    saveVariableAsJsonObject(extension);
                }
            }
        }
        return processExtensionModel;
    }

    private void saveVariableAsJsonObject(Extension extension) {
        for (VariableDefinition variableDefinition : extension.getProperties().values()) {
            //如果不在变量类型中，或是json类型，则使用json解析
            if (!variableTypeMap.containsKey(variableDefinition.getType()) || variableDefinition.getType().equals("json")) {
                variableDefinition.setValue(objectMapper.convertValue(variableDefinition.getValue(),JsonNode.class));
            }
        }
    }

}
