/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.process.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 扩展
 */
public class Extension {

    private static final ProcessVariablesMapping EMPTY_PROCESS_VARIABLES_MAPPING = new ProcessVariablesMapping();
    private Map<String, VariableDefinition> properties = new HashMap<>();
    private Map<String, ProcessVariablesMapping> mappings = new HashMap<>();
    private Map<String, ProcessConstantsMapping> constants = new HashMap<>();
    private TemplatesDefinition templates = new TemplatesDefinition();

    public Map<String, VariableDefinition> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, VariableDefinition> properties) {
        this.properties = properties;
    }

    public Map<String, ProcessVariablesMapping> getMappings() {
        return mappings;
    }

    public void setMappings(Map<String, ProcessVariablesMapping> mappings) {
        this.mappings = mappings;
    }

    public Map<String, ProcessConstantsMapping> getConstants() {
        return constants;
    }

    public void setConstants(Map<String, ProcessConstantsMapping> constants) {
        this.constants = constants;
    }

    public ProcessConstantsMapping getConstantForFlowElement(String flowElementUUID) {
        ProcessConstantsMapping processConstantsMapping = constants.get(flowElementUUID);
        return processConstantsMapping != null ? processConstantsMapping : new ProcessConstantsMapping();
    }

    public ProcessVariablesMapping getMappingForFlowElement(String flowElementUUID) {
        ProcessVariablesMapping processVariablesMapping = mappings.get(flowElementUUID);
        return processVariablesMapping != null ? processVariablesMapping : EMPTY_PROCESS_VARIABLES_MAPPING;
    }

    public Optional<TemplateDefinition> findAssigneeTemplateForTask(String taskUUID) {
        return templates.findAssigneeTemplateForTask(taskUUID);
    }

    public Optional<TemplateDefinition> findCandidateTemplateForTask(String taskUUID) {
        return templates.findCandidateTemplateForTask(taskUUID);
    }

    public VariableDefinition getProperty(String propertyUUID) {
        return properties != null ? properties.get(propertyUUID) : null;
    }

    public VariableDefinition getPropertyByName(String name) {
        if (properties != null) {
            for (Map.Entry<String, VariableDefinition> variableDefinition : properties.entrySet()) {
                if (variableDefinition.getValue() != null) {
                    if (Objects.equals(variableDefinition.getValue().getName(), name)) {
                        return variableDefinition.getValue();
                    }
                }
            }
        }
        return null;
    }

    public boolean hasMapping(String taskId) {
        return mappings.get(taskId) != null;
    }

    public boolean shouldMapAllInputs(String elementId) {
        ProcessVariablesMapping processVariablesMapping = mappings.get(elementId);
        return processVariablesMapping.getMappingType() != null &&
                (processVariablesMapping.getMappingType().equals(ProcessVariablesMapping.MappingType.MAP_ALL_INPUTS) ||
                        processVariablesMapping.getMappingType().equals(ProcessVariablesMapping.MappingType.MAP_ALL));
    }

    public boolean shouldMapAllOutputs(String elementId) {
        ProcessVariablesMapping processVariablesMapping = mappings.get(elementId);
        return processVariablesMapping.getMappingType() != null &&
                (processVariablesMapping.getMappingType().equals(ProcessVariablesMapping.MappingType.MAP_ALL_OUTPUTS) ||
                        processVariablesMapping.getMappingType().equals(ProcessVariablesMapping.MappingType.MAP_ALL));
    }

    public TemplatesDefinition getTemplates() {
        return templates;
    }

    public void setTemplates(TemplatesDefinition templates) {
        this.templates = templates;
    }

}
