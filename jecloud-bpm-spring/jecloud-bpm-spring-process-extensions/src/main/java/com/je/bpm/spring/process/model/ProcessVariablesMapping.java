/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.process.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 流程变量映射
 */
public class ProcessVariablesMapping {

    /**
     * 映射类型
     */
    private MappingType mappingType;
    /**
     * 输入映射
     */
    private Map<String, Mapping> inputs = new HashMap<>();
    /**
     * 输出映射
     */
    private Map<String, Mapping> outputs = new HashMap<>();

    public Map<String, Mapping> getInputs() {
        return inputs;
    }

    public void setInputs(Map<String, Mapping> inputs) {
        this.inputs = inputs;
    }

    public Mapping getInputMapping(String inputName) {
        return inputs.get(inputName);
    }

    public Map<String, Mapping> getOutputs() {
        return outputs;
    }

    public void setOutputs(Map<String, Mapping> outputs) {
        this.outputs = outputs;
    }

    public MappingType getMappingType() {
        return mappingType;
    }

    public void setMappingType(MappingType mappingType) {
        this.mappingType = mappingType;
    }

    /**
     * 流程变量映射类型定义
     */
    public enum MappingType {
        /**
         * 所有：包含输入与输出
         */
        MAP_ALL,
        /**
         * 输入
         */
        MAP_ALL_INPUTS,
        /**
         * 输出
         */
        MAP_ALL_OUTPUTS
    }
}
