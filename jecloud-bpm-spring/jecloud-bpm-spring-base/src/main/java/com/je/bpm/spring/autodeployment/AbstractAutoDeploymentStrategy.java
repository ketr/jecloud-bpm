/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.autodeployment;

import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.process.validation.ValidationError;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.impl.util.io.InputStreamSource;
import com.je.bpm.engine.repository.DeploymentBuilder;
import com.je.bpm.spring.project.ApplicationUpgradeContextService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ContextResource;
import org.springframework.core.io.Resource;
import java.io.IOException;
import java.util.List;

/**
 * Abstract base class for implementations of {@link AutoDeploymentStrategy}.
 */
public abstract class AbstractAutoDeploymentStrategy implements AutoDeploymentStrategy {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractAutoDeploymentStrategy.class);

    private ApplicationUpgradeContextService applicationUpgradeContextService;

    public AbstractAutoDeploymentStrategy(ApplicationUpgradeContextService applicationUpgradeContextService) {
        this.applicationUpgradeContextService = applicationUpgradeContextService;
    }

    /**
     * Gets the deployment mode this strategy handles.
     *
     * @return the name of the deployment mode
     */
    protected abstract String getDeploymentMode();

    @Override
    public boolean handlesMode(final String mode) {
        return StringUtils.equalsIgnoreCase(mode, getDeploymentMode());
    }

    /**
     * Determines the name to be used for the provided resource.
     *
     * @param resource the resource to get the name for
     * @return the name of the resource
     */
    protected String determineResourceName(final Resource resource) {
        String resourceName;
        if (resource instanceof ContextResource) {
            resourceName = ((ContextResource) resource).getPathWithinContext();
        } else if (resource instanceof ByteArrayResource) {
            resourceName = resource.getDescription();
        } else {
            try {
                resourceName = resource.getFile().getAbsolutePath();
            } catch (IOException e) {
                resourceName = resource.getFilename();
            }
        }
        return resourceName;
    }

    protected boolean validateModel(Resource resource, final RepositoryService repositoryService) {

        String resourceName = determineResourceName(resource);

        if (isProcessDefinitionResource(resourceName)) {
            try {
                BpmnXMLConverter converter = new BpmnXMLConverter();
                BpmnModel bpmnModel = converter.convertToBpmnModel(new InputStreamSource(resource.getInputStream()), true,
                        false);
                List<ValidationError> validationErrors = repositoryService.validateProcess(bpmnModel);
                if (validationErrors != null && !validationErrors.isEmpty()) {
                    StringBuilder warningBuilder = new StringBuilder();
                    StringBuilder errorBuilder = new StringBuilder();

                    for (ValidationError error : validationErrors) {
                        if (error.isWarning()) {
                            warningBuilder.append(error.toString());
                            warningBuilder.append("\n");
                        } else {
                            errorBuilder.append(error.toString());
                            errorBuilder.append("\n");
                        }

                        // Write out warnings (if any)
                        if (warningBuilder.length() > 0) {
                            LOGGER.warn("Following warnings encountered during process validation: "
                                    + warningBuilder.toString());
                        }

                        if (errorBuilder.length() > 0) {
                            LOGGER.error("Errors while parsing:\n" + errorBuilder.toString());
                            return false;
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Error parsing XML", e);
                return false;
            }
        }
        return true;
    }

    private boolean isProcessDefinitionResource(String resource) {
        return resource.endsWith(".bpmn20.xml") || resource.endsWith(".bpmn");
    }

    protected DeploymentBuilder loadApplicationUpgradeContext(DeploymentBuilder deploymentBuilder) {
        if (applicationUpgradeContextService != null) {
            loadProjectManifest(deploymentBuilder);
            loadEnforcedAppVersion(deploymentBuilder);
        }
        return deploymentBuilder;
    }

    private void loadProjectManifest(DeploymentBuilder deploymentBuilder) {
        if (applicationUpgradeContextService.hasProjectManifest()) {
            try {
                deploymentBuilder.setProjectManifest(applicationUpgradeContextService.loadProjectManifest());
            } catch (IOException e) {
                LOGGER.warn("Manifest of application not found. Project release version will not be set for deployment.");
            }
        }
    }

    private void loadEnforcedAppVersion(DeploymentBuilder deploymentBuilder) {
        if (applicationUpgradeContextService.hasEnforcedAppVersion()) {
            deploymentBuilder.setEnforcedAppVersion(applicationUpgradeContextService.getEnforcedAppVersion());
            LOGGER.warn("Enforced application version set to" + applicationUpgradeContextService.getEnforcedAppVersion().toString());
        } else {
            LOGGER.warn("Enforced application version not set.");
        }
    }
}
