/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring;

import com.je.bpm.engine.impl.calendar.*;
import com.je.bpm.engine.impl.util.DefaultClockImpl;
import com.je.bpm.engine.runtime.Clock;

/**
 * Creates an advanced cycle business calendar manager (ACBCM). The ACBCM can handle daylight savings changes when the scheduled time zone is different than the server time zone.
 * <p>
 * Create a factory bean
 *
 * <pre>
 * &lt;bean id="businessCalendarManagerFactory" class="org.activiti.spring.SpringAdvancedBusinessCalendarManagerFactory" /&gt;
 * </pre>
 * <p>
 * Add the manager to your org.activiti.spring.SpringProcessEngineConfiguration bean
 *
 * <pre>
 *  &lt;bean id="processEngineConfiguration" class="org.activiti.spring.SpringProcessEngineConfiguration"&gt;
 *    ...
 *    &lt;property name="businessCalendarManager"&gt;
 *      &lt;bean id="advancedBusinessCalendarManager" factory-bean="businessCalendarManagerFactory" factory-method="getBusinessCalendarManager" /&gt;
 *    &lt;/property&gt;
 *    ...
 *  &lt;/bean&gt;
 * </pre>
 *
 * @see AdvancedCycleBusinessCalendar
 */
public class SpringAdvancedBusinessCalendarManagerFactory {

    private Integer defaultScheduleVersion;

    private Clock clock;

    public Integer getDefaultScheduleVersion() {
        return defaultScheduleVersion;
    }

    public void setDefaultScheduleVersion(Integer defaultScheduleVersion) {
        this.defaultScheduleVersion = defaultScheduleVersion;
    }

    public Clock getClock() {
        if (clock == null) {
            clock = new DefaultClockImpl();
        }
        return clock;
    }

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    public BusinessCalendarManager getBusinessCalendarManager() {
        MapBusinessCalendarManager mapBusinessCalendarManager = new MapBusinessCalendarManager();
        mapBusinessCalendarManager.addBusinessCalendar(DurationBusinessCalendar.NAME, new DurationBusinessCalendar(getClock()));
        mapBusinessCalendarManager.addBusinessCalendar(DueDateBusinessCalendar.NAME, new DueDateBusinessCalendar(getClock()));
        mapBusinessCalendarManager.addBusinessCalendar(AdvancedCycleBusinessCalendar.NAME, new AdvancedCycleBusinessCalendar(getClock(), defaultScheduleVersion));

        return mapBusinessCalendarManager;
    }

}
