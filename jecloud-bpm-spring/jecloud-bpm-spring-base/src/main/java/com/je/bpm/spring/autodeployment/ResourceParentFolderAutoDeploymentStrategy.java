/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.autodeployment;

import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.repository.DeploymentBuilder;
import com.je.bpm.spring.project.ApplicationUpgradeContextService;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Implementation of {@link AutoDeploymentStrategy} that performs a separate deployment for each set of {@link Resource}s that share the same parent folder. The namehint is used to prefix the names of
 * deployments. If the parent folder for a {@link Resource} cannot be determined, the resource's name is used.
 */
public class ResourceParentFolderAutoDeploymentStrategy extends AbstractAutoDeploymentStrategy {

    /**
     * The deployment mode this strategy handles.
     */
    public static final String DEPLOYMENT_MODE = "resource-parent-folder";

    private static final String DEPLOYMENT_NAME_PATTERN = "%s.%s";

    public ResourceParentFolderAutoDeploymentStrategy(ApplicationUpgradeContextService applicationUpgradeContextService) {
        super(applicationUpgradeContextService);
    }

    @Override
    protected String getDeploymentMode() {
        return DEPLOYMENT_MODE;
    }

    @Override
    public void deployResources(final String deploymentNameHint, final Resource[] resources, final RepositoryService repositoryService) {

        // Create a deployment for each distinct parent folder using the name
        // hint
        // as a prefix
        final Map<String, Set<Resource>> resourcesMap = createMap(resources);

        for (final Entry<String, Set<Resource>> group : resourcesMap.entrySet()) {

            final String deploymentName = determineDeploymentName(deploymentNameHint, group.getKey());

            DeploymentBuilder deploymentBuilder = repositoryService.createDeployment().enableDuplicateFiltering().name(deploymentName);

            for (final Resource resource : group.getValue()) {
                final String resourceName = determineResourceName(resource);
                deploymentBuilder.addInputStream(resourceName, resource);
            }

            loadApplicationUpgradeContext(deploymentBuilder).deploy();
        }

    }

    private Map<String, Set<Resource>> createMap(final Resource[] resources) {
        final Map<String, Set<Resource>> resourcesMap = new HashMap<String, Set<Resource>>();

        for (final Resource resource : resources) {
            final String parentFolderName = determineGroupName(resource);
            if (resourcesMap.get(parentFolderName) == null) {
                resourcesMap.put(parentFolderName, new HashSet<Resource>());
            }
            resourcesMap.get(parentFolderName).add(resource);
        }
        return resourcesMap;
    }

    private String determineGroupName(final Resource resource) {
        String result = determineResourceName(resource);
        try {
            if (resourceParentIsDirectory(resource)) {
                result = resource.getFile().getParentFile().getName();
            }
        } catch (IOException e) {
            // no-op, fallback to resource name
        }
        return result;
    }

    private boolean resourceParentIsDirectory(final Resource resource) throws IOException {
        return resource.getFile() != null && resource.getFile().getParentFile() != null && resource.getFile().getParentFile().isDirectory();
    }

    private String determineDeploymentName(final String deploymentNameHint, final String groupName) {
        return String.format(DEPLOYMENT_NAME_PATTERN, deploymentNameHint, groupName);
    }
}
