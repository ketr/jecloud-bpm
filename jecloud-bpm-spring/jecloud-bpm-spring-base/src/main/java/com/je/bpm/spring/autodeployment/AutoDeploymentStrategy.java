/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.autodeployment;

import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.internal.Internal;
import org.springframework.core.io.Resource;

/**
 * Strategy interface for implementations of automatically deploying resources. A strategy may perform any amount of deployments for the {@link Resource}s it is provided with.
 * 用于实现自动部署资源的策略接口。策略可以为其提供的{@link Resource}执行任意数量的部署。
 *
 * A strategy is capable of handling deployments corresponding to a certain indicated deployment mode. This applicability is verified using the {@link #handlesMode(String)} method.
 * 策略能够处理与特定指定部署模式对应的部署。使用{@link#handlesMode（String）}方法验证了这种适用性
 */
@Internal
public interface AutoDeploymentStrategy {

    /**
     * Determines whether the strategy handles deployments for the provided deployment mode.
     * 确定策略是否处理所提供部署模式的部署。
     * @param mode the mode to determine handling for
     * @return true if the strategy handles the mode; false otherwise
     */
    boolean handlesMode(final String mode);
    
    /**
     * Performs deployment for the provided resources, using the provided name as a hint and the provided {@link RepositoryService} to perform deployment(s).
     * 使用提供的名称作为提示并使用提供的{@link RepositoryService}执行所提供资源的部署。
     * @param deploymentNameHint the hint for the name of deployment(s) performed
     * @param resources          the resources to be deployed
     * @param repositoryService  the repository service to use for deployment(s)
     */
    void deployResources(final String deploymentNameHint, final Resource[] resources, final RepositoryService repositoryService);

}
