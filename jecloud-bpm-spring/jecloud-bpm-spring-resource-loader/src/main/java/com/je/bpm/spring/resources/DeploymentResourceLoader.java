/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.resources;

import com.je.bpm.engine.RepositoryService;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 部署资源加载器
 *
 * @param <T>
 */
public class DeploymentResourceLoader<T> {

    private RepositoryService repositoryService;

    private Map<String, List<T>> loadedResources = new HashMap<>();

    public List<T> loadResourcesForDeployment(String deploymentId, ResourceReader<T> resourceLoaderDescriptor) {
        List<T> resources = loadedResources.get(deploymentId);
        if (resources != null) {
            return resources;
        }
        List<String> resourceNames = repositoryService.getDeploymentResourceNames(deploymentId);
        if (resourceNames != null && !resourceNames.isEmpty()) {
            List<String> selectedResources = resourceNames.stream().filter(resourceLoaderDescriptor.getResourceNameSelector()).collect(Collectors.toList());
            resources = loadResources(deploymentId, resourceLoaderDescriptor, selectedResources);
        } else {
            resources = new ArrayList<>();
        }
        loadedResources.put(deploymentId, resources);
        return resources;
    }

    private List<T> loadResources(String deploymentId, ResourceReader<T> resourceReader, List<String> selectedResources) {
        List<T> resources = new ArrayList<>();
        for (String name : selectedResources) {
            try (InputStream resourceAsStream = repositoryService.getResourceAsStream(deploymentId, name)) {
                T resource = resourceReader.read(resourceAsStream);
                if (resource != null) {
                    resources.add(resource);
                }
            } catch (IOException e) {
                throw new IllegalStateException("Unable to read process extension", e);
            }
        }
        return resources;
    }

    public void setRepositoryService(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }
}
