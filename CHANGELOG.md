## 3.0.6 (2024-12-24)
### Features
- feat : 添加项目信息
- feat : 作废报错问题修复
- feat : 项目组织添加权限解析
- feat : 处理人sql适配
- feat : 添加附件
- feat : 请求结束后，清理线程数据
- feat : 添加自动判空
- feat : 添加合并处理
- feat : 添加产品组织实现

### Bug Fixes
- fix : 候选节点接多人节点，提交后，没有取回按钮
- fix : 改签通过后，待办人员没有更新bug
- fix : 改签报错问题修复
- fix : 驳回待办没有加载到bean信息bug修复
- fix : 获取model时，如果category为空，则不继续往下查询加签的数据
- fix : 兼容版本修复，加签字段无法查询导致系统起不来bug修复

## 3.0.5 (2024-10-30)
### Features
- feature : 修改简易审批，添加节点控制
- feature : 不传递beanID，获取流程图
- feature : 添加节点不简易审批控制
- feature : 添加减签逻辑
- feature : 添加节点逻辑

### Bug Fixes
- fix : 加签，发起之后异常修复

## 3.0.4 (2024-09-19)
### Features
- feature : 传阅时，判断是否有未处理的传阅信息，如果存在则不添加
- feature : 多人节点支持退回和驳回
- feat : 传阅人员，如果是部门、角色的话，添加权限
- feat : 流程结束后，未审阅的传阅信息依旧展示

### Bug Fixes
- fix : 审阅按钮不展示bug修复
- fix : 传阅人员,自定义方法设置bean名称失败修复
- fix : 取回再退回，显示取回之前的节点信息，bug修复
- fix : 人员选择sql处理异常修复
- fix : oracle兼容适配

## 3.0.3 (2024-07-26)
### Features
- feature （加签）: 加签，如果前一个节点是多人节点，由后台获取线的id
- feature （撤销）: 流程撤销后，将bean中相关的流程字段清空

## 3.0.2 (2024-06-25)
### Features
- feature （撤销）: 流程撤销添加条件判断，添加撤销日志和删除实例日志

## 3.0.1 (2024-06-01)
### Features
- feature （流程结束事件）: 流程结束，先将bean的信息设置成已结束，再执行自定义事件

### Bug Fixes
- fix （取回按钮展示）: 取回按钮展示问题修复。退回，再次提交不显示取回按钮bug修复

## 3.0.0 (2024-04-25)
### Features
- feat （人员选择）: 添加异步组织架构树解析
- feat （form配置）: 当流程不在我当期登录人身上时，也加载表单配置信息，只展示不能编辑
- feat （redis）: 添加redis缓存
- feat （redis）: redis
- feat （修改获取bpmModel接口）: 根据piid获取当前实例的bpmModel
- -feature:修改工具类根据流程实例获取流程定义的方法
- feat （自定义人员接口）: 自定义方法添加操作标识和流程启动人信息
- -feature:增加证书控制，调整版本至3.0.0

### Bug Fixes
- fix （委托按钮展示）: 委托按钮展示bug修复

## 2.2.3 (2024-02-23)
### Features
- feat （运行节点接口）: 添加获取运行节点接口
- feat （达梦）: 支持达梦数据库
- feat （多人撤回）: 多人撤回
- -feature:增加Kingbase8数据库适配
- feat （多人取回）: 多人节点无法取回
- feat （紧急状态）: 添加紧急状态
- feat （简易审批意见）: 添加简易审批意见框控制
- feat （人员矩阵解析）: 人员矩阵添加部门类型
- feat （调整版本）: 2.2.3
- feat （撤回）: 添加撤回操作
- feat （撤回）: 添加启动人撤回流程
- feat （调整版本）: 2.2.2

### Bug Fixes
- fix （减签）: 减签人员异常修复
- fix （作废）: 作废异常修复

## 2.2.1 (2023-12-15)
### Features
- feat （人员）: 添加人员矩阵处理
- feat （催办）: 添加催办模板

### Bug Fixes
- fix （人员调整）: 处理人员调整异常

## 2.2.0 (2023-11-15)
### Features
- feat （加签/回签）: 添加加签和回签逻辑

### Bug Fixes
- fix （自定义事件）: 处理人异常处理

## 2.1.2 (2023-10-27)
### Bug Fixes
- fix : 获取model时，如果category为空，则不继续往下查询加签的数据
- fix : 兼容版本修复，加签字段无法查询导致系统起不来bug修复

## 2.1.0 (2023-10-20)
## 2.0.9 (2023-09-22)
## 2.0.8 (2023-09-15)
## 1.4.1 (2023-09-08)
### Bug Fixes
- fix : 获取model时，如果category为空，则不继续往下查询加签的数据
- fix : 兼容版本修复，加签字段无法查询导致系统起不来bug修复
