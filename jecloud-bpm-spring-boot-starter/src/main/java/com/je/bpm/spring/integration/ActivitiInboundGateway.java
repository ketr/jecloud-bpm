/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.integration;

import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.delegate.DelegateExecution;
import org.springframework.integration.gateway.MessagingGatewaySupport;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * As a process enters a wait-state, this forwards the
 * flow into Spring Integration. Spring Integration flows
 * may ultimately return a reply message and that will signal the
 * execution.
 *
 */
public class ActivitiInboundGateway extends MessagingGatewaySupport {

    private String executionId = "executionId";
    private String processInstanceId = "processInstanceId";
    private String processDefinitionId = "processDefinitionId";

    private final ProcessVariableHeaderMapper headerMapper;
    private ProcessEngine processEngine;

    private Set<String> sync = new ConcurrentSkipListSet<String>();

    public ActivitiInboundGateway(ProcessEngine processEngine, String... pvsOrHeadersToPreserve) {
        Collections.addAll(this.sync, pvsOrHeadersToPreserve);
        this.processEngine = processEngine;
        this.headerMapper = new ProcessVariableHeaderMapper(sync);
        this.initializeDefaultPreservedHeaders();
    }

    protected void initializeDefaultPreservedHeaders() {
        this.sync.add(executionId);
        this.sync.add(processDefinitionId);
        this.sync.add(processInstanceId);
    }

    public void execute(IntegrationActivityBehavior receiveTaskActivityBehavior, DelegateExecution execution) {
        Map<String, Object> stringObjectMap = new HashMap<String, Object>();
        stringObjectMap.put(executionId, execution.getId());

        stringObjectMap.put(processInstanceId, execution.getProcessInstanceId());
        stringObjectMap.put(processDefinitionId, execution.getProcessDefinitionId());
        stringObjectMap.putAll(headerMapper.toHeaders(execution.getVariables()));
        MessageBuilder<?> mb = MessageBuilder.withPayload(execution).copyHeaders(stringObjectMap);
        Message<?> reply = sendAndReceiveMessage(mb.build());
        if (null != reply) {
            Map<String, Object> vars = new HashMap<String, Object>();
            headerMapper.fromHeaders(reply.getHeaders(), vars);

            for (String k : vars.keySet()) {
                processEngine.getRuntimeService().setVariable(execution.getId(), k, vars.get(k));
            }
            receiveTaskActivityBehavior.leave(execution);
        }
    }

    public void signal(IntegrationActivityBehavior receiveTaskActivityBehavior, DelegateExecution execution, String signalName, Object data) {
        receiveTaskActivityBehavior.leave(execution);
    }


}
