/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring;

import com.je.bpm.api.runtime.process.impl.APIProcessDefinitionConverter;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.model.process.event.impl.ProcessDeployedEventImpl;
import com.je.bpm.model.process.event.impl.ProcessDeployedEvents;
import com.je.bpm.model.process.events.ProcessDeployedEvent;
import com.je.bpm.model.process.model.ProcessDefinition;
import com.je.bpm.runtime.process.event.listener.ProcessRuntimeEventListener;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ProcessDeployedEventProducer extends AbstractActivitiSmartLifeCycle {

    private RepositoryService repositoryService;
    private APIProcessDefinitionConverter converter;
    private List<ProcessRuntimeEventListener<ProcessDeployedEvent>> listeners;
    private ApplicationEventPublisher eventPublisher;

    public ProcessDeployedEventProducer(RepositoryService repositoryService,
                                        APIProcessDefinitionConverter converter,
                                        List<ProcessRuntimeEventListener<ProcessDeployedEvent>> listeners,
                                        ApplicationEventPublisher eventPublisher) {
        this.repositoryService = repositoryService;
        this.converter = converter;
        this.listeners = listeners;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void doStart() {
        repositoryService.checkVersionEvent();
        List<ProcessDefinition> processDefinitions = converter.from(repositoryService.createProcessDefinitionQuery().latestVersion().list());
        List<ProcessDeployedEvent> processDeployedEvents = new ArrayList<>();
        for (ProcessDefinition processDefinition : processDefinitions) {
            try (InputStream inputStream = repositoryService.getProcessModel(processDefinition.getId())) {
                String xmlModel = StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8);
                ProcessDeployedEventImpl processDeployedEvent = new ProcessDeployedEventImpl(processDefinition, xmlModel);
                processDeployedEvents.add(processDeployedEvent);
                for (ProcessRuntimeEventListener<ProcessDeployedEvent> listener : listeners) {
                    listener.onEvent(processDeployedEvent);
                }
            } catch (IOException e) {
                throw new ActivitiException("Error occurred while getting process model '" + processDefinition.getId() + "' : ", e);
            }
        }
        if (!processDeployedEvents.isEmpty()) {
            eventPublisher.publishEvent(new ProcessDeployedEvents(processDeployedEvents));
        }
    }

    @Override
    public void doStop() {
        // nothing

    }
}
