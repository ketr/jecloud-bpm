/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.boot;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.je.bpm.engine.impl.history.HistoryLevel;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.util.List;

import static java.util.Arrays.asList;

@ConfigurationProperties("spring.activiti")
public class ActivitiProperties {

    private boolean checkProcessDefinitions = true;
    private boolean asyncExecutorActivate = true;
    private String deploymentName = "SpringAutoDeployment";
    private String databaseSchemaUpdate = "true";
    private String databaseSchema;
    private boolean dbHistoryUsed = false;
    private HistoryLevel historyLevel = HistoryLevel.AUDIT;
    private String processDefinitionLocationPrefix = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + "**/processes/";
    private List<String> processDefinitionLocationSuffixes = asList("**.bpmn20.xml", "**.bpmn");
    private List<String> customMybatisMappers;
    private List<String> customMybatisXMLMappers;
    private boolean useStrongUuids = true;
    private boolean copyVariablesToLocalForTasks = true;
    private String deploymentMode = "default";
    private boolean serializePOJOsInVariablesToJson = true;
    private String javaClassFieldForJackson = JsonTypeInfo.Id.CLASS.getDefaultPropertyName();

    public boolean isAsyncExecutorActivate() {
        return asyncExecutorActivate;
    }

    public void setAsyncExecutorActivate(boolean asyncExecutorActivate) {
        this.asyncExecutorActivate = asyncExecutorActivate;
    }

    public boolean isCheckProcessDefinitions() {
        return checkProcessDefinitions;
    }

    public void setCheckProcessDefinitions(boolean checkProcessDefinitions) {
        this.checkProcessDefinitions = checkProcessDefinitions;
    }

    public String getDeploymentName() {
        return deploymentName;
    }

    public void setDeploymentName(String deploymentName) {
        this.deploymentName = deploymentName;
    }

    public String getDatabaseSchemaUpdate() {
        return databaseSchemaUpdate;
    }

    public void setDatabaseSchemaUpdate(String databaseSchemaUpdate) {
        this.databaseSchemaUpdate = databaseSchemaUpdate;
    }

    public String getDatabaseSchema() {
        return databaseSchema;
    }

    public void setDatabaseSchema(String databaseSchema) {
        this.databaseSchema = databaseSchema;
    }

    public boolean isDbHistoryUsed() {
        return dbHistoryUsed;
    }

    public void setDbHistoryUsed(boolean isDbHistoryUsed) {
        this.dbHistoryUsed = isDbHistoryUsed;
    }

    public HistoryLevel getHistoryLevel() {
        return historyLevel;
    }

    public void setHistoryLevel(HistoryLevel historyLevel) {
        this.historyLevel = historyLevel;
    }

    public String getProcessDefinitionLocationPrefix() {
        return processDefinitionLocationPrefix;
    }

    public void setProcessDefinitionLocationPrefix(
            String processDefinitionLocationPrefix) {
        this.processDefinitionLocationPrefix = processDefinitionLocationPrefix;
    }

    public List<String> getProcessDefinitionLocationSuffixes() {
        return processDefinitionLocationSuffixes;
    }

    public void setProcessDefinitionLocationSuffixes(
            List<String> processDefinitionLocationSuffixes) {
        this.processDefinitionLocationSuffixes = processDefinitionLocationSuffixes;
    }

    public List<String> getCustomMybatisMappers() {
        return customMybatisMappers;
    }

    public void setCustomMybatisMappers(List<String> customMyBatisMappers) {
        this.customMybatisMappers = customMyBatisMappers;
    }

    public List<String> getCustomMybatisXMLMappers() {
        return customMybatisXMLMappers;
    }

    public void setCustomMybatisXMLMappers(List<String> customMybatisXMLMappers) {
        this.customMybatisXMLMappers = customMybatisXMLMappers;
    }

    public boolean isUseStrongUuids() {
        return useStrongUuids;
    }

    public void setUseStrongUuids(boolean useStrongUuids) {
        this.useStrongUuids = useStrongUuids;
    }

    public boolean isCopyVariablesToLocalForTasks() {
        return copyVariablesToLocalForTasks;
    }

    public void setCopyVariablesToLocalForTasks(boolean copyVariablesToLocalForTasks) {
        this.copyVariablesToLocalForTasks = copyVariablesToLocalForTasks;
    }

    public String getDeploymentMode() {
        return deploymentMode;
    }

    public void setDeploymentMode(String deploymentMode) {
        this.deploymentMode = deploymentMode;
    }

    public boolean isSerializePOJOsInVariablesToJson() {
        return serializePOJOsInVariablesToJson;
    }

    public void setSerializePOJOsInVariablesToJson(boolean serializePOJOsInVariablesToJson) {
        this.serializePOJOsInVariablesToJson = serializePOJOsInVariablesToJson;
    }

    public String getJavaClassFieldForJackson() {
        return javaClassFieldForJackson;
    }

    public void setJavaClassFieldForJackson(String javaClassFieldForJackson) {
        this.javaClassFieldForJackson = javaClassFieldForJackson;
    }
}
