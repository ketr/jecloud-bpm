/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.integration;

import com.je.bpm.engine.ProcessEngine;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

/**
 * Spring Integration Java Configuration DSL integration for Activiti.
 *
 */
public class Activiti {

    /**
     * This is the component that you'll use in your Spring Integration
     * {@link org.springframework.integration.dsl.IntegrationFlow}.
     */
    public static ActivitiInboundGateway inboundGateway(ProcessEngine processEngine, String... varsToPreserve) {
        return new ActivitiInboundGateway(processEngine, varsToPreserve);
    }

    /**
     * This is the bean to expose and then reference
     * from your Activiti BPMN flow in an expression.
     */
    public static IntegrationActivityBehavior inboundGatewayActivityBehavior(ActivitiInboundGateway gateway) {
        return new IntegrationActivityBehavior(gateway);
    }

    /**
     * Any message that enters this {@link MessageHandler}
     * containing a {@code executionId} parameter will trigger a
     * {@link org.activiti.engine.RuntimeService#signalEventReceived(String)}.
     */
    public static MessageHandler signallingMessageHandler(final ProcessEngine processEngine) {
        return new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
                String executionId = message.getHeaders().containsKey("executionId") ? (String) message.getHeaders().get("executionId") : (String) null;
                if (null != executionId){
                    processEngine.getRuntimeService().trigger(executionId);
                }
            }
        };
    }
}
