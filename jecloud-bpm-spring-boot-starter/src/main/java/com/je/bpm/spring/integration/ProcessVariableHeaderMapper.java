/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.integration;

import org.springframework.integration.mapping.HeaderMapper;
import org.springframework.messaging.MessageHeaders;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

public class ProcessVariableHeaderMapper implements HeaderMapper<Map<String, Object>> {

    private final Set<String> keysToPreserve = new ConcurrentSkipListSet<String>();

    public ProcessVariableHeaderMapper(Set<String> sync) {
        this.keysToPreserve.addAll(sync);
    }

    @Override
    public void fromHeaders(MessageHeaders headers, Map<String, Object> target) {
        // inbound SI msg. take the headers and convert it to
        sync(this.keysToPreserve, headers, target);
    }

    @Override
    public Map<String, Object> toHeaders(Map<String, Object> source) {
        Map<String, Object> matches = sync(this.keysToPreserve,source,new HashMap<String, Object>());
        return matches;
    }

    private static Map<String, Object> sync(Set<String> keys,Map<String, Object> in,Map<String, Object> out) {
        for (String k : keys) {
            if (in.containsKey(k)) {
                out.put(k, in.get(k));
            }
        }
        return out;
    }
}
