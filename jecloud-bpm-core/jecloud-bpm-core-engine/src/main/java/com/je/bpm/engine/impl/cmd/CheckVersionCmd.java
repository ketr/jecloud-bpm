/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;

import java.io.Serializable;
import java.sql.*;

/**
 * Gives access to a deployed process model, e.g., a BPMN 2.0 XML file, through a stream of bytes.
 * <p>
 * 获取部署过程模型 Cmd
 */
public class CheckVersionCmd implements Command<Void>, Serializable {

    private static final long serialVersionUID = 1L;
    protected String processDefinitionId;

    public CheckVersionCmd() {

    }

    @Override
    public Void execute(CommandContext commandContext) {
        Connection connection = null;
        PreparedStatement checkStatement = null;
        Statement ddlStatement = null;

        try {
            // 1. 获取 databaseId
            String databaseId = commandContext.getProcessEngineConfiguration().getDatabaseType();

            // 2. 根据 databaseId 设置检查 SQL 和 DDL SQL
            String checkColumnSql;
            String addColumnSql;

            if ("oracle".equalsIgnoreCase(databaseId)) {
                // Oracle SQL
                checkColumnSql = "SELECT COLUMN_NAME FROM ALL_TAB_COLUMNS WHERE TABLE_NAME = 'ACT_RE_PROCDEF'  AND COLUMN_NAME = 'IS_CS_'";
                addColumnSql = "ALTER TABLE ACT_RE_PROCDEF ADD IS_CS_ NUMBER(11) DEFAULT NULL COMMENT '加签流程'";
            } else {
                // MySQL SQL
                checkColumnSql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'act_re_procdef' AND COLUMN_NAME = 'IS_CS_'";
                addColumnSql = "ALTER TABLE act_re_procdef ADD COLUMN IS_CS_ INT(11) DEFAULT NULL COMMENT '加签流程'";
            }

            // 3. 执行检查字段是否存在的 SQL
            connection = commandContext.getProcessEngineConfiguration().getDataSource().getConnection();
            checkStatement = connection.prepareStatement(checkColumnSql);
            ResultSet rs = checkStatement.executeQuery();

            // 4. 如果字段不存在，则执行 DDL 语句添加字段
            if (!rs.next()) {
                ddlStatement = connection.createStatement();
                ddlStatement.executeUpdate(addColumnSql);
            }

            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error checking or modifying the database schema", e);
        } finally {
            // 关闭资源
            try {
                if (ddlStatement != null) ddlStatement.close();
                if (checkStatement != null) checkStatement.close();
                if (connection != null) connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }


}
