/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;


import com.je.bpm.engine.internal.Internal;

import java.util.Collection;
import java.util.List;

@Internal
public interface IdentityLinkEntityManager extends EntityManager<IdentityLinkEntity> {

    List<IdentityLinkEntity> findIdentityLinksByTaskId(String taskId);

    List<IdentityLinkEntity> findIdentityLinksByProcessInstanceId(String processInstanceId);

    List<IdentityLinkEntity> findIdentityLinksByProcessDefinitionId(String processDefinitionId);

    List<IdentityLinkEntity> findIdentityLinkByTaskUserGroupAndType(String taskId, String userId, String groupId, String type);

    List<IdentityLinkEntity> findIdentityLinkByProcessInstanceUserGroupAndType(String processInstanceId, String userId, String groupId, String type);

    IdentityLinkEntity findStartUserIdentityLinkByProcessInstanceUser(String processInstanceId);

    List<IdentityLinkEntity> findIdentityLinkByProcessDefinitionUserAndGroup(String processDefinitionId, String userId, String groupId);


    IdentityLinkEntity addIdentityLink(ExecutionEntity executionEntity, String userId, String groupId, String type);

    IdentityLinkEntity addIdentityLink(TaskEntity taskEntity, String userId, String groupId, String type);

    IdentityLinkEntity addIdentityLink(ProcessDefinitionEntity processDefinitionEntity, String userId, String groupId);

    /**
     * Adds an IdentityLink for the given user id with the specified type,
     * but only if the user is not associated with the execution entity yet.
     **/
    IdentityLinkEntity involveUser(ExecutionEntity executionEntity, String userId, String type);

    void addCandidateUser(TaskEntity taskEntity, String userId);

    void addCandidateUsers(TaskEntity taskEntity, Collection<String> candidateUsers);

    void addCandidateGroup(TaskEntity taskEntity, String groupId);

    void addCandidateGroups(TaskEntity taskEntity, Collection<String> candidateGroups);

    void addGroupIdentityLink(TaskEntity taskEntity, String groupId, String identityLinkType);

    void addUserIdentityLink(TaskEntity taskEntity, String userId, String identityLinkType);


    void deleteIdentityLink(IdentityLinkEntity identityLink, boolean cascadeHistory);

    void deleteIdentityLink(ExecutionEntity executionEntity, String userId, String groupId, String type);

    void deleteIdentityLink(TaskEntity taskEntity, String userId, String groupId, String type);

    void deleteIdentityLink(ProcessDefinitionEntity processDefinitionEntity, String userId, String groupId);

    void deleteIdentityLinksByTaskId(String taskId);

    void deleteIdentityLinksByProcDef(String processDefId);

}
