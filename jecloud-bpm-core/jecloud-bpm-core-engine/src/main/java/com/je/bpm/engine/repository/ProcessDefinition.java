/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.repository;

import com.je.bpm.engine.internal.Internal;

/**
 * An object structure representing an executable process composed of activities and transitions.
 * <p>
 * Business processes are often created with graphical editors that store the process definition in certain file format. These files can be added to a {@link Deployment} artifact, such as for example
 * a Business Archive (.bar) file.
 * <p>
 * At deploy time, the engine will then parse the process definition files to an executable instance of this class, that can be used to start a {@link ProcessInstance}.
 */
@Internal
public interface ProcessDefinition {

    /**
     * unique identifier
     */
    String getId();

    /**
     * category name which is derived from the targetNamespace attribute in the definitions element
     */
    String getCategory();

    /**
     * label used for display purposes
     */
    String getName();

    /**
     * unique name for all versions this process definitions
     */
    String getKey();

    /**
     * description of this process
     **/
    String getDescription();

    /**
     * version of this process definition
     */
    int getVersion();

    /**
     * name of {@link com.je.bpm.engine.RepositoryService#getResourceAsStream(String, String) the resource} of this process definition.
     */
    String getResourceName();

    /**
     * The deployment in which this process definition is contained.
     */
    String getDeploymentId();

    Integer getIsCs();

    void setIsCs(Integer isCs);

    /**
     * The resource name in the deployment of the diagram image (if any).
     */
    String getDiagramResourceName();

    /**
     * Does this process definition has a {@link FormService#getStartFormData(String) start form key}.
     */
    boolean hasStartFormKey();

    /**
     * Does this process definition has a graphical notation defined (such that a diagram can be generated)?
     */
    boolean hasGraphicalNotation();

    /**
     * Returns true if the process definition is in suspended state.
     */
    boolean isSuspended();

    /**
     * The tenant identifier of this process definition
     */
    String getTenantId();

    /**
     * The engine version for this process definition (5 or 6)
     */
    String getEngineVersion();

    void setAppVersion(Integer appVersion);

    Integer getAppVersion();

}
