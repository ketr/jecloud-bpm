/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.parser.factory;

import com.je.bpm.core.model.ActivitiListener;
import com.je.bpm.core.model.EventListener;
import com.je.bpm.engine.delegate.*;
import com.je.bpm.engine.delegate.event.ActivitiEventListener;
import com.je.bpm.engine.internal.Internal;

/**
 * Factory class used by the {@link com.je.bpm.engine.impl.bpmn.parser.BpmnParser} and {@link com.je.bpm.engine.impl.bpmn.parser.BpmnParse} to instantiate the behaviour classes for {@link TaskListener} and {@link ExecutionListener} usages.
 *
 * You can provide your own implementation of this class. This way, you can give different execution semantics to the standard construct.
 *
 * The easiest and advisable way to implement your own {@link ListenerFactory} is to extend the {@link DefaultListenerFactory}.
 *
 * An instance of this interface can be injected in the {@link com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl} and its subclasses.
 *
 */
@Internal
public interface ListenerFactory {

  TaskListener createClassDelegateTaskListener(ActivitiListener activitiListener);

  TaskListener createExpressionTaskListener(ActivitiListener activitiListener);

  TaskListener createDelegateExpressionTaskListener(ActivitiListener activitiListener);

  ExecutionListener createClassDelegateExecutionListener(ActivitiListener activitiListener);

  ExecutionListener createExpressionExecutionListener(ActivitiListener activitiListener);

  ExecutionListener createDelegateExpressionExecutionListener(ActivitiListener activitiListener);

  TransactionDependentExecutionListener createTransactionDependentDelegateExpressionExecutionListener(ActivitiListener activitiListener);

  ActivitiEventListener createClassDelegateEventListener(EventListener eventListener);

  ActivitiEventListener createDelegateExpressionEventListener(EventListener eventListener);

  ActivitiEventListener createEventThrowingEventListener(EventListener eventListener);

  CustomPropertiesResolver createClassDelegateCustomPropertiesResolver(ActivitiListener activitiListener);

  CustomPropertiesResolver createExpressionCustomPropertiesResolver(ActivitiListener activitiListener);

  CustomPropertiesResolver createDelegateExpressionCustomPropertiesResolver(ActivitiListener activitiListener);

  TransactionDependentTaskListener createTransactionDependentDelegateExpressionTaskListener(ActivitiListener activitiListener);
}
