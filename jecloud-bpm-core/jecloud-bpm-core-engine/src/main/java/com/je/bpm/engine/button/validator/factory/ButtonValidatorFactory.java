/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.button.validator.factory;

import com.je.bpm.core.model.button.factory.ButtonEnum;
import com.je.bpm.engine.button.validator.*;

import java.util.HashMap;
import java.util.Map;

public class ButtonValidatorFactory {

    public ButtonValidatorFactory() {
        init();
    }

    private static final Map<String, ButtonValidator> map = new HashMap<>();

    private static void addButton(String code, ButtonValidator buttonValidator) {
        map.put(code, buttonValidator);
    }

    public static ButtonValidator getButtonValidator(String code) {
        return map.get(code);
    }

    private void init() {
        addButton(ButtonEnum.ACTIVE_BTN.getCode(), new ProcessActivateButtonValidator());
        addButton(ButtonEnum.CANCEL_BTN.getCode(), new ProcessCancelButtonValidator());
        addButton(ButtonEnum.HANG_BTN.getCode(), new ProcessHangButtonValidator());
        addButton(ButtonEnum.INVALID_BTN.getCode(), new ProcessInvalidButtonValidator());
        addButton(ButtonEnum.SPONSOR_BTN.getCode(), new ProcessSponsorButtonValidator());
        addButton(ButtonEnum.START_BTN.getCode(), new ProcessStartButtonValidator());
        addButton(ButtonEnum.CANCEL_DELEGATE_BTN.getCode(), new TaskCancelDelegateButtonValidator());
        addButton(ButtonEnum.ADD_SIGNATURE_NODE.getCode(), new TaskAddSignatureNodeButtonValidator());
        addButton(ButtonEnum.DEL_SIGNATURE_NODE.getCode(), new TaskDelSignatureNodeButtonValidator());
        addButton(ButtonEnum.CHANGE_ASSIGNEE_BTN.getCode(), new TaskChangeAssigneeButtonValidator());
        addButton(ButtonEnum.RECEIVE_BTN.getCode(), new TaskClaimButtonValidator());
        addButton(ButtonEnum.DELEGATE_BTN.getCode(), new TaskDelegateButtonValidator());
        addButton(ButtonEnum.DIRECT_SEND_BTN.getCode(), new TaskDirectSendButtonValidator());
        addButton(ButtonEnum.DISMISS_BTN.getCode(), new TaskDismissButtonValidator());
        addButton(ButtonEnum.GOBACK_BTN.getCode(), new TaskGobackButtonValidator());
        addButton(ButtonEnum.PASSROUND_BTN.getCode(), new TaskPassroundButtonValidator());
        addButton(ButtonEnum.PASSROUND_READ_BTN.getCode(), new TaskPassroundReadButtonValidator());
        addButton(ButtonEnum.RETRIEVE_BTN.getCode(), new TaskRetrieveButtonValidator());
        addButton(ButtonEnum.SUBMIT_BTN.getCode(), new TaskSubmitButtonValidator());
        addButton(ButtonEnum.TRANSFER_BTN.getCode(), new TaskTransferButtonValidator());
        addButton(ButtonEnum.URGE_BTN.getCode(), new TaskUrgeButtonValidator());
        addButton(ButtonEnum.COUNTERSIGNED_ABSTAIN_BTN.getCode(), new TaskCountersignedAbstainBtnButtonValidator());
        addButton(ButtonEnum.COUNTERSIGNED_PASS_BTN.getCode(), new TaskCountersignedPassBtnButtonValidator());
        addButton(ButtonEnum.COUNTERSIGNED_VETO_BTN.getCode(), new TaskCountersignedVetoBtnButtonValidator());
        addButton(ButtonEnum.COUNTERSIGNED_ADD_SIGNATURE_BTN.getCode(), new TaskCountersignedAddSignatureButtonValidator());
        addButton(ButtonEnum.COUNTERSIGNED_VISA_REDUCTION_BTN.getCode(), new TaskCountersignedVisaReductionButtonValidator());
        addButton(ButtonEnum.COUNTERSIGNED_REBOOK_REDUCTION_BTN.getCode(), new TaskCountersignedRebookReductionButtonValidator());
        addButton(ButtonEnum.CUSTOM_SUBMIT_BTN.getCode(), new TaskSubmitButtonValidator());
        addButton(ButtonEnum.COUNTERSIGN_BTN.getCode(), new TaskCountersignButtonValidator());
        addButton(ButtonEnum.SIGN_BACK_BTN.getCode(), new TaskSignBackButtonValidator());
        addButton(ButtonEnum.STAGING_BTN.getCode(), new TaskStagingButtonValidator());
        addButton(ButtonEnum.WITHDRAW_BTN.getCode(), new TaskWithdrawButtonValidator());


    }

}
