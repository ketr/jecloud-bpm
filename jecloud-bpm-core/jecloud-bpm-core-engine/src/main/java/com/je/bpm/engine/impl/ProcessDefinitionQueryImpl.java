/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl;

import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.interceptor.CommandExecutor;
import com.je.bpm.engine.impl.persistence.entity.SuspensionState;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.engine.repository.ProcessDefinitionQuery;

import java.util.List;
import java.util.Set;

public class ProcessDefinitionQueryImpl extends AbstractQuery<ProcessDefinitionQuery, ProcessDefinition> implements ProcessDefinitionQuery {

    private static final long serialVersionUID = 1L;

    private String id;
    private Set<String> ids;
    private String category;
    private String categoryLike;
    private String categoryNotEquals;
    private String name;
    private String nameLike;
    private String deploymentId;
    private Set<String> deploymentIds;
    private String key;
    private String keyLike;
    private Set<String> keys;
    private String resourceName;
    private String resourceNameLike;
    private Integer version;
    private Integer versionGt;
    private Integer versionGte;
    private Integer versionLt;
    private Integer versionLte;
    private boolean latest;
    private boolean notCs = true;
    private SuspensionState suspensionState;
    private String authorizationUserId;
    private String procDefId;
    private String tenantId;
    private String tenantIdLike;
    private boolean withoutTenantId;

    private String eventSubscriptionName;
    private String eventSubscriptionType;

    public ProcessDefinitionQueryImpl() {
    }

    public ProcessDefinitionQueryImpl(CommandContext commandContext) {
        super(commandContext);
    }

    public ProcessDefinitionQueryImpl(CommandExecutor commandExecutor) {
        super(commandExecutor);
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionId(String processDefinitionId) {
        this.id = processDefinitionId;
        return this;
    }

    @Override
    public ProcessDefinitionQuery processDefinitionIds(Set<String> processDefinitionIds) {
        this.ids = processDefinitionIds;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionCategory(String category) {
        if (category == null) {
            throw new ActivitiIllegalArgumentException("category is null");
        }
        this.category = category;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionCategoryLike(String categoryLike) {
        if (categoryLike == null) {
            throw new ActivitiIllegalArgumentException("categoryLike is null");
        }
        this.categoryLike = categoryLike;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionCategoryNotEquals(String categoryNotEquals) {
        if (categoryNotEquals == null) {
            throw new ActivitiIllegalArgumentException("categoryNotEquals is null");
        }
        this.categoryNotEquals = categoryNotEquals;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionName(String name) {
        if (name == null) {
            throw new ActivitiIllegalArgumentException("name is null");
        }
        this.name = name;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionNameLike(String nameLike) {
        if (nameLike == null) {
            throw new ActivitiIllegalArgumentException("nameLike is null");
        }
        this.nameLike = nameLike;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl deploymentId(String deploymentId) {
        if (deploymentId == null) {
            throw new ActivitiIllegalArgumentException("id is null");
        }
        this.deploymentId = deploymentId;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl deploymentIds(Set<String> deploymentIds) {
        if (deploymentIds == null) {
            throw new ActivitiIllegalArgumentException("ids are null");
        }
        this.deploymentIds = deploymentIds;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionKey(String key) {
        if (key == null) {
            throw new ActivitiIllegalArgumentException("key is null");
        }
        this.key = key;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionKeys(Set<String> keys) {
        if (keys == null) {
            throw new ActivitiIllegalArgumentException("keys is null");
        }
        this.keys = keys;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionKeyLike(String keyLike) {
        if (keyLike == null) {
            throw new ActivitiIllegalArgumentException("keyLike is null");
        }
        this.keyLike = keyLike;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionResourceName(String resourceName) {
        if (resourceName == null) {
            throw new ActivitiIllegalArgumentException("resourceName is null");
        }
        this.resourceName = resourceName;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionResourceNameLike(String resourceNameLike) {
        if (resourceNameLike == null) {
            throw new ActivitiIllegalArgumentException("resourceNameLike is null");
        }
        this.resourceNameLike = resourceNameLike;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl processDefinitionVersion(Integer version) {
        checkVersion(version);
        this.version = version;
        return this;
    }

    @Override
    public ProcessDefinitionQuery processDefinitionVersionGreaterThan(Integer processDefinitionVersion) {
        checkVersion(processDefinitionVersion);
        this.versionGt = processDefinitionVersion;
        return this;
    }

    @Override
    public ProcessDefinitionQuery processDefinitionVersionGreaterThanOrEquals(Integer processDefinitionVersion) {
        checkVersion(processDefinitionVersion);
        this.versionGte = processDefinitionVersion;
        return this;
    }

    @Override
    public ProcessDefinitionQuery processDefinitionVersionLowerThan(Integer processDefinitionVersion) {
        checkVersion(processDefinitionVersion);
        this.versionLt = processDefinitionVersion;
        return this;
    }

    @Override
    public ProcessDefinitionQuery processDefinitionVersionLowerThanOrEquals(Integer processDefinitionVersion) {
        checkVersion(processDefinitionVersion);
        this.versionLte = processDefinitionVersion;
        return this;
    }

    protected void checkVersion(Integer version) {
        if (version == null) {
            throw new ActivitiIllegalArgumentException("version is null");
        } else if (version <= 0) {
            throw new ActivitiIllegalArgumentException("version must be positive");
        }
    }

    @Override
    public ProcessDefinitionQueryImpl latestVersion() {
        this.latest = true;
        return this;
    }

    @Override
    public ProcessDefinitionQueryImpl setNotCs(boolean cs) {
        this.notCs = cs;
        return this;
    }

    @Override
    public ProcessDefinitionQuery active() {
        this.suspensionState = SuspensionState.ACTIVE;
        return this;
    }

    @Override
    public ProcessDefinitionQuery suspended() {
        this.suspensionState = SuspensionState.SUSPENDED;
        return this;
    }

    @Override
    public ProcessDefinitionQuery processDefinitionTenantId(String tenantId) {
        if (tenantId == null) {
            throw new ActivitiIllegalArgumentException("processDefinition tenantId is null");
        }
        this.tenantId = tenantId;
        return this;
    }

    @Override
    public ProcessDefinitionQuery processDefinitionTenantIdLike(String tenantIdLike) {
        if (tenantIdLike == null) {
            throw new ActivitiIllegalArgumentException("process definition tenantId is null");
        }
        this.tenantIdLike = tenantIdLike;
        return this;
    }

    @Override
    public ProcessDefinitionQuery processDefinitionWithoutTenantId() {
        this.withoutTenantId = true;
        return this;
    }

    public ProcessDefinitionQuery messageEventSubscription(String messageName) {
        return eventSubscription("message", messageName);
    }

    @Override
    public ProcessDefinitionQuery messageEventSubscriptionName(String messageName) {
        return eventSubscription("message", messageName);
    }

    public ProcessDefinitionQuery processDefinitionStarter(String procDefId) {
        this.procDefId = procDefId;
        return this;
    }

    public ProcessDefinitionQuery eventSubscription(String eventType, String eventName) {
        if (eventName == null) {
            throw new ActivitiIllegalArgumentException("event name is null");
        }
        if (eventType == null) {
            throw new ActivitiException("event type is null");
        }
        this.eventSubscriptionType = eventType;
        this.eventSubscriptionName = eventName;
        return this;
    }

    // sorting ////////////////////////////////////////////
    @Override
    public ProcessDefinitionQuery orderByDeploymentId() {
        return orderBy(ProcessDefinitionQueryProperty.DEPLOYMENT_ID);
    }

    @Override
    public ProcessDefinitionQuery orderByProcessDefinitionKey() {
        return orderBy(ProcessDefinitionQueryProperty.PROCESS_DEFINITION_KEY);
    }

    @Override
    public ProcessDefinitionQuery orderByProcessDefinitionCategory() {
        return orderBy(ProcessDefinitionQueryProperty.PROCESS_DEFINITION_CATEGORY);
    }

    @Override
    public ProcessDefinitionQuery orderByProcessDefinitionId() {
        return orderBy(ProcessDefinitionQueryProperty.PROCESS_DEFINITION_ID);
    }

    @Override
    public ProcessDefinitionQuery orderByProcessDefinitionVersion() {
        return orderBy(ProcessDefinitionQueryProperty.PROCESS_DEFINITION_VERSION);
    }

    @Override
    public ProcessDefinitionQuery orderByProcessDefinitionAppVersion() {
        return orderBy(ProcessDefinitionQueryProperty.PROCESS_DEFINITION_APP_VERSION);
    }

    @Override
    public ProcessDefinitionQuery orderByProcessDefinitionName() {
        return orderBy(ProcessDefinitionQueryProperty.PROCESS_DEFINITION_NAME);
    }

    @Override
    public ProcessDefinitionQuery orderByTenantId() {
        return orderBy(ProcessDefinitionQueryProperty.PROCESS_DEFINITION_TENANT_ID);
    }

    // results ////////////////////////////////////////////

    @Override
    public long executeCount(CommandContext commandContext) {
        checkQueryOk();
        return commandContext.getProcessDefinitionEntityManager().findProcessDefinitionCountByQueryCriteria(this);
    }

    @Override
    public List<ProcessDefinition> executeList(CommandContext commandContext, Page page) {
        checkQueryOk();
        return commandContext.getProcessDefinitionEntityManager().findProcessDefinitionsByQueryCriteria(this, page);
    }

    @Override
    public void checkQueryOk() {
        super.checkQueryOk();
    }

    // getters ////////////////////////////////////////////

    public String getDeploymentId() {
        return deploymentId;
    }

    public Set<String> getDeploymentIds() {
        return deploymentIds;
    }

    public String getId() {
        return id;
    }

    public Set<String> getIds() {
        return ids;
    }

    public String getName() {
        return name;
    }

    public String getNameLike() {
        return nameLike;
    }

    public String getKey() {
        return key;
    }

    public String getKeyLike() {
        return keyLike;
    }

    public Set<String> getKeys() {
        return keys;
    }

    public Integer getVersion() {
        return version;
    }

    public Integer getVersionGt() {
        return versionGt;
    }

    public Integer getVersionGte() {
        return versionGte;
    }

    public Integer getVersionLt() {
        return versionLt;
    }

    public Integer getVersionLte() {
        return versionLte;
    }

    public boolean isLatest() {
        return latest;
    }

    public boolean isNotCs() {
        return notCs;
    }

    public String getCategory() {
        return category;
    }

    public String getCategoryLike() {
        return categoryLike;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getResourceNameLike() {
        return resourceNameLike;
    }

    public SuspensionState getSuspensionState() {
        return suspensionState;
    }

    public void setSuspensionState(SuspensionState suspensionState) {
        this.suspensionState = suspensionState;
    }

    public String getCategoryNotEquals() {
        return categoryNotEquals;
    }

    public String getTenantId() {
        return tenantId;
    }

    public String getTenantIdLike() {
        return tenantIdLike;
    }

    public boolean isWithoutTenantId() {
        return withoutTenantId;
    }

    public String getAuthorizationUserId() {
        return authorizationUserId;
    }

    public String getProcDefId() {
        return procDefId;
    }

    public String getEventSubscriptionName() {
        return eventSubscriptionName;
    }

    public String getEventSubscriptionType() {
        return eventSubscriptionType;
    }

    public ProcessDefinitionQueryImpl startableByUser(String userId) {
        if (userId == null) {
            throw new ActivitiIllegalArgumentException("userId is null");
        }
        this.authorizationUserId = userId;
        return this;
    }
}
