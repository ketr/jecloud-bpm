/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.event.logger.handler;

/**

 */
public interface Fields {

  String ACTIVITY_ID = "activityId";
  String ACTIVITY_NAME = "activityName";
  String ACTIVITY_TYPE = "activityType";
  String ASSIGNEE = "assignee";
  String BEHAVIOR_CLASS = "behaviorClass";
  String BUSINESS_KEY = "businessKey";
  String CATEGORY = "category";
  String CREATE_TIME = "createTime";
  String DESCRIPTION = "description";
  String DUE_DATE = "dueDate";
  String DURATION = "duration";
  String ERROR_CODE = "errorCode";
  String END_TIME = "endTime";
  String EXECUTION_ID = "executionId";
  String FORM_KEY = "formKey";
  String ID = "id";
  String MESSAGE_NAME = "messageName";
  String MESSAGE_DATA = "messageData";
  String NAME = "name";
  String OWNER = "owner";
  String PRIORITY = "priority";
  String PROCESS_DEFINITION_ID = "processDefinitionId";
  String TASK_DEFINITION_KEY = "taskDefinitionKey";
  String PROCESS_INSTANCE_ID = "processInstanceId";
  String PROCESS_INSTANCE_NAME = "processInstanceName";
  String SIGNAL_NAME = "signalName";
  String SIGNAL_DATA = "signalData";
  String SOURCE_ACTIVITY_ID = "sourceActivityId";
  String SOURCE_ACTIVITY_NAME = "sourceActivityName";
  String SOURCE_ACTIVITY_TYPE = "sourceActivityType";
  String SOURCE_ACTIVITY_BEHAVIOR_CLASS = "sourceActivityBehaviorClass";
  String TARGET_ACTIVITY_ID = "targetActivityId";
  String TARGET_ACTIVITY_NAME = "targetActivityName";
  String TARGET_ACTIVITY_TYPE = "targetActivityType";
  String TARGET_ACTIVITY_BEHAVIOR_CLASS = "targetActivityBehaviorClass";
  String TENANT_ID = "tenantId";
  String TIMESTAMP = "timeStamp";
  String USER_ID = "userId";
  String LOCAL_VARIABLES = "localVariables";
  String VARIABLES = "variables";
  String VALUE = "value";
  String VALUE_BOOLEAN = "booleanValue";
  String VALUE_DATE = "dateValue";
  String VALUE_DOUBLE = "doubleValue";
  String VALUE_INTEGER = "integerValue";
  String VALUE_JSON = "jsonValue";
  String VALUE_LONG = "longValue";
  String VALUE_SHORT = "shortValue";
  String VALUE_STRING = "stringValue";
  String VALUE_UUID = "uuidValue";
  String VARIABLE_TYPE = "variableType";

}
