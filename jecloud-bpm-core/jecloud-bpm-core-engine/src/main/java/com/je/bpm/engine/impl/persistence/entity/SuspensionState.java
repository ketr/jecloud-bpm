/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.internal.Internal;

/**
 * Contains a predefined set of states for process definitions and process instances
 * 包含流程定义和流程实例的预定义状态集
 */
@Internal
public interface SuspensionState {

    SuspensionState ACTIVE = new SuspensionStateImpl(1, "active");
    SuspensionState SUSPENDED = new SuspensionStateImpl(2, "suspended");

    int getStateCode();

    // default implementation ///////////////////////////////////////////////////
    class SuspensionStateImpl implements SuspensionState {

        public final int stateCode;
        protected final String name;

        public SuspensionStateImpl(int suspensionCode, String string) {
            this.stateCode = suspensionCode;
            this.name = string;
        }

        @Override
        public int getStateCode() {
            return stateCode;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + stateCode;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            SuspensionStateImpl other = (SuspensionStateImpl) obj;
            if (stateCode != other.stateCode) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    // helper class /////////////////////////////////////////

    public static class SuspensionStateUtil {

        public static void setSuspensionState(ProcessDefinitionEntity processDefinitionEntity, SuspensionState state) {
            if (processDefinitionEntity.getSuspensionState() == state.getStateCode()) {
                throw new ActivitiException("Cannot set suspension state '" + state + "' for " + processDefinitionEntity + "': already in state '" + state + "'.");
            }
            processDefinitionEntity.setSuspensionState(state.getStateCode());
            dispatchStateChangeEvent(processDefinitionEntity, state);
        }

        public static void setSuspensionState(ExecutionEntity executionEntity, SuspensionState state) {
            if (executionEntity.getSuspensionState() == state.getStateCode()) {
                throw new ActivitiException("Cannot set suspension state '" + state + "' for " + executionEntity + "': already in state '" + state + "'.");
            }
            executionEntity.setSuspensionState(state.getStateCode());
            dispatchStateChangeEvent(executionEntity, state);
        }

        public static void setSuspensionState(TaskEntity taskEntity, SuspensionState state) {
            if (taskEntity.getSuspensionState() == state.getStateCode()) {
                throw new ActivitiException("Cannot set suspension state '" + state + "' for " + taskEntity + "': already in state '" + state + "'.");
            }
            taskEntity.setSuspensionState(state.getStateCode());
            dispatchStateChangeEvent(taskEntity, state);
        }

        protected static void dispatchStateChangeEvent(Object entity, SuspensionState state) {
            if (Context.getCommandContext() != null && Context.getCommandContext().getEventDispatcher().isEnabled()) {
                ActivitiEventType eventType = null;
                if (state == SuspensionState.ACTIVE) {
                    eventType = ActivitiEventType.ENTITY_ACTIVATED;
                } else {
                    eventType = ActivitiEventType.ENTITY_SUSPENDED;
                }
                Context.getCommandContext().getEventDispatcher().dispatchEvent(ActivitiEventBuilder.createEntityEvent(eventType, entity));
            }
        }
    }

}
