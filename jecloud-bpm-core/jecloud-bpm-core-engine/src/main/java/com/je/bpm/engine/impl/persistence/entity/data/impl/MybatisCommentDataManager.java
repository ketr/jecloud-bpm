/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.CommentEntity;
import com.je.bpm.engine.impl.persistence.entity.CommentEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.data.AbstractDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.CommentDataManager;
import com.je.bpm.engine.task.Comment;
import com.je.bpm.engine.task.Event;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MybatisCommentDataManager extends AbstractDataManager<CommentEntity> implements CommentDataManager {

    public MybatisCommentDataManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public Class<? extends CommentEntity> getManagedEntityClass() {
        return CommentEntityImpl.class;
    }

    @Override
    public CommentEntity create() {
        return new CommentEntityImpl();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Comment> findCommentsByTaskId(String taskId) {
        return getDbSqlSession().selectList("selectCommentsByTaskId", taskId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Comment> findCommentsByTaskIdAndType(String taskId, String type) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("taskId", taskId);
        params.put("type", type);
        return getDbSqlSession().selectListWithRawParameter("selectCommentsByTaskIdAndType", params, 0, Integer.MAX_VALUE);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Comment> findCommentsByType(String type) {
        return getDbSqlSession().selectList("selectCommentsByType", type);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Event> findEventsByTaskId(String taskId) {
        return getDbSqlSession().selectList("selectEventsByTaskId", taskId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Event> findEventsByProcessInstanceId(String processInstanceId) {
        return getDbSqlSession().selectList("selectEventsByProcessInstanceId", processInstanceId);
    }

    @Override
    public void deleteCommentsByTaskId(String taskId) {
        getDbSqlSession().delete("deleteCommentsByTaskId", taskId, CommentEntityImpl.class);
    }

    @Override
    public void deleteCommentsByProcessInstanceId(String processInstanceId) {
        getDbSqlSession().delete("deleteCommentsByProcessInstanceId", processInstanceId, CommentEntityImpl.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Comment> findCommentsByProcessInstanceId(String processInstanceId) {
        return getDbSqlSession().selectList("selectCommentsByProcessInstanceId", processInstanceId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Comment> findCommentsByProcessInstanceId(String processInstanceId, String type) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("processInstanceId", processInstanceId);
        params.put("type", type);
        return getDbSqlSession().selectListWithRawParameter("selectCommentsByProcessInstanceIdAndType", params, 0, Integer.MAX_VALUE);
    }

    @Override
    public Comment findComment(String commentId) {
        return findById(commentId);
    }

    @Override
    public Event findEvent(String commentId) {
        return findById(commentId);
    }

    @Override
    public int updateComment(CommentEntity comment) {
        return getDbSqlSession().update("updateComment", comment);
    }
}
