/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.calendar;

import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.runtime.ClockReader;

import java.util.Date;

@Internal
public class CycleBusinessCalendar extends BusinessCalendarImpl {

    public static String NAME = "cycle";

    public CycleBusinessCalendar(ClockReader clockReader) {
        super(clockReader);
    }

    @Override
    public Date resolveDuedate(String duedateDescription, int maxIterations) {
        try {
            if (duedateDescription != null && duedateDescription.startsWith("R")) {
                return new DurationHelper(duedateDescription, maxIterations, clockReader).getDateAfter();
            } else {
                CronExpression ce = new CronExpression(duedateDescription, clockReader);
                return ce.getTimeAfter(clockReader.getCurrentTime());
            }

        } catch (Exception e) {
            throw new ActivitiException("Failed to parse cron expression: " + duedateDescription, e);
        }

    }

    @Override
    public Boolean validateDuedate(String duedateDescription, int maxIterations, Date endDate, Date newTimer) {
        if (endDate != null) {
            return super.validateDuedate(duedateDescription, maxIterations, endDate, newTimer);
        }
        // end date could be part of the chron expression
        try {
            if (duedateDescription != null && duedateDescription.startsWith("R")) {
                return new DurationHelper(duedateDescription, maxIterations, clockReader).isValidDate(newTimer);
            } else {
                return true;
            }

        } catch (Exception e) {
            throw new ActivitiException("Failed to parse cron expression: " + duedateDescription, e);
        }

    }

}
