/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.behavior;

import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.task.KaiteMultiUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.DynamicBpmnConstants;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.delegate.TaskListener;
import com.je.bpm.engine.delegate.event.ActivitiEventDispatcher;
import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.cmd.DismissTaskCmd;
import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.el.ExpressionManager;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntityManager;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 多人审批用户任务
 */
public class KaiteMultiUserTaskActivityBehavior extends KaiteBaseUserTaskActivityBehavior {

    private static final Logger logger = LoggerFactory.getLogger(KaiteUserTaskActivityBehavior.class);

    private KaiteMultiUserTask kaiteMultiUserTask;

    public KaiteMultiUserTaskActivityBehavior(KaiteMultiUserTask kaiteMultiUserTask) {
        this.kaiteMultiUserTask = kaiteMultiUserTask;
    }

    @Override
    public void execute(DelegateExecution execution) {
        if (execution.getVariable(DismissTaskCmd.IS_DISMISS_KEY) == null || !execution.getVariable(DismissTaskCmd.IS_DISMISS_KEY, Boolean.class)) {
            execution.removeVariable(DismissTaskCmd.IS_DISMISS_KEY);
            execution.removeVariable(DismissTaskCmd.DISMISS_INFO_KEY);
        }
        CommandContext commandContext = Context.getCommandContext();
        TaskEntityManager taskEntityManager = commandContext.getTaskEntityManager();
        TaskEntity task = taskEntityManager.create();
        String eachLoopUser = execution.getVariableInstanceLocal(MultiInstanceActivityBehavior.EACH_LOOP_USER).getTextValue();
        if (Strings.isNullOrEmpty(eachLoopUser)) {
            task.setAssignee((String) execution.getVariableLocal(MultiInstanceActivityBehavior.EACH_LOOP_USER));
        } else {
            task.setAssignee(eachLoopUser);
        }
        ExecutionEntity executionEntity = (ExecutionEntity) execution;
        task.setExecution(executionEntity);
        task.setTaskDefinitionKey(kaiteMultiUserTask.getId());
        task.setBusinessKey(executionEntity.getProcessInstanceBusinessKey());
        String activeTaskName;
        String activeTaskDescription;
        String activeTaskCategory;
        String activeTaskAssignee;
        String activeTaskOwner;
        ProcessEngineConfigurationImpl processEngineConfiguration = Context.getProcessEngineConfiguration();
        BpmnModel bpmnModel = processEngineConfiguration.getRepositoryService().getBpmnModel(executionEntity.getProcessDefinitionId(), executionEntity.getProcessInstanceId(),executionEntity.getProcessInstanceBusinessKey());
        validatePersonnelClearance(task.getAssignee(), bpmnModel, task.getTaskDefinitionKey());
        ExpressionManager expressionManager = processEngineConfiguration.getExpressionManager();
        if (Context.getProcessEngineConfiguration().isEnableProcessDefinitionInfoCache()) {
            ObjectNode taskElementProperties = Context.getBpmnOverrideElementProperties(kaiteMultiUserTask.getId(), execution.getProcessDefinitionId());
            activeTaskName = getActiveValue(kaiteMultiUserTask.getName(), DynamicBpmnConstants.USER_TASK_NAME, taskElementProperties);
            activeTaskDescription = getActiveValue(kaiteMultiUserTask.getDocumentation(), DynamicBpmnConstants.USER_TASK_DESCRIPTION, taskElementProperties);
            activeTaskCategory = getActiveValue(kaiteMultiUserTask.getCategory(), DynamicBpmnConstants.USER_TASK_CATEGORY, taskElementProperties);
        } else {
            activeTaskName = kaiteMultiUserTask.getName();
            activeTaskDescription = kaiteMultiUserTask.getDocumentation();
            activeTaskCategory = kaiteMultiUserTask.getCategory();
        }
        activeTaskAssignee = "";
        activeTaskOwner = "";

        if (StringUtils.isNotEmpty(activeTaskName)) {
            String name;
            try {
                name = (String) expressionManager.createExpression(activeTaskName).getValue(execution);
            } catch (ActivitiException e) {
                name = activeTaskName;
                logger.warn("property not found in task name expression " + e.getMessage());
            }
            task.setName(name);
        }

        if (StringUtils.isNotEmpty(activeTaskDescription)) {
            String description;
            try {
                description = (String) expressionManager.createExpression(activeTaskDescription).getValue(execution);
            } catch (ActivitiException e) {
                description = activeTaskDescription;
                logger.warn("property not found in task description expression " + e.getMessage());
            }
            task.setDescription(description);
        }

        if (StringUtils.isNotEmpty(activeTaskCategory)) {
            final Object category = expressionManager.createExpression(activeTaskCategory).getValue(execution);
            if (category != null) {
                if (category instanceof String) {
                    task.setCategory((String) category);
                } else {
                    throw new ActivitiIllegalArgumentException("Category expression does not resolve to a string: " + activeTaskCategory);
                }
            }
        }

        task.setAppVersion(executionEntity.getProcessInstance().getAppVersion());
        taskEntityManager.insert(task, executionEntity);
//        task.setVariablesLocal(calculateInputVariables(execution));

        JSONObject variableObj = handleBaseUserTaskConfig(kaiteMultiUserTask, task, expressionManager, bpmnModel);
//        task.setVariableLocal(TASK_GLOBAL_VAR, variableObj.toJSONString());

        boolean skipUserTask = false;

        // Handling assignments need to be done after the task is inserted, to have an id
        if (!skipUserTask) {
            handleAssignments(taskEntityManager, activeTaskAssignee, activeTaskOwner, task, expressionManager, execution);
        }

        processEngineConfiguration.getListenerNotificationHelper().executeTaskListeners(task, TaskListener.EVENTNAME_CREATE);

        // All properties set, now fire events
        if (Context.getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
            ActivitiEventDispatcher eventDispatcher = Context.getProcessEngineConfiguration().getEventDispatcher();
            eventDispatcher.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.TASK_CREATED, task));
            if (task.getAssignee() != null) {
                eventDispatcher.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.TASK_ASSIGNED, task));
            }
        }

        addUpcomingInfo(bpmnModel, execution, task.getAssignee(), task, false);
        //自动传阅
        automaticCirculation(task, bpmnModel);
        //添加预警
        addWarning(task, bpmnModel);
        Object upcomingInfoObj = Context.getCommandContext().getAttribute(UPCOMINGINFO);
        if (upcomingInfoObj == null) {
            Map<String, String> params = new HashMap<>();
            params.put("user", task.getAssignee());
            UpcomingCommentInfoDTO upcomingInfo = UpcomingCommentInfoDTO.build(SubmitTypeEnum.ADD, Context.getCommandContext().getBean(), task.getBusinessKey(),
                    "加签", task.getId(), params, "");
            upcomingInfo.setNodeId(task.getTaskDefinitionKey());
            upcomingInfo.setPiid(task.getProcessInstanceId());
            commandContext.getProcessEngineConfiguration().getActivitiUpcomingRun().completeUpcoming(upcomingInfo);
        }

        DelegateExecution parentExecution = getMultiInstanceScopeExecution(execution);
        task.setVariableLocal(MultiInstanceActivityBehavior.PROCESSING_USERS_INFO, parentExecution.getVariable(MultiInstanceActivityBehavior.PROCESSING_USERS_INFO));
        parentExecution.setVariableLocal(KaiteBaseUserTaskActivityBehavior.FORM_VAR_NAME, execution.getVariable(FORM_VAR_NAME));
        parentExecution.setVariableLocal(KaiteBaseUserTaskActivityBehavior.DIRECT_TASK_ID, execution.getVariable(DIRECT_TASK_ID));
        parentExecution.setVariableLocal(KaiteBaseUserTaskActivityBehavior.DIRECT_TASK_NAME, execution.getVariable(DIRECT_TASK_NAME));
        parentExecution.setVariableLocal(KaiteBaseUserTaskActivityBehavior.TASK_GLOBAL_VAR, variableObj.toJSONString());
        parentExecution.setVariableLocal(KaiteBaseUserTaskActivityBehavior.PREV_ASSIGNEE, execution.getVariable(PREV_ASSIGNEE));
        parentExecution.setVariableLocal(KaiteBaseUserTaskActivityBehavior.DIRECT_TASK_TARGET, execution.getVariable(DIRECT_TASK_TARGET));
        parentExecution.setVariableLocal(MultiInstanceActivityBehavior.PASS_PRINCIPAL, execution.getVariable(MultiInstanceActivityBehavior.PASS_PRINCIPAL));
        if (skipUserTask) {
            taskEntityManager.deleteTask(task, null, false, false);
            leave(execution);
        }

    }

    protected DelegateExecution getMultiInstanceScopeExecution(DelegateExecution executionEntity) {
        DelegateExecution multiInstanceRootExecution = null;
        DelegateExecution currentExecution = executionEntity;
        while (currentExecution != null && multiInstanceRootExecution == null && currentExecution.getParent() != null) {
            if (currentExecution.isMultiInstanceRoot()) {
                multiInstanceRootExecution = currentExecution;
            } else {
                currentExecution = currentExecution.getParent();
            }
        }
        return multiInstanceRootExecution;
    }

}
