/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.task;

import com.je.bpm.engine.internal.Internal;
import java.util.Date;

/**
 * Any type of content that is be associated with a task or with a process instance.
 *
 * @deprecated this interface and its implementations are going to be removed in future iterations
 * Atttachments doesn't belong to the Process/Task Runtime
 *
 * 与任务或流程实例关联的任何类型的内容,Atttachments不属于流程/任务运行时。
 */
@Internal
public interface Attachment {

  /** unique id for this attachment */
  String getId();

  /** free user defined short (max 255 chars) name for this attachment */
  String getName();

  /** free user defined short (max 255 chars) name for this attachment */
  void setName(String name);

  /**
   * long (max 255 chars) explanation what this attachment is about in context of the task and/or process instance it's linked to.
   */
  String getDescription();

  /**
   * long (max 255 chars) explanation what this attachment is about in context of the task and/or process instance it's linked to.
   */
  void setDescription(String description);

  /**
   * indication of the type of content that this attachment refers to. Can be mime type or any other indication.
   * 内容类型。可以是mime类型或任何其他
   */
  String getType();

  /** reference to the task to which this attachment is associated. */
  String getTaskId();

  /**
   * reference to the process instance to which this attachment is associated.
   */
  String getProcessInstanceId();

  /**
   * the remote URL in case this is remote content. If the attachment content was {@link com.je.bpm.engine.TaskService#createAttachment(String, String, String, String, String, java.io.InputStream) uploaded with an
   * input stream}, then this method returns null and the content can be fetched with {@link com.je.bpm.engine.TaskService#getAttachmentContent(String)}.
   */
  String getUrl();

  /** reference to the user who created this attachment. */
  String getUserId();

  /** timestamp when this attachment was created */
  Date getTime();

  /** timestamp when this attachment was created */
  void setTime(Date time);

  /** the id of the byte array entity storing the content */
  String getContentId();

}
