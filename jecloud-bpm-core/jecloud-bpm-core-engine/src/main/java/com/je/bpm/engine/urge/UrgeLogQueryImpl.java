/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.urge;

import com.je.bpm.engine.impl.AbstractQuery;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import java.util.Date;
import java.util.List;

/**
 * 催办日志查询实现
 */
public class UrgeLogQueryImpl extends AbstractQuery<UrgeLogQuery, UrgeLog> implements UrgeLogQuery {

    protected String processDefinitionId;
    protected String processInstanceId;
    protected String processName;
    protected String type;
    protected Date createTime;
    protected Date createTimeBefore;
    protected Date createTimeAfter;
    protected String taskId;
    protected String taskName;
    protected String from;
    protected String fromName;
    protected String to;
    protected String toName;
    protected String description;
    protected String readState;
    protected Date readTime;
    protected Date readTimeBefore;
    protected Date readTimeAfter;

    @Override
    public long executeCount(CommandContext commandContext) {
        return 0;
    }

    @Override
    public List<UrgeLog> executeList(CommandContext commandContext, Page page) {
        return null;
    }

    @Override
    public UrgeLogQuery processDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    @Override
    public UrgeLogQuery processInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }

    @Override
    public UrgeLogQuery processName(String processName) {
        this.processName = processName;
        return this;
    }

    @Override
    public UrgeLogQuery createTime(Date date) {
        this.createTime = date;
        return this;
    }

    @Override
    public UrgeLogQuery taskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    @Override
    public UrgeLogQuery taskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    @Override
    public UrgeLogQuery from(String from) {
        this.from = from;
        return this;
    }

    @Override
    public UrgeLogQuery fromName(String fromName) {
        this.fromName = fromName;
        return this;
    }

    @Override
    public UrgeLogQuery to(String to) {
        this.to = to;
        return this;
    }

    @Override
    public UrgeLogQuery toName(String toName) {
        this.toName = toName;
        return this;
    }

    @Override
    public UrgeLogQuery type(String type) {
        this.type = type;
        return this;
    }

    @Override
    public UrgeLogQuery description(String description) {
        this.description = description;
        return this;
    }

    @Override
    public UrgeLogQuery readState(String readState) {
        this.readState = readState;
        return this;
    }

    @Override
    public UrgeLogQuery readTime(Date date) {
        this.readTime = date;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Date getCreateTimeBefore() {
        return createTimeBefore;
    }

    public Date getCreateTimeAfter() {
        return createTimeAfter;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getReadState() {
        return readState;
    }

    public String getType() {
        return type;
    }

    public String getProcessName() {
        return processName;
    }

    public String getFromName() {
        return fromName;
    }

    public String getToName() {
        return toName;
    }

    public String getDescription() {
        return description;
    }

    public Date getReadTime() {
        return readTime;
    }

    public Date getReadTimeBefore() {
        return readTimeBefore;
    }

    public Date getReadTimeAfter() {
        return readTimeAfter;
    }
}
