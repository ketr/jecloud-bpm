/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.util;

import com.je.bpm.engine.delegate.DelegateExecution;

import java.io.*;
import java.util.List;
import java.util.Map;

public class ShellCommandExecutor implements CommandExecutor {

    private Boolean waitFlag;
    private final Boolean cleanEnvBoolean;
    private final Boolean redirectErrorFlag;
    private final String directoryStr;
    private final String resultVariableStr;
    private final String errorCodeVariableStr;
    private final List<String> argList;

    public ShellCommandExecutor(Boolean waitFlag, Boolean cleanEnvBoolean, Boolean redirectErrorFlag, String directoryStr, String resultVariableStr, String errorCodeVariableStr, List<String> argList) {
        this.waitFlag = waitFlag;
        this.cleanEnvBoolean = cleanEnvBoolean;
        this.redirectErrorFlag = redirectErrorFlag;
        this.directoryStr = directoryStr;
        this.resultVariableStr = resultVariableStr;
        this.errorCodeVariableStr = errorCodeVariableStr;
        this.argList = argList;
    }

    public ShellCommandExecutor(ShellExecutorContext context) {
        this(context.getWaitFlag(),
                context.getCleanEnvBoolan(),
                context.getRedirectErrorFlag(),
                context.getDirectoryStr(),
                context.getResultVariableStr(),
                context.getErrorCodeVariableStr(),
                context.getArgList());
    }

    @Override
    public void executeCommand(DelegateExecution execution) throws Exception {
        if (argList != null && argList.size() > 0) {
            ProcessBuilder processBuilder = new ProcessBuilder(argList);
            processBuilder.redirectErrorStream(getRedirectErrorFlag());
            if (getCleanEnvBoolean()) {
                Map<String, String> env = processBuilder.environment();
                env.clear();
            }
            if (getDirectoryStr() != null && getDirectoryStr().length() > 0){
                processBuilder.directory(new File(getDirectoryStr()));
            }
            Process process = processBuilder.start();
            if (getWaitFlag()) {
                int errorCode = process.waitFor();

                if (getResultVariableStr() != null) {
                    String result = convertStreamToStr(process.getInputStream());
                    execution.setVariable(getResultVariableStr(), result);
                }

                if (getErrorCodeVariableStr() != null) {
                    execution.setVariable(getErrorCodeVariableStr(), Integer.toString(errorCode));

                }

            }
        }
    }

    private String convertStreamToStr(InputStream is) throws IOException {

        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    public Boolean getWaitFlag() {
        return waitFlag;
    }

    public void setWaitFlag(Boolean waitFlag) {
        this.waitFlag = waitFlag;
    }

    public Boolean getCleanEnvBoolean() {
        return cleanEnvBoolean;
    }

    public Boolean getRedirectErrorFlag() {
        return redirectErrorFlag;
    }

    public String getDirectoryStr() {
        return directoryStr;
    }

    public String getResultVariableStr() {
        return resultVariableStr;
    }

    public String getErrorCodeVariableStr() {
        return errorCodeVariableStr;
    }

    public List<String> getArgList() {
        return argList;
    }

}
