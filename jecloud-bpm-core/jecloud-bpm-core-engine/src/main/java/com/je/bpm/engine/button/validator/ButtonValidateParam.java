/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.button.validator;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.engine.impl.cmd.GetTaskButtonVariableCmd;
import com.je.bpm.engine.impl.persistence.entity.VariableInstanceEntity;
import com.je.bpm.engine.task.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 获取按钮所使用的变量，属性加载 the {@link GetTaskButtonVariableCmd}
 */
public class ButtonValidateParam {

    /**
     * 模型
     */
    private List<BpmnModel> bpmnModels;
    /**
     * 没有启动时加载所有模型，用于展示流程图
     */
    private List<BpmnModel> notStartAllBpmModels;
    /**
     * 登录用户
     */
    private String logUserId;
    /**
     * bean创建用户
     */
    private String beanUserId;
    /**
     * 按钮config变量entity
     */
    private List<VariableInstanceEntity> buttonVariableList = new ArrayList<>();
    /**
     * 按钮config变量entity
     */
    private List<Task> taskList = new ArrayList<>();
    /**
     * 业务bean
     */
    private Map<String, Object> bean;
    /**
     * 是否启动
     */
    private Boolean starting = false;
    /**
     * 功能code
     */
    private String funcCode;
    /**
     * 业务beanId
     */
    private String beanId;
    /**
     * 是否结束
     */
    private Boolean ending = false;
    /**
     * 是否挂起
     *
     * @return
     */
    private Boolean hang = false;
    /**
     * 流程启动人
     */
    private String starter;
    /**
     * 当前登陆人是否在model可启动角色
     */
    private Boolean logUserRoleInStartRoleIds;

    private Map<String, Object> rootExecutionVars;

    private Map<String, Object> startUserButtons;

    public Map<String, Object> getRootExecutionVars() {
        return rootExecutionVars;
    }

    public void setRootExecutionVars(Map<String, Object> rootExecutionVars) {
        this.rootExecutionVars = rootExecutionVars;
    }

    public static ButtonValidateParam builder() {
        return new ButtonValidateParam();
    }

    public ButtonValidateParam bpmnModel(List<BpmnModel> bpmnModels) {
        this.setBpmnModel(bpmnModels);
        return this;
    }

    public List<BpmnModel> getNotStartAllBpmModels() {
        return notStartAllBpmModels;
    }

    public ButtonValidateParam setNotStartAllBpmModels(List<BpmnModel> notStartAllBpmModels) {
        this.notStartAllBpmModels = notStartAllBpmModels;
        return this;
    }

    public ButtonValidateParam starter(String starter) {
        this.setStarter(starter);
        return this;
    }

    public ButtonValidateParam taskList(List<Task> tasks) {
        this.taskList = tasks;
        return this;
    }

    public ButtonValidateParam funcCode(String funcCode) {
        this.setFuncCode(funcCode);
        return this;
    }

    public ButtonValidateParam logUserId(String logUserId) {
        this.setLogUserId(logUserId);
        return this;
    }

    public ButtonValidateParam buttonVariableList(List<VariableInstanceEntity> buttonVariableList) {
        this.setButtonVariableList(buttonVariableList);
        return this;
    }

    public ButtonValidateParam bean(Map<String, Object> bean) {
        this.setBean(bean);
        return this;
    }

    public ButtonValidateParam starting(Boolean starting) {
        this.setStarting(starting);
        return this;
    }

    public ButtonValidateParam ending(Boolean ending) {
        this.setEnding(ending);
        return this;
    }

    public ButtonValidateParam beanUserId(String beanUserId) {
        this.setBeanUserId(beanUserId);
        return this;
    }

    public ButtonValidateParam beanId(String beanId) {
        this.setBeanId(beanId);
        return this;
    }

    public ButtonValidateParam hang(Boolean hang) {
        this.setHang(hang);
        return this;
    }

    public String getBeanId() {
        return beanId;
    }

    private void setBeanId(String beanId) {
        this.beanId = beanId;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    private void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public List<BpmnModel> getBpmnModels() {
        return bpmnModels;
    }

    private void setBpmnModel(List<BpmnModel> bpmnModels) {
        this.bpmnModels = bpmnModels;
    }

    public String getLogUserId() {
        return logUserId;
    }

    private void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public String getBeanUserId() {
        return beanUserId;
    }

    private void setBeanUserId(String beanUserId) {
        this.beanUserId = beanUserId;
    }

    public List<VariableInstanceEntity> getButtonVariableList() {
        return buttonVariableList;
    }

    private void setButtonVariableList(List<VariableInstanceEntity> buttonVariableList) {
        this.buttonVariableList = buttonVariableList;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    private void setBean(Map<String, Object> bean) {
        this.bean = bean;
    }

    public Boolean getStarting() {
        return starting;
    }

    private void setStarting(Boolean starting) {
        this.starting = starting;
    }

    public Boolean getEnding() {
        return ending;
    }

    private void setEnding(Boolean ending) {
        this.ending = ending;
    }

    public String getFuncCode() {
        return funcCode;
    }

    private void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public Boolean getHang() {
        return hang;
    }

    private void setHang(Boolean hang) {
        this.hang = hang;
    }

    public String getStarter() {
        return starter;
    }

    public void setStarter(String starter) {
        this.starter = starter;
    }

    public void setBpmnModels(List<BpmnModel> bpmnModels) {
        this.bpmnModels = bpmnModels;
    }

    public Boolean getLogUserRoleInStartRoleIds() {
        return logUserRoleInStartRoleIds;
    }

    public void setLogUserRoleInStartRoleIds(Boolean logUserRoleInStartRoleIds) {
        this.logUserRoleInStartRoleIds = logUserRoleInStartRoleIds;
    }

    public Map<String, Object> getStartUserButtons() {
        return startUserButtons;
    }

    public void setStartUserButtons(Map<String, Object> startUserButtons) {
        this.startUserButtons = startUserButtons;
    }
}
