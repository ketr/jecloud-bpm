/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.FlowNode;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.gateway.InclusiveGateway;
import com.je.bpm.core.model.task.*;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiObjectNotFoundException;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.Task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取提交任务节点信息
 */
public class FindSubmitElementCmd implements Command<List<Map<SequenceFlow, Boolean>>>, Serializable {

    private static final long serialVersionUID = 1L;
    protected String executionId;
    protected String taskId;
    protected Map<String, Object> bean;
    protected String pdid;
    protected String prod;
    protected String businessKey;

    public FindSubmitElementCmd(String pdid, String taskId, Map<String, Object> bean, String prod, String businessKey) {
        this.taskId = taskId;
        this.pdid = pdid;
        this.bean = bean;
        this.prod = prod;
        this.businessKey = businessKey;
    }

    @Override
    public List<Map<SequenceFlow, Boolean>> execute(CommandContext commandContext) {
        if (Strings.isNullOrEmpty(taskId)) {
            return findSponsorNextElement(commandContext);
        }
        TaskEntity task = commandContext.getTaskEntityManager().findById(taskId);
        if (task == null) {
            throw new ActivitiObjectNotFoundException("Cannot find task with id " + taskId, Task.class);
        }
        if (task.isSuspended()) {
            throw new ActivitiException("this task is suspended");
        }
        return findNextElement(commandContext, task);
    }

    /**
     * 发起可以提交的节点信息
     *
     * @param commandContext
     * @return
     */
    private List<Map<SequenceFlow, Boolean>> findSponsorNextElement(CommandContext commandContext) {
        List<Map<SequenceFlow, Boolean>> result = new ArrayList<>();
        RepositoryService repositoryService = commandContext.getProcessEngineConfiguration().getRepositoryService();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(pdid, "", businessKey);
        List<FlowElement> list = bpmnModel.getMainProcess().getFlowElementList();
        for (FlowElement flowElement : list) {
            if (flowElement instanceof StartEvent) {
                StartEvent startEvent = (StartEvent) flowElement;
                FlowElement firstNode = startEvent.getOutgoingFlows().get(0);
                String targetRef = ((SequenceFlow) firstNode).getTargetRef();
                for (FlowElement kaiteUserTask : list) {
                    if (kaiteUserTask.getId().equals(targetRef)) {
                        //TODO 下个节点如果是分支聚合则跳过 看下个节点是否配置了可跳跃，并且可处理中是否包含自己 如果是接着寻找下下个节点直到不满足
                        FlowNode flowNode = getNextElement(commandContext, (KaiteTask) kaiteUserTask, true, "");
                        Boolean isJump = false;
                        if (!flowNode.getId().equals(kaiteUserTask.getId())) {
                            isJump = true;
                        }
                        List<SequenceFlow> outgoingList = flowNode.getOutgoingFlows();
                        buildGoingList(outgoingList, result, isJump);
                        break;
                    }
                }
                break;
            }
        }
        return result;
    }

    private List<Map<SequenceFlow, Boolean>> findNextElement(CommandContext commandContext, TaskEntity task) {
        List<Map<SequenceFlow, Boolean>> result = new ArrayList<>();
        RepositoryService repositoryService = commandContext.getProcessEngineConfiguration().getRepositoryService();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId(), task.getProcessInstanceId(), task.getBusinessKey());
        FlowElement taskFlowElement = bpmnModel.getFlowElement(task.getTaskDefinitionKey());
        if (taskFlowElement == null) {
            throw new ActivitiException(String.format("Can't find task %s definiton from the process.", task.getName()));
        }
        KaiteTask kaiteTask = (KaiteTask) taskFlowElement;
        //TODO 看下个节点是否配置了可跳跃，并且可处理中是否包含自己 如果是接着寻找下下个节点直到不满足
        FlowElement element = getNextElement(commandContext, kaiteTask, false, task.getProcessInstanceId());
        Boolean isJump = false;
        if (!kaiteTask.getId().equals(element.getId())) {
            isJump = true;
        }
        if (element instanceof KaiteLoopUserTask) {
            Map<SequenceFlow, Boolean> map = new HashMap<>();
            KaiteTask kaiteTaskNow = (KaiteTask) element;
            map.put(kaiteTaskNow.getIncomingFlows().get(0), false);
            result.add(map);
        }
        FlowNode flowNode = (FlowNode) element;
        List<SequenceFlow> outgoingList = flowNode.getOutgoingFlows();
        if (outgoingList.isEmpty()) {
            throw new ActivitiException(String.format("Can't find next node", task.getName()));
        }
        buildGoingList(outgoingList, result, isJump);
        return result;
    }

    private void buildGoingList(List<SequenceFlow> outgoingList, List<Map<SequenceFlow, Boolean>> result, boolean isJump) {
        for (SequenceFlow sequenceFlow : outgoingList) {
            String conditionExpression = sequenceFlow.getConditionExpression();
//            service => crm,beanName
            //TODO 把值取出来，进行解析
            if (DelegateHelper.isBoolean(conditionExpression, bean)) {
                Map<SequenceFlow, Boolean> map = new HashMap<>();
                map.put(sequenceFlow, isJump);
                result.add(map);
            }
        }
    }

    /**
     * @param commandContext
     * @param flowNode       当前节点
     */
    private FlowNode getNextElement(CommandContext commandContext, FlowNode flowNode, Boolean isSponsor, String piid) {
        RepositoryService repositoryService = commandContext.getProcessEngineConfiguration().getRepositoryService();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(pdid, piid, businessKey);

        //如果目标节点是网关节点 找可以走下去的唯一分支节点 如果是多个分支 自己选择走哪个，合并也是
        List<Map<SequenceFlow, Boolean>> result = new ArrayList<>();
        List<SequenceFlow> outgoingList = flowNode.getOutgoingFlows();
        if (outgoingList.isEmpty()) {
            throw new ActivitiException(String.format("Can't find next node %s", flowNode.getName()));
        }
        buildGoingList(outgoingList, result, false);
        if (result.size() > 1) {
            return flowNode;
        }
        for (Map<SequenceFlow, Boolean> sequenceFlowBooleanMap : result) {
            for (Map.Entry<SequenceFlow, Boolean> sequenceFlowBooleanEntry : sequenceFlowBooleanMap.entrySet()) {
                SequenceFlow sequenceFlow = sequenceFlowBooleanEntry.getKey();
                FlowElement flowElement = bpmnModel.getMainProcess().getFlowElementMap().get(sequenceFlow.getTargetRef());
                if (flowElement instanceof InclusiveGateway) {
                    return flowNode;
                }
            }
        }
        //获取节点处理人信息
        for (Map<SequenceFlow, Boolean> sequenceFlowBooleanMap : result) {
            for (Map.Entry<SequenceFlow, Boolean> sequenceFlowBooleanEntry : sequenceFlowBooleanMap.entrySet()) {
                SequenceFlow sequenceFlow = sequenceFlowBooleanEntry.getKey();
                //当前登录人是否在可处理人里边
                TaskService taskService = commandContext.getProcessEngineConfiguration().getTaskService();
                FlowElement flowElement = bpmnModel.getMainProcess().getFlowElementMap().get(sequenceFlow.getTargetRef());
                if (flowElement instanceof EndEvent) {
                    return flowNode;
                }
                if (flowElement instanceof InclusiveGateway) {
                    return flowNode;
                }
                //任务、判断、固定人，只有这3个节点支持可跳跃
                if (!(flowElement instanceof KaiteUserTask) && !(flowElement instanceof KaiteFixedUserTask) && !(flowElement instanceof KaiteDecideUserTask)) {
                    return flowNode;
                }
                KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
                //如果下个节点勾选了可跳跃
                if (!kaiteBaseUserTask.getTaskBasicConfig().getJump()) {
                    return flowNode;
                }
                Boolean containsCurrentUser = taskService.assignerContainsCurrentUser(pdid, (KaiteBaseUserTask) flowElement, taskId, prod, bean, businessKey);

                if (containsCurrentUser) {
                    //如果包含 接着找下个节点
                    return getNextElement(commandContext, (FlowNode) flowElement, false, piid);
                }
            }
        }
        return flowNode;
    }

}
