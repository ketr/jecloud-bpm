/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.cache;

import com.je.bpm.engine.impl.persistence.entity.Entity;

import java.util.*;

import static java.util.Collections.emptyList;

public class EntityCacheImpl implements EntityCache {

    protected Map<Class<?>, Map<String, CachedEntity>> cachedObjects = new HashMap<Class<?>, Map<String, CachedEntity>>();

    @Override
    public CachedEntity put(Entity entity, boolean storeState) {
        Map<String, CachedEntity> classCache = cachedObjects.get(entity.getClass());
        if (classCache == null) {
            classCache = new HashMap<String, CachedEntity>();
            cachedObjects.put(entity.getClass(), classCache);
        }
        CachedEntity cachedObject = new CachedEntity(entity, storeState);
        classCache.put(entity.getId(), cachedObject);
        return cachedObject;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T findInCache(Class<T> entityClass, String id) {
        CachedEntity cachedObject = null;
        Map<String, CachedEntity> classCache = cachedObjects.get(entityClass);
        if (classCache == null) {
            classCache = findClassCacheByCheckingSubclasses(entityClass);
        }
        if (classCache != null) {
            cachedObject = classCache.get(id);
        }
        if (cachedObject != null) {
            return (T) cachedObject.getEntity();
        }
        return null;
    }

    protected Map<String, CachedEntity> findClassCacheByCheckingSubclasses(Class<?> entityClass) {
        for (Class<?> clazz : cachedObjects.keySet()) {
            if (entityClass.isAssignableFrom(clazz)) {
                return cachedObjects.get(clazz);
            }
        }
        return null;
    }

    @Override
    public void cacheRemove(Class<?> entityClass, String entityId) {
        Map<String, CachedEntity> classCache = cachedObjects.get(entityClass);
        if (classCache == null) {
            return;
        }
        classCache.remove(entityId);
    }

    @Override
    public <T> Collection<CachedEntity> findInCacheAsCachedObjects(Class<T> entityClass) {
        Map<String, CachedEntity> classCache = cachedObjects.get(entityClass);
        if (classCache != null) {
            return classCache.values();
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> List<T> findInCache(Class<T> entityClass) {
        Map<String, CachedEntity> classCache = cachedObjects.get(entityClass);

        if (classCache == null) {
            classCache = findClassCacheByCheckingSubclasses(entityClass);
        }

        if (classCache != null) {
            List<T> entities = new ArrayList<T>(classCache.size());
            for (CachedEntity cachedObject : classCache.values()) {
                entities.add((T) cachedObject.getEntity());
            }
            return entities;
        }

        return emptyList();
    }

    @Override
    public Map<Class<?>, Map<String, CachedEntity>> getAllCachedEntities() {
        return cachedObjects;
    }

    @Override
    public void close() {

    }

    @Override
    public void flush() {

    }

}
