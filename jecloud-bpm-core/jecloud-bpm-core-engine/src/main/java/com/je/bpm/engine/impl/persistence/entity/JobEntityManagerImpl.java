/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;


import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.JobQueryImpl;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.CountingExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.data.DataManager;
import com.je.bpm.engine.impl.persistence.entity.data.JobDataManager;
import com.je.bpm.engine.runtime.Job;

import java.util.List;

public class JobEntityManagerImpl extends AbstractEntityManager<JobEntity> implements JobEntityManager {

    protected JobDataManager jobDataManager;

    public JobEntityManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration, JobDataManager jobDataManager) {
        super(processEngineConfiguration);
        this.jobDataManager = jobDataManager;
    }

    @Override
    protected DataManager<JobEntity> getDataManager() {
        return jobDataManager;
    }

    @Override
    public boolean insertJobEntity(JobEntity timerJobEntity) {
        return doInsert(timerJobEntity,
                true);
    }

    @Override
    public void insert(JobEntity jobEntity,
                       boolean fireCreateEvent) {
        doInsert(jobEntity,
                fireCreateEvent);
    }

    protected boolean doInsert(JobEntity jobEntity,
                               boolean fireCreateEvent) {
        // add link to execution
        if (jobEntity.getExecutionId() != null) {
            ExecutionEntity execution = getExecutionEntityManager().findById(jobEntity.getExecutionId());
            if (execution != null) {
                execution.getJobs().add(jobEntity);

                // Inherit tenant if (if applicable)
                if (execution.getTenantId() != null) {
                    jobEntity.setTenantId(execution.getTenantId());
                }

                if (isExecutionRelatedEntityCountEnabled(execution)) {
                    CountingExecutionEntity countingExecutionEntity = (CountingExecutionEntity) execution;
                    countingExecutionEntity.setJobCount(countingExecutionEntity.getJobCount() + 1);
                }
            } else {
                return false;
            }
        }

        super.insert(jobEntity,
                fireCreateEvent);
        return true;
    }

    public List<JobEntity> findJobsToExecute(Page page) {
        return jobDataManager.findJobsToExecute(page);
    }

    @Override
    public List<JobEntity> findJobsByExecutionId(String executionId) {
        return jobDataManager.findJobsByExecutionId(executionId);
    }

    @Override
    public List<JobEntity> findJobsByProcessDefinitionId(String processDefinitionId) {
        return jobDataManager.findJobsByProcessDefinitionId(processDefinitionId);
    }

    @Override
    public List<JobEntity> findJobsByTypeAndProcessDefinitionId(String jobTypeTimer,
                                                                String id) {
        return jobDataManager.findJobsByTypeAndProcessDefinitionId(jobTypeTimer,
                id);
    }

    @Override
    public List<JobEntity> findJobsByProcessInstanceId(String processInstanceId) {
        return jobDataManager.findJobsByProcessInstanceId(processInstanceId);
    }

    @Override
    public List<JobEntity> findExpiredJobs(Page page) {
        return jobDataManager.findExpiredJobs(page);
    }

    @Override
    public void resetExpiredJob(String jobId) {
        jobDataManager.resetExpiredJob(jobId);
    }

    @Override
    public List<Job> findJobsByQueryCriteria(JobQueryImpl jobQuery, Page page) {
        return jobDataManager.findJobsByQueryCriteria(jobQuery, page);
    }

    @Override
    public long findJobCountByQueryCriteria(JobQueryImpl jobQuery) {
        return jobDataManager.findJobCountByQueryCriteria(jobQuery);
    }

    @Override
    public void updateJobTenantIdForDeployment(String deploymentId, String newTenantId) {
        jobDataManager.updateJobTenantIdForDeployment(deploymentId, newTenantId);
    }

    @Override
    public void delete(JobEntity jobEntity) {
        super.delete(jobEntity);
        deleteExceptionByteArrayRef(jobEntity);
        removeExecutionLink(jobEntity);
        // Send event
        if (getEventDispatcher().isEnabled()) {
            getEventDispatcher().dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_DELETED, this));
        }
    }

    @Override
    public void delete(JobEntity entity, boolean fireDeleteEvent) {
        if (entity.getExecutionId() != null && isExecutionRelatedEntityCountEnabledGlobally()) {
            CountingExecutionEntity executionEntity = (CountingExecutionEntity) getExecutionEntityManager().findById(entity.getExecutionId());
            if (isExecutionRelatedEntityCountEnabled(executionEntity)) {
                executionEntity.setJobCount(executionEntity.getJobCount() - 1);
            }
        }
        super.delete(entity, fireDeleteEvent);
    }

    /**
     * Removes the job's execution's reference to this job, if the job has an associated execution.
     * Subclasses may override to provide custom implementations.
     */
    protected void removeExecutionLink(JobEntity jobEntity) {
        if (jobEntity.getExecutionId() != null) {
            ExecutionEntity execution = getExecutionEntityManager().findById(jobEntity.getExecutionId());
            if (execution != null) {
                execution.getJobs().remove(jobEntity);
            }
        }
    }

    /**
     * Deletes a the byte array used to store the exception information.  Subclasses may override
     * to provide custom implementations.
     */
    protected void deleteExceptionByteArrayRef(JobEntity jobEntity) {
        ByteArrayRef exceptionByteArrayRef = jobEntity.getExceptionByteArrayRef();
        if (exceptionByteArrayRef != null) {
            exceptionByteArrayRef.delete(false);
        }
    }

    public JobDataManager getJobDataManager() {
        return jobDataManager;
    }

    public void setJobDataManager(JobDataManager jobDataManager) {
        this.jobDataManager = jobDataManager;
    }
}
