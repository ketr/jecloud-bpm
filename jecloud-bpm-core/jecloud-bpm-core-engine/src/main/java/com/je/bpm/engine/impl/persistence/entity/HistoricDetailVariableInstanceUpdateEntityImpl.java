/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.variable.VariableType;
import org.apache.commons.lang3.StringUtils;

/**


 */
public class HistoricDetailVariableInstanceUpdateEntityImpl extends HistoricDetailEntityImpl implements HistoricDetailVariableInstanceUpdateEntity {

  private static final long serialVersionUID = 1L;

  protected int revision;

  protected String name;
  protected VariableType variableType;

  protected Long longValue;
  protected Double doubleValue;
  protected String textValue;
  protected String textValue2;
  protected ByteArrayRef byteArrayRef;

  protected Object cachedValue;

  public HistoricDetailVariableInstanceUpdateEntityImpl() {
    this.detailType = "VariableUpdate";
  }
  @Override
  public Object getPersistentState() {
    // HistoricDetailVariableInstanceUpdateEntity is immutable, so always
    // the same object is returned
    return HistoricDetailVariableInstanceUpdateEntityImpl.class;
  }
  @Override
  public Object getValue() {
    if (!variableType.isCachable() || cachedValue == null) {
      cachedValue = variableType.getValue(this);
    }
    return cachedValue;
  }
  @Override
  public String getVariableTypeName() {
    return (variableType != null ? variableType.getTypeName() : null);
  }
  @Override
  public int getRevisionNext() {
    return revision + 1;
  }

  // byte array value /////////////////////////////////////////////////////////

  @Override
  public byte[] getBytes() {
    if (byteArrayRef != null) {
      return byteArrayRef.getBytes(false);
    }
    return null;
  }
  @Override
  public ByteArrayRef getByteArrayRef() {
    return byteArrayRef;
  }
  @Override
  public void setBytes(byte[] bytes) {
    String byteArrayName = "hist.detail.var-" + name;
    if (byteArrayRef == null) {
      byteArrayRef = new ByteArrayRef();
    }
    byteArrayRef.setValue(byteArrayName, bytes,false);
  }

  // getters and setters ////////////////////////////////////////////////////////
  @Override
  public int getRevision() {
    return revision;
  }
  @Override
  public void setRevision(int revision) {
    this.revision = revision;
  }
  @Override
  public String getVariableName() {
    return name;
  }
  @Override
  public void setName(String name) {
    this.name = name;
  }
  @Override
  public String getName() {
    return name;
  }
  @Override
  public VariableType getVariableType() {
    return variableType;
  }
  @Override
  public void setVariableType(VariableType variableType) {
    this.variableType = variableType;
  }
  @Override
  public Long getLongValue() {
    return longValue;
  }
  @Override
  public void setLongValue(Long longValue) {
    this.longValue = longValue;
  }
  @Override
  public Double getDoubleValue() {
    return doubleValue;
  }
  @Override
  public void setDoubleValue(Double doubleValue) {
    this.doubleValue = doubleValue;
  }
  @Override
  public String getTextValue() {
    return textValue;
  }
  @Override
  public void setTextValue(String textValue) {
    this.textValue = textValue;
  }
  @Override
  public String getTextValue2() {
    return textValue2;
  }
  @Override
  public void setTextValue2(String textValue2) {
    this.textValue2 = textValue2;
  }
  @Override
  public Object getCachedValue() {
    return cachedValue;
  }
  @Override
  public void setCachedValue(Object cachedValue) {
    this.cachedValue = cachedValue;
  }

  // common methods ///////////////////////////////////////////////////////////////

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("HistoricDetailVariableInstanceUpdateEntity[");
    sb.append("id=").append(id);
    sb.append(", name=").append(name);
    sb.append(", type=").append(variableType != null ? variableType.getTypeName() : "null");
    if (longValue != null) {
      sb.append(", longValue=").append(longValue);
    }
    if (doubleValue != null) {
      sb.append(", doubleValue=").append(doubleValue);
    }
    if (textValue != null) {
      sb.append(", textValue=").append(StringUtils.abbreviate(textValue, 40));
    }
    if (textValue2 != null) {
      sb.append(", textValue2=").append(StringUtils.abbreviate(textValue2, 40));
    }
    if (byteArrayRef != null && byteArrayRef.getId() != null) {
      sb.append(", byteArrayValueId=").append(byteArrayRef.getId());
    }
    sb.append("]");
    return sb.toString();
  }

}
