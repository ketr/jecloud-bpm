/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.task;

import com.google.common.base.Strings;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.gateway.InclusiveGateway;
import com.je.bpm.core.model.task.*;

public class GetTakeNodeNameUtil {

    public static GetTakeNodeNameUtil builder() {
        return new GetTakeNodeNameUtil();
    }

    public String getTakeNodeName(FlowElement flowElement) {
        if (Strings.isNullOrEmpty(flowElement.getName())) {
            String nodeName = "";
            if (flowElement instanceof KaiteCandidateUserTask) {
                nodeName = KaiteTaskCategoryEnum.KAITE_CANDIDATE_USERTASK.getName();
            } else if (flowElement instanceof KaiteCounterSignUserTask) {
                nodeName = KaiteTaskCategoryEnum.KAITE_COUNTERSIGN_USERTASK.getName();
            } else if (flowElement instanceof KaiteMultiUserTask) {
                nodeName = KaiteTaskCategoryEnum.KAITE_MULTI_USERTASK.getName();
            } else if (flowElement instanceof KaiteFixedUserTask) {
                nodeName = KaiteTaskCategoryEnum.KAITE_FIXED_USERTASK.getName();
            } else if (flowElement instanceof KaiteRandomUserTask) {
                nodeName = KaiteTaskCategoryEnum.KAITE_RANDOM_USERTASK.getName();
            } else if (flowElement instanceof KaiteLoopUserTask) {
                nodeName = KaiteTaskCategoryEnum.KAITE_LOOP_USERTASK.getName();
            } else if (flowElement instanceof KaiteDecideUserTask) {
                nodeName = KaiteTaskCategoryEnum.KAITE_DECIDE_USERTASK.getName();
            } else if (flowElement instanceof InclusiveGateway) {
                nodeName = KaiteTaskCategoryEnum.INCLUSIVE_GATEWAY.getName();
            } else if (flowElement instanceof EndEvent) {
                nodeName = KaiteTaskCategoryEnum.JSON_END.getName();
            }
            return nodeName;
        } else {
            return flowElement.getName();
        }
    }

    public static String getTakeNodeName(String nodeId) {
        if (Strings.isNullOrEmpty(nodeId)) {
            return "";
        }

        String nodeName = "";
        if (nodeId.startsWith(KaiteTaskCategoryEnum.JSON_KAITE_USERTASK.getType())) {
            nodeName = KaiteTaskCategoryEnum.JSON_KAITE_USERTASK.getName();
        } else if (nodeId.startsWith(KaiteTaskCategoryEnum.JSON_KAITE_FIXED_USERTASK.getType())) {
            nodeName = KaiteTaskCategoryEnum.JSON_KAITE_FIXED_USERTASK.getName();
        } else if (nodeId.startsWith(KaiteTaskCategoryEnum.JSON_KAITE_RANDOM_USERTASK.getType())) {
            nodeName = KaiteTaskCategoryEnum.JSON_KAITE_RANDOM_USERTASK.getName();
        } else if (nodeId.startsWith(KaiteTaskCategoryEnum.JSON_KAITE_DECIDE_USERTASK.getType())) {
            nodeName = KaiteTaskCategoryEnum.JSON_KAITE_DECIDE_USERTASK.getName();
        } else if (nodeId.startsWith(KaiteTaskCategoryEnum.JSON_KAITE_COUNTERSIGN_USERTASK.getType())) {
            nodeName = KaiteTaskCategoryEnum.JSON_KAITE_COUNTERSIGN_USERTASK.getName();
        } else if (nodeId.startsWith(KaiteTaskCategoryEnum.JSON_KAITE_CANDIDATE_USERTASK.getType())) {
            nodeName = KaiteTaskCategoryEnum.JSON_KAITE_CANDIDATE_USERTASK.getName();
        } else if (nodeId.startsWith(KaiteTaskCategoryEnum.JSON_KAITE_LOOP_USERTASK.getType())) {
            nodeName = KaiteTaskCategoryEnum.JSON_KAITE_LOOP_USERTASK.getName();
        } else if (nodeId.startsWith(KaiteTaskCategoryEnum.JSON_KAITE_MULTI_USERTASK.getType())) {
            nodeName = KaiteTaskCategoryEnum.JSON_KAITE_MULTI_USERTASK.getName();
        }

        return nodeName;
    }

}
