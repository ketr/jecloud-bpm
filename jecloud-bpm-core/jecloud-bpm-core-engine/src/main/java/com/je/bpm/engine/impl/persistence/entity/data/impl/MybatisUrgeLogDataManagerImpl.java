/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.UrgeLogEntity;
import com.je.bpm.engine.impl.persistence.entity.UrgeLogEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.data.AbstractDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.UrgeLogDataManager;
import com.je.bpm.engine.passround.PassRoundQuery;
import com.je.bpm.engine.passround.PassRoundQueryImpl;
import com.je.bpm.engine.urge.UrgeLogQuery;
import com.je.bpm.engine.urge.UrgeLogQueryImpl;

import java.util.List;

public class MybatisUrgeLogDataManagerImpl extends AbstractDataManager<UrgeLogEntity> implements UrgeLogDataManager {

    public MybatisUrgeLogDataManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public Class<? extends UrgeLogEntity> getManagedEntityClass() {
        return UrgeLogEntityImpl.class;
    }

    @Override
    public UrgeLogEntity create() {
        return new UrgeLogEntityImpl();
    }

    @Override
    public void read(String taskId, String userId) {
        PassRoundQuery passRoundQuery = new PassRoundQueryImpl();
        passRoundQuery.taskId(taskId);
        passRoundQuery.to(userId);
        getDbSqlSession().update("updatePassRoundState", passRoundQuery);
    }

    @Override
    public List<UrgeLogEntity> findByTaskId(String taskId) {
        return getDbSqlSession().selectList("findByTaskId", taskId);
    }

    @Override
    public List<UrgeLogEntity> findByProcessInstanceId(String processInstanceId) {
        return getDbSqlSession().selectList("findByProcessInstanceId", processInstanceId);
    }

    @Override
    public List<UrgeLogEntity> findByProcessDefinitionId(String processDefinitionId) {
        return getDbSqlSession().selectList("findByProcessDefinitionId", processDefinitionId);
    }

    @Override
    public List<UrgeLogEntity> findByTaskIdAndTo(String taskId, String to) {
        UrgeLogQuery urgeLogQuery = new UrgeLogQueryImpl();
        urgeLogQuery.taskId(taskId);
        urgeLogQuery.to(to);
        return getDbSqlSession().selectList("findByTaskIdAndTo", urgeLogQuery);
    }

    @Override
    public List<UrgeLogEntity> findByTo(String to) {
        return getDbSqlSession().selectList("findByTaskIdAndTo", to);
    }

    @Override
    public List<UrgeLogEntity> findByTaskIdAndFrom(String taskId, String from) {
        UrgeLogQuery urgeLogQuery = new UrgeLogQueryImpl();
        urgeLogQuery.taskId(taskId);
        urgeLogQuery.to(from);
        return getDbSqlSession().selectList("findByTaskIdAndTo", urgeLogQuery);
    }

    @Override
    public List<UrgeLogEntity> findByFrom(String from) {
        return getDbSqlSession().selectList("findByTaskIdAndTo", from);
    }

}
