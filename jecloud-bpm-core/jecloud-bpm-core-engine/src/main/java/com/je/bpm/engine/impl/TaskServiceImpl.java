/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.bpm.core.model.FlowNode;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.delegate.event.ActivitiCountersignedOpinionType;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.cmd.*;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.impl.persistence.entity.VariableInstance;
import com.je.bpm.engine.runtime.DataObject;
import com.je.bpm.engine.task.*;

import java.io.InputStream;
import java.util.*;

public class TaskServiceImpl extends ServiceImpl implements TaskService {

    public TaskServiceImpl(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public Task newTask() {
        return newTask(null);
    }

    @Override
    public Task newTask(String taskId) {
        return commandExecutor.execute(new NewTaskCmd(taskId));
    }

    @Override
    public Task saveTask(Task task) {
        return commandExecutor.execute(new SaveTaskCmd(task));
    }

    @Override
    public void deleteTask(String taskId) {
        commandExecutor.execute(new DeleteTaskCmd(taskId, null, false));
    }

    @Override
    public void deleteTasks(Collection<String> taskIds) {
        commandExecutor.execute(new DeleteTaskCmd(taskIds, null, false));
    }

    @Override
    public void deleteTask(String taskId, boolean cascade) {
        commandExecutor.execute(new DeleteTaskCmd(taskId, null, cascade));
    }

    @Override
    public void deleteTasks(Collection<String> taskIds, boolean cascade) {
        commandExecutor.execute(new DeleteTaskCmd(taskIds, null, cascade));
    }

    @Override
    public void deleteTask(String taskId, String deleteReason) {
        commandExecutor.execute(new DeleteTaskCmd(taskId, deleteReason, false));
    }

    @Override
    public void deleteTask(String taskId, String deleteReason, boolean cancel) {
        commandExecutor.execute(new DeleteTaskCmd(taskId, deleteReason, false, cancel));
    }

    @Override
    public void deleteTasks(Collection<String> taskIds, String deleteReason) {
        commandExecutor.execute(new DeleteTaskCmd(taskIds, deleteReason, false, false));
    }

    @Override
    public void deleteTasks(Collection<String> taskIds, String deleteReason, boolean cancel) {
        commandExecutor.execute(new DeleteTaskCmd(taskIds, deleteReason, false, cancel));
    }

    @Override
    public void setAssignee(String taskId, String userId) {
        commandExecutor.execute(new AddIdentityLinkCmd(taskId, userId, AddIdentityLinkCmd.IDENTITY_USER, IdentityLinkType.ASSIGNEE));
    }

    @Override
    public void setOwner(String taskId, String userId) {
        commandExecutor.execute(new AddIdentityLinkCmd(taskId, userId, AddIdentityLinkCmd.IDENTITY_USER, IdentityLinkType.OWNER));
    }

    @Override
    public void addCandidateUser(String taskId, String userId) {
        commandExecutor.execute(new AddIdentityLinkCmd(taskId, userId, AddIdentityLinkCmd.IDENTITY_USER, IdentityLinkType.CANDIDATE));
    }

    @Override
    public void addCandidateGroup(String taskId, String groupId) {
        commandExecutor.execute(new AddIdentityLinkCmd(taskId, groupId, AddIdentityLinkCmd.IDENTITY_GROUP, IdentityLinkType.CANDIDATE));
    }

    @Override
    public void addUserIdentityLink(String taskId, String userId, String identityLinkType) {
        commandExecutor.execute(new AddIdentityLinkCmd(taskId, userId, AddIdentityLinkCmd.IDENTITY_USER, identityLinkType));
    }

    @Override
    public void addGroupIdentityLink(String taskId, String groupId, String identityLinkType) {
        commandExecutor.execute(new AddIdentityLinkCmd(taskId, groupId, AddIdentityLinkCmd.IDENTITY_GROUP, identityLinkType));
    }

    @Override
    public void deleteCandidateGroup(String taskId, String groupId) {
        commandExecutor.execute(new DeleteIdentityLinkCmd(taskId, null, groupId, IdentityLinkType.CANDIDATE));
    }

    @Override
    public void deleteCandidateUser(String taskId, String userId) {
        commandExecutor.execute(new DeleteIdentityLinkCmd(taskId, userId, null, IdentityLinkType.CANDIDATE));
    }

    @Override
    public void deleteGroupIdentityLink(String taskId, String groupId, String identityLinkType) {
        commandExecutor.execute(new DeleteIdentityLinkCmd(taskId, null, groupId, identityLinkType));
    }

    @Override
    public void deleteUserIdentityLink(String taskId, String userId, String identityLinkType) {
        commandExecutor.execute(new DeleteIdentityLinkCmd(taskId, userId, null, identityLinkType));
    }

    @Override
    public List<IdentityLink> getIdentityLinksForTask(String taskId) {
        return commandExecutor.execute(new GetIdentityLinksForTaskCmd(taskId));
    }

    @Override
    public void claim(String taskId, String prod, Map<String, Object> bean, String userId) {
        commandExecutor.execute(new ClaimTaskCmd(taskId, prod, bean, userId));
    }

    @Override
    public void unclaim(String taskId) {
        commandExecutor.execute(new ClaimTaskCmd(taskId, null, null, null));
    }

    @Override
    public void complete(String taskId) {
        commandExecutor.execute(new CompleteTaskCmd(taskId, null));
    }

    @Override
    public void complete(String taskId, String assigneeUser, String target, String prod, Map<String, Object> bean, String comment, String sequentials, String isJump, String files) {
        commandExecutor.execute(new CompleteTaskCmd(taskId, assigneeUser, target, prod, bean, comment, sequentials, isJump, files));
    }

    @Override
    public void complete(String taskId, String assigneeUser, String target, String prod, Map<String, Object> bean, String comment, String sequentials, String isJump, boolean countersign, boolean signBack, Object histVar, String files) {
        commandExecutor.execute(new CompleteTaskCmd(taskId, assigneeUser, target, prod, bean, comment, sequentials, isJump, countersign, signBack, histVar, files));
    }

    @Override
    public void delegateTask(String taskId, String prod, Map<String, Object> bean, String userId, String comment, String assignee, String files) {
        commandExecutor.execute(new DelegateTaskCmd(taskId, prod, bean, userId, comment, assignee, files));
    }

    @Override
    public void cancelDelegateTask(String taskId, String prod, Map<String, Object> bean) {
        commandExecutor.execute(new CancelDelegateTaskCmd(taskId, prod, bean));
    }

    @Override
    public void transferTask(String taskId, String prod, Map<String, Object> bean, String userId, String comment, String assignee, String files) {
        commandExecutor.execute(new TransferTaskCmd(taskId, prod, bean, userId, comment, assignee, files));
    }

    @Override
    public void checkTransferTask(String taskId) {
        commandExecutor.execute(new CheckTransferTaskCmd(taskId));
    }

    @Override
    public void setPriority(String taskId, int priority) {
        commandExecutor.execute(new SetTaskPriorityCmd(taskId, priority));
    }

    @Override
    public void setDueDate(String taskId, Date dueDate) {
        commandExecutor.execute(new SetTaskDueDateCmd(taskId, dueDate));
    }

    @Override
    public TaskQuery createTaskQuery() {
        return new TaskQueryImpl(commandExecutor, processEngineConfiguration.getDatabaseType());
    }

    @Override
    public NativeTaskQuery createNativeTaskQuery() {
        return new NativeTaskQueryImpl(commandExecutor);
    }

    @Override
    public Map<String, Object> getVariables(String taskId) {
        return commandExecutor.execute(new GetTaskVariablesCmd(taskId, null, false));
    }

    @Override
    public Map<String, Object> getVariablesLocal(String taskId) {
        return commandExecutor.execute(new GetTaskVariablesCmd(taskId, null, true));
    }

    @Override
    public Map<String, Object> getVariables(String taskId, Collection<String> variableNames) {
        return commandExecutor.execute(new GetTaskVariablesCmd(taskId, variableNames, false));
    }

    @Override
    public Map<String, Object> getVariablesLocal(String taskId, Collection<String> variableNames) {
        return commandExecutor.execute(new GetTaskVariablesCmd(taskId, variableNames, true));
    }

    @Override
    public Object getVariable(String taskId, String variableName) {
        return commandExecutor.execute(new GetTaskVariableCmd(taskId, variableName, false));
    }

    @Override
    public <T> T getVariable(String taskId, String variableName, Class<T> variableClass) {
        return variableClass.cast(getVariable(taskId, variableName));
    }

    @Override
    public boolean hasVariable(String taskId, String variableName) {
        return commandExecutor.execute(new HasTaskVariableCmd(taskId, variableName, false));
    }

    @Override
    public Object getVariableLocal(String taskId, String variableName) {
        return commandExecutor.execute(new GetTaskVariableCmd(taskId, variableName, true));
    }

    @Override
    public <T> T getVariableLocal(String taskId, String variableName, Class<T> variableClass) {
        return variableClass.cast(getVariableLocal(taskId, variableName));
    }

    @Override
    public List<VariableInstance> getVariableInstancesLocalByTaskIds(Set<String> taskIds) {
        return commandExecutor.execute(new GetTasksLocalVariablesCmd(taskIds));
    }

    @Override
    public List<Map<String, String>> getGobackAndRetrieveNodeInfo(String taskId, String type) {
        return commandExecutor.execute(new GetGobackAndRetrieveNodeInfoCmd(taskId, type));
    }

    @Override
    public Map<String, String> getDirectDeliveryName(String taskId) {
        return commandExecutor.execute(new GetDirectDeliveryNameCmd(taskId));
    }

    @Override
    public boolean hasVariableLocal(String taskId, String variableName) {
        return commandExecutor.execute(new HasTaskVariableCmd(taskId, variableName, true));
    }

    @Override
    public void setVariable(String taskId, String variableName, Object value) {
        if (variableName == null) {
            throw new ActivitiIllegalArgumentException("variableName is null");
        }
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put(variableName, value);
        commandExecutor.execute(new SetTaskVariablesCmd(taskId, variables, false));
    }

    @Override
    public void setVariableLocal(String taskId, String variableName, Object value) {
        if (variableName == null) {
            throw new ActivitiIllegalArgumentException("variableName is null");
        }
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put(variableName, value);
        commandExecutor.execute(new SetTaskVariablesCmd(taskId, variables, true));
    }

    @Override
    public void setVariables(String taskId, Map<String, ? extends Object> variables) {
        commandExecutor.execute(new SetTaskVariablesCmd(taskId, variables, false));
    }

    @Override
    public void setVariablesLocal(String taskId, Map<String, ? extends Object> variables) {
        commandExecutor.execute(new SetTaskVariablesCmd(taskId, variables, true));
    }

    @Override
    public void removeVariable(String taskId, String variableName) {
        Collection<String> variableNames = new ArrayList<String>();
        variableNames.add(variableName);
        commandExecutor.execute(new RemoveTaskVariablesCmd(taskId, variableNames, false));
    }

    @Override
    public void removeVariableLocal(String taskId, String variableName) {
        Collection<String> variableNames = new ArrayList<String>(1);
        variableNames.add(variableName);
        commandExecutor.execute(new RemoveTaskVariablesCmd(taskId, variableNames, true));
    }

    @Override
    public void removeVariables(String taskId, Collection<String> variableNames) {
        commandExecutor.execute(new RemoveTaskVariablesCmd(taskId, variableNames, false));
    }

    @Override
    public void removeVariablesLocal(String taskId, Collection<String> variableNames) {
        commandExecutor.execute(new RemoveTaskVariablesCmd(taskId, variableNames, true));
    }

    @Override
    public Comment addComment(String taskId, String processInstance, String message) {
        return commandExecutor.execute(new AddCommentCmd(taskId, processInstance, message));
    }

    @Override
    public Comment addComment(String taskId, String processInstance, String type, String message) {
        if (type.equals(Comment.FILES_COMMENT) && Strings.isNullOrEmpty(message)) {
            return null;
        }
        return commandExecutor.execute(new AddCommentCmd(taskId, processInstance, type, message));
    }

    @Override
    public Comment getComment(String commentId) {
        return commandExecutor.execute(new GetCommentCmd(commentId));
    }

    @Override
    public Event getEvent(String eventId) {
        return commandExecutor.execute(new GetTaskEventCmd(eventId));
    }

    @Override
    public List<Comment> getTaskComments(String taskId) {
        return commandExecutor.execute(new GetTaskCommentsCmd(taskId));
    }

    @Override
    public List<Comment> getTaskComments(String taskId, String type) {
        return commandExecutor.execute(new GetTaskCommentsByTypeCmd(taskId, type));
    }

    @Override
    public List<Comment> getCommentsByType(String type) {
        return commandExecutor.execute(new GetTypeCommentsCmd(type));
    }

    @Override
    public List<Event> getTaskEvents(String taskId) {
        return commandExecutor.execute(new GetTaskEventsCmd(taskId));
    }

    @Override
    public List<Comment> getProcessInstanceComments(String processInstanceId) {
        return commandExecutor.execute(new GetProcessInstanceCommentsCmd(processInstanceId));
    }

    @Override
    public List<Comment> getProcessInstanceComments(String processInstanceId, String type) {
        return commandExecutor.execute(new GetProcessInstanceCommentsCmd(processInstanceId, type));
    }

    @Override
    public Attachment createAttachment(String attachmentType, String taskId, String processInstanceId, String attachmentName, String attachmentDescription, InputStream content) {
        return commandExecutor.execute(new CreateAttachmentCmd(attachmentType, taskId, processInstanceId, attachmentName, attachmentDescription, content, null));
    }

    @Override
    public Attachment createAttachment(String attachmentType, String taskId, String processInstanceId, String attachmentName, String attachmentDescription, String url) {
        return commandExecutor.execute(new CreateAttachmentCmd(attachmentType, taskId, processInstanceId, attachmentName, attachmentDescription, null, url));
    }

    @Override
    public InputStream getAttachmentContent(String attachmentId) {
        return commandExecutor.execute(new GetAttachmentContentCmd(attachmentId));
    }

    @Override
    public void deleteAttachment(String attachmentId) {
        commandExecutor.execute(new DeleteAttachmentCmd(attachmentId));
    }

    @Override
    public void deleteComments(String taskId, String processInstanceId) {
        commandExecutor.execute(new DeleteCommentCmd(taskId, processInstanceId, null));
    }

    @Override
    public void deleteComment(String commentId) {
        commandExecutor.execute(new DeleteCommentCmd(null, null, commentId));
    }

    @Override
    public Attachment getAttachment(String attachmentId) {
        return commandExecutor.execute(new GetAttachmentCmd(attachmentId));
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Attachment> getTaskAttachments(String taskId) {
        return (List<Attachment>) commandExecutor.execute(new GetTaskAttachmentsCmd(taskId));
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Attachment> getProcessInstanceAttachments(String processInstanceId) {
        return (List<Attachment>) commandExecutor.execute(new GetProcessInstanceAttachmentsCmd(processInstanceId));
    }

    @Override
    public void saveAttachment(Attachment attachment) {
        commandExecutor.execute(new SaveAttachmentCmd(attachment));
    }

    @Override
    public List<Task> getSubTasks(String parentTaskId) {
        return commandExecutor.execute(new GetSubTasksCmd(parentTaskId));
    }

    @Override
    public VariableInstance getVariableInstance(String taskId, String variableName) {
        return commandExecutor.execute(new GetTaskVariableInstanceCmd(taskId, variableName, false));
    }

    @Override
    public VariableInstance getVariableInstanceLocal(String taskId, String variableName) {
        return commandExecutor.execute(new GetTaskVariableInstanceCmd(taskId, variableName, true));
    }

    @Override
    public Map<String, VariableInstance> getVariableInstances(String taskId) {
        return commandExecutor.execute(new GetTaskVariableInstancesCmd(taskId, null, false));
    }

    @Override
    public Map<String, VariableInstance> getVariableInstances(String taskId, Collection<String> variableNames) {
        return commandExecutor.execute(new GetTaskVariableInstancesCmd(taskId, variableNames, false));
    }

    @Override
    public Map<String, VariableInstance> getVariableInstancesLocal(String taskId) {
        return commandExecutor.execute(new GetTaskVariableInstancesCmd(taskId, null, true));
    }

    @Override
    public Map<String, VariableInstance> getVariableInstancesLocal(String taskId, Collection<String> variableNames) {
        return commandExecutor.execute(new GetTaskVariableInstancesCmd(taskId, variableNames, true));
    }

    @Override
    public Map<String, DataObject> getDataObjects(String taskId) {
        return commandExecutor.execute(new GetTaskDataObjectsCmd(taskId, null));
    }

    @Override
    public Map<String, DataObject> getDataObjects(String taskId, String locale, boolean withLocalizationFallback) {
        return commandExecutor.execute(new GetTaskDataObjectsCmd(taskId, null, locale, withLocalizationFallback));
    }

    @Override
    public Map<String, DataObject> getDataObjects(String taskId, Collection<String> dataObjectNames) {
        return commandExecutor.execute(new GetTaskDataObjectsCmd(taskId, dataObjectNames));
    }

    @Override
    public Map<String, DataObject> getDataObjects(String taskId, Collection<String> dataObjectNames, String locale, boolean withLocalizationFallback) {
        return commandExecutor.execute(new GetTaskDataObjectsCmd(taskId, dataObjectNames, locale, withLocalizationFallback));
    }

    @Override
    public DataObject getDataObject(String taskId, String dataObject) {
        return commandExecutor.execute(new GetTaskDataObjectCmd(taskId, dataObject));
    }

    @Override
    public DataObject getDataObject(String taskId, String dataObjectName, String locale, boolean withLocalizationFallback) {
        return commandExecutor.execute(new GetTaskDataObjectCmd(taskId, dataObjectName, locale, withLocalizationFallback));
    }

    @Override
    public void passRoundTask(String taskId) {
        commandExecutor.execute(new PassRoundTaskCmd(taskId));
    }

    @Override
    public void passRoundTask(String taskId, List<String> users, String prod, String beanId, String tableCode) {
        commandExecutor.execute(new PassRoundTaskCmd(taskId, users, prod, beanId, tableCode));
    }

    @Override
    public void passRoundTask(String taskId, List<String> users, String prod, String beanId, String tableCode, String from) {
        commandExecutor.execute(new PassRoundTaskCmd(taskId, users, prod, beanId, tableCode, from));
    }

    @Override
    public void readPassRoundTask(String taskId, String comment, String files) {
        commandExecutor.execute(new PassRoundReadedTaskCmd(taskId, comment, files));
    }

    @Override
    public void readPassRoundTask(String taskId, String userId, String comment, String files) {
        commandExecutor.execute(new PassRoundReadedTaskCmd(taskId, userId, comment));
    }

    @Override
    public void delayTask(String taskId) {
        commandExecutor.execute(new DelayCmd(taskId));
    }

    @Override
    public void delayTask(String taskId, Date nextDueTime) {
        commandExecutor.execute(new DelayCmd(taskId, nextDueTime));
    }

    @Override
    public void delayTask(String taskId, Date nextDueTime, List<String> receivers) {
        commandExecutor.execute(new DelayCmd(taskId, nextDueTime, receivers));
    }

    @Override
    public void delayLogRead(String delayId) {
        commandExecutor.execute(new DelayLogReadCmd(delayId));
    }

    @Override
    public void checkAndEarlyWarning(String processInstanceId) {
        commandExecutor.execute(new EarlyWarningCheckCmd(processInstanceId));
    }

    @Override
    public void checkAndEarlyWarning() {
        commandExecutor.execute(new EarlyWarningCheckCmd());
    }

    @Override
    public void earlyWarningRead(String earlyWarningId) {
        commandExecutor.execute(new EarlyWarningReadCmd(earlyWarningId));
    }


    @Override
    public void urgeTask(List<String> reminderMethod, List<Map<String, String>> personBeingUrged, String urgentContent,
                         List<Map<String, String>> ccUser, String ccContent, String taskId, String currentNodeId, String funcCode) {
        commandExecutor.execute(new UrgeTaskCmd(reminderMethod, personBeingUrged, urgentContent, ccUser, ccContent, taskId, currentNodeId, funcCode));
    }


    @Override
    public void readUrgeLog(String urgeLogId) {
        commandExecutor.execute(new UrgeReadLogCmd(urgeLogId));
    }

    @Override
    public void jump(String currentTaskId, String targetDefinitionKey, Map<String, Object> variables) {
        commandExecutor.execute(new JumpTaskCmd(currentTaskId, targetDefinitionKey, variables));
    }

    @Override
    public void retrieveTask(String taskId, String prod, Map<String, Object> bean, String beanId) {
        commandExecutor.execute(new RetrieveTaskCmd(taskId, prod, bean, null, beanId, ""));
    }

    @Override
    public void retrieveTask(String taskId, String prod, Map<String, Object> bean, String comment, String beanId, String files) {
        commandExecutor.execute(new RetrieveTaskCmd(taskId, prod, bean, comment, beanId, files));
    }

    @Override
    public void withdrawTask(String taskId, String prod, Map<String, Object> bean, String comment, String beanId, String files) {
        commandExecutor.execute(new WithdrawTaskCmd(taskId, prod, bean, comment, beanId, files));
    }

    @Override
    public void withdrawTask(String taskId, String prod, Map<String, Object> bean, String beanId) {
        commandExecutor.execute(new WithdrawTaskCmd(taskId, prod, bean, "", beanId, ""));
    }

    @Override
    public void gobackTask(String taskId) {
        commandExecutor.execute(new GoBackTaskCmd(taskId, null, null, null, ""));
    }

    @Override
    public void gobackTask(String taskId, String prod, Map<String, Object> bean, String comment, String files) {
        commandExecutor.execute(new GoBackTaskCmd(taskId, prod, bean, comment, files));
    }

    @Override
    public void directComplete(String taskId, Map<String, Object> bean, String prod) {
        commandExecutor.execute(new DirectCompleteCmd(taskId, null, bean, prod, "", ""));
    }

    @Override
    public void directComplete(String taskId, String comment, Map<String, Object> bean, String prod, String files) {
        commandExecutor.execute(new DirectCompleteCmd(taskId, comment, bean, prod, "", files));
    }

    @Override
    public void dismissTask(String taskId, String definitionKey) {
        commandExecutor.execute(new DismissTaskCmd(taskId, definitionKey, null, null, null, ""));
    }

    @Override
    public void dismissTask(String taskId, String definitionKey, String comment, Map<String, Object> bean, String prod, String files) {
        commandExecutor.execute(new DismissTaskCmd(taskId, definitionKey, comment, bean, prod, files));
    }

    @Override
    public void loop(String taskId) {
        commandExecutor.execute(new LoopCompleteCmd(taskId, null, null));
    }

    @Override
    public void loop(String taskId, String assigneeUser) {
        commandExecutor.execute(new LoopCompleteCmd(taskId, assigneeUser, null));
    }

    @Override
    public void loop(String taskId, String assigneeUser, Map<String, Object> variables) {
        commandExecutor.execute(new LoopCompleteCmd(taskId, assigneeUser, variables));
    }

    @Override
    public void loop(String taskId, String assigneeUser, Map<String, Object> variables, Map<String, Object> transientVariables) {
        commandExecutor.execute(new LoopCompleteCmd(taskId, assigneeUser, variables, transientVariables));
    }

    @Override
    public void loop(String taskId, String assigneeUser, Map<String, Object> variables, boolean localScope) {
        commandExecutor.execute(new LoopCompleteCmd(taskId, null, null, assigneeUser, variables, localScope));
    }

    @Override
    public Map<String, Object> getCurrentTask(String processInstanceId) {
        return null;
    }

    @Override
    public void pass(String taskId, String prod, Map<String, Object> bean, String comment, String files) {
        commandExecutor.execute(new PassTaskCmd(taskId, prod, bean, comment, files));
    }

    @Override
    public void abstain(String taskId, String prod, Map<String, Object> bean, String comment, String files) {
        commandExecutor.execute(new AbstainTaskCmd(taskId, prod, bean, comment, files));
    }

    @Override
    public void veto(String taskId, String prod, Map<String, Object> bean, String comment, String files) {
        commandExecutor.execute(new VetoTaskCmd(taskId, prod, bean, comment, files));
    }

    @Override
    public void countersignedAddSignature(String assignee, String taskId) {
        commandExecutor.execute(new CountersignedAddSignatureCmd(assignee, taskId));
    }

    @Override
    public void multiTaskHandover(String assignee, String toAssignee, String taskId) {
        commandExecutor.execute(new MultiTaskHandoverCmd(assignee, toAssignee, taskId));
    }

    @Override
    public void personnelAdjustments(String assignee, String taskId) {
        commandExecutor.execute(new PersonnelAdjustmentsCmd(assignee, taskId));
    }

    @Override
    public void countersignedVisaReduction(String assignee, String taskId) {
        commandExecutor.execute(new CountersignedVisaReductionCmd(assignee, taskId));
    }

    @Override
    public void countersignedRebook(String assignee, String taskId, String comment, Map<String, Object> bean, String prod, ActivitiCountersignedOpinionType opinionType, String files) {
        commandExecutor.execute(new CountersignedRebookCmd(assignee, taskId, comment, bean, prod, opinionType, files));
    }

    @Override
    public void countersignedChangePerson(String assignee, String prod, Map<String, Object> bean, String taskId, String assigneeJson) {
        commandExecutor.execute(new CountersignedChangePersonCmd(assignee, prod, bean, taskId, assigneeJson));
    }

    @Override
    public void taskAdjustingPersonnel(String assignee, String prod, String taskId) {
        commandExecutor.execute(new TaskAdjustingPersonnelCmd(assignee, prod, taskId));
    }

    @Override
    public List<String> getCounterSignStaff(String taskId) {
        return commandExecutor.execute(new CounterFindSignStaffCmd(taskId));
    }

    @Override
    public String getRootExecutionIdByTaskId(String taskId) {
        return commandExecutor.execute(new GetRootExecutionIdByTaskIdCmd(taskId));
    }

    @Override
    public List<TaskEntity> getTaskIdByRootExecutionId(String rootExecutionId) {
        return commandExecutor.execute(new GetTaskIdByRootExecutionIdCmd(rootExecutionId));
    }

    @Override
    public Map<String, Object> getCounterSignerOperationalUsers(String taskId) {
        return commandExecutor.execute(new GetCounterSignerOperationalUsersCmd(taskId));
    }

    @Override
    public Object getAssignment(Boolean addOwn, Boolean multiple, String pdid, KaiteBaseUserTask kaiteBaseUserTask, String taskId,
                                String prod, Map<String, Object> bean, FlowNode flowNode, String operationId, String businessKey) {
        return commandExecutor.execute(new GetAssignmentCmd(addOwn, multiple, pdid, kaiteBaseUserTask, taskId, prod, bean, flowNode, operationId, businessKey));
    }

    @Override
    public String getUserNameByUserId(String userId) {
        return commandExecutor.execute(new FindUserInfoByUserIdCmd(userId));
    }

    @Override
    public Boolean assignerContainsCurrentUser(String pdid, KaiteBaseUserTask kaiteBaseUserTask, String taskId,
                                               String prod, Map<String, Object> bean, String businessKey) {
        return (Boolean) commandExecutor.execute(new FindAssignerContainsCurrentUserCmd(pdid, kaiteBaseUserTask, taskId, prod, bean, businessKey));
    }

    @Override
    public Object getCirculatedAssignment(String taskId, String prod, Map<String, Object> bean) {
        return commandExecutor.execute(new GetCirculatedAssignmentCmd(taskId, prod, bean));
    }

    @Override
    public void multipleCountersign(String assignee, String taskId, String comment, String files) {
        commandExecutor.execute(new CountersignCmd(assignee, taskId, comment, files));
    }

    @Override
    public void multipleSignBack(String taskId, String prod, Map<String, Object> bean, String comment, String beanId, String signBackId, String files) {
        commandExecutor.execute(new SignBackCmd(taskId, prod, bean, comment, beanId, signBackId, files));
    }

    @Override
    public void staging(String taskId, String comment, String files) {
        commandExecutor.execute(new StagingCmd(taskId, comment, files));
    }

    @Override
    public List<JSONObject> getRunNodes(String piid) {
        return commandExecutor.execute(new GetRunNodesCmd(piid));
    }

    @Override
    public List<JSONObject> getAllNode(String piid) {
        return commandExecutor.execute(new GetAllNodeCmd(piid));
    }

    @Override
    public Void adjustRunningNode(String piid, String currentNodeId, String toNodeId, Map<String, Object> bean, String prod) {
        return commandExecutor.execute(new AdjustRunningNodeCmd(piid, currentNodeId, toNodeId, bean, prod));
    }

}
