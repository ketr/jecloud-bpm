/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence;

import com.je.bpm.engine.delegate.event.ActivitiEventDispatcher;
import com.je.bpm.engine.impl.asyncexecutor.AsyncExecutor;
import com.je.bpm.engine.impl.asyncexecutor.JobManager;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.history.HistoryManager;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.interceptor.CommandExecutor;
import com.je.bpm.engine.impl.persistence.entity.*;
import com.je.bpm.engine.runtime.Clock;

public abstract class AbstractManager {

    protected ProcessEngineConfigurationImpl processEngineConfiguration;

    public AbstractManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
        this.processEngineConfiguration = processEngineConfiguration;
    }

    // Command scoped

    protected CommandContext getCommandContext() {
        return Context.getCommandContext();
    }

    protected <T> T getSession(Class<T> sessionClass) {
        return getCommandContext().getSession(sessionClass);
    }

    // Engine scoped

    protected ProcessEngineConfigurationImpl getProcessEngineConfiguration() {
        return processEngineConfiguration;
    }

    protected CommandExecutor getCommandExecutor() {
        return getProcessEngineConfiguration().getCommandExecutor();
    }

    protected Clock getClock() {
        return getProcessEngineConfiguration().getClock();
    }

    protected AsyncExecutor getAsyncExecutor() {
        return getProcessEngineConfiguration().getAsyncExecutor();
    }

    protected ActivitiEventDispatcher getEventDispatcher() {
        return getProcessEngineConfiguration().getEventDispatcher();
    }

    protected HistoryManager getHistoryManager() {
        return getProcessEngineConfiguration().getHistoryManager();
    }

    protected JobManager getJobManager() {
        return getProcessEngineConfiguration().getJobManager();
    }

    protected DeploymentEntityManager getDeploymentEntityManager() {
        return getProcessEngineConfiguration().getDeploymentEntityManager();
    }

    protected ResourceEntityManager getResourceEntityManager() {
        return getProcessEngineConfiguration().getResourceEntityManager();
    }

    protected CsResourceEntityManager getCsResourceEntityManager() {
        return getProcessEngineConfiguration().getCsResourceEntityManager();
    }

    protected ByteArrayEntityManager getByteArrayEntityManager() {
        return getProcessEngineConfiguration().getByteArrayEntityManager();
    }

    protected CsByteArrayEntityManager getCsByteArrayEntityManager() {
        return getProcessEngineConfiguration().getCsByteArrayEntityManager();
    }

    protected ProcessDefinitionEntityManager getProcessDefinitionEntityManager() {
        return getProcessEngineConfiguration().getProcessDefinitionEntityManager();
    }

    protected ProcessDefinitionInfoEntityManager getProcessDefinitionInfoEntityManager() {
        return getProcessEngineConfiguration().getProcessDefinitionInfoEntityManager();
    }

    protected ModelEntityManager getModelEntityManager() {
        return getProcessEngineConfiguration().getModelEntityManager();
    }

    protected ExecutionEntityManager getExecutionEntityManager() {
        return getProcessEngineConfiguration().getExecutionEntityManager();
    }

    protected TaskEntityManager getTaskEntityManager() {
        return getProcessEngineConfiguration().getTaskEntityManager();
    }

    protected IdentityLinkEntityManager getIdentityLinkEntityManager() {
        return getProcessEngineConfiguration().getIdentityLinkEntityManager();
    }

    protected EventSubscriptionEntityManager getEventSubscriptionEntityManager() {
        return getProcessEngineConfiguration().getEventSubscriptionEntityManager();
    }

    protected VariableInstanceEntityManager getVariableInstanceEntityManager() {
        return getProcessEngineConfiguration().getVariableInstanceEntityManager();
    }

    protected JobEntityManager getJobEntityManager() {
        return getProcessEngineConfiguration().getJobEntityManager();
    }

    protected TimerJobEntityManager getTimerJobEntityManager() {
        return getProcessEngineConfiguration().getTimerJobEntityManager();
    }

    protected SuspendedJobEntityManager getSuspendedJobEntityManager() {
        return getProcessEngineConfiguration().getSuspendedJobEntityManager();
    }

    protected DeadLetterJobEntityManager getDeadLetterJobEntityManager() {
        return getProcessEngineConfiguration().getDeadLetterJobEntityManager();
    }

    protected HistoricProcessInstanceEntityManager getHistoricProcessInstanceEntityManager() {
        return getProcessEngineConfiguration().getHistoricProcessInstanceEntityManager();
    }

    protected HistoricDetailEntityManager getHistoricDetailEntityManager() {
        return getProcessEngineConfiguration().getHistoricDetailEntityManager();
    }

    protected HistoricActivityInstanceEntityManager getHistoricActivityInstanceEntityManager() {
        return getProcessEngineConfiguration().getHistoricActivityInstanceEntityManager();
    }

    protected HistoricVariableInstanceEntityManager getHistoricVariableInstanceEntityManager() {
        return getProcessEngineConfiguration().getHistoricVariableInstanceEntityManager();
    }

    protected HistoricTaskInstanceEntityManager getHistoricTaskInstanceEntityManager() {
        return getProcessEngineConfiguration().getHistoricTaskInstanceEntityManager();
    }

    protected HistoricIdentityLinkEntityManager getHistoricIdentityLinkEntityManager() {
        return getProcessEngineConfiguration().getHistoricIdentityLinkEntityManager();
    }

    protected AttachmentEntityManager getAttachmentEntityManager() {
        return getProcessEngineConfiguration().getAttachmentEntityManager();
    }

    protected CommentEntityManager getCommentEntityManager() {
        return getProcessEngineConfiguration().getCommentEntityManager();
    }
}
