/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data;

import com.je.bpm.engine.impl.ExecutionQueryImpl;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.ProcessInstanceQueryImpl;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.runtime.Execution;
import com.je.bpm.engine.runtime.ProcessInstance;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;


public interface ExecutionDataManager extends DataManager<ExecutionEntity> {

    ExecutionEntity findSubProcessInstanceBySuperExecutionId(final String superExecutionId);

    List<ExecutionEntity> findChildExecutionsByParentExecutionId(final String parentExecutionId);

    List<ExecutionEntity> findChildExecutionsByProcessInstanceId(final String processInstanceId);

    List<ExecutionEntity> findExecutionsByParentExecutionAndActivityIds(final String parentExecutionId, final Collection<String> activityIds);

    long findExecutionCountByQueryCriteria(ExecutionQueryImpl executionQuery);

    List<ExecutionEntity> findExecutionsByQueryCriteria(ExecutionQueryImpl executionQuery, Page page);

    long findProcessInstanceCountByQueryCriteria(ProcessInstanceQueryImpl executionQuery);

    List<ProcessInstance> findProcessInstanceByQueryCriteria(ProcessInstanceQueryImpl executionQuery);

    List<ExecutionEntity> findExecutionsByRootProcessInstanceId(String rootProcessInstanceId);

    List<ExecutionEntity> findExecutionsByProcessInstanceId(String processInstanceId);

    List<ProcessInstance> findProcessInstanceAndVariablesByQueryCriteria(ProcessInstanceQueryImpl executionQuery);

    Collection<ExecutionEntity> findInactiveExecutionsByProcessInstanceId(final String processInstanceId);

    Collection<ExecutionEntity> findInactiveExecutionsByActivityIdAndProcessInstanceId(final String activityId, final String processInstanceId);

    List<String> findProcessInstanceIdsByProcessDefinitionId(String processDefinitionId);

    List<Execution> findExecutionsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults);

    List<ProcessInstance> findProcessInstanceByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults);

    long findExecutionCountByNativeQuery(Map<String, Object> parameterMap);

    void updateExecutionTenantIdForDeployment(String deploymentId, String newTenantId);

    void updateProcessInstanceLockTime(String processInstanceId, Date lockDate, Date expirationTime);

    void updateAllExecutionRelatedEntityCountFlags(boolean newValue);

    void clearProcessInstanceLockTime(String processInstanceId);

}
