/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.DismissNodeInfo;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.config.task.TaskDismissConfigImpl;
import com.je.bpm.core.model.task.*;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.ActivitiObjectNotFoundException;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.history.HistoricTaskInstance;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.GetTakeNodeNameUtil;
import com.je.bpm.engine.task.Task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取驳回任务节点信息
 */
public class FindDismissElementCmd implements Command<List<DismissNodeInfo>>, Serializable {

    private static final long serialVersionUID = 1L;
    protected String executionId;
    protected String taskId;
    protected Map<String, Object> bean;

    public FindDismissElementCmd(String executionId, String taskId, Map<String, Object> bean) {
        this.executionId = executionId;
        this.taskId = taskId;
        this.bean = bean;
    }

    @Override
    public List<DismissNodeInfo> execute(CommandContext commandContext) {
        if (taskId == null) {
            throw new ActivitiIllegalArgumentException("taskId is null");
        }
        TaskEntity task = commandContext.getTaskEntityManager().findById(taskId);
        if (task == null) {
            throw new ActivitiObjectNotFoundException("Cannot find task with id " + taskId, Task.class);
        }
        if (task.isSuspended()) {
            throw new ActivitiException("this task is suspended");
        }
        return findNextElement(commandContext, task);
    }

    private List<DismissNodeInfo> findNextElement(CommandContext commandContext, TaskEntity task) {
        List<DismissNodeInfo> dismissNodeInfos = new ArrayList<>();
        RepositoryService repositoryService = commandContext.getProcessEngineConfiguration().getRepositoryService();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId(), task.getProcessInstanceId(),task.getBusinessKey());
        FlowElement taskFlowElement = bpmnModel.getFlowElement(task.getTaskDefinitionKey());
        if (taskFlowElement == null) {
            throw new ActivitiException(String.format("Can't find task %s definiton from the process.", task.getName()));
        }
        KaiteBaseUserTask kaiteTask = (KaiteBaseUserTask) taskFlowElement;
        TaskDismissConfigImpl taskDismissConfig = kaiteTask.getTaskDismissConfig();
        if (!taskDismissConfig.isEnable()) {
            return dismissNodeInfos;
        }
        String id = taskDismissConfig.getDismissTaskId();
        if (Strings.isNullOrEmpty(id)) {
            return dismissNodeInfos;
        }
        //查找已处理节点信息
        List<HistoricTaskInstance> historicTaskInstanceList = commandContext.getProcessEngineConfiguration().getHistoryService()
                .createHistoricTaskInstanceQuery()
                .processInstanceId(task.getProcessInstanceId())
                .orderByTaskCreateTime().desc()
                .list();
        Map<String, DismissNodeInfo> dismissNodeInfoMap = new HashMap<>();
        for (HistoricTaskInstance historicTaskInstance : historicTaskInstanceList) {
            if (!id.contains(historicTaskInstance.getTaskDefinitionKey())) {
                continue;
            }
            DismissNodeInfo dismissNodeInfo = new DismissNodeInfo();
            dismissNodeInfo.setId(historicTaskInstance.getTaskDefinitionKey());
            if (Strings.isNullOrEmpty(historicTaskInstance.getName())) {
                FlowElement flowElement = bpmnModel.getFlowElement(historicTaskInstance.getTaskDefinitionKey());
                if (Strings.isNullOrEmpty(flowElement.getName())) {
                    String takeNodeName = GetTakeNodeNameUtil.builder().getTakeNodeName(flowElement);
                    dismissNodeInfo.setName(takeNodeName);
                }else {
                    dismissNodeInfo.setName(flowElement.getName());
                }
            } else {
                dismissNodeInfo.setName(historicTaskInstance.getName());
            }
            dismissNodeInfoMap.put(historicTaskInstance.getTaskDefinitionKey(), dismissNodeInfo);
        }

        for (String key : dismissNodeInfoMap.keySet()) {
            dismissNodeInfos.add(dismissNodeInfoMap.get(key));
        }

        return dismissNodeInfos;
    }

}
