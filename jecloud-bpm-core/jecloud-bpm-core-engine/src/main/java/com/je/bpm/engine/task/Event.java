/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.task;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Exposes twitter-like feeds for tasks and process instances.
 *
 * @see {@link com.je.bpm.engine.TaskService#getTaskEvents(String)
 */
public interface Event extends Serializable {

    /**
     * A user identity link was added with following message parts: [0] userId [1] identity link type (aka role)
     */
    String ACTION_ADD_USER_LINK = "AddUserLink";

    /**
     * A user identity link was added with following message parts: [0] userId [1] identity link type (aka role)
     */
    String ACTION_DELETE_USER_LINK = "DeleteUserLink";

    /**
     * A group identity link was added with following message parts: [0] groupId [1] identity link type (aka role)
     */
    String ACTION_ADD_GROUP_LINK = "AddGroupLink";

    /**
     * A group identity link was added with following message parts: [0] groupId [1] identity link type (aka role)
     */
    String ACTION_DELETE_GROUP_LINK = "DeleteGroupLink";

    /**
     * An user comment was added with the short version of the comment as message.
     */
    String ACTION_ADD_COMMENT = "AddComment";

    /**
     * An attachment was added with the attachment name as message.
     */
    String ACTION_ADD_ATTACHMENT = "AddAttachment";

    /**
     * An attachment was deleted with the attachment name as message.
     */
    String ACTION_DELETE_ATTACHMENT = "DeleteAttachment";

    /**
     * Unique identifier for this event
     */
    String getId();

    /**
     * Indicates the type of action and also indicates the meaning of the parts as exposed in {@link #getMessageParts()}
     */
    String getAction();

    /**
     * The meaning of the message parts is defined by the action as you can find in {@link #getAction()}
     */
    List<String> getMessageParts();

    /**
     * The message that can be used in case this action only has a single message part.
     */
    String getMessage();

    /**
     * reference to the user that made the comment
     */
    String getUserId();

    /**
     * time and date when the user made the comment
     */
    Date getTime();

    /**
     * reference to the task on which this comment was made
     */
    String getTaskId();

    /**
     * reference to the process instance on which this comment was made
     */
    String getProcessInstanceId();

}
