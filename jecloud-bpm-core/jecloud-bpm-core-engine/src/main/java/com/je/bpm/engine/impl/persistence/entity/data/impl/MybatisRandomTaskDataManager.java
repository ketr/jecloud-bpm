/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.CachedEntityMatcher;
import com.je.bpm.engine.impl.persistence.entity.IdentityLinkEntity;
import com.je.bpm.engine.impl.persistence.entity.RandomTaskEntity;
import com.je.bpm.engine.impl.persistence.entity.RandomTaskEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.data.AbstractDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.RandomTaskDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.impl.cachematcher.IdentityLinksByProcInstMatcher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @ClassName 随机节点
 */
public class MybatisRandomTaskDataManager extends AbstractDataManager<RandomTaskEntity> implements RandomTaskDataManager {

    protected CachedEntityMatcher<IdentityLinkEntity> identityLinkByProcessInstanceMatcher = new IdentityLinksByProcInstMatcher();

    public MybatisRandomTaskDataManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public Class<? extends RandomTaskEntity> getManagedEntityClass() {
        return RandomTaskEntityImpl.class;
    }

    @Override
    public RandomTaskEntity create() {
        return new RandomTaskEntityImpl();
    }

    @Override
    public List<RandomTaskEntity> findRandomTaskByTaskId(String taskId) {
        return getDbSqlSession().selectList("selectRandomByTask", taskId);
    }

    @Override
    public List<RandomTaskEntity> findRandomTaskByPiid(String piid) {
        return getDbSqlSession().selectList("selectRandomByPiid", piid);
    }

    @Override
    public List<RandomTaskEntity> findRandomTaskByPiidOderByCount(String piid) {
        return getDbSqlSession().selectList("selectRandomByPiidOrderByCount", piid);
    }

    @Override
    public List<RandomTaskEntity> findRandomTaskByPdidOderByCount(String pdid) {
        return getDbSqlSession().selectList("selectRandomByPdidOrderByCount", pdid);
    }

    @Override
    public List<RandomTaskEntity> selectRandomByProcessKeyAndFlowElementIdOrderByCount(String processKey,String flowElementId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("processKey", processKey);
        params.put("flowElementId", flowElementId);
        return getDbSqlSession().selectList("selectRandomByProcessKeyAndFlowElementIdOrderByCount",params);
    }

    @Override
    public void deleteRandomByProcessKey(String processKey) {
        getDbSqlSession().delete("deleteRandomByProcessKey", processKey, RandomTaskEntityImpl.class);
    }
}
