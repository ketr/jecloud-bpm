/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.deployer;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.engine.impl.bpmn.parser.BpmnParse;
import com.je.bpm.engine.impl.persistence.entity.DeploymentEntity;
import com.je.bpm.engine.impl.persistence.entity.ProcessDefinitionEntity;
import com.je.bpm.engine.impl.persistence.entity.ResourceEntity;

import java.util.List;
import java.util.Map;

/**
 * An intermediate representation of a DeploymentEntity which keeps track of all of the entity's
 * ProcessDefinitionEntities and resources, and BPMN parses, models, and processes associated
 * with each ProcessDefinitionEntity - all produced by parsing the deployment.
 * <p>
 * The ProcessDefinitionEntities are expected to be "not fully set-up" - they may be inconsistent with the
 * DeploymentEntity and/or the persisted versions, and if the deployment is new, they will not yet be persisted.
 */
public class ParsedDeployment {

    protected DeploymentEntity deploymentEntity;

    protected List<ProcessDefinitionEntity> processDefinitions;
    protected Map<ProcessDefinitionEntity, BpmnParse> mapProcessDefinitionsToParses;
    protected Map<ProcessDefinitionEntity, ResourceEntity> mapProcessDefinitionsToResources;

    public ParsedDeployment(
            DeploymentEntity entity, List<ProcessDefinitionEntity> processDefinitions,
            Map<ProcessDefinitionEntity, BpmnParse> mapProcessDefinitionsToParses,
            Map<ProcessDefinitionEntity, ResourceEntity> mapProcessDefinitionsToResources) {
        this.deploymentEntity = entity;
        this.processDefinitions = processDefinitions;
        this.mapProcessDefinitionsToParses = mapProcessDefinitionsToParses;
        this.mapProcessDefinitionsToResources = mapProcessDefinitionsToResources;
    }

    public DeploymentEntity getDeployment() {
        return deploymentEntity;
    }

    public List<ProcessDefinitionEntity> getAllProcessDefinitions() {
        return processDefinitions;
    }

    public ResourceEntity getResourceForProcessDefinition(ProcessDefinitionEntity processDefinition) {
        return mapProcessDefinitionsToResources.get(processDefinition);
    }

    public BpmnParse getBpmnParseForProcessDefinition(ProcessDefinitionEntity processDefinition) {
        return mapProcessDefinitionsToParses.get(processDefinition);
    }

    public BpmnModel getBpmnModelForProcessDefinition(ProcessDefinitionEntity processDefinition) {
        BpmnParse parse = getBpmnParseForProcessDefinition(processDefinition);

        return (parse == null ? null : parse.getBpmnModel());
    }

    public Process getProcessModelForProcessDefinition(ProcessDefinitionEntity processDefinition) {
        BpmnModel model = getBpmnModelForProcessDefinition(processDefinition);

        return (model == null ? null : model.getProcessById(processDefinition.getKey()));
    }

}
