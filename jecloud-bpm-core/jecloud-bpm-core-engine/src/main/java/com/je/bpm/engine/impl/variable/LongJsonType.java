/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.variable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.engine.ActivitiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.nio.charset.StandardCharsets;

public class LongJsonType extends SerializableType {

    private static final Logger logger = LoggerFactory.getLogger(LongJsonType.class);
    public static final String LONG_JSON = "longJson";

    private final int minLength;
    private ObjectMapper objectMapper;
    private boolean serializePOJOsInVariablesToJson;
    private JsonTypeConverter jsonTypeConverter;

    public LongJsonType(int minLength, ObjectMapper objectMapper, boolean serializePOJOsInVariablesToJson, JsonTypeConverter jsonTypeConverter) {
        this.minLength = minLength;
        this.objectMapper = objectMapper;
        this.serializePOJOsInVariablesToJson = serializePOJOsInVariablesToJson;
        this.jsonTypeConverter = jsonTypeConverter;
    }

    @Override
    public String getTypeName() {
        return LONG_JSON;
    }

    @Override
    public boolean isAbleToStore(Object value) {
        if (value == null) {
            return true;
        }
        if (JsonNode.class.isAssignableFrom(value.getClass()) ||
                (objectMapper.canSerialize(value.getClass()) && serializePOJOsInVariablesToJson)) {
            try {
                return objectMapper.writeValueAsString(value).length() >= minLength;
            } catch (JsonProcessingException e) {
                logger.error("Error writing json variable of type " + value.getClass(), e);
            }
        }

        return false;
    }

    @Override
    public byte[] serialize(Object value, ValueFields valueFields) {
        if (value == null) {
            return null;
        }
        String json = null;
        try {
            json = objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            logger.error("Error writing long json variable " + valueFields.getName(), e);
        }
        try {
            valueFields.setTextValue2(value.getClass().getName());
            return json.getBytes(StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new ActivitiException("Error getting bytes from json variable", e);
        }
    }

    @Override
    public Object deserialize(byte[] bytes, ValueFields valueFields) {
        Object jsonValue = null;
        try {
            jsonValue = jsonTypeConverter.convertToValue(objectMapper.readTree(bytes), valueFields);
        } catch (Exception e) {
            logger.error("Error reading json variable " + valueFields.getName(), e);
        }
        return jsonValue;
    }
}
