/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cfg;

/**

 */
public class PerformanceSettings {

  /**
   * Experimental setting: if true, whenever an execution is fetched from the data store,
   * the whole execution tree is fetched in the same roundtrip.
   *
   * Less roundtrips to the database outweighs doing many, smaller fetches and often
   * multiple executions from the same tree are needed anyway when executing process instances.
   */
  protected boolean enableEagerExecutionTreeFetching;

  /**
   * Experimental setting: keeps a count on each execution that holds
   * how many variables, jobs, tasks, event subscriptions, etc. the execution has.
   *
   * This makes the delete more performant as a query is not needed anymore
   * to check if there is related data. However, maintaining the count
   * does mean more updates to the execution and potentially more optimistic locking opportunities.
   * Typically keeping the counts lead to better performance as deletes are a large part of the
   * execution tree maintenance.
   */
  protected boolean enableExecutionRelationshipCounts;

  /**
   * If false, no check will be done on boot.
   */
  protected boolean validateExecutionRelationshipCountConfigOnBoot = true;

  /**
   * Experimental setting: in certain places in the engine (execution/process instance/historic process instance/
   * tasks/data objects) localization is supported. When this setting is false,
   * localization is completely disabled, which gives a small performance gain.
   */
  protected boolean enableLocalization = true;

  public boolean isEnableEagerExecutionTreeFetching() {
    return enableEagerExecutionTreeFetching;
  }

  public void setEnableEagerExecutionTreeFetching(boolean enableEagerExecutionTreeFetching) {
    this.enableEagerExecutionTreeFetching = enableEagerExecutionTreeFetching;
  }

  public boolean isEnableExecutionRelationshipCounts() {
    return enableExecutionRelationshipCounts;
  }

  public void setEnableExecutionRelationshipCounts(boolean enableExecutionRelationshipCounts) {
    this.enableExecutionRelationshipCounts = enableExecutionRelationshipCounts;
  }

  public boolean isValidateExecutionRelationshipCountConfigOnBoot() {
    return validateExecutionRelationshipCountConfigOnBoot;
  }

  public void setValidateExecutionRelationshipCountConfigOnBoot(boolean validateExecutionRelationshipCountConfigOnBoot) {
    this.validateExecutionRelationshipCountConfigOnBoot = validateExecutionRelationshipCountConfigOnBoot;
  }

  public boolean isEnableLocalization() {
    return enableLocalization;
  }

  public void setEnableLocalization(boolean enableLocalization) {
    this.enableLocalization = enableLocalization;
  }

}
