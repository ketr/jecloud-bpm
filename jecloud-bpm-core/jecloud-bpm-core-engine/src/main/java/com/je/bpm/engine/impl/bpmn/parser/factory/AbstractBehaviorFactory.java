/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.parser.factory;

import com.je.bpm.core.model.FieldExtension;
import com.je.bpm.engine.delegate.Expression;
import com.je.bpm.engine.impl.bpmn.parser.FieldDeclaration;
import com.je.bpm.engine.impl.delegate.BpmnMessagePayloadMappingProviderFactory;
import com.je.bpm.engine.impl.delegate.MessagePayloadMappingProviderFactory;
import com.je.bpm.engine.impl.delegate.ThrowMessageDelegateFactory;
import com.je.bpm.engine.impl.el.ExpressionManager;
import com.je.bpm.engine.impl.el.FixedValue;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBehaviorFactory {

    protected ExpressionManager expressionManager;
    private ThrowMessageDelegateFactory throwMessageDelegateFactory = new ThrowMessageDelegateFactory() {
    };
    private MessagePayloadMappingProviderFactory messagePayloadMappingProviderFactory = new BpmnMessagePayloadMappingProviderFactory();
    private MessageExecutionContextFactory messageExecutionContextFactory = new DefaultMessageExecutionContextFactory();

    public List<FieldDeclaration> createFieldDeclarations(List<FieldExtension> fieldList) {
        List<FieldDeclaration> fieldDeclarations = new ArrayList<FieldDeclaration>();

        for (FieldExtension fieldExtension : fieldList) {
            FieldDeclaration fieldDeclaration = null;
            if (StringUtils.isNotEmpty(fieldExtension.getExpression())) {
                fieldDeclaration = new FieldDeclaration(fieldExtension.getFieldName(), Expression.class.getName(), expressionManager.createExpression(fieldExtension.getExpression()));
            } else {
                fieldDeclaration = new FieldDeclaration(fieldExtension.getFieldName(), Expression.class.getName(), new FixedValue(fieldExtension.getStringValue()));
            }

            fieldDeclarations.add(fieldDeclaration);
        }
        return fieldDeclarations;
    }

    public ExpressionManager getExpressionManager() {
        return expressionManager;
    }

    public void setExpressionManager(ExpressionManager expressionManager) {
        this.expressionManager = expressionManager;
    }

    public ThrowMessageDelegateFactory getThrowMessageDelegateFactory() {
        return throwMessageDelegateFactory;
    }

    public void setThrowMessageDelegateFactory(ThrowMessageDelegateFactory throwMessageDelegateFactory) {
        this.throwMessageDelegateFactory = throwMessageDelegateFactory;
    }

    public MessagePayloadMappingProviderFactory getMessagePayloadMappingProviderFactory() {
        return messagePayloadMappingProviderFactory;
    }

    public void setMessagePayloadMappingProviderFactory(MessagePayloadMappingProviderFactory messagePayloadMappingProviderFactory) {
        this.messagePayloadMappingProviderFactory = messagePayloadMappingProviderFactory;
    }


    public MessageExecutionContextFactory getMessageExecutionContextFactory() {
        return messageExecutionContextFactory;
    }


    public void setMessageExecutionContextFactory(MessageExecutionContextFactory messageExecutionContextFactory) {
        this.messageExecutionContextFactory = messageExecutionContextFactory;
    }

}
