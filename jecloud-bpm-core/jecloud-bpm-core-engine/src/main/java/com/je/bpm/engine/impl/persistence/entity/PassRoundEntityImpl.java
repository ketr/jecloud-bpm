/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.db.BulkDeleteable;
import com.je.bpm.engine.task.PassRoundReadState;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 传阅实体定义实现
 */
public class PassRoundEntityImpl extends AbstractEntity implements PassRoundEntity, Serializable, BulkDeleteable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String processInstanceId;
    private String processDefinitionId;
    private String taskId;
    private String taskName;
    private String from;
    private String to;
    private String content;
    private Date createTime;
    private PassRoundReadState readState;
    private String readStateString;
    private Date readTime;
    protected boolean forcedUpdate;

    @Override
    public void forceUpdate() {
        this.forcedUpdate = true;
    }

    @Override
    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    @Override
    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    @Override
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    @Override
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    @Override
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String getTaskName() {
        return taskName;
    }

    @Override
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public void setFrom(String from) {
        this.from = from;
    }

    @Override
    public String getTo() {
        return to;
    }

    @Override
    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public void setReadState(PassRoundReadState passRoundReadState) {
        this.readState = passRoundReadState;
        this.readStateString = passRoundReadState.toString();
    }

    @Override
    public String getReadStateString() {
        return readStateString;
    }

    @Override
    public PassRoundReadState getReadState() {
        return readState;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public Date getReadTime() {
        return readTime;
    }

    @Override
    public void setReadTime(Date readTime) {
        this.readTime = readTime;
    }

    @Override
    public Object getPersistentState() {
        Map<String, Object> persistentState = new HashMap();
        persistentState.put("taskId", this.taskId);
        persistentState.put("taskName", this.taskName);
        persistentState.put("from", this.from);
        persistentState.put("to", this.to);
        persistentState.put("content", this.content);
        persistentState.put("readTime", this.readTime);
        if (processDefinitionId != null) {
            persistentState.put("processDefinitionId", this.processDefinitionId);
        }
        if (processInstanceId != null) {
            persistentState.put("processInstanceId", this.processInstanceId);
        }
        if (createTime != null) {
            persistentState.put("createTime", this.createTime);
        }
        if (readState != null) {
            persistentState.put("readState", this.readState);
        }
        if (forcedUpdate) {
            persistentState.put("forcedUpdate", Boolean.TRUE);
        }
        return persistentState;
    }

}
