/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.helper;

import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.delegate.event.ActivitiEvent;
import com.je.bpm.engine.delegate.event.ActivitiEventListener;
import com.je.bpm.engine.impl.util.ReflectUtil;

/**
 * An {@link ActivitiEventListener} implementation which uses a classname to create a delegate {@link ActivitiEventListener} instance to use for event notification. <br>
 * <br>
 * <p>
 * In case an entityClass was passed in the constructor, only events that are {@link com.je.bpm.engine.delegate.event.ActivitiEntityEvent}'s that target an entity of the given type, are dispatched to the delegate.
 */
public class DelegateActivitiEventListener extends BaseDelegateEventListener {

    protected String className;
    protected ActivitiEventListener delegateInstance;
    protected boolean failOnException = false;

    public DelegateActivitiEventListener(String className, Class<?> entityClass) {
        this.className = className;
        setEntityClass(entityClass);
    }

    @Override
    public void onEvent(ActivitiEvent event) {
        if (isValidEvent(event)) {
            getDelegateInstance().onEvent(event);
        }
    }

    @Override
    public boolean isFailOnException() {
        if (delegateInstance != null) {
            return delegateInstance.isFailOnException();
        }
        return failOnException;
    }

    protected ActivitiEventListener getDelegateInstance() {
        if (delegateInstance == null) {
            Object instance = ReflectUtil.instantiate(className);
            if (instance instanceof ActivitiEventListener) {
                delegateInstance = (ActivitiEventListener) instance;
            } else {
                // Force failing of the listener invocation, since the delegate
                // cannot be created
                failOnException = true;
                throw new ActivitiIllegalArgumentException("Class " + className + " does not implement " + ActivitiEventListener.class.getName());
            }
        }
        return delegateInstance;
    }
}
