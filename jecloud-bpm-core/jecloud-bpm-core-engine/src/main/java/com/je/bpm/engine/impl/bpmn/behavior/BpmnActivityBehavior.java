/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.behavior;

import com.je.bpm.engine.ActivitiEngineAgenda;
import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.JobEntity;
import com.je.bpm.engine.impl.persistence.entity.TimerJobEntity;
import java.io.Serializable;
import java.util.List;
import static com.je.bpm.engine.impl.bpmn.behavior.MappingExecutionContext.buildMappingExecutionContext;

/**
 * Helper class for implementing BPMN 2.0 activities, offering convenience methods specific to BPMN 2.0.
 * 用于实现BPMN 2.0活动的帮助器类，提供特定于BPMN 2.0的方便方法。
 * This class can be used by inheritance or aggregation.
 * 此类可由继承或聚合使用。
 */
public class BpmnActivityBehavior implements Serializable {

    private static final long serialVersionUID = 1L;

    private VariablesCalculator variablesCalculator = new NoneVariablesCalculator();



    /**
     * Performs the default outgoing BPMN 2.0 behavior, which is having parallel paths of executions for the outgoing sequence flow.
     * 执行默认的传出BPMN 2.0行为，该行为具有传出序列流的并行执行路径。
     * More precisely: every sequence flow that has a condition which evaluates to true (or which doesn't have a condition), is selected for continuation of the process instance. If multiple sequencer
     * flow are selected, multiple, parallel paths of executions are created.
     */
    public void performDefaultOutgoingBehavior(ExecutionEntity activityExecution) {
        performOutgoingBehavior(activityExecution, true, false);
    }

    /**
     * Performs the default outgoing BPMN 2.0 behavior (@see {@link #performDefaultOutgoingBehavior(ExecutionEntity)}), but without checking the conditions on the outgoing sequence flow.
     * 执行默认的传出BPMN 2.0行为（@see{@link#performDefaultOutgoingBehavior（ExecutionEntity）}），但不检查传出序列流上的条件。
     * This means that every outgoing sequence flow is selected for continuing the process instance, regardless of having a condition or not. In case of multiple outgoing sequence flow, multiple
     * parallel paths of executions will be created.
     * 这意味着，无论是否有条件，都会选择每个传出序列流来继续流程实例。在多个传出序列流的情况下，将创建多个并行执行路径。
     */
    public void performIgnoreConditionsOutgoingBehavior(ExecutionEntity activityExecution) {
        performOutgoingBehavior(activityExecution, false, false);
    }

    /**
     * Actual implementation of leaving an activity.
     * 离开活动的实际执行情况。
     * @param execution                      The current execution context
     * @param checkConditions                Whether or not to check conditions before determining whether or not to take a transition.
     * @param throwExceptionIfExecutionStuck If true, an {@link com.je.bpm.engine.ActivitiException} will be thrown in case no transition could be found to leave the activity.
     */
    protected void performOutgoingBehavior(ExecutionEntity execution, boolean checkConditions, boolean throwExceptionIfExecutionStuck) {
        //将变量传播到父级
        propagateVariablesToParent(execution);
        //计划采取传出序列流操作
        getAgenda().planTakeOutgoingSequenceFlowsOperation(execution,true);
    }

    /**
     * dispatch job canceled event for job associated with given execution entity
     * 与给定执行实体关联的作业的调度作业取消事件
     * @param activityExecution
     */
    protected void dispatchJobCanceledEvents(ExecutionEntity activityExecution) {
        if (activityExecution != null) {
            List<JobEntity> jobs = activityExecution.getJobs();
            for (JobEntity job : jobs) {
                if (Context.getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
                    Context.getProcessEngineConfiguration().getEventDispatcher().dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.JOB_CANCELED,job));
                }
            }

            List<TimerJobEntity> timerJobs = activityExecution.getTimerJobs();
            for (TimerJobEntity job : timerJobs) {
                if (Context.getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
                    Context.getProcessEngineConfiguration().getEventDispatcher().dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.JOB_CANCELED,job));
                }
            }
        }
    }

    protected ActivitiEngineAgenda getAgenda() {
        return Context.getAgenda();
    }

    private void propagateVariablesToParent(ExecutionEntity execution) {
        ExecutionEntity parentExecution = execution.getParent();
        if (parentExecution != null) {
            parentExecution.setVariables(variablesCalculator
                    .calculateOutPutVariables(buildMappingExecutionContext(execution), execution.getVariablesLocal()));
        }
    }

    public void setVariablesCalculator(VariablesCalculator variablesCalculator) {
        this.variablesCalculator = variablesCalculator;
    }
}
