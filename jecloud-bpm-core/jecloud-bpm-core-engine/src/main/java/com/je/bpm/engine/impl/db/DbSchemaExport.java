/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.db;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.*;

public class DbSchemaExport {

    public static void main(String[] args) throws Exception {
        if (args == null || args.length != 1) {
            System.err.println("Syntax: java -cp ... org.activiti.engine.impl.db.DbSchemaExport <path-to-properties-file> <path-to-export-file>");
            return;
        }
        File propertiesFile = new File(args[0]);
        if (!propertiesFile.exists()) {
            System.err.println("File '" + args[0] + "' doesn't exist \n" + "Syntax: java -cp ... org.activiti.engine.impl.db.DbSchemaExport <path-to-properties-file> <path-to-export-file>\n");
            return;
        }
        Properties properties = new Properties();
        properties.load(new FileInputStream(propertiesFile));

        String jdbcDriver = properties.getProperty("jdbc.driver");
        String jdbcUrl = properties.getProperty("jdbc.url");
        String jdbcUsername = properties.getProperty("jdbc.username");
        String jdbcPassword = properties.getProperty("jdbc.password");

        Class.forName(jdbcDriver);
        Connection connection = DriverManager.getConnection(jdbcUrl, jdbcUsername, jdbcPassword);
        try {
            DatabaseMetaData meta = connection.getMetaData();

            SortedSet<String> tableNames = new TreeSet<String>();
            ResultSet tables = meta.getTables(null, null, null, null);
            while (tables.next()) {
                String tableName = tables.getString(3);
                tableNames.add(tableName);
            }

            for (String tableName : tableNames) {
                Map<String, String> columnDescriptions = new HashMap<String, String>();
                ResultSet columns = meta.getColumns(null, null, tableName, null);
                while (columns.next()) {
                    String columnName = columns.getString(4);
                    String columnTypeAndSize = columns.getString(6) + " " + columns.getInt(7);
                    columnDescriptions.put(columnName, columnTypeAndSize);
                }

                for (String columnName : new TreeSet<String>(columnDescriptions.keySet())) {
                    System.out.println("  " + columnName + " " + columnDescriptions.get(columnName));
                }

                SortedSet<String> indexNames = new TreeSet<String>();
                ResultSet indexes = meta.getIndexInfo(null, null, tableName, false, true);
                while (indexes.next()) {
                    String indexName = indexes.getString(6);
                    indexNames.add(indexName);
                }
                for (String indexName : indexNames) {
                    System.out.println(indexName);
                }
                System.out.println();
            }

        } catch (Exception e) {
            e.printStackTrace();
            connection.close();
        }
    }
}
