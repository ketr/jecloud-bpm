/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.cfg;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;

/**
 * Implementations of this class can be plugged into a {@link ProcessEngineConfigurationImpl}. Such implementations can configure the engine in any way programmatically possible.
 * 此类的实现可以插入到{@link ProcessEngineConfigurationImpl}中。这样的实现可以以任何编程方式配置引擎
 */
public interface ProcessEngineConfigurator {

    /**
     * Called <b>before</b> any initialisation has been done. This can for example be useful to change configuration settings before anything that uses those properties is created.
     * <p>
     * Allows to tweak the process engine by passing the {@link ProcessEngineConfigurationImpl} which allows tweaking it programmatically.
     * <p>
     * An example is the jdbc url. When a {@link ProcessEngineConfigurator} instance wants to change it, it needs to do it in this method, or otherwise the datasource would already have been created
     * with the 'old' value for the jdbc url.
     */
    void beforeInit(ProcessEngineConfigurationImpl processEngineConfiguration);

    /**
     * Called when the engine boots up, before it is usable, but after the initialisation of internal objects is done.
     * <p>
     * Allows to tweak the process engine by passing the {@link ProcessEngineConfigurationImpl} which allows tweaking it programmatically.
     * <p>
     * An example is the ldap user/group manager, which is an addition to the engine. No default properties need to be overridden for this (otherwise the
     * {@link #beforeInit(ProcessEngineConfigurationImpl)} method should be used) so the logic contained in this method is executed after initialisation of the default objects.
     * <p>
     * Probably a better name would be 'afterInit' (cfr {@link #beforeInit(ProcessEngineConfigurationImpl)}), but not possible due to backwards compatibility.
     */
    void configure(ProcessEngineConfigurationImpl processEngineConfiguration);

    /**
     * When the {@link ProcessEngineConfigurator} instances are used, they are first ordered by this priority number (lowest to highest). If you have dependencies between
     * {@link ProcessEngineConfigurator} instances, use the priorities accordingly to order them as needed.
     */
    int getPriority();

}
