/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.history;

import com.je.bpm.engine.ActivitiIllegalArgumentException;

/**
 * Enum that contains all possible history-levels.
 */
public enum HistoryLevel {

    /**
     * 不保存任何的历史数据，因此，在流程执行过程中，这是最高效的
     */
    NONE("none"),
    /**
     * 级别高于none，保存流程实例与流程行为，其他数据不保存。
     */
    ACTIVITY("activity"),
    /**
     * 除activity级别会保存的数据外，还会保存全部的流程任务及其属性。audit为history的默认值
     */
    AUDIT("audit"),
    /**
     * 保存历史数据的最高级别，除了会保存audit级别的数据外，还会保存其他全部流程相关的细节数据，包括一些流程参数等。
     */
    FULL("full");

    private String key;

    private HistoryLevel(String key) {
        this.key = key;
    }

    /**
     * @param key string representation of level
     * @return {@link HistoryLevel} for the given key
     * @throws com.je.bpm.engine.ActivitiException when passed in key doesn't correspond to existing level
     */
    public static HistoryLevel getHistoryLevelForKey(String key) {
        for (HistoryLevel level : values()) {
            if (level.key.equals(key)) {
                return level;
            }
        }
        throw new ActivitiIllegalArgumentException("Illegal value for history-level: " + key);
    }

    /**
     * String representation of this history-level.
     */
    public String getKey() {
        return key;
    }

    /**
     * Checks if the given level is the same as, or higher in order than the level this method is executed on.
     */
    public boolean isAtLeast(HistoryLevel level) {
        // Comparing enums actually compares the location of values declared in
        // the enum
        return this.compareTo(level) >= 0;
    }

}
