/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.history;

import com.je.bpm.engine.internal.Internal;

import java.util.Date;
import java.util.Map;

/**
 * A single execution of a whole process definition that is stored permanently.
 */
@Internal
public interface HistoricProcessInstance {

    /**
     * The process instance id (== as the id for the runtime {@link com.je.bpm.engine.runtime.ProcessInstance process instance}).
     */
    String getId();

    /**
     * The user provided unique reference to this process instance.
     */
    String getBusinessKey();

    /**
     * The process definition reference.
     */
    String getProcessDefinitionId();

    /**
     * The name of the process definition of the process instance.
     */
    String getProcessDefinitionName();

    /**
     * The key of the process definition of the process instance.
     */
    String getProcessDefinitionKey();

    /**
     * The version of the process definition of the process instance.
     */
    Integer getProcessDefinitionVersion();

    /**
     * The deployment id of the process definition of the process instance.
     */
    String getDeploymentId();

    /**
     * The time the process was started.
     */
    Date getStartTime();

    /**
     * The time the process was ended.
     */
    Date getEndTime();

    /**
     * The difference between {@link #getEndTime()} and {@link #getStartTime()} .
     */
    Long getDurationInMillis();

    /**
     * Reference to the activity in which this process instance ended. Note that a process instance can have multiple end events, in this case it might not be deterministic which activity id will be
     * referenced here. Use a {@link HistoricActivityInstanceQuery} instead to query for end events of the process instance (use the activityTYpe attribute)
     */
    String getEndActivityId();

    /**
     * The authenticated user that started this process instance.
     *
     * @see IdentityService#setAuthenticatedUserId(String)
     */
    String getStartUserId();

    /**
     * The start activity.
     */
    String getStartActivityId();

    /**
     * Obtains the reason for the process instance's deletion.
     */
    String getDeleteReason();

    /**
     * The process instance id of a potential super process instance or null if no super process instance exists
     */
    String getSuperProcessInstanceId();

    /**
     * The tenant identifier for the process instance.
     */
    String getTenantId();

    /**
     * The name for the process instance.
     */
    String getName();

    /**
     * The description for the process instance.
     */
    String getDescription();

    /**
     * Returns the process variables if requested in the process instance query
     */
    Map<String, Object> getProcessVariables();
}
