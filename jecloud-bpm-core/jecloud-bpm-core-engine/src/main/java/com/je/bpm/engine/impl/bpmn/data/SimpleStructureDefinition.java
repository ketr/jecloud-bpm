/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a simple in memory structure
 */
public class SimpleStructureDefinition implements FieldBaseStructureDefinition {

    protected String id;

    protected List<String> fieldNames;

    protected List<Class<?>> fieldTypes;

    public SimpleStructureDefinition(String id) {
        this.id = id;
        this.fieldNames = new ArrayList<String>();
        this.fieldTypes = new ArrayList<Class<?>>();
    }

    @Override
    public int getFieldSize() {
        return this.fieldNames.size();
    }

    @Override
    public String getId() {
        return this.id;
    }

    public void setFieldName(int index, String fieldName, Class<?> type) {
        this.growListToContain(index, this.fieldNames);
        this.growListToContain(index, this.fieldTypes);
        this.fieldNames.set(index, fieldName);
        this.fieldTypes.set(index, type);
    }

    private void growListToContain(int index, List<?> list) {
        if (!(list.size() - 1 >= index)) {
            for (int i = list.size(); i <= index; i++) {
                list.add(null);
            }
        }
    }

    @Override
    public String getFieldNameAt(int index) {
        return this.fieldNames.get(index);
    }

    @Override
    public Class<?> getFieldTypeAt(int index) {
        return this.fieldTypes.get(index);
    }

    @Override
    public StructureInstance createInstance() {
        return new FieldBaseStructureInstance(this);
    }
}
