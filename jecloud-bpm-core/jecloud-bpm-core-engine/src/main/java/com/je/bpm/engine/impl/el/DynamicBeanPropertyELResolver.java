/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.el;

import com.je.bpm.engine.impl.util.ReflectUtil;

import javax.el.ELContext;
import javax.el.ELException;
import javax.el.ELResolver;
import java.beans.FeatureDescriptor;
import java.util.Iterator;

/**
 * A {@link ELResolver} for dynamic bean properties
 */
public class DynamicBeanPropertyELResolver extends ELResolver {

    protected Class<?> subject;

    protected String readMethodName;

    protected String writeMethodName;

    protected boolean readOnly;

    public DynamicBeanPropertyELResolver(boolean readOnly, Class<?> subject, String readMethodName, String writeMethodName) {
        this.readOnly = readOnly;
        this.subject = subject;
        this.readMethodName = readMethodName;
        this.writeMethodName = writeMethodName;
    }

    public DynamicBeanPropertyELResolver(Class<?> subject, String readMethodName, String writeMethodName) {
        this(false, subject, readMethodName, writeMethodName);
    }

    @Override
    public Class<?> getCommonPropertyType(ELContext context, Object base) {
        if (this.subject.isInstance(base)) {
            return Object.class;
        } else {
            return null;
        }
    }

    @Override
    public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
        return null;
    }

    @Override
    public Class<?> getType(ELContext context, Object base, Object property) {
        if (base == null || this.getCommonPropertyType(context, base) == null) {
            return null;
        }

        context.setPropertyResolved(true);
        return Object.class;
    }

    @Override
    public Object getValue(ELContext context, Object base, Object property) {
        if (base == null || this.getCommonPropertyType(context, base) == null) {
            return null;
        }

        String propertyName = property.toString();
        try {
            Object value = ReflectUtil.invoke(base, this.readMethodName, new Object[]{propertyName});
            context.setPropertyResolved(true);
            return value;
        } catch (Exception e) {
            throw new ELException(e);
        }
    }

    @Override
    public boolean isReadOnly(ELContext context, Object base, Object property) {
        return this.readOnly;
    }

    @Override
    public void setValue(ELContext context, Object base, Object property, Object value) {
        if (base == null || this.getCommonPropertyType(context, base) == null) {
            return;
        }

        String propertyName = property.toString();
        try {
            ReflectUtil.invoke(base, this.writeMethodName, new Object[]{propertyName, value});
            context.setPropertyResolved(true);
        } catch (Exception e) {
            throw new ELException(e);
        }
    }
}
