/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.runtime;

import com.je.bpm.engine.internal.Internal;

import java.util.Date;
import java.util.Map;

/**
 * Represents one execution of a {@link com.je.bpm.engine.repository.ProcessDefinition}.
 */
@Internal
public interface ProcessInstance extends Execution {

    /**
     * The id of the process definition of the process instance.
     */
    String getProcessDefinitionId();

    /**
     * The name of the process definition of the process instance.
     */
    String getProcessDefinitionName();

    /**
     * The key of the process definition of the process instance.
     */
    String getProcessDefinitionKey();

    /**
     * The version of the process definition of the process instance.
     */
    Integer getProcessDefinitionVersion();

    /**
     * The deployment id of the process definition of the process instance.
     */
    String getDeploymentId();

    /**
     * The business key of this process instance.
     */
    String getBusinessKey();

    /**
     * returns true if the process instance is suspended
     */
    @Override
    boolean isSuspended();

    /**
     * Returns the process variables if requested in the process instance query
     */
    Map<String, Object> getProcessVariables();

    /**
     * The tenant identifier of this process instance
     */
    String getTenantId();

    /**
     * Returns the name of this process instance.
     */
    String getName();

    /**
     * Returns the description of this process instance.
     */
    String getDescription();

    /**
     * Returns the localized name of this process instance.
     */
    String getLocalizedName();

    /**
     * Returns the localized description of this process instance.
     */
    String getLocalizedDescription();

    /**
     * Returns the start time of this process instance.
     */
    Date getStartTime();

    /**
     * Returns the user id of this process instance.
     */
    String getStartUserId();

    void setAppVersion(Integer appVersion);

    Integer getAppVersion();

}
