/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.asyncexecutor.multitenant;

import com.je.bpm.engine.impl.asyncexecutor.AsyncExecutor;
import com.je.bpm.engine.impl.asyncexecutor.DefaultAsyncJobExecutor;
import com.je.bpm.engine.impl.asyncexecutor.JobManager;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.cfg.multitenant.TenantInfoHolder;
import com.je.bpm.engine.runtime.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * An {@link AsyncExecutor} that has one {@link AsyncExecutor} per tenant.
 * So each tenant has its own acquiring threads and it's own threadpool for executing jobs.
 */
public class ExecutorPerTenantAsyncExecutor implements TenantAwareAsyncExecutor {

    private static final Logger logger = LoggerFactory.getLogger(ExecutorPerTenantAsyncExecutor.class);

    protected TenantInfoHolder tenantInfoHolder;
    protected TenantAwareAsyncExecutorFactory tenantAwareAyncExecutorFactory;

    protected Map<String, AsyncExecutor> tenantExecutors = new HashMap<String, AsyncExecutor>();

    protected ProcessEngineConfigurationImpl processEngineConfiguration;
    protected boolean active;
    protected boolean autoActivate;

    public ExecutorPerTenantAsyncExecutor(TenantInfoHolder tenantInfoHolder) {
        this(tenantInfoHolder, null);
    }

    public ExecutorPerTenantAsyncExecutor(TenantInfoHolder tenantInfoHolder, TenantAwareAsyncExecutorFactory tenantAwareAyncExecutorFactory) {
        this.tenantInfoHolder = tenantInfoHolder;
        this.tenantAwareAyncExecutorFactory = tenantAwareAyncExecutorFactory;
    }

    @Override
    public Set<String> getTenantIds() {
        return tenantExecutors.keySet();
    }

    public void addTenantAsyncExecutor(String tenantId, boolean startExecutor) {
        AsyncExecutor tenantExecutor = null;

        if (tenantAwareAyncExecutorFactory == null) {
            tenantExecutor = new DefaultAsyncJobExecutor();
        } else {
            tenantExecutor = tenantAwareAyncExecutorFactory.createAsyncExecutor(tenantId);
        }

        tenantExecutor.setProcessEngineConfiguration(processEngineConfiguration);

        if (tenantExecutor instanceof DefaultAsyncJobExecutor) {
            DefaultAsyncJobExecutor defaultAsyncJobExecutor = (DefaultAsyncJobExecutor) tenantExecutor;
            defaultAsyncJobExecutor.setAsyncJobsDueRunnable(new TenantAwareAcquireAsyncJobsDueRunnable(defaultAsyncJobExecutor, tenantInfoHolder, tenantId));
            defaultAsyncJobExecutor.setTimerJobRunnable(new TenantAwareAcquireTimerJobsRunnable(defaultAsyncJobExecutor, tenantInfoHolder, tenantId));
            defaultAsyncJobExecutor.setExecuteAsyncRunnableFactory(new TenantAwareExecuteAsyncRunnableFactory(tenantInfoHolder, tenantId));
            defaultAsyncJobExecutor.setResetExpiredJobsRunnable(new TenantAwareResetExpiredJobsRunnable(defaultAsyncJobExecutor, tenantInfoHolder, tenantId));
        }

        tenantExecutors.put(tenantId, tenantExecutor);

        if (startExecutor) {
            tenantExecutor.start();
        }
    }

    @Override
    public void removeTenantAsyncExecutor(String tenantId) {
        shutdownTenantExecutor(tenantId);
        tenantExecutors.remove(tenantId);
    }

    protected AsyncExecutor determineAsyncExecutor() {
        return tenantExecutors.get(tenantInfoHolder.getCurrentTenantId());
    }

    @Override
    public boolean executeAsyncJob(Job job) {
        return determineAsyncExecutor().executeAsyncJob(job);
    }

    public JobManager getJobManager() {
        // Should never be accessed on this class, should be accessed on the actual AsyncExecutor
        throw new UnsupportedOperationException();
    }

    @Override
    public void setProcessEngineConfiguration(ProcessEngineConfigurationImpl processEngineConfiguration) {
        this.processEngineConfiguration = processEngineConfiguration;
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setProcessEngineConfiguration(processEngineConfiguration);
        }
    }

    @Override
    public ProcessEngineConfigurationImpl getProcessEngineConfiguration() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isAutoActivate() {
        return autoActivate;
    }

    @Override
    public void setAutoActivate(boolean isAutoActivate) {
        autoActivate = isAutoActivate;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void start() {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.start();
        }
        active = true;
    }

    @Override
    public synchronized void shutdown() {
        for (String tenantId : tenantExecutors.keySet()) {
            shutdownTenantExecutor(tenantId);
        }
        active = false;
    }

    protected void shutdownTenantExecutor(String tenantId) {
        logger.info("Shutting down async executor for tenant " + tenantId);
        tenantExecutors.get(tenantId).shutdown();
    }

    @Override
    public String getLockOwner() {
        return determineAsyncExecutor().getLockOwner();
    }

    @Override
    public int getTimerLockTimeInMillis() {
        return determineAsyncExecutor().getTimerLockTimeInMillis();
    }

    @Override
    public void setTimerLockTimeInMillis(int lockTimeInMillis) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setTimerLockTimeInMillis(lockTimeInMillis);
        }
    }

    @Override
    public int getAsyncJobLockTimeInMillis() {
        return determineAsyncExecutor().getAsyncJobLockTimeInMillis();
    }

    @Override
    public void setAsyncJobLockTimeInMillis(int lockTimeInMillis) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setAsyncJobLockTimeInMillis(lockTimeInMillis);
        }
    }

    @Override
    public int getDefaultTimerJobAcquireWaitTimeInMillis() {
        return determineAsyncExecutor().getDefaultTimerJobAcquireWaitTimeInMillis();
    }

    @Override
    public void setDefaultTimerJobAcquireWaitTimeInMillis(int waitTimeInMillis) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setDefaultTimerJobAcquireWaitTimeInMillis(waitTimeInMillis);
        }
    }

    @Override
    public int getDefaultAsyncJobAcquireWaitTimeInMillis() {
        return determineAsyncExecutor().getDefaultAsyncJobAcquireWaitTimeInMillis();
    }

    @Override
    public void setDefaultAsyncJobAcquireWaitTimeInMillis(int waitTimeInMillis) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setDefaultAsyncJobAcquireWaitTimeInMillis(waitTimeInMillis);
        }
    }

    @Override
    public int getDefaultQueueSizeFullWaitTimeInMillis() {
        return determineAsyncExecutor().getDefaultQueueSizeFullWaitTimeInMillis();
    }

    @Override
    public void setDefaultQueueSizeFullWaitTimeInMillis(int defaultQueueSizeFullWaitTimeInMillis) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setDefaultQueueSizeFullWaitTimeInMillis(defaultQueueSizeFullWaitTimeInMillis);
        }
    }

    @Override
    public int getMaxAsyncJobsDuePerAcquisition() {
        return determineAsyncExecutor().getMaxAsyncJobsDuePerAcquisition();
    }

    @Override
    public void setMaxAsyncJobsDuePerAcquisition(int maxJobs) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setMaxAsyncJobsDuePerAcquisition(maxJobs);
        }
    }

    @Override
    public int getMaxTimerJobsPerAcquisition() {
        return determineAsyncExecutor().getMaxTimerJobsPerAcquisition();
    }

    @Override
    public void setMaxTimerJobsPerAcquisition(int maxJobs) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setMaxTimerJobsPerAcquisition(maxJobs);
        }
    }

    @Override
    public int getRetryWaitTimeInMillis() {
        return determineAsyncExecutor().getRetryWaitTimeInMillis();
    }

    @Override
    public void setRetryWaitTimeInMillis(int retryWaitTimeInMillis) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setRetryWaitTimeInMillis(retryWaitTimeInMillis);
        }
    }

    @Override
    public int getResetExpiredJobsInterval() {
        return determineAsyncExecutor().getResetExpiredJobsInterval();
    }

    @Override
    public void setResetExpiredJobsInterval(int resetExpiredJobsInterval) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setResetExpiredJobsInterval(resetExpiredJobsInterval);
        }
    }

    @Override
    public int getResetExpiredJobsPageSize() {
        return determineAsyncExecutor().getResetExpiredJobsPageSize();
    }

    @Override
    public void setResetExpiredJobsPageSize(int resetExpiredJobsPageSize) {
        for (AsyncExecutor asyncExecutor : tenantExecutors.values()) {
            asyncExecutor.setResetExpiredJobsPageSize(resetExpiredJobsPageSize);
        }
    }

}
