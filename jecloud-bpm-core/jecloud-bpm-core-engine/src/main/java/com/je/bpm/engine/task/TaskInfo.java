/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.task;

import com.je.bpm.engine.internal.Internal;

import java.util.Date;
import java.util.Map;

@Internal
public interface TaskInfo {

    /**
     * DB id of the task.
     */
    String getId();

    /**
     * Name or title of the task.
     */
    String getName();

    /**
     * Free text description of the task.
     */
    String getDescription();

    /**
     * Indication of how important/urgent this task is
     */
    int getPriority();

    /**
     * The {@link User.getId() userId} of the person that is responsible for this task.
     */
    String getOwner();

    /**
     * The {@link User.getId() userId} of the person to which this task is delegated.
     */
    String getAssignee();

    /**
     * Reference to the process instance or null if it is not related to a process instance.
     */
    String getProcessInstanceId();

    /**
     * Reference to the path of execution or null if it is not related to a process instance.
     */
    String getExecutionId();

    /**
     * Reference to the process definition or null if it is not related to a process.
     */
    String getProcessDefinitionId();

    /**
     * The date/time when this task was created
     */
    Date getCreateTime();

    /**
     * The id of the activity in the process defining this task or null if this is not related to a process
     */
    String getTaskDefinitionKey();

    /**
     * Due date of the task.
     */
    Date getDueDate();

    /**
     * The category of the task. This is an optional field and allows to 'tag' tasks as belonging to a certain category.
     */
    String getCategory();

    /**
     * The parent task for which this task is a subtask
     */
    String getParentTaskId();

    /**
     * The tenant identifier of this task
     */
    String getTenantId();

    /**
     * The form key for the user task
     */
    String getFormKey();

    /**
     * Returns the local task variables if requested in the task query
     */
    Map<String, Object> getTaskLocalVariables();

    /**
     * Returns the process variables if requested in the task query
     */
    Map<String, Object> getProcessVariables();

    /**
     * The claim time of this task
     */
    Date getClaimTime();

    String getBusinessKey();
}
