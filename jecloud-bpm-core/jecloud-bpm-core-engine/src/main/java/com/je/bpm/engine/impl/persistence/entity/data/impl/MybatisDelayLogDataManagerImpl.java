/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl;

import com.je.bpm.engine.delay.DelayLogQuery;
import com.je.bpm.engine.delay.DelayLogQueryImpl;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.DelayLogEntity;
import com.je.bpm.engine.impl.persistence.entity.DelayLogEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.data.AbstractDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.DelayLogDataManager;
import java.util.List;

public class MybatisDelayLogDataManagerImpl extends AbstractDataManager<DelayLogEntity> implements DelayLogDataManager {

    public MybatisDelayLogDataManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public Class<? extends DelayLogEntity> getManagedEntityClass() {
        return DelayLogEntityImpl.class;
    }

    @Override
    public DelayLogEntity create() {
        return new DelayLogEntityImpl();
    }

    @Override
    public List<DelayLogEntity> findByProcessDefinitionId(String processDefinitionId) {
        return getDbSqlSession().selectList("com.je.bpm.engine.impl.persistence.entity.DelayLogEntityImpl.findByProcessDefinitionId",processDefinitionId);
    }

    @Override
    public List<DelayLogEntity> findByProcessInstanceId(String processInstanceId) {
        return getDbSqlSession().selectList("com.je.bpm.engine.impl.persistence.entity.DelayLogEntityImpl.findByProcessInstanceId",processInstanceId);
    }

    @Override
    public List<DelayLogEntity> findByTaskId(String taskId) {
        return getDbSqlSession().selectList("com.je.bpm.engine.impl.persistence.entity.DelayLogEntityImpl.findByTaskId",taskId);
    }

    @Override
    public List<DelayLogEntity> findByTaskIdAndAssignee(String taskId, String assignee) {
        DelayLogQuery delayLogQuery = new DelayLogQueryImpl();
        delayLogQuery.taskId(taskId);
        delayLogQuery.assignee(assignee);
        return getDbSqlSession().selectList("com.je.bpm.engine.impl.persistence.entity.DelayLogEntityImpl.findByTaskIdAndAssignee",delayLogQuery);
    }

    @Override
    public List<DelayLogEntity> findByTaskIdAndAssigneeWithReadState(String taskId, String assignee, String readState) {
        DelayLogQuery delayLogQuery = new DelayLogQueryImpl();
        delayLogQuery.taskId(taskId);
        delayLogQuery.assignee(assignee);
        delayLogQuery.readState(readState);
        return getDbSqlSession().selectList("com.je.bpm.engine.impl.persistence.entity.DelayLogEntityImpl.findByTaskIdAndAssigneeWithReadState",delayLogQuery);
    }

}
