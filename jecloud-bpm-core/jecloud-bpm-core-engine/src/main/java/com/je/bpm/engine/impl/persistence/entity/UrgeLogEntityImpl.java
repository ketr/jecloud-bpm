/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.db.BulkDeleteable;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 催办实体实现
 */
public class UrgeLogEntityImpl extends AbstractEntity implements UrgeLogEntity, Serializable, BulkDeleteable {
    /**
     * piid
     */
    private String processInstanceId;
    /**
     * pdid
     */
    private String processDefinitionId;
    /**
     * 流程名称
     */
    private String processName;
    /**
     * 任务id
     */
    private String taskId;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 催办类型 URGE CC
     */
    private String reminderType;
    /**
     * 催办类型 URGE CC
     */
    private String reminderName;
    /**
     * 提醒方式
     */
    private String reminderMethodName;
    /**
     * 提醒方式
     */
    private String reminderMethodCode;
    /**
     * 发送人
     */
    private String from;
    /**
     * 发送人名称
     */
    private String fromName;
    /**
     * 接收人
     */
    private String to;
    /**
     * 接收人名称
     */
    private String toName;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 是否查看
     */
    private String readState;
    /**
     * 查看时间
     */
    private Date readTime;
    /**
     * 节点的key
     */
    private String nodeId;
    /**
     * 内容
     */
    private String content;
    /**
     * 钉钉
     */
    private String jeCloudDingTalkId;

    @Override
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    @Override
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    @Override
    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    @Override
    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    @Override
    public String getProcessName() {
        return processName;
    }

    @Override
    public void setProcessName(String processName) {
        this.processName = processName;
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    @Override
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String getTaskName() {
        return taskName;
    }

    @Override
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    @Override
    public String getReminderType() {
        return reminderType;
    }

    @Override
    public void setReminderType(String reminderType) {
        this.reminderType = reminderType;
    }

    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public void setFrom(String from) {
        this.from = from;
    }

    @Override
    public String getFromName() {
        return fromName;
    }

    @Override
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    @Override
    public String getTo() {
        return to;
    }

    @Override
    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String getToName() {
        return toName;
    }

    @Override
    public void setToName(String toName) {
        this.toName = toName;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String getReadState() {
        return readState;
    }

    @Override
    public void setReadState(String readState) {
        this.readState = readState;
    }

    @Override
    public Date getReadTime() {
        return readTime;
    }

    @Override
    public void setReadTime(Date readTime) {
        this.readTime = readTime;
    }

    @Override
    public String getNodeId() {
        return nodeId;
    }

    @Override
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReminderName() {
        return reminderName;
    }

    public void setReminderName(String reminderName) {
        this.reminderName = reminderName;
    }

    public String getReminderMethodName() {
        return reminderMethodName;
    }

    public void setReminderMethodName(String reminderMethodName) {
        this.reminderMethodName = reminderMethodName;
    }

    public String getReminderMethodCode() {
        return reminderMethodCode;
    }

    public void setReminderMethodCode(String reminderMethodCode) {
        this.reminderMethodCode = reminderMethodCode;
    }

    public String getJeCloudDingTalkId() {
        return jeCloudDingTalkId;
    }

    public void setJeCloudDingTalkId(String jeCloudDingTalkId) {
        this.jeCloudDingTalkId = jeCloudDingTalkId;
    }

    @Override
    public Object getPersistentState() {
        Map<String, Object> persistentState = new HashMap();
        persistentState.put("taskId", this.taskId);
        persistentState.put("taskName", this.taskName);
        persistentState.put("from", this.from);
        persistentState.put("fromName", this.fromName);
        persistentState.put("to", this.to);
        persistentState.put("toName", this.toName);
        persistentState.put("readTime", this.readTime);
        if (processDefinitionId != null) {
            persistentState.put("processDefinitionId", this.processDefinitionId);
        }
        if (processInstanceId != null) {
            persistentState.put("processInstanceId", this.processInstanceId);
        }
        if (createTime != null) {
            persistentState.put("createTime", this.createTime);
        }
        if (readState != null) {
            persistentState.put("readState", this.readState);
        }

        return persistentState;
    }

}
