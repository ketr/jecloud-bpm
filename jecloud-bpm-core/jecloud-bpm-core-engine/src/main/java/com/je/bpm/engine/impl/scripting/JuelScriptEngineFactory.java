/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.scripting;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Collections.unmodifiableList;

/**
 * Factory to create {@link JuelScriptEngine}s.
 */
public class JuelScriptEngineFactory implements ScriptEngineFactory {

    private static List<String> names;
    private static List<String> extensions;
    private static List<String> mimeTypes;

    static {
        names = singletonList("juel");
        extensions = names;
        mimeTypes = unmodifiableList(new ArrayList<String>(0));
    }

    @Override
    public String getEngineName() {
        return "juel";
    }

    @Override
    public String getEngineVersion() {
        return "1.0";
    }

    @Override
    public List<String> getExtensions() {
        return extensions;
    }

    @Override
    public String getLanguageName() {
        return "JSP 2.1 EL";
    }

    @Override
    public String getLanguageVersion() {
        return "2.1";
    }

    @Override
    public String getMethodCallSyntax(String obj, String method, String... arguments) {
        throw new UnsupportedOperationException("Method getMethodCallSyntax is not supported");
    }

    @Override
    public List<String> getMimeTypes() {
        return mimeTypes;
    }

    @Override
    public List<String> getNames() {
        return names;
    }

    @Override
    public String getOutputStatement(String toDisplay) {
        // We will use out:print function to output statements
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append("out:print(\"");

        int length = toDisplay.length();
        for (int i = 0; i < length; i++) {
            char c = toDisplay.charAt(i);
            switch (c) {
                case '"':
                    stringBuffer.append("\\\"");
                    break;
                case '\\':
                    stringBuffer.append("\\\\");
                    break;
                default:
                    stringBuffer.append(c);
                    break;
            }
        }
        stringBuffer.append("\")");
        return stringBuffer.toString();
    }

    @Override
    public String getParameter(String key) {
        if (key.equals(ScriptEngine.NAME)) {
            return getLanguageName();
        } else if (key.equals(ScriptEngine.ENGINE)) {
            return getEngineName();
        } else if (key.equals(ScriptEngine.ENGINE_VERSION)) {
            return getEngineVersion();
        } else if (key.equals(ScriptEngine.LANGUAGE)) {
            return getLanguageName();
        } else if (key.equals(ScriptEngine.LANGUAGE_VERSION)) {
            return getLanguageVersion();
        } else if (key.equals("THREADING")) {
            return "MULTITHREADED";
        } else {
            return null;
        }
    }

    @Override
    public String getProgram(String... statements) {
        // Each statement is wrapped in '${}' to comply with EL
        StringBuilder buf = new StringBuilder();
        if (statements.length != 0) {
            for (int i = 0; i < statements.length; i++) {
                buf.append("${");
                buf.append(statements[i]);
                buf.append("} ");
            }
        }
        return buf.toString();
    }

    @Override
    public ScriptEngine getScriptEngine() {
        return new JuelScriptEngine(this);
    }

}
