/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.deployer;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ProcessEngineConfiguration;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.DeploymentEntity;
import com.je.bpm.engine.impl.persistence.entity.IdentityLinkEntity;
import com.je.bpm.engine.impl.persistence.entity.ProcessDefinitionEntity;
import com.je.bpm.engine.impl.persistence.entity.ProcessDefinitionEntityManager;
import com.je.bpm.engine.task.IdentityLinkType;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Methods for working with deployments.  Much of the actual work of {@link BpmnDeployer} is
 * done by orchestrating the different pieces of work this class does; by having them here,
 * we allow other deployers to make use of them.
 */
public class BpmnDeploymentHelper  {

  protected TimerManager timerManager;
  protected EventSubscriptionManager eventSubscriptionManager;

  /**
   * Verifies that no two process definitions share the same key, to prevent database unique
   * index violation.
   *
   * @throws ActivitiException if any two processes have the same key
   */
  public void verifyProcessDefinitionsDoNotShareKeys(
      Collection<ProcessDefinitionEntity> processDefinitions) {
    Set<String> keySet = new LinkedHashSet<String>();
    for (ProcessDefinitionEntity processDefinition : processDefinitions) {
      if (keySet.contains(processDefinition.getKey())) {
        throw new ActivitiException(
            "The deployment contains process definitions with the same key (process id attribute), this is not allowed");
      }
      keySet.add(processDefinition.getKey());
    }
  }

  /**
   * Updates all the process definition entities to match the deployment's values for tenant,
   * engine version, and deployment id.
   */
  public void copyDeploymentValuesToProcessDefinitions(DeploymentEntity deployment,
                                                       List<ProcessDefinitionEntity> processDefinitions) {
    String engineVersion = deployment.getEngineVersion();
    String tenantId = deployment.getTenantId();
    String deploymentId = deployment.getId();

    for (ProcessDefinitionEntity processDefinition : processDefinitions) {

      // Backwards compatibility
      if (engineVersion != null) {
        processDefinition.setEngineVersion(engineVersion);
      }

      // process definition inherits the tenant id
      if (tenantId != null) {
        processDefinition.setTenantId(tenantId);
      }

      processDefinition.setDeploymentId(deploymentId);
    }
  }

  /**
   * Updates all the process definition entities to have the correct resource names.
   */
  public void setResourceNamesOnProcessDefinitions(ParsedDeployment parsedDeployment) {
    for (ProcessDefinitionEntity processDefinition : parsedDeployment.getAllProcessDefinitions()) {
      String resourceName = parsedDeployment.getResourceForProcessDefinition(processDefinition).getName();
      processDefinition.setResourceName(resourceName);
    }
  }

  /**
   * Gets the most recent persisted process definition that matches this one for tenant and key.
   * If none is found, returns null.  This method assumes that the tenant and key are properly
   * set on the process definition entity.
   */
  public ProcessDefinitionEntity getMostRecentVersionOfProcessDefinition(ProcessDefinitionEntity processDefinition) {
    String key = processDefinition.getKey();
    String tenantId = processDefinition.getTenantId();
    ProcessDefinitionEntityManager processDefinitionManager
      = Context.getCommandContext().getProcessEngineConfiguration().getProcessDefinitionEntityManager();

    ProcessDefinitionEntity existingDefinition = null;

    if (tenantId != null && !tenantId.equals(ProcessEngineConfiguration.NO_TENANT_ID)) {
      existingDefinition = processDefinitionManager.findLatestProcessDefinitionByKeyAndTenantId(key, tenantId);
    } else {
      existingDefinition = processDefinitionManager.findLatestProcessDefinitionByKey(key);
    }

    return existingDefinition;
  }

  /**
   * Gets the persisted version of the already-deployed process definition.  Note that this is
   * different from {@link #getMostRecentVersionOfProcessDefinition} as it looks specifically for
   * a process definition that is already persisted and attached to a particular deployment,
   * rather than the latest version across all deployments.
   */
  public ProcessDefinitionEntity getPersistedInstanceOfProcessDefinition(ProcessDefinitionEntity processDefinition) {
    String deploymentId = processDefinition.getDeploymentId();
    if (StringUtils.isEmpty(processDefinition.getDeploymentId())) {
      throw new IllegalStateException("Provided process definition must have a deployment id.");
    }

    ProcessDefinitionEntityManager processDefinitionManager
      = Context.getCommandContext().getProcessEngineConfiguration().getProcessDefinitionEntityManager();
    ProcessDefinitionEntity persistedProcessDefinition = null;
    if (processDefinition.getTenantId() == null || ProcessEngineConfiguration.NO_TENANT_ID.equals(processDefinition.getTenantId())) {
      persistedProcessDefinition = processDefinitionManager.findProcessDefinitionByDeploymentAndKey(deploymentId, processDefinition.getKey());
    } else {
      persistedProcessDefinition = processDefinitionManager.findProcessDefinitionByDeploymentAndKeyAndTenantId(deploymentId, processDefinition.getKey(), processDefinition.getTenantId());
    }

    return persistedProcessDefinition;
  }

  /**
   * Updates all timers and events for the process definition.  This removes obsolete message and signal
   * subscriptions and timers, and adds new ones.
   */
  public void updateTimersAndEvents(ProcessDefinitionEntity processDefinition,
      ProcessDefinitionEntity previousProcessDefinition, ParsedDeployment parsedDeployment) {

    Process process = parsedDeployment.getProcessModelForProcessDefinition(processDefinition);
    BpmnModel bpmnModel = parsedDeployment.getBpmnModelForProcessDefinition(processDefinition);

    eventSubscriptionManager.removeObsoleteMessageEventSubscriptions(previousProcessDefinition);
    eventSubscriptionManager.addMessageEventSubscriptions(processDefinition, process, bpmnModel);

    eventSubscriptionManager.removeObsoleteSignalEventSubScription(previousProcessDefinition);
    eventSubscriptionManager.addSignalEventSubscriptions(Context.getCommandContext(), processDefinition, process, bpmnModel);

    timerManager.removeObsoleteTimers(processDefinition);
    timerManager.scheduleTimers(processDefinition, process);
  }

  enum ExpressionType {
    USER, GROUP
  }

  /**
   * @param processDefinition
   */
  public void addAuthorizationsForNewProcessDefinition(Process process, ProcessDefinitionEntity processDefinition) {
    CommandContext commandContext = Context.getCommandContext();

    addAuthorizationsFromIterator(commandContext, process.getCandidateStarterUsers(), processDefinition, ExpressionType.USER);
    addAuthorizationsFromIterator(commandContext, process.getCandidateStarterGroups(), processDefinition, ExpressionType.GROUP);
  }

  protected void addAuthorizationsFromIterator(CommandContext commandContext, List<String> expressions,
      ProcessDefinitionEntity processDefinition, ExpressionType expressionType) {

    if (expressions != null) {
      Iterator<String> iterator = expressions.iterator();
      while (iterator.hasNext()) {
        @SuppressWarnings("cast")
        String expression = iterator.next();
        IdentityLinkEntity identityLink = commandContext.getIdentityLinkEntityManager().create();
        identityLink.setProcessDef(processDefinition);
        if (expressionType.equals(ExpressionType.USER)) {
          identityLink.setUserId(expression);
        } else if (expressionType.equals(ExpressionType.GROUP)) {
          identityLink.setGroupId(expression);
        }
        identityLink.setType(IdentityLinkType.CANDIDATE);
        commandContext.getIdentityLinkEntityManager().insert(identityLink);
      }
    }

  }

  public TimerManager getTimerManager() {
    return timerManager;
  }

  public void setTimerManager(TimerManager timerManager) {
    this.timerManager = timerManager;
  }

  public EventSubscriptionManager getEventSubscriptionManager() {
    return eventSubscriptionManager;
  }

  public void setEventSubscriptionManager(EventSubscriptionManager eventSubscriptionManager) {
    this.eventSubscriptionManager = eventSubscriptionManager;
  }
}
