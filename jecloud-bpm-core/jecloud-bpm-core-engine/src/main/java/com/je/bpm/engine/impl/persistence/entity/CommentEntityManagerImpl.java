/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.data.CommentDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.DataManager;
import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.task.Comment;
import com.je.bpm.engine.task.Event;
import java.util.List;

@Internal
public class CommentEntityManagerImpl extends AbstractEntityManager<CommentEntity> implements CommentEntityManager {

    protected CommentDataManager commentDataManager;

    public CommentEntityManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration, CommentDataManager commentDataManager) {
        super(processEngineConfiguration);
        this.commentDataManager = commentDataManager;
    }

    @Override
    protected DataManager<CommentEntity> getDataManager() {
        return commentDataManager;
    }

    @Override
    public void insert(CommentEntity commentEntity) {
        checkHistoryEnabled();

        insert(commentEntity, false);

        Comment comment = (Comment) commentEntity;
        if (getEventDispatcher().isEnabled()) {
            // Forced to fetch the process-instance to associate the right
            // process definition
            String processDefinitionId = null;
            String processInstanceId = comment.getProcessInstanceId();
            if (comment.getProcessInstanceId() != null) {
                ExecutionEntity process = getExecutionEntityManager().findById(comment.getProcessInstanceId());
                if (process != null) {
                    processDefinitionId = process.getProcessDefinitionId();
                }
            }
            getEventDispatcher().dispatchEvent(
                    ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_CREATED, commentEntity, processInstanceId, processInstanceId, processDefinitionId));
            getEventDispatcher().dispatchEvent(
                    ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_INITIALIZED, commentEntity, processInstanceId, processInstanceId, processDefinitionId));
        }
    }

    @Override
    public List<Comment> findCommentsByTaskId(String taskId) {
        checkHistoryEnabled();
        return commentDataManager.findCommentsByTaskId(taskId);
    }

    @Override
    public List<Comment> findCommentsByTaskIdAndType(String taskId, String type) {
        checkHistoryEnabled();
        return commentDataManager.findCommentsByTaskIdAndType(taskId, type);
    }

    @Override
    public List<Comment> findCommentsByType(String type) {
        checkHistoryEnabled();
        return commentDataManager.findCommentsByType(type);
    }

    @Override
    public List<Event> findEventsByTaskId(String taskId) {
        checkHistoryEnabled();
        return commentDataManager.findEventsByTaskId(taskId);
    }

    @Override
    public List<Event> findEventsByProcessInstanceId(String processInstanceId) {
        checkHistoryEnabled();
        return commentDataManager.findEventsByProcessInstanceId(processInstanceId);
    }

    @Override
    public void deleteCommentsByTaskId(String taskId) {
        checkHistoryEnabled();
        commentDataManager.deleteCommentsByTaskId(taskId);
    }

    @Override
    public void deleteCommentsByProcessInstanceId(String processInstanceId) {
        checkHistoryEnabled();
        commentDataManager.deleteCommentsByProcessInstanceId(processInstanceId);
    }

    @Override
    public List<Comment> findCommentsByProcessInstanceId(String processInstanceId) {
        checkHistoryEnabled();
        return commentDataManager.findCommentsByProcessInstanceId(processInstanceId);
    }

    @Override
    public List<Comment> findCommentsByProcessInstanceId(String processInstanceId, String type) {
        checkHistoryEnabled();
        return commentDataManager.findCommentsByProcessInstanceId(processInstanceId, type);
    }

    @Override
    public Comment findComment(String commentId) {
        return commentDataManager.findComment(commentId);
    }

    @Override
    public Event findEvent(String commentId) {
        return commentDataManager.findEvent(commentId);
    }

    @Override
    public void delete(CommentEntity commentEntity) {
        checkHistoryEnabled();

        delete(commentEntity, false);

        Comment comment = (Comment) commentEntity;
        if (getEventDispatcher().isEnabled()) {
            // Forced to fetch the process-instance to associate the right
            // process definition
            String processDefinitionId = null;
            String processInstanceId = comment.getProcessInstanceId();
            if (comment.getProcessInstanceId() != null) {
                ExecutionEntity process = getExecutionEntityManager().findById(comment.getProcessInstanceId());
                if (process != null) {
                    processDefinitionId = process.getProcessDefinitionId();
                }
            }
            getEventDispatcher().dispatchEvent(
                    ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_DELETED, commentEntity, processInstanceId, processInstanceId, processDefinitionId));
        }
    }

    protected void checkHistoryEnabled() {
        if (!getHistoryManager().isHistoryEnabled()) {
            throw new ActivitiException("In order to use comments, history should be enabled");
        }
    }

    public CommentDataManager getCommentDataManager() {
        return commentDataManager;
    }

    public void setCommentDataManager(CommentDataManager commentDataManager) {
        this.commentDataManager = commentDataManager;
    }

}
