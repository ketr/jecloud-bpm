/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.alibaba.fastjson2.JSONObject;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.CommentEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.Comment;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 任务回签操作
 */
public class SignBackCmd extends NeedsActiveTaskCmd<Void> implements Serializable {

    private static final DateFormat nodeDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private String comment;

    private String beanId;

    private String signBackId;

    public SignBackCmd(String taskId, String prod, Map<String, Object> bean, String comment, String beanId, String signBackId, String files) {
        super(taskId, prod, bean);
        this.taskId = taskId;
        this.comment = comment;
        this.beanId = beanId;
        this.signBackId = signBackId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSignBackId() {
        return signBackId;
    }

    public void setSignBackId(String signBackId) {
        this.signBackId = signBackId;
    }

    @Override
    public Void execute(CommandContext commandContext, TaskEntity task) {
        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(task.getProcessDefinitionId(), task.getProcessInstanceId(), task.getBusinessKey());
        FlowElement flowElement = bpmnModel.getMainProcess().getFlowElement(task.getTaskDefinitionKey());
        if (!(flowElement instanceof KaiteBaseUserTask)) {
            return null;
        }
        Comment comment = commandContext.getProcessEngineConfiguration().getCommentDataManager().findComment(signBackId);
        CommentEntity commentEntity = (CommentEntity) comment;
        String message = commentEntity.getFullMessage();
        JSONObject messageJson = JSONObject.parseObject(message);
        messageJson.put("signBackUserIdComment", this.comment);
        messageJson.put("submitTime", nodeDateFormat.format(new Date()));
        //回签 PROCESSED 正常处理
        messageJson.put("type", "SIGN_BACK");
        System.out.println("json=" + messageJson.toJSONString());
        commentEntity.setMessage(messageJson.toJSONString());
        commentEntity.setFullMessage(messageJson.toJSONString());

        commandContext.getProcessEngineConfiguration().getCommentDataManager().updateComment(commentEntity);
        JSONObject signBackUserInfo = new JSONObject();
        signBackUserInfo.put("userId", task.getOwner());
        signBackUserInfo.put("startTime", nodeDateFormat.format(new Date()));

//        commandContext.getProcessEngineConfiguration().getTaskService().addComment(taskId, task.getProcessInstanceId(),
//                Comment.SIGN_BACK_USER_ID, signBackUserInfo.toString());
        return null;
    }

}
