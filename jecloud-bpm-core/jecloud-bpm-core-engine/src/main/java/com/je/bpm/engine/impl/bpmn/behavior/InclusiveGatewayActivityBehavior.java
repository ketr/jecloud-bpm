/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.behavior;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.engine.approvalnotice.ActivitiApprovalNotice;
import com.je.bpm.engine.approvalnotice.TaskApprovalNoticeEnum;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.delegate.InactiveActivityBehavior;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntityManager;
import com.je.bpm.engine.impl.util.ExecutionGraphUtil;
import com.je.bpm.engine.impl.util.ProcessDefinitionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation of the Inclusive Gateway/OR gateway/inclusive data-based gateway as defined in the BPMN specification.
 */
public class InclusiveGatewayActivityBehavior extends GatewayActivityBehavior implements InactiveActivityBehavior {

    private static final long serialVersionUID = 1L;

    private static Logger logger = LoggerFactory.getLogger(InclusiveGatewayActivityBehavior.class.getName());

    @Override
    public void execute(DelegateExecution execution) {
        // The join in the inclusive gateway works as follows:
        // When an execution enters it, it is inactivated.
        // All the inactivated executions stay in the inclusive gateway
        // until ALL executions that CAN reach the inclusive gateway have reached it.
        //
        // This check is repeated on execution changes until the inactivated
        // executions leave the gateway.
        String pdId = execution.getProcessDefinitionId();
        String piid = execution.getProcessInstanceId();
        CommandContext commandContext = Context.getCommandContext();
        //获取bpmnModel
        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(pdId, piid,execution.getProcessInstanceBusinessKey());
        //获取审批告知的Bean信息
        ActivitiApprovalNotice approvalNotice = commandContext.getProcessEngineConfiguration().getActivitiApprovalNotice();
        //添加审批告知
        commandContext.addAttribute(KaiteBaseUserTaskActivityBehavior.SUBMIT_IS_GATEWAY, true);
        addApprovalNotice(execution, bpmnModel, approvalNotice);
        //此方法后获取bpmnModel 会导致变量清除不掉，节点执行错误，网关节点后的节点导致直接删除掉，
        execution.inactivate();
        executeInclusiveGatewayLogic((ExecutionEntity) execution);
    }

    @Override
    public void executeInactive(ExecutionEntity executionEntity) {
        executeInclusiveGatewayLogic(executionEntity);
    }

    protected void executeInclusiveGatewayLogic(ExecutionEntity execution) {
        CommandContext commandContext = Context.getCommandContext();
        ExecutionEntityManager executionEntityManager = commandContext.getExecutionEntityManager();
        lockFirstParentScope(execution);
        Collection<ExecutionEntity> allExecutions = executionEntityManager.findChildExecutionsByProcessInstanceId(execution.getProcessInstanceId());
        Iterator<ExecutionEntity> executionIterator = allExecutions.iterator();
        boolean oneExecutionCanReachGateway = false;
        while (!oneExecutionCanReachGateway && executionIterator.hasNext()) {
            ExecutionEntity executionEntity = executionIterator.next();
            if (!executionEntity.getActivityId().equals(execution.getCurrentActivityId())) {
                boolean canReachGateway = ExecutionGraphUtil.isReachable(execution.getProcessDefinitionId(),
                        executionEntity.getActivityId(), execution.getCurrentActivityId(), execution.getProcessInstanceId(),execution.getProcessInstanceBusinessKey());
                if (canReachGateway) {
                    oneExecutionCanReachGateway = true;
                }
            } else if (executionEntity.getActivityId().equals(execution.getCurrentActivityId()) && executionEntity.isActive()) {
                // Special case: the execution has reached the inc gw, but the operation hasn't been executed yet for that execution
                oneExecutionCanReachGateway = true;
            }
        }

        // If no execution can reach the gateway, the gateway activates and executes fork behavior
        if (!oneExecutionCanReachGateway) {

            logger.debug("Inclusive gateway cannot be reached by any execution and is activated");

            // Kill all executions here (except the incoming)
            Collection<ExecutionEntity> executionsInGateway = executionEntityManager
                    .findInactiveExecutionsByActivityIdAndProcessInstanceId(execution.getCurrentActivityId(), execution.getProcessInstanceId());
            for (ExecutionEntity executionEntityInGateway : executionsInGateway) {
                if (!executionEntityInGateway.getId().equals(execution.getId())) {
                    commandContext.getHistoryManager().recordActivityEnd(executionEntityInGateway, null);
                    executionEntityManager.deleteExecutionAndRelatedData(executionEntityInGateway, null);
                }
            }
            // Leave
            commandContext.getAgenda().planTakeOutgoingSequenceFlowsOperation(execution, true);
        }
    }

    /**
     * 添加审批告知
     */
    public void addApprovalNotice(DelegateExecution execution, BpmnModel bpmnModel, ActivitiApprovalNotice approvalNotice) {
        Object variable = Context.getCommandContext().getAttribute(CommandContext.APPROVALNOTICE);
        if (variable instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) variable;
            //提交人
            String assignee = jsonObject.getString("assignee");
            //审批意见
            String comment = jsonObject.getString("comment");
            //节点id
            String taskId = jsonObject.getString("taskId");
            //提交类型name
            String submitType = jsonObject.getString("submitType");
            //是否多人节点
            String isMultiStr = jsonObject.getString("isMulti");
            Boolean isMulti = Boolean.valueOf(isMultiStr);
            //审批告之事件配置类型
            List<TaskApprovalNoticeEnum> taskApprovalNoticeEnumList =
                    (List<TaskApprovalNoticeEnum>) jsonObject.get("approvalNoticeType");
            if (null == taskApprovalNoticeEnumList || taskApprovalNoticeEnumList.size() == 0) {
                return;
            }
            //多人节点从变量datas获取代办人，单人节点从task获取
            String toAssignee = "";
            if (isMulti) {
                List<String> toAssigneeList = (List<String>) execution.getVariable("datas");
                if (null != toAssigneeList && toAssigneeList.size() > 0) {
                    for (String assigneeStr : toAssigneeList) {
                        if (Strings.isNullOrEmpty(toAssignee)) {
                            toAssignee = assigneeStr;
                        } else {
                            toAssignee = toAssignee + "," + assigneeStr;
                        }
                    }
                }
            }
            String beanId = execution.getProcessInstanceBusinessKey();
            String modelName = "";
            if (null != bpmnModel) {
                modelName = bpmnModel.getMainProcess().getName();
            } else {
                BpmnModel bpmnModel1 = ProcessDefinitionUtil.getBpmnModel(execution.getProcessDefinitionId(), execution.getProcessInstanceId(),execution.getProcessInstanceBusinessKey());
                if (bpmnModel1 != null) {
                    modelName = bpmnModel1.getMainProcess().getName();
                }
            }
            //send
            approvalNotice.sendNotice(taskApprovalNoticeEnumList, execution, assignee, comment, submitType, toAssignee, modelName, beanId);
        }
    }
}
