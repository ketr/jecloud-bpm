/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.ValuedDataObject;
import com.je.bpm.core.model.process.SubProcess;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.ActivitiObjectNotFoundException;
import com.je.bpm.engine.DynamicBpmnConstants;
import com.je.bpm.engine.impl.DataObjectImpl;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.impl.persistence.entity.VariableInstance;
import com.je.bpm.engine.impl.util.ProcessDefinitionUtil;
import com.je.bpm.engine.runtime.DataObject;
import com.je.bpm.engine.task.Task;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class GetTaskDataObjectsCmd implements Command<Map<String, DataObject>>, Serializable {

    private static final long serialVersionUID = 1L;
    protected String taskId;
    protected Collection<String> variableNames;
    protected String locale;
    protected boolean withLocalizationFallback;

    public GetTaskDataObjectsCmd(String taskId,
                                 Collection<String> variableNames) {
        this.taskId = taskId;
        this.variableNames = variableNames;
    }

    public GetTaskDataObjectsCmd(String taskId,
                                 Collection<String> variableNames,
                                 String locale,
                                 boolean withLocalizationFallback) {
        this.taskId = taskId;
        this.variableNames = variableNames;
        this.locale = locale;
        this.withLocalizationFallback = withLocalizationFallback;
    }

    public Map<String, DataObject> execute(CommandContext commandContext) {
        if (taskId == null) {
            throw new ActivitiIllegalArgumentException("taskId is null");
        }

        TaskEntity task = commandContext.getTaskEntityManager().findById(taskId);

        if (task == null) {
            throw new ActivitiObjectNotFoundException("task " + taskId + " doesn't exist", Task.class);
        }

        Map<String, DataObject> dataObjects = null;
        Map<String, VariableInstance> variables = null;
        if (variableNames == null) {
            variables = task.getVariableInstances();
        } else {
            variables = task.getVariableInstances(variableNames,
                    false);
        }

        if (variables != null) {
            dataObjects = new HashMap<>(variables.size());

            for (Entry<String, VariableInstance> entry : variables.entrySet()) {
                VariableInstance variableEntity = entry.getValue();

                String localizedName = null;
                String localizedDescription = null;

                ExecutionEntity executionEntity = commandContext.getExecutionEntityManager().findById(variableEntity.getExecutionId());
                while (!executionEntity.isScope()) {
                    executionEntity = executionEntity.getParent();
                }

                BpmnModel bpmnModel = ProcessDefinitionUtil.getBpmnModel(executionEntity.getProcessDefinitionId(), executionEntity.getProcessInstanceId(),executionEntity.getBusinessKey());
                ValuedDataObject foundDataObject = null;
                if (executionEntity.getParentId() == null) {
                    for (ValuedDataObject dataObject : bpmnModel.getMainProcess().getDataObjects()) {
                        if (dataObject.getName().equals(variableEntity.getName())) {
                            foundDataObject = dataObject;
                            break;
                        }
                    }
                } else {
                    SubProcess subProcess = (SubProcess) bpmnModel.getFlowElement(executionEntity.getActivityId());
                    for (ValuedDataObject dataObject : subProcess.getDataObjects()) {
                        if (dataObject.getName().equals(variableEntity.getName())) {
                            foundDataObject = dataObject;
                            break;
                        }
                    }
                }

                if (locale != null && foundDataObject != null) {
                    ObjectNode languageNode = Context.getLocalizationElementProperties(locale,
                            foundDataObject.getId(),
                            task.getProcessDefinitionId(),
                            withLocalizationFallback);

                    if (languageNode != null) {
                        JsonNode nameNode = languageNode.get(DynamicBpmnConstants.LOCALIZATION_NAME);
                        if (nameNode != null) {
                            localizedName = nameNode.asText();
                        }
                        JsonNode descriptionNode = languageNode.get(DynamicBpmnConstants.LOCALIZATION_DESCRIPTION);
                        if (descriptionNode != null) {
                            localizedDescription = descriptionNode.asText();
                        }
                    }
                }

                if (foundDataObject != null) {
                    dataObjects.put(variableEntity.getName(),
                            new DataObjectImpl(variableEntity.getName(),
                                    variableEntity.getValue(),
                                    foundDataObject.getDocumentation(),
                                    foundDataObject.getType(),
                                    localizedName,
                                    localizedDescription,
                                    foundDataObject.getId()));
                }
            }
        }

        return dataObjects;
    }
}
