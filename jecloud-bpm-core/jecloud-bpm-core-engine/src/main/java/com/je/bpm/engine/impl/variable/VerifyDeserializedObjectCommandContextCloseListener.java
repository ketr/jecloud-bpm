/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.variable;


import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.interceptor.CommandContextCloseListener;

/**
 * A {@link CommandContextCloseListener} that holds one {@link DeserializedObject} instance that
 * is added by the {@link SerializableType}.
 *
 * On the {@link #closing(CommandContext)} of the {@link CommandContext}, the {@link DeserializedObject}
 * will be verified if it is dirty. If so, it will update the right entities such that changes will be flushed.
 *
 * It's important that this happens in the {@link #closing(CommandContext)}, as this happens before
 * the {@link CommandContext#close()} is called and when all the sessions are flushed
 * (including the {@link com.je.bpm.engine.impl.db.DbSqlSession} in the relational DB case (the data needs to be ready then).
 */
public class VerifyDeserializedObjectCommandContextCloseListener implements CommandContextCloseListener {

    protected DeserializedObject deserializedObject;

    public VerifyDeserializedObjectCommandContextCloseListener(DeserializedObject deserializedObject) {
        this.deserializedObject = deserializedObject;
    }

    @Override
    public void closing(CommandContext commandContext) {
        deserializedObject.verifyIfBytesOfSerializedObjectChanged();
    }

    @Override
    public void closed(CommandContext commandContext) {

    }

    @Override
    public void afterSessionsFlush(CommandContext commandContext) {

    }

    @Override
    public void closeFailure(CommandContext commandContext) {

    }

}
