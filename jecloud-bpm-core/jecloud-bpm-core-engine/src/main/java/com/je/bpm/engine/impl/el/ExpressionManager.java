/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.el;

import com.je.bpm.common.el.ActivitiElContext;
import com.je.bpm.common.el.ELResolverReflectionBlockerDecorator;
import com.je.bpm.common.el.ReadOnlyMapELResolver;
import com.je.bpm.engine.delegate.Expression;
import com.je.bpm.engine.delegate.VariableScope;
import com.je.bpm.engine.impl.bpmn.data.ItemInstance;
import com.je.bpm.engine.impl.persistence.entity.VariableScopeImpl;
import de.odysseus.el.ExpressionFactoryImpl;
import javax.el.*;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Central manager for all expressions.
 * </p>
 * <p>
 * Process parsers will use this to build expression objects that are stored in the process definitions.
 * </p>
 * <p>
 * Then also this class is used as an entry point for runtime evaluation of the expressions.
 * </p>
 */
public class ExpressionManager {

    protected ExpressionFactory expressionFactory;
    /**
     * Default implementation (does nothing)
     */
    protected ELContext parsingElContext = new ParsingElContext();
    protected Map<Object, Object> beans;

    public ExpressionManager() {
        this(null);
    }

    public ExpressionManager(boolean initFactory) {
        this(null, initFactory);
    }

    public ExpressionManager(Map<Object, Object> beans) {
        this(beans, true);
    }

    public ExpressionManager(Map<Object, Object> beans, boolean initFactory) {
        // Use the ExpressionFactoryImpl in activiti build in version of juel,
        // with parametrised method expressions enabled
        if (initFactory) {
            expressionFactory = new ExpressionFactoryImpl();
        }
        this.beans = beans;
    }

    public Expression createExpression(String expression) {
        ValueExpression valueExpression = expressionFactory.createValueExpression(parsingElContext, expression.trim(), Object.class);
        return new JuelExpression(valueExpression, expression);
    }

    public void setExpressionFactory(ExpressionFactory expressionFactory) {
        this.expressionFactory = expressionFactory;
    }

    public ELContext getElContext(VariableScope variableScope) {
        ELContext elContext = null;
        if (variableScope instanceof VariableScopeImpl) {
            VariableScopeImpl variableScopeImpl = (VariableScopeImpl) variableScope;
            elContext = variableScopeImpl.getCachedElContext();
        }

        if (elContext == null) {
            elContext = createElContext(variableScope);
            if (variableScope instanceof VariableScopeImpl) {
                ((VariableScopeImpl) variableScope).setCachedElContext(elContext);
            }
        }

        return elContext;
    }

    protected ActivitiElContext createElContext(VariableScope variableScope) {
        ELResolver elResolver = createElResolver(variableScope);
        return new ActivitiElContext(elResolver);
    }

    protected ELResolver createElResolver(VariableScope variableScope) {
        CompositeELResolver elResolver = new CompositeELResolver();
        elResolver.add(new VariableScopeElResolver(variableScope));
        addBeansResolver(elResolver);
        addBaseResolvers(elResolver);
        return elResolver;
    }

    protected void addBeansResolver(CompositeELResolver elResolver) {
        if (beans != null) {
            // ACT-1102: Also expose all beans in configuration when using
            // standalone activiti, not
            // in spring-context
            elResolver.add(new ReadOnlyMapELResolver(beans));
        }
    }

    private void addBaseResolvers(CompositeELResolver elResolver) {
        elResolver.add(new ArrayELResolver());
        elResolver.add(new ListELResolver());
        elResolver.add(new MapELResolver());
        elResolver.add(new CustomMapperJsonNodeELResolver());
        elResolver.add(new DynamicBeanPropertyELResolver(ItemInstance.class, "getFieldValue", "setFieldValue")); // TODO: needs verification
        elResolver.add(new ELResolverReflectionBlockerDecorator(new BeanELResolver()));
    }

    public Map<Object, Object> getBeans() {
        return beans;
    }

    public void setBeans(Map<Object, Object> beans) {
        this.beans = beans;
    }

    public ELContext getElContext(Map<String, Object> availableVariables) {
        CompositeELResolver elResolver = new CompositeELResolver();
        elResolver.add(new ReadOnlyMapELResolver(new HashMap<>(availableVariables)));
        addBaseResolvers(elResolver);
        return new ActivitiElContext(elResolver);
    }
}
