/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

/**
 * A dummy implementation of {@link VariableInstance}, used for storing transient variables
 * on a {@link VariableScope}, as the {@link VariableScope} works with instances of {@link VariableInstance}
 * and not with raw key/values.
 * <p>
 * Nothing more than a thin wrapper around a name and value. All the other methods are not implemented.
 */
public class TransientVariableInstance implements VariableInstance {

    public static String TYPE_TRANSIENT = "transient";

    protected String variableName;
    protected Object variableValue;

    public TransientVariableInstance(String variableName, Object variableValue) {
        this.variableName = variableName;
        this.variableValue = variableValue;
    }

    @Override
    public String getName() {
        return variableName;
    }

    @Override
    public String getTextValue() {
        return null;
    }

    @Override
    public void setTextValue(String textValue) {

    }

    @Override
    public String getTextValue2() {
        return null;
    }

    @Override
    public void setTextValue2(String textValue2) {

    }

    @Override
    public Long getLongValue() {
        return null;
    }

    @Override
    public void setLongValue(Long longValue) {

    }

    @Override
    public Double getDoubleValue() {
        return null;
    }

    @Override
    public void setDoubleValue(Double doubleValue) {

    }

    @Override
    public byte[] getBytes() {
        return null;
    }

    @Override
    public void setBytes(byte[] bytes) {

    }

    @Override
    public Object getCachedValue() {
        return null;
    }

    @Override
    public void setCachedValue(Object cachedValue) {

    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public void setId(String id) {

    }

    @Override
    public boolean isInserted() {
        return false;
    }

    @Override
    public void setInserted(boolean inserted) {

    }

    @Override
    public boolean isUpdated() {
        return false;
    }

    @Override
    public void setUpdated(boolean updated) {

    }

    @Override
    public boolean isDeleted() {
        return false;
    }

    @Override
    public void setDeleted(boolean deleted) {

    }

    @Override
    public Object getPersistentState() {
        return null;
    }

    @Override
    public void setRevision(int revision) {

    }

    @Override
    public int getRevision() {
        return 0;
    }

    @Override
    public int getRevisionNext() {
        return 0;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void setProcessInstanceId(String processInstanceId) {

    }

    @Override
    public void setExecutionId(String executionId) {

    }

    @Override
    public Object getValue() {
        return variableValue;
    }

    @Override
    public void setValue(Object value) {
        variableValue = value;
    }

    @Override
    public String getTypeName() {
        return TYPE_TRANSIENT;
    }

    @Override
    public void setTypeName(String typeName) {

    }

    @Override
    public String getProcessInstanceId() {
        return null;
    }

    @Override
    public String getTaskId() {
        return null;
    }

    @Override
    public void setTaskId(String taskId) {

    }

    @Override
    public String getExecutionId() {
        return null;
    }

}
