/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.delegate;

import java.util.Map;
import java.util.Optional;

public class ThrowMessage {

    private String name;
    private Optional<Map<String, Object>> payload = Optional.empty();
    private Optional<String> businessKey = Optional.empty();
    private Optional<String> correlationKey = Optional.empty();

    private ThrowMessage(ThrowMessagBuilder builder) {
        this.name = builder.name;
        this.payload = builder.payload;
        this.businessKey = builder.businessKey;
        this.correlationKey = builder.correlationKey;
    }

    ThrowMessage() {}

    public ThrowMessage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Optional<Map<String, Object>> getPayload() {
        return payload;
    }

    public Optional<String> getBusinessKey() {
        return businessKey;
    }

    public Optional<String> getCorrelationKey() {
        return correlationKey;
    }

    /**
     * Creates builder to build {@link ThrowMessage}.
     *
     * @return created builder
     */
    public static INameStage builder() {
        return new ThrowMessagBuilder();
    }

    /**
     * Definition of a stage for staged builder.
     */
    public interface INameStage {

        /**
         * Builder method for name parameter.
         *
         * @param name field to set
         * @return builder
         */
        public IBuildStage name(String name);
    }

    /**
     * Definition of a stage for staged builder.
     */
    public interface IBuildStage {

        /**
         * Builder method for payload parameter.
         *
         * @param payload field to set
         * @return builder
         */
        public IBuildStage payload(Optional<Map<String, Object>> payload);

        /**
         * Builder method for businessKey parameter.
         *
         * @param businessKey field to set
         * @return builder
         */
        public IBuildStage businessKey(Optional<String> businessKey);

        /**
         * Builder method for correlationKey parameter.
         *
         * @param correlationKey field to set
         * @return builder
         */
        public IBuildStage correlationKey(Optional<String> correlationKey);

        /**
         * Builder method of the builder.
         *
         * @return built class
         */
        public ThrowMessage build();
    }

    /**
     * Builder to build {@link ThrowMessage}.
     */
    public static final class ThrowMessagBuilder implements INameStage, IBuildStage {

        private String name;
        private Optional<Map<String, Object>> payload = Optional.empty();
        private Optional<String> businessKey = Optional.empty();
        private Optional<String> correlationKey = Optional.empty();

        private ThrowMessagBuilder() {
        }

        @Override
        public IBuildStage name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public IBuildStage payload(Optional<Map<String, Object>> payload) {
            this.payload = payload;
            return this;
        }

        @Override
        public IBuildStage businessKey(Optional<String> businessKey) {
            this.businessKey = businessKey;
            return this;
        }

        @Override
        public IBuildStage correlationKey(Optional<String> correlationKey) {
            this.correlationKey = correlationKey;
            return this;
        }

        @Override
        public ThrowMessage build() {
            return new ThrowMessage(this);
        }
    }
}
