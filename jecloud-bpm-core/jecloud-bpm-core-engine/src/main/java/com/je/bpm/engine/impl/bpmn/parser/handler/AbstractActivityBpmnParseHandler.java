/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.parser.handler;

import com.je.bpm.core.model.Activity;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.FlowNode;
import com.je.bpm.core.model.MultiInstanceLoopCharacteristics;
import com.je.bpm.engine.impl.bpmn.behavior.AbstractBpmnActivityBehavior;
import com.je.bpm.engine.impl.bpmn.behavior.MultiInstanceActivityBehavior;
import com.je.bpm.engine.impl.bpmn.parser.BpmnParse;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.el.ExpressionManager;
import org.apache.commons.lang3.StringUtils;


public abstract class AbstractActivityBpmnParseHandler<T extends FlowNode> extends AbstractFlowNodeBpmnParseHandler<T> {

    @Override
    public void parse(BpmnParse bpmnParse, BaseElement element) {
        super.parse(bpmnParse, element);

        if (element instanceof Activity && ((Activity) element).getLoopCharacteristics() != null) {
            createMultiInstanceLoopCharacteristics(bpmnParse, (Activity) element);
        }
    }

    protected void createMultiInstanceLoopCharacteristics(BpmnParse bpmnParse, Activity modelActivity) {

        MultiInstanceLoopCharacteristics loopCharacteristics = modelActivity.getLoopCharacteristics();

        MultiInstanceActivityBehavior miActivityBehavior = createMultiInstanceActivityBehavior(
                bpmnParse, modelActivity, loopCharacteristics);

        modelActivity.setBehavior(miActivityBehavior);

        ExpressionManager expressionManager = Context.getProcessEngineConfiguration().getExpressionManager();

        // loop cardinality
        if (StringUtils.isNotEmpty(loopCharacteristics.getLoopCardinality())) {
            miActivityBehavior.setLoopCardinalityExpression(expressionManager.createExpression(loopCharacteristics.getLoopCardinality()));
        }

        // completion condition
        if (StringUtils.isNotEmpty(loopCharacteristics.getCompletionCondition())) {
            miActivityBehavior.setCompletionConditionExpression(expressionManager.createExpression(loopCharacteristics.getCompletionCondition()));
        }

        // activiti:collection
        if (StringUtils.isNotEmpty(loopCharacteristics.getInputDataItem())) {
            if (loopCharacteristics.getInputDataItem().contains("{")) {
                miActivityBehavior.setCollectionExpression(expressionManager.createExpression(loopCharacteristics.getInputDataItem()));
            } else {
                miActivityBehavior.setCollectionVariable(loopCharacteristics.getInputDataItem());
            }
        }

        // activiti:elementVariable
        if (StringUtils.isNotEmpty(loopCharacteristics.getElementVariable())) {
            miActivityBehavior.setCollectionElementVariable(loopCharacteristics.getElementVariable());
        }

        // activiti:elementIndexVariable
        if (StringUtils.isNotEmpty(loopCharacteristics.getElementIndexVariable())) {
            miActivityBehavior.setCollectionElementIndexVariable(loopCharacteristics.getElementIndexVariable());
        }

        if (StringUtils.isNotEmpty(loopCharacteristics.getLoopDataOutputRef())) {
            miActivityBehavior.setLoopDataOutputRef(loopCharacteristics.getLoopDataOutputRef());
        }

        if (StringUtils.isNotEmpty(loopCharacteristics.getOutputDataItem())) {
            miActivityBehavior.setOutputDataItem(loopCharacteristics.getOutputDataItem());
        }

    }

    private MultiInstanceActivityBehavior createMultiInstanceActivityBehavior(BpmnParse bpmnParse,
                                                                              Activity modelActivity, MultiInstanceLoopCharacteristics loopCharacteristics) {
        MultiInstanceActivityBehavior miActivityBehavior = null;

        if (loopCharacteristics.isSequential()) {
            miActivityBehavior = bpmnParse.getActivityBehaviorFactory().createSequentialMultiInstanceBehavior(modelActivity, (AbstractBpmnActivityBehavior) modelActivity.getBehavior());
        } else {
            miActivityBehavior = bpmnParse.getActivityBehaviorFactory().createParallelMultiInstanceBehavior(modelActivity, (AbstractBpmnActivityBehavior) modelActivity.getBehavior());
        }
        return miActivityBehavior;
    }
}
