/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine;

import com.je.bpm.engine.history.*;
import com.je.bpm.engine.impl.persistence.entity.PassRoundEntity;
import com.je.bpm.engine.internal.Internal;

import java.util.List;

/**
 * Service exposing information about ongoing and past process instances. This is different from the runtime information in the sense that this runtime information only contains the actual runtime
 * state at any given moment and it is optimized for runtime process execution performance. The history information is optimized for easy querying and remains permanent in the persistent storage.
 * 服务公开有关正在进行的和过去的流程实例的信息。 这与运行时信息不同，因为此运行时信息仅包含实际运行时
 * 任何给定时刻的状态，并针对运行时流程执行性能进行了优化。 历史信息经过优化，便于查询，并永久保存在持久存储中。
 * 历史Service，可以查询所有历史数据，例如：流程实例，任务，活动，变量，附件等
 */
@Internal
public interface HistoryService {

    /**
     * Creates a new programmatic query to search for {@link HistoricProcessInstance}s.
     */
    HistoricProcessInstanceQuery createHistoricProcessInstanceQuery();

    /**
     * Creates a new programmatic query to search for {@link HistoricActivityInstance}s.
     */
    HistoricActivityInstanceQuery createHistoricActivityInstanceQuery();

    /**
     * Creates a new programmatic query to search for {@link HistoricTaskInstance}s.
     */
    HistoricTaskInstanceQuery createHistoricTaskInstanceQuery();

    /**
     * Creates a new programmatic query to search for {@link HistoricDetail}s.
     */
    HistoricDetailQuery createHistoricDetailQuery();

    /**
     * Returns a new {@link com.je.bpm.engine.query.NativeQuery} for process definitions.
     */
    NativeHistoricDetailQuery createNativeHistoricDetailQuery();

    /**
     * Creates a new programmatic query to search for {@link HistoricVariableInstance}s.
     */
    HistoricVariableInstanceQuery createHistoricVariableInstanceQuery();

    /**
     * Returns a new {@link com.je.bpm.engine.query.NativeQuery} for process definitions.
     */
    NativeHistoricVariableInstanceQuery createNativeHistoricVariableInstanceQuery();

    /**
     * Deletes historic task instance. This might be useful for tasks that are {@link TaskService#newTask() dynamically created} and then {@link TaskService#complete(String) completed}. If the historic
     * task instance doesn't exist, no exception is thrown and the method returns normal.
     */
    void deleteHistoricTaskInstance(String taskId);

    /**
     * Deletes historic process instance. All historic activities, historic task and historic details (variable updates, form properties) are deleted as well.
     */
    void deleteHistoricProcessInstance(String processInstanceId);

    /**
     * creates a native query to search for {@link HistoricProcessInstance}s via SQL
     */
    NativeHistoricProcessInstanceQuery createNativeHistoricProcessInstanceQuery();

    /**
     * creates a native query to search for {@link HistoricTaskInstance}s via SQL
     */
    NativeHistoricTaskInstanceQuery createNativeHistoricTaskInstanceQuery();

    /**
     * creates a native query to search for {@link HistoricActivityInstance}s via SQL
     */
    NativeHistoricActivityInstanceQuery createNativeHistoricActivityInstanceQuery();

    /**
     * Retrieves the {@link HistoricIdentityLink}s associated with the given task. Such an {@link IdentityLink} informs how a certain identity (eg. group or user) is associated with a certain task (eg.
     * as candidate, assignee, etc.), even if the task is completed as opposed to {@link IdentityLink}s which only exist for active tasks.
     */
    List<HistoricIdentityLink> getHistoricIdentityLinksForTask(String taskId);

    /**
     * Retrieves the {@link HistoricIdentityLink}s associated with the given process instance. Such an {@link IdentityLink} informs how a certain identity (eg. group or user) is associated with a
     * certain process instance, even if the instance is completed as opposed to {@link IdentityLink}s which only exist for active instances.
     */
    List<HistoricIdentityLink> getHistoricIdentityLinksForProcessInstance(String processInstanceId);

    /**
     * Allows to retrieve the {@link ProcessInstanceHistoryLog} for one process instance.
     */
    ProcessInstanceHistoryLogQuery createProcessInstanceHistoryLogQuery(String processInstanceId);

    String getCountersignedTitle(String taskId);

    Void deleteGarbageHistoricVariableInstanceByProcessInstanceId(String parentProcessInstanceId);

    List<PassRoundEntity> getPassRoundByTaskId(String taskId, String userId);

    List<PassRoundEntity> getPassRoundByProcessInstanceId(String processInstanceId);

    List<PassRoundEntity> getPassRoundByProcessInstanceIdAndTo(String processInstanceId,String to);

}
