/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.helper;

import com.je.bpm.engine.delegate.Expression;
import com.je.bpm.engine.delegate.VariableScope;
import com.je.bpm.engine.impl.bpmn.parser.FieldDeclaration;
import com.je.bpm.engine.impl.cfg.DelegateExpressionFieldInjectionMode;
import com.je.bpm.engine.impl.context.Context;

import java.util.List;

/**

 */
public class DelegateExpressionUtil {

  public static Object resolveDelegateExpression(Expression expression, VariableScope variableScope) {
    return resolveDelegateExpression(expression, variableScope, null);
  }

  public static Object resolveDelegateExpression(Expression expression,
      VariableScope variableScope, List<FieldDeclaration> fieldDeclarations) {

    // Note: we can't cache the result of the expression, because the
    // execution can change: eg. delegateExpression='${mySpringBeanFactory.randomSpringBean()}'
    Object delegate = expression.getValue(variableScope);
    if (fieldDeclarations != null && fieldDeclarations.size() > 0) {
      DelegateExpressionFieldInjectionMode injectionMode = Context.getProcessEngineConfiguration().getDelegateExpressionFieldInjectionMode();
      if (injectionMode.equals(DelegateExpressionFieldInjectionMode.COMPATIBILITY)) {
        ClassDelegate.applyFieldDeclaration(fieldDeclarations, delegate, true);
      } else if (injectionMode.equals(DelegateExpressionFieldInjectionMode.MIXED)) {
        ClassDelegate.applyFieldDeclaration(fieldDeclarations, delegate, false);
      }

    }

    return delegate;
  }

}
