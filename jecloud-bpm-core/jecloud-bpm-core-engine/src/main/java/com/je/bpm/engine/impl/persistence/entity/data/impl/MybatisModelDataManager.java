/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl;

import com.je.bpm.engine.impl.ModelQueryImpl;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.ModelEntity;
import com.je.bpm.engine.impl.persistence.entity.ModelEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.data.AbstractDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.ModelDataManager;
import com.je.bpm.engine.repository.Model;

import java.util.List;
import java.util.Map;

/**
 *
 */
public class MybatisModelDataManager extends AbstractDataManager<ModelEntity> implements ModelDataManager {

    public MybatisModelDataManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public Class<? extends ModelEntity> getManagedEntityClass() {
        return ModelEntityImpl.class;
    }

    @Override
    public ModelEntity create() {
        return new ModelEntityImpl();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Model> findModelsByQueryCriteria(ModelQueryImpl query, Page page) {
        return getDbSqlSession().selectList("selectModelsByQueryCriteria", query, page);
    }

    @Override
    public List<Model> findModelsByMultipleConditionsOrQueryCriteria(ModelQueryImpl query, Page page) {
        return getDbSqlSession().selectList("selectModelsByMultipleConditionsOrQueryCriteria", query, page);
    }

    @Override
    public long findModelCountByQueryCriteria(ModelQueryImpl query) {
        return (Long) getDbSqlSession().selectOne("selectModelCountByQueryCriteria", query);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Model> findModelsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
        return getDbSqlSession().selectListWithRawParameter("selectModelByNativeQuery", parameterMap, firstResult, maxResults);
    }

    @Override
    public long findModelCountByNativeQuery(Map<String, Object> parameterMap) {
        return (Long) getDbSqlSession().selectOne("selectModelCountByNativeQuery", parameterMap);
    }

}
