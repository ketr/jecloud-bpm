/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine;

import com.je.bpm.engine.internal.Internal;

/**
 * Provides access to all the services that expose the BPM and workflow operations.
 *
 * <ul>
 * <li>
 * <b>{@link com.je.bpm.engine.RuntimeService}: </b> Allows the creation of {@link com.je.bpm.engine.repository.Deployment}s and the starting of and searching on
 * {@link com.je.bpm.engine.runtime.ProcessInstance}s.</li>
 * <li>
 * <b>{@link com.je.bpm.engine.TaskService}: </b> Exposes operations to manage human (standalone) {@link com.je.bpm.engine.task.Task}s, such as claiming, completing and assigning tasks</li>
 * <li>
 * <b>{@link com.je.bpm.engine.ManagementService}: </b> Exposes engine admin and maintenance operations</li>
 * <li>
 * <b>{@link com.je.bpm.engine.HistoryService}: </b> Service exposing information about ongoing and past process instances.</li>
 * </ul>
 *
 * Typically, there will be only one central ProcessEngine instance needed in a end-user application. Building a ProcessEngine is done through a {@link ProcessEngineConfiguration} instance and is a
 * costly operation which should be avoided. For that purpose, it is advised to store it in a static field or JNDI location (or something similar). This is a thread-safe object, so no special
 * precautions need to be taken.
 *
 */
@Internal
public interface ProcessEngine {

  /** the version of the activiti library */
  String VERSION = "7.1.0-M6"; // Note the extra -x at the end. To cater for snapshot releases with different database changes

  /**
   * The name as specified in 'process-engine-name' in the activiti.cfg.xml configuration file. The default name for a process engine is 'default
   */
  String getName();

  void close();

  RepositoryService getRepositoryService();

  RuntimeService getRuntimeService();

  TaskService getTaskService();

  HistoryService getHistoryService();

  ManagementService getManagementService();

  DynamicBpmnService getDynamicBpmnService();

  ProcessEngineConfiguration getProcessEngineConfiguration();

}
