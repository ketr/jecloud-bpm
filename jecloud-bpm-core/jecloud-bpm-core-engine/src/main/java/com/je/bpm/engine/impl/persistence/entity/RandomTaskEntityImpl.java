/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;
import com.je.bpm.engine.impl.db.BulkDeleteable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class RandomTaskEntityImpl extends AbstractEntityNoRevision implements RandomTaskEntity, Serializable, BulkDeleteable {

    private static final long serialVersionUID = 1L;

    protected String userId;
    protected String userName;
    protected String taskId;
    protected String countNum ;
    protected String time ;
    protected String processInstanceId;
    protected String processDefinitionId;
    protected String flowElementId;

    protected String processKey;
    public RandomTaskEntityImpl(){

    }

    @Override
    public Object getPersistentState() {
        Map<String, Object> persistentState = new HashMap<String, Object>();
        persistentState.put("id", this.id);
        if (this.userId != null) {
            persistentState.put("userId", this.userId);
        }
        if (this.userName != null) {
            persistentState.put("userName", this.userName);
        }
        if (this.taskId != null) {
            persistentState.put("taskId", this.taskId);
        }
        if (this.countNum != null) {
            persistentState.put("countNum", this.countNum);
        }
        if (this.time != null) {
            persistentState.put("time", this.time);
        }
        if (this.processInstanceId != null) {
            persistentState.put("processInstanceId", this.processInstanceId);
        }
        if (this.processDefinitionId != null) {
            persistentState.put("processDefinitionId", this.processDefinitionId);
        }
        if (this.flowElementId != null) {
            persistentState.put("flowElementId", this.flowElementId);
        }
        if (this.processKey != null) {
            persistentState.put("processKey", this.processKey);
        }
        return persistentState;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public void setCountNum(String countNum) {
        this.countNum = countNum;
    }

    @Override
    public String getCountNum() {
        return countNum;
    }

    @Override
    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String getTime() {
        return time;
    }

    @Override
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    @Override
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    @Override
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    @Override
    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    @Override
    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    @Override
    public void setFlowElementId(String flowElementId) {
        this.flowElementId=flowElementId;
    }

    @Override
    public String getFlowElementId() {
        return flowElementId;
    }

    @Override
    public void setProcessKey(String processKey) {
        this.processKey=processKey;
    }

    @Override
    public String getProcessKey() {
        return processKey;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IdentityLinkEntity[id=").append(id);
        if (userId != null) {
            sb.append(", userId=").append(userId);
        }
        if (userName != null) {
            sb.append(", userName=").append(userName);
        }
        if (taskId != null) {
            sb.append(", taskId=").append(taskId);
        }
        if (countNum != null) {
            sb.append(", countNum=").append(countNum);
        }
        if (time != null) {
            sb.append(", time=").append(time);
        }
        if (processInstanceId != null) {
            sb.append(", processInstanceId=").append(processInstanceId);
        }
        if (processDefinitionId != null) {
            sb.append(", processDefId=").append(processDefinitionId);
        }
        if (flowElementId != null) {
            sb.append(", flowElementId=").append(flowElementId);
        }
        if (processKey != null) {
            sb.append(", processKey=").append(processKey);
        }
        sb.append("]");
        return sb.toString();
    }
}
