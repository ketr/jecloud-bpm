/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.earlyWarning;

import com.je.bpm.engine.impl.AbstractQuery;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import java.util.Date;
import java.util.List;

public class EarlyWarningLogQueryImpl extends AbstractQuery<EarlyWarningLogQuery, EarlyWarningLog> implements EarlyWarningLogQuery {

    private String taskId;
    private String taskName;
    private String processInstanceId;
    private String processDefinitionId;
    private Date nextTime;
    private Date beforeNextTime;
    private Date afterNextTime;
    private Date createTime;
    private Date beforeCreateTime;
    private Date afterCreateTime;
    private String description;
    private String readState;
    private Date readTime;
    private Date beforeReadTime;
    private Date afterReadTime;
    private Integer warnNum;
    private String assignee;

    @Override
    public EarlyWarningLogQuery taskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    @Override
    public EarlyWarningLogQuery taskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    @Override
    public EarlyWarningLogQuery nextTime(Date date) {
        this.nextTime = date;
        return this;
    }

    @Override
    public EarlyWarningLogQuery beforeNextTime(Date date) {
        this.beforeNextTime = date;
        return this;
    }

    @Override
    public EarlyWarningLogQuery afterNextTime(Date date) {
        this.afterNextTime = date;
        return this;
    }

    @Override
    public EarlyWarningLogQuery createTime(Date date) {
        this.createTime = date;
        return this;
    }

    @Override
    public EarlyWarningLogQuery beforeCreateTime(Date date) {
        this.beforeCreateTime = date;
        return this;
    }

    @Override
    public EarlyWarningLogQuery afterCreateTime(Date date) {
        this.afterCreateTime = date;
        return this;
    }

    @Override
    public EarlyWarningLogQuery description(String description) {
        this.description = description;
        return this;
    }

    @Override
    public EarlyWarningLogQuery warnState(String warnState) {
        this.warnState(warnState);
        return this;
    }

    @Override
    public EarlyWarningLogQuery readState(String readState) {
        this.readState = readState;
        return this;
    }

    @Override
    public EarlyWarningLogQuery readTime(Date date) {
        this.readTime = date;
        return this;
    }

    @Override
    public EarlyWarningLogQuery beforeReadTime(Date date) {
        this.beforeReadTime = date;
        return this;
    }

    @Override
    public EarlyWarningLogQuery afterReadTime(Date date) {
        this.afterReadTime = date;
        return this;
    }

    @Override
    public EarlyWarningLogQuery processInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }

    @Override
    public EarlyWarningLogQuery processDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    @Override
    public EarlyWarningLogQuery warnNum(int warnNum) {
        this.warnNum = warnNum;
        return this;
    }

    @Override
    public EarlyWarningLogQuery assignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    @Override
    public long executeCount(CommandContext commandContext) {
        return 0;
    }

    @Override
    public List<EarlyWarningLog> executeList(CommandContext commandContext, Page page) {
        return null;
    }
}
