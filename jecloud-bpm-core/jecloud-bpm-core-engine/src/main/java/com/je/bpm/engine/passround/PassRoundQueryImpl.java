/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.passround;

import com.je.bpm.engine.impl.AbstractQuery;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.interceptor.CommandContext;

import java.util.Date;
import java.util.List;

public class PassRoundQueryImpl extends AbstractQuery<PassRoundQuery, PassRound> implements PassRoundQuery {

    protected String processDefinitionId;
    protected String processInstanceId;
    protected Date createTime;
    protected Date createTimeBefore;
    protected Date createTimeAfter;
    protected String taskId;
    protected String taskName;
    protected String from;
    protected String to;
    protected String readStateString;
    protected Date readTime;
    protected Date readTimeBefore;
    protected Date readTimeAfter;
    private String content;


    @Override
    public long executeCount(CommandContext commandContext) {
        return 0;
    }

    @Override
    public List<PassRound> executeList(CommandContext commandContext, Page page) {
        return null;
    }

    @Override
    public PassRoundQuery processDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    @Override
    public PassRoundQuery processInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }

    @Override
    public PassRoundQuery createTime(Date date) {
        this.createTime = date;
        return this;
    }

    @Override
    public PassRoundQuery taskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    @Override
    public PassRoundQuery taskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    @Override
    public PassRoundQuery from(String from) {
        this.from = from;
        return this;
    }

    @Override
    public PassRoundQuery to(String to) {
        this.to = to;
        return this;
    }

    @Override
    public PassRoundQuery readStateString(String readStateString) {
        this.readStateString = readStateString;
        return this;
    }

    @Override
    public PassRoundQuery readTime(Date date) {
        this.readTime = date;
        return this;
    }

    @Override
    public PassRoundQuery content(String comment) {
        this.content = comment;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Date getCreateTimeBefore() {
        return createTimeBefore;
    }

    public Date getCreateTimeAfter() {
        return createTimeAfter;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getReadStateString() {
        return readStateString;
    }

    public Date getReadTime() {
        return readTime;
    }

    public Date getReadTimeBefore() {
        return readTimeBefore;
    }

    public Date getReadTimeAfter() {
        return readTimeAfter;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
