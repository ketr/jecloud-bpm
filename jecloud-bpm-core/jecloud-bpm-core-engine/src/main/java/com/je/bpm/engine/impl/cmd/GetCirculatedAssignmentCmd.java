/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.core.model.config.task.PassRoundResource;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.ActivitiObjectNotFoundException;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.Task;
import com.je.bpm.runtime.shared.identity.BO.PassParserUserBo;
import com.je.bpm.runtime.shared.identity.ResultPassUserParser;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 获取传阅详细信息
 */
public class GetCirculatedAssignmentCmd implements Command<Object>, Serializable {
    /**
     * 节点id
     */
    private String taskId;
    private Map<String, Object> bean;
    private String prod;

    public GetCirculatedAssignmentCmd(String taskId, String prod, Map<String, Object> bean) {
        this.taskId = taskId;
        this.bean = bean;
        this.prod = prod;
    }

    public Object execute(CommandContext commandContext) {
        TaskEntity task = commandContext.getTaskEntityManager().findById(taskId);
        if (task == null) {
            throw new ActivitiObjectNotFoundException("Cannot find task with id " + taskId, Task.class);
        }

        //获取当前节点信息
        KaiteBaseUserTask kaiteBaseUserTask = commandContext.getProcessEngineConfiguration().getRuntimeService()
                .getCurrentElement(task.getProcessDefinitionId(), task.getProcessInstanceId(),
                        taskId, bean);
        //人员配置信息
        List<PassRoundResource> list = kaiteBaseUserTask.getTaskPassRoundConfig().getPassRoundResourceList();

        String startUserId = commandContext.getProcessEngineConfiguration().getRuntimeService()
                .getStartIdentityLinksForProcessInstance(task.getProcessInstanceId());
        String userId = Authentication.getAuthenticatedUser().getDeptId();
        ResultPassUserParser resultPassUserParser = commandContext.getProcessEngineConfiguration().getResultPassUserParser();
        String nodeId = task.getTaskDefinitionKey();
        String nodeName = task.getName();
        PassParserUserBo passParserUserBo = PassParserUserBo.builder()
                .setBean(bean).setList(list).setNodeId(nodeId).setNodeName(nodeName).setProd(prod)
                .setStartUserId(startUserId)
                .setUserId(userId)
                .build();
        return resultPassUserParser.parserResultUser(passParserUserBo);
    }

    protected String getStringVariable(DelegateExecution execution, String variableName) {
        Object value = execution.getVariable(variableName);
        if (value == null) {
            value = execution.getParent().getVariable(variableName);
        }
        return (String) (value != null ? value : null);
    }


}
