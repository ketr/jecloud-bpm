/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.event.logger.handler;

import com.je.bpm.engine.delegate.event.ActivitiEntityEvent;
import com.je.bpm.engine.delegate.event.ActivitiEntityWithVariablesEvent;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.EventLogEntryEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class TaskCompletedEventHandler extends AbstractTaskEventHandler {

    @Override
    public EventLogEntryEntity generateEventLogEntry(CommandContext commandContext) {

        ActivitiEntityEvent activitiEntityEvent = (ActivitiEntityEvent) event;

        TaskEntity task = (TaskEntity) activitiEntityEvent.getEntity();
        Map<String, Object> data = handleCommonTaskFields(task);

        long duration = timeStamp.getTime() - task.getCreateTime().getTime();
        putInMapIfNotNull(data, Fields.DURATION, duration);

        if (event instanceof ActivitiEntityWithVariablesEvent) {
            ActivitiEntityWithVariablesEvent activitiEntityWithVariablesEvent = (ActivitiEntityWithVariablesEvent) event;
            if (activitiEntityWithVariablesEvent.getVariables() != null && !activitiEntityWithVariablesEvent.getVariables().isEmpty()) {
                Map<String, Object> variableMap = new HashMap<String, Object>();
                for (Object variableName : activitiEntityWithVariablesEvent.getVariables().keySet()) {
                    putInMapIfNotNull(variableMap, (String) variableName, activitiEntityWithVariablesEvent.getVariables().get(variableName));
                }
                if (activitiEntityWithVariablesEvent.isLocalScope()) {
                    putInMapIfNotNull(data, Fields.LOCAL_VARIABLES, variableMap);
                } else {
                    putInMapIfNotNull(data, Fields.VARIABLES, variableMap);
                }
            }

        }

        return createEventLogEntry(task.getProcessDefinitionId(), task.getProcessInstanceId(), task.getExecutionId(), task.getId(), data);
    }

}
