/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.scripting;

import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.delegate.VariableScope;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;

import java.util.List;

import static java.util.Arrays.asList;


public class VariableScopeResolver implements Resolver {

    protected ProcessEngineConfigurationImpl processEngineConfiguration;
    protected VariableScope variableScope;

    protected String variableScopeKey = "execution";

    protected static final String processEngineConfigurationKey = "processEngineConfiguration";
    protected static final String runtimeServiceKey = "runtimeService";
    protected static final String taskServiceKey = "taskService";
    protected static final String repositoryServiceKey = "repositoryService";
    protected static final String managementServiceKey = "managementService";
    protected static final String historyServiceKey = "historyService";
    protected static final String formServiceKey = "formService";

    protected static final List<String> KEYS = asList(
            processEngineConfigurationKey, runtimeServiceKey, taskServiceKey,
            repositoryServiceKey, managementServiceKey, historyServiceKey, formServiceKey);

    public VariableScopeResolver(ProcessEngineConfigurationImpl processEngineConfiguration, VariableScope variableScope) {

        this.processEngineConfiguration = processEngineConfiguration;

        if (variableScope == null) {
            throw new ActivitiIllegalArgumentException("variableScope cannot be null");
        }
        if (variableScope instanceof ExecutionEntity) {
            variableScopeKey = "execution";
        } else if (variableScope instanceof TaskEntity) {
            variableScopeKey = "task";
        } else {
            throw new ActivitiException("unsupported variable scope type: " + variableScope.getClass().getName());
        }
        this.variableScope = variableScope;
    }

    @Override
    public boolean containsKey(Object key) {
        return variableScopeKey.equals(key) || KEYS.contains(key) || variableScope.hasVariable((String) key);
    }

    @Override
    public Object get(Object key) {
        if (variableScopeKey.equals(key)) {
            return variableScope;
        } else if (processEngineConfigurationKey.equals(key)) {
            return processEngineConfiguration;
        } else if (runtimeServiceKey.equals(key)) {
            return processEngineConfiguration.getRuntimeService();
        } else if (taskServiceKey.equals(key)) {
            return processEngineConfiguration.getTaskService();
        } else if (repositoryServiceKey.equals(key)) {
            return processEngineConfiguration.getRepositoryService();
        } else if (managementServiceKey.equals(key)) {
            return processEngineConfiguration.getManagementService();
        }

        return variableScope.getVariable((String) key);
    }
}
