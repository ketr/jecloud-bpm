/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.engine.ProcessEngineConfiguration;
import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.DeploymentEntity;
import com.je.bpm.engine.impl.persistence.entity.ProcessDefinitionEntity;
import com.je.bpm.engine.impl.persistence.entity.ResourceEntity;
import com.je.bpm.engine.impl.repository.DeploymentBuilderImpl;
import com.je.bpm.engine.repository.Deployment;

import java.io.Serializable;
import java.util.*;

/**
 *
 */
public class DeployCmd<T> implements Command<Deployment>, Serializable {

    private static final long serialVersionUID = 1L;
    protected DeploymentBuilderImpl deploymentBuilder;

    public DeployCmd(DeploymentBuilderImpl deploymentBuilder) {
        this.deploymentBuilder = deploymentBuilder;
    }

    @Override
    public Deployment execute(CommandContext commandContext) {
        return executeDeploy(commandContext);
    }

    protected Deployment executeDeploy(CommandContext commandContext) {
        DeploymentEntity deployment = deploymentBuilder.getDeployment();

        deployment.setDeploymentTime(commandContext.getProcessEngineConfiguration().getClock().getCurrentTime());
        //设置项目发布版本
        setProjectReleaseVersion(deployment);
        deployment.setVersion(1);
        //是否启用了重复过滤器
        if (deploymentBuilder.isDuplicateFilterEnabled()) {

            List<Deployment> existingDeployments = new ArrayList<Deployment>();
            if (deployment.getTenantId() == null || ProcessEngineConfiguration.NO_TENANT_ID.equals(deployment.getTenantId())) {
                DeploymentEntity existingDeployment = commandContext.getDeploymentEntityManager().findLatestDeploymentByName(deployment.getName());
                if (existingDeployment != null) {
                    existingDeployments.add(existingDeployment);
                }
            } else {
                List<Deployment> deploymentList = commandContext.getProcessEngineConfiguration().getRepositoryService().createDeploymentQuery().deploymentName(deployment.getName())
                        .deploymentTenantId(deployment.getTenantId()).orderByDeploymentId().desc().list();

                if (!deploymentList.isEmpty()) {
                    existingDeployments.addAll(deploymentList);
                }
            }

            DeploymentEntity existingDeployment = null;
            if (!existingDeployments.isEmpty()) {
                existingDeployment = (DeploymentEntity) existingDeployments.get(0);
            }

            if (existingDeployment != null) {
                if (deploymentsDiffer(deployment, existingDeployment)) {
                    applyUpgradeLogic(deployment, existingDeployment);
                } else {
                    return existingDeployment;
                }
            }
        }

        deployment.setNew(true);

        // Save the data
        commandContext.getDeploymentEntityManager().insert(deployment);

        if (commandContext.getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
            commandContext.getProcessEngineConfiguration().getEventDispatcher().dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_CREATED, deployment));
        }

        // Deployment settings
        Map<String, Object> deploymentSettings = new HashMap<String, Object>();
        deploymentSettings.put(DeploymentSettings.IS_BPMN20_XSD_VALIDATION_ENABLED, deploymentBuilder.isBpmn20XsdValidationEnabled());
        deploymentSettings.put(DeploymentSettings.IS_PROCESS_VALIDATION_ENABLED, deploymentBuilder.isProcessValidationEnabled());

        // Actually deploy
        commandContext.getProcessEngineConfiguration().getDeploymentManager().deploy(deployment, deploymentSettings);

        if (deploymentBuilder.getProcessDefinitionsActivationDate() != null) {
            scheduleProcessDefinitionActivation(commandContext, deployment);
        }

        if (commandContext.getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
            commandContext.getProcessEngineConfiguration().getEventDispatcher().dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_INITIALIZED, deployment));
        }

        return deployment;
    }

    private void setProjectReleaseVersion(DeploymentEntity deployment) {
        if (deploymentBuilder.hasProjectManifestSet()) {
            deployment.setProjectReleaseVersion(deploymentBuilder.getProjectManifest().getVersion());
        }
    }

    private void applyUpgradeLogic(DeploymentEntity deployment,
                                   DeploymentEntity existingDeployment) {
        if (deploymentBuilder.hasEnforcedAppVersion()) {
            deployment.setVersion(deploymentBuilder.getEnforcedAppVersion());
        } else if (deploymentBuilder.hasProjectManifestSet()) {
            deployment.setVersion(existingDeployment.getVersion() + 1);
        }
    }

    protected boolean deploymentsDiffer(DeploymentEntity deployment,
                                        DeploymentEntity saved) {

        if (deploymentBuilder.hasEnforcedAppVersion()) {
            return deploymentsDifferWhenEnforcedAppVersionIsSet(saved);
        } else if (deploymentBuilder.hasProjectManifestSet()) {
            return deploymentsDifferWhenProjectManifestIsSet(deployment, saved);
        } else {
            return deploymentsDifferDefault(deployment, saved);
        }
    }

    private boolean deploymentsDifferWhenEnforcedAppVersionIsSet(DeploymentEntity saved) {
        return !deploymentBuilder.getEnforcedAppVersion().equals(saved.getVersion());
    }

    private boolean deploymentsDifferWhenProjectManifestIsSet(DeploymentEntity deployment,
                                                              DeploymentEntity saved) {
        return !deployment.getProjectReleaseVersion().equals(saved.getProjectReleaseVersion());
    }

    private boolean deploymentsDifferDefault(DeploymentEntity deployment, DeploymentEntity saved) {
        if (deployment.getResources() == null || saved.getResources() == null) {
            return true;
        }

        Map<String, ResourceEntity> resources = deployment.getResources();
        Map<String, ResourceEntity> savedResources = saved.getResources();

        for (String resourceName : resources.keySet()) {
            ResourceEntity savedResource = savedResources.get(resourceName);

            if (savedResource == null) {
                return true;
            }

            if (!savedResource.isGenerated()) {
                ResourceEntity resource = resources.get(resourceName);

                byte[] bytes = resource.getBytes();
                byte[] savedBytes = savedResource.getBytes();
                if (!Arrays.equals(bytes, savedBytes)) {
                    return true;
                }
            }
        }
        return false;
    }


    protected void scheduleProcessDefinitionActivation(CommandContext commandContext, DeploymentEntity deployment) {
        for (ProcessDefinitionEntity processDefinitionEntity : deployment.getDeployedArtifacts(ProcessDefinitionEntity.class)) {

            // If activation date is set, we first suspend all the process
            // definition
            SuspendProcessDefinitionCmd suspendProcessDefinitionCmd = new SuspendProcessDefinitionCmd(processDefinitionEntity, false, null, deployment.getTenantId());
            suspendProcessDefinitionCmd.execute(commandContext);

            // And we schedule an activation at the provided date
            ActivateProcessDefinitionCmd activateProcessDefinitionCmd = new ActivateProcessDefinitionCmd(processDefinitionEntity, false, deploymentBuilder.getProcessDefinitionsActivationDate(),
                    deployment.getTenantId());
            activateProcessDefinitionCmd.execute(commandContext);
        }
    }

}
