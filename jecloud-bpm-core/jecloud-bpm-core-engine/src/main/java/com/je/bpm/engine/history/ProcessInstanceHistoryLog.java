/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.history;

import com.je.bpm.engine.internal.Internal;

import java.util.Date;
import java.util.List;

/**
 * A trail of data for a given process instance.
 */
@Internal
public interface ProcessInstanceHistoryLog {

    /**
     * The process instance id (== as the id for the runtime {@link com.je.bpm.engine.runtime.ProcessInstance process instance}).
     */
    String getId();

    /**
     * The user provided unique reference to this process instance.
     */
    String getBusinessKey();

    /**
     * The process definition reference.
     */
    String getProcessDefinitionId();

    /**
     * The time the process was started.
     */
    Date getStartTime();

    /**
     * The time the process was ended.
     */
    Date getEndTime();

    /**
     * The difference between {@link #getEndTime()} and {@link #getStartTime()} .
     */
    Long getDurationInMillis();

    /**
     * The authenticated user that started this process instance.
     */
    String getStartUserId();

    /**
     * The start activity.
     */
    String getStartActivityId();

    /**
     * Obtains the reason for the process instance's deletion.
     */
    String getDeleteReason();

    /**
     * The process instance id of a potential super process instance or null if no super process instance exists
     */
    String getSuperProcessInstanceId();

    /**
     * The tenant identifier for the process instance.
     */
    String getTenantId();

    /**
     * The trail of data, ordered by date (ascending). Gives a replay of the process instance.
     */
    List<HistoricData> getHistoricData();

}
