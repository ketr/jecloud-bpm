/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.behavior;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.task.TaskBasicConfigImpl;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.core.model.task.KaiteDecideUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.delegate.TaskListener;
import com.je.bpm.engine.delegate.event.ActivitiEventDispatcher;
import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.cmd.DismissTaskCmd;
import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.el.ExpressionManager;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntityManager;
import com.je.bpm.engine.task.Comment;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 判断任务节点行为
 */
public class KaiteDecideUserTaskActivityBehavior extends KaiteBaseUserTaskActivityBehavior {

    private static final Logger logger = LoggerFactory.getLogger(KaiteDecideUserTaskActivityBehavior.class);

    private KaiteDecideUserTask kaiteDecideUserTask;

    public KaiteDecideUserTaskActivityBehavior(KaiteDecideUserTask kaiteDecideUserTask) {
        this.kaiteDecideUserTask = kaiteDecideUserTask;
    }

    @Override
    public void execute(DelegateExecution execution) {
        if (execution.getVariable(DismissTaskCmd.IS_DISMISS_KEY) == null || !execution.getVariable(DismissTaskCmd.IS_DISMISS_KEY, Boolean.class)) {
            execution.removeVariable(DismissTaskCmd.IS_DISMISS_KEY);
            execution.removeVariable(DismissTaskCmd.DISMISS_INFO_KEY);
        }
        CommandContext commandContext = Context.getCommandContext();
        TaskEntityManager taskEntityManager = commandContext.getTaskEntityManager();

        TaskEntity task = taskEntityManager.create();
        ExecutionEntity executionEntity = (ExecutionEntity) execution;
        task.setExecution(executionEntity);
        task.setTaskDefinitionKey(kaiteDecideUserTask.getId());
        task.setBusinessKey(executionEntity.getProcessInstanceBusinessKey());

        ProcessEngineConfigurationImpl processEngineConfiguration = Context.getProcessEngineConfiguration();
        ExpressionManager expressionManager = processEngineConfiguration.getExpressionManager();

        BpmnModel bpmnModel = processEngineConfiguration.getRepositoryService().getBpmnModel(executionEntity.getProcessDefinitionId(), executionEntity.getProcessInstanceId(), executionEntity.getBusinessKey());

        FlowElement flowElement = bpmnModel.getFlowElement(kaiteDecideUserTask.getId());
        if (flowElement == null) {
            throw new ActivitiException("Can't find the flow element from the definition.");
        }

        KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
        TaskBasicConfigImpl taskBasicConfig = kaiteBaseUserTask.getTaskBasicConfig();

        String activeTaskName = kaiteDecideUserTask.getName();
        String activeTaskDescription = kaiteDecideUserTask.getDocumentation();
        String format = String.format(ASSIGNEE_VARIABLE, kaiteDecideUserTask.getId());
        String activeTaskAssignee = "";
        if (Strings.isNullOrEmpty(executionEntity.getVariable(kaiteBaseUserTask.getId(), String.class))) {
            if (Strings.isNullOrEmpty(executionEntity.getVariable(format, String.class))) {
                activeTaskAssignee = format;
            } else {
                activeTaskAssignee = executionEntity.getVariable(format, String.class);
            }
        } else {
            activeTaskAssignee = executionEntity.getVariable(kaiteBaseUserTask.getId(), String.class);
        }
        //任务名称
        if (StringUtils.isNotEmpty(activeTaskName)) {
            String name;
            try {
                name = (String) expressionManager.createExpression(activeTaskName).getValue(execution);
            } catch (ActivitiException e) {
                name = activeTaskName;
                logger.warn("property not found in task name expression " + e.getMessage());
            }
            task.setName(name);
        }

        //任务描述
        if (StringUtils.isNotEmpty(activeTaskDescription)) {
            String description;
            try {
                description = (String) expressionManager.createExpression(activeTaskDescription).getValue(execution);
            } catch (ActivitiException e) {
                description = activeTaskDescription;
                logger.warn("property not found in task description expression " + e.getMessage());
            }
            task.setDescription(description);
        }


        task.setAppVersion(executionEntity.getProcessInstance().getAppVersion());
        taskEntityManager.insert(task, executionEntity);
        task.setVariablesLocal(calculateInputVariables(execution));

        boolean skipUserTask = false;
        Boolean isSponsor = false;

        //获取 是否是跳跃变量 和 跳跃的目标节点变量
        String isJump = (commandContext.getAttribute(commandContext.IS_JUMP) != null) ? commandContext.getAttribute(commandContext.IS_JUMP).toString() : "0";
        // 是否是跳跃，并且跳跃的目标节点和当前节点相同
        if (!Strings.isNullOrEmpty(isJump) && "1".equals(isJump)) {
            String targetNodeId = commandContext.getAttribute(commandContext.JUMP_TARGET_NODE_ID).toString();
            if (!Strings.isNullOrEmpty(targetNodeId) && !kaiteBaseUserTask.getId().equals(targetNodeId)) {
                skipUserTask = true;
                isSponsor = true;
                //可处理人改为当前登录人
                activeTaskAssignee = Authentication.getAuthenticatedUser().getDeptId();
                if (commandContext.getAttribute(commandContext.COMMENT) == null) {
                    commandContext.addAttribute(commandContext.COMMENT, "同意");
                }
            }
            //如果是跳跃，找到选择到目标节点线
            List<SequenceFlow> results = new ArrayList<>();
            buildGoingList(kaiteBaseUserTask.getOutgoingFlows(), results, executionEntity);
            if (results.size() > 1) {
                for (SequenceFlow sequenceFlow : results) {
                    if (!sequenceFlow.getTargetRef().equals(targetNodeId)) {
                        continue;
                    }
                    execution.setTransientVariable("outgoing", sequenceFlow.getId());
                }
            }
        }
        // Handling assignments need to be done after the task is inserted, to have an id
        validatePersonnelClearance(activeTaskAssignee, bpmnModel, task.getTaskDefinitionKey());
        handleAssignments(taskEntityManager, activeTaskAssignee, null, task, expressionManager, execution);

        JSONObject variableObj = handleBaseUserTaskConfig(kaiteDecideUserTask, task, expressionManager, bpmnModel);
        task.setVariableLocal(TASK_GLOBAL_VAR, variableObj.toJSONString());

        if (variableObj.containsKey("skip") && variableObj.getBoolean("skip")) {
            skipUserTask = true;
            isSponsor = true;
            variableObj.remove("skip");
        } else {
            if (Context.getCommandContext().getAttribute(SUBMIT_IS_GATEWAY) == null) {
                //添加审批告知
                addApprovalNotice(commandContext, execution, task);
            }
            //计算跳过表达式
//            if (StringUtils.isNotEmpty(activeTaskSkipExpression)) {
//                Expression skipExpression = expressionManager.createExpression(activeTaskSkipExpression);
//                skipUserTask = SkipExpressionUtil.isSkipExpressionEnabled(execution, skipExpression) && SkipExpressionUtil.shouldSkipFlowElement(execution, skipExpression);
//            }
        }
        //执行当前任务创建监听
        processEngineConfiguration.getListenerNotificationHelper().executeTaskListeners(task, TaskListener.EVENTNAME_CREATE);

        // All properties set, now fire events
        if (Context.getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
            ActivitiEventDispatcher eventDispatcher = Context.getProcessEngineConfiguration().getEventDispatcher();
            eventDispatcher.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.TASK_CREATED, task));
            if (task.getAssignee() != null) {
                eventDispatcher.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.TASK_ASSIGNED, task));
            }
        }
        //添加待办信息
        addUpcomingInfo(bpmnModel, execution, task.getAssignee(), task, isSponsor);
        //自动传阅
        automaticCirculation(task, bpmnModel);
        //添加预警
        addWarning(task, bpmnModel);
        if (skipUserTask) {
            //历史排序，防止第一个节点排序问题
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Object comment = commandContext.getAttribute(commandContext.COMMENT);
            String commentStr = "";
            if (comment instanceof String) {
                commentStr = String.valueOf(comment);
            }
            commandContext.getProcessEngineConfiguration().getTaskService().addComment(task.getId(), task.getProcessInstanceId(),
                    Comment.USER_COMMENT, getJustComment());
            UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(null, commentStr,
                    SubmitTypeEnum.SUBMIT, task.getBusinessKey(), task.getId(), null);
            commandContext.getProcessEngineConfiguration().getActivitiUpcomingRun().completeUpcoming(upcomingInfo);
            taskEntityManager.deleteTask(task, null, false, false);
            leave(execution);
        } else {
            //如果是合并处理，判断当前节点是否处理过
            boolean mergeApplication = kaiteBaseUserTask.getTaskBasicConfig().isMergeApplication();
            if (!mergeApplication) {
                return;
            }
            mergeProcessingMethod(bpmnModel, task, execution);
        }

    }

    @Override
    public void trigger(DelegateExecution execution, String signalName, Object signalData) {
        super.trigger(execution, signalName, signalData);
    }

    private void buildGoingList(List<SequenceFlow> outgoingList, List<SequenceFlow> result, ExecutionEntity execution) {
        Map<String, Object> bean = (Map<String, Object>) Context.getCommandContext().getAttribute(CommandContext.BEAN);
        for (SequenceFlow sequenceFlow : outgoingList) {
            String conditionExpression = sequenceFlow.getConditionExpression();
            if (DelegateHelper.isBoolean(conditionExpression, bean)) {
                result.add(sequenceFlow);
            }
        }
    }

}
