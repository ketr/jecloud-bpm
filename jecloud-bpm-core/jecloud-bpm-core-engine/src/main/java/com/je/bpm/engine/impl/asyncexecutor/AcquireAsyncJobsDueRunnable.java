/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.asyncexecutor;

import com.je.bpm.engine.ActivitiOptimisticLockingException;
import com.je.bpm.engine.impl.cmd.AcquireJobsCmd;
import com.je.bpm.engine.impl.interceptor.CommandExecutor;
import com.je.bpm.engine.impl.persistence.entity.JobEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;

public class AcquireAsyncJobsDueRunnable implements Runnable {

    private static Logger log = LoggerFactory.getLogger(AcquireAsyncJobsDueRunnable.class);

    protected final AsyncExecutor asyncExecutor;

    protected volatile boolean isInterrupted;
    protected final Object MONITOR = new Object();
    protected final AtomicBoolean isWaiting = new AtomicBoolean(false);

    protected long millisToWait;

    public AcquireAsyncJobsDueRunnable(AsyncExecutor asyncExecutor) {
        this.asyncExecutor = asyncExecutor;
    }

    @Override
    public synchronized void run() {
        log.info("{} starting to acquire async jobs due");
        Thread.currentThread().setName("activiti-acquire-async-jobs");
        final CommandExecutor commandExecutor = asyncExecutor.getProcessEngineConfiguration().getCommandExecutor();

        while (!isInterrupted) {

            try {
                AcquiredJobEntities acquiredJobs = commandExecutor.execute(new AcquireJobsCmd(asyncExecutor));
                boolean allJobsSuccessfullyOffered = true;
                for (JobEntity job : acquiredJobs.getJobs()) {
                    boolean jobSuccessFullyOffered = asyncExecutor.executeAsyncJob(job);
                    if (!jobSuccessFullyOffered) {
                        allJobsSuccessfullyOffered = false;
                    }
                }

                // If all jobs are executed, we check if we got back the amount we expected
                // If not, we will wait, as to not query the database needlessly.
                // Otherwise, we set the wait time to 0, as to query again immediately.
                millisToWait = asyncExecutor.getDefaultAsyncJobAcquireWaitTimeInMillis();
                int jobsAcquired = acquiredJobs.size();
                if (jobsAcquired >= asyncExecutor.getMaxAsyncJobsDuePerAcquisition()) {
                    millisToWait = 0;
                }

                // If the queue was full, we wait too (even if we got enough jobs back), as not overload the queue
                if (millisToWait == 0 && !allJobsSuccessfullyOffered) {
                    millisToWait = asyncExecutor.getDefaultQueueSizeFullWaitTimeInMillis();
                }
            } catch (ActivitiOptimisticLockingException optimisticLockingException) {
                if (log.isDebugEnabled()) {
                    log.debug("Optimistic locking exception during async job acquisition. If you have multiple async executors running against the same database, "
                                    + "this exception means that this thread tried to acquire a due async job, which already was acquired by another async executor acquisition thread."
                                    + "This is expected behavior in a clustered environment. "
                                    + "You can ignore this message if you indeed have multiple async executor acquisition threads running against the same database. " + "Exception message: {}",
                            optimisticLockingException.getMessage());
                }
            } catch (Throwable e) {
                log.error("exception during async job acquisition: {}", e.getMessage(), e);
                millisToWait = asyncExecutor.getDefaultAsyncJobAcquireWaitTimeInMillis();
            }

            if (millisToWait > 0) {
                try {
                    if (log.isDebugEnabled()) {
                        log.debug("async job acquisition thread sleeping for {} millis", millisToWait);
                    }
                    synchronized (MONITOR) {
                        if (!isInterrupted) {
                            isWaiting.set(true);
                            MONITOR.wait(millisToWait);
                        }
                    }

                    if (log.isDebugEnabled()) {
                        log.debug("async job acquisition thread woke up");
                    }
                } catch (InterruptedException e) {
                    if (log.isDebugEnabled()) {
                        log.debug("async job acquisition wait interrupted");
                    }
                } finally {
                    isWaiting.set(false);
                }
            }
        }

        log.info("{} stopped async job due acquisition");
    }

    public void stop() {
        synchronized (MONITOR) {
            isInterrupted = true;
            if (isWaiting.compareAndSet(true, false)) {
                MONITOR.notifyAll();
            }
        }
    }

    public long getMillisToWait() {
        return millisToWait;
    }

    public void setMillisToWait(long millisToWait) {
        this.millisToWait = millisToWait;
    }
}
