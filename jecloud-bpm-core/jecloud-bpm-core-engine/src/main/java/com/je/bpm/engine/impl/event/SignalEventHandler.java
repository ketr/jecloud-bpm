/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.event;

import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiObjectNotFoundException;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.delegate.event.ActivitiSignalEvent;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.EventSubscriptionEntity;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.util.ProcessDefinitionUtil;
import com.je.bpm.engine.impl.util.ProcessInstanceHelper;
import com.je.bpm.engine.repository.ProcessDefinition;

import java.util.Map;

/**
 * 信号事件处理器
 */
public class SignalEventHandler extends AbstractEventHandler {

    public static final String EVENT_HANDLER_TYPE = "signal";

    @Override
    public String getEventHandlerType() {
        return EVENT_HANDLER_TYPE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void handleEvent(EventSubscriptionEntity eventSubscription, Object payload, CommandContext commandContext) {
        if (eventSubscription.getExecutionId() != null) {
            dispatchActivitySignalledEvent(eventSubscription.getExecution(), eventSubscription.getEventName(), payload, commandContext);
            super.handleEvent(eventSubscription, payload, commandContext);
        } else if (eventSubscription.getProcessDefinitionId() != null) {
            // Find initial flow element matching the signal start event
            String processDefinitionId = eventSubscription.getProcessDefinitionId();
            ProcessDefinition processDefinition = ProcessDefinitionUtil.getProcessDefinition(processDefinitionId);
            if (processDefinition == null) {
                throw new ActivitiObjectNotFoundException("No process definition found for id '" + processDefinitionId + "'", ProcessDefinition.class);
            }
            if (processDefinition.isSuspended()) {
                throw new ActivitiException("Could not handle signal: process definition with id: " + processDefinitionId + " is suspended");
            }

            Process process = ProcessDefinitionUtil.getProcess(processDefinitionId, eventSubscription.getProcessInstanceId(), "");
            FlowElement flowElement = process.getFlowElement(eventSubscription.getActivityId(), true);
            if (flowElement == null) {
                throw new ActivitiException("Could not find matching FlowElement for activityId " + eventSubscription.getActivityId());
            }

            // Start process instance via that flow element
            Map<String, Object> variables = null;
            if (payload instanceof Map) {
                variables = (Map<String, Object>) payload;
            }

            ProcessInstanceHelper processInstanceHelper = commandContext.getProcessEngineConfiguration().getProcessInstanceHelper();
            ExecutionEntity executionEntity = processInstanceHelper.createProcessInstanceWithInitialFlowElement(processDefinition, null, null, flowElement, process);
            DelegateExecution execution = executionEntity.getExecutions().get(0);
            dispatchActivitySignalledEvent(execution, eventSubscription.getEventName(), payload, commandContext);
            processInstanceHelper.startProcessInstance(executionEntity, commandContext, variables, flowElement, null);
        } else {
            throw new ActivitiException("Invalid signal handling: no execution nor process definition set");
        }
    }

    protected void dispatchActivitySignalledEvent(DelegateExecution execution, String signalName, Object payload, CommandContext commandContext) {
        if (commandContext.getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
            ActivitiSignalEvent signalEvent = ActivitiEventBuilder.createActivitiySignalledEvent(execution, signalName, payload);
            Context.getProcessEngineConfiguration().getEventDispatcher().dispatchEvent(signalEvent);
        }
    }

}
