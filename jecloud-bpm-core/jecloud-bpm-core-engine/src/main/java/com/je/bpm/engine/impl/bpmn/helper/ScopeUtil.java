/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.helper;

import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.persistence.entity.*;
import com.je.bpm.engine.impl.util.CollectionUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 */
public class ScopeUtil {

  /**
   * we create a separate execution for each compensation handler invocation.
   */
  public static void throwCompensationEvent(List<CompensateEventSubscriptionEntity> eventSubscriptions, DelegateExecution execution, boolean async) {

    ExecutionEntityManager executionEntityManager = Context.getCommandContext().getExecutionEntityManager();

    // first spawn the compensating executions
    for (EventSubscriptionEntity eventSubscription : eventSubscriptions) {
      ExecutionEntity compensatingExecution = null;

      // check whether compensating execution is already created (which is the case when compensating an embedded subprocess,
      // where the compensating execution is created when leaving the subprocess and holds snapshot data).
      if (eventSubscription.getConfiguration() != null) {
        compensatingExecution = executionEntityManager.findById(eventSubscription.getConfiguration());
        compensatingExecution.setParent(compensatingExecution.getProcessInstance());
        compensatingExecution.setEventScope(false);
      } else {
        compensatingExecution = executionEntityManager.createChildExecution((ExecutionEntity) execution);
        eventSubscription.setConfiguration(compensatingExecution.getId());
      }

    }

    // signal compensation events in reverse order of their 'created' timestamp
    Collections.sort(eventSubscriptions, new Comparator<EventSubscriptionEntity>() {
      @Override
      public int compare(EventSubscriptionEntity o1, EventSubscriptionEntity o2) {
        return o2.getCreated().compareTo(o1.getCreated());
      }
    });

    for (CompensateEventSubscriptionEntity compensateEventSubscriptionEntity : eventSubscriptions) {
      Context.getCommandContext().getEventSubscriptionEntityManager().eventReceived(compensateEventSubscriptionEntity, null, async);
    }
  }

  /**
   * Creates a new event scope execution and moves existing event subscriptions to this new execution
   */
  public static void createCopyOfSubProcessExecutionForCompensation(ExecutionEntity subProcessExecution) {
    EventSubscriptionEntityManager eventSubscriptionEntityManager = Context.getCommandContext().getEventSubscriptionEntityManager();
    List<EventSubscriptionEntity> eventSubscriptions = eventSubscriptionEntityManager.findEventSubscriptionsByExecutionAndType(subProcessExecution.getId(), "compensate");

    List<CompensateEventSubscriptionEntity> compensateEventSubscriptions = new ArrayList<CompensateEventSubscriptionEntity>();
    for (EventSubscriptionEntity event : eventSubscriptions) {
      if (event instanceof CompensateEventSubscriptionEntity) {
        compensateEventSubscriptions.add((CompensateEventSubscriptionEntity) event);
      }
    }

    if (CollectionUtil.isNotEmpty(compensateEventSubscriptions)) {

      ExecutionEntity processInstanceExecutionEntity = subProcessExecution.getProcessInstance();

      ExecutionEntity eventScopeExecution = Context.getCommandContext().getExecutionEntityManager().createChildExecution(processInstanceExecutionEntity);
      eventScopeExecution.setActive(false);
      eventScopeExecution.setEventScope(true);
      eventScopeExecution.setCurrentFlowElement(subProcessExecution.getCurrentFlowElement());

      // copy local variables to eventScopeExecution by value. This way,
      // the eventScopeExecution references a 'snapshot' of the local variables
      new SubProcessVariableSnapshotter().setVariablesSnapshots(subProcessExecution, eventScopeExecution);

      // set event subscriptions to the event scope execution:
      for (CompensateEventSubscriptionEntity eventSubscriptionEntity : compensateEventSubscriptions) {
        eventSubscriptionEntityManager.delete(eventSubscriptionEntity);

        CompensateEventSubscriptionEntity newSubscription = eventSubscriptionEntityManager.insertCompensationEvent(
            eventScopeExecution, eventSubscriptionEntity.getActivityId());
        newSubscription.setConfiguration(eventSubscriptionEntity.getConfiguration());
        newSubscription.setCreated(eventSubscriptionEntity.getCreated());
      }

      CompensateEventSubscriptionEntity eventSubscription = eventSubscriptionEntityManager.insertCompensationEvent(
          processInstanceExecutionEntity, eventScopeExecution.getCurrentFlowElement().getId());
      eventSubscription.setConfiguration(eventScopeExecution.getId());
    }
  }
}
