/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.impl.db.HasRevision;
import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.runtime.Execution;
import com.je.bpm.engine.runtime.ProcessInstance;

import java.util.Date;
import java.util.List;

@Internal
public interface ExecutionEntity extends DelegateExecution, Execution, ProcessInstance, Entity, HasRevision {

  void setBusinessKey(String businessKey);

  void setProcessDefinitionId(String processDefinitionId);

  void setProcessDefinitionKey(String processDefinitionKey);

  void setProcessDefinitionName(String processDefinitionName);

  void setProcessDefinitionVersion(Integer processDefinitionVersion);

  void setDeploymentId(String deploymentId);

  ExecutionEntity getProcessInstance();

  void setProcessInstance(ExecutionEntity processInstance);

  @Override
  ExecutionEntity getParent();

  void setParent(ExecutionEntity parent);

  ExecutionEntity getSuperExecution();

  void setSuperExecution(ExecutionEntity superExecution);

  ExecutionEntity getSubProcessInstance();

  void setSubProcessInstance(ExecutionEntity subProcessInstance);

  void setRootProcessInstanceId(String rootProcessInstanceId);

  public void setParentProcessInstanceId(String parentProcessInstanceId);

  ExecutionEntity getRootProcessInstance();

  void setRootProcessInstance(ExecutionEntity rootProcessInstance);

  @Override
  List<? extends ExecutionEntity> getExecutions();

  void addChildExecution(ExecutionEntity executionEntity);

  List<TaskEntity> getTasks();

  List<EventSubscriptionEntity> getEventSubscriptions();

  List<JobEntity> getJobs();

  List<TimerJobEntity> getTimerJobs();

  List<IdentityLinkEntity> getIdentityLinks();

  void setProcessInstanceId(String processInstanceId);

  void setParentId(String parentId);

  void setEnded(boolean isEnded);

  @Override
  void setEventName(String eventName);

  String getDeleteReason();

  void setDeleteReason(String deleteReason);

  int getSuspensionState();

  void setSuspensionState(int suspensionState);

  boolean isEventScope();

  void setEventScope(boolean isEventScope);

  @Override
  boolean isMultiInstanceRoot();

  @Override
  void setMultiInstanceRoot(boolean isMultiInstanceRoot);

  void setName(String name);

  void setDescription(String description);

  void setLocalizedName(String localizedName);

  void setLocalizedDescription(String localizedDescription);

  void setTenantId(String tenantId);

  Date getLockTime();

  void setLockTime(Date lockTime);

  boolean isDeleted();

  void setDeleted(boolean isDeleted);

  void forceUpdate();

  String getStartUserId();

  void setStartUserId(String startUserId);

  Date getStartTime();

  void setStartTime(Date startTime);

}
