/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.image.generator;

import com.je.bpm.core.model.BpmnModel;
import java.io.InputStream;
import java.util.List;

/**
 * This interface declares methods to generate process diagram
 */
public interface ProcessDiagramGenerator {

    /**
     * Generates a diagram of the given process definition, using the diagram interchange information of the process.
     * If there is no interchange information available, an ActivitiInterchangeInfoNotFoundException is thrown.
     *
     * @param bpmnModel             bpmn model to get diagram for
     * @param highLightedActivities activities to highlight
     * @param highLightedFlows      flows to highlight
     * @param activityFontName      override the default activity font
     * @param labelFontName         override the default label font
     */
    InputStream generateDiagram(BpmnModel bpmnModel, List<String> highLightedActivities, List<String> highLightedFlows, String activityFontName, String labelFontName, String annotationFontName);

    /**
     * Generates a diagram of the given process definition, using the diagram interchange information of the process,
     * or the default diagram image, if generateDefaultDiagram param is true.
     *
     * @param bpmnModel              bpmn model to get diagram for
     * @param highLightedActivities  activities to highlight
     * @param highLightedFlows       flows to highlight
     * @param activityFontName       override the default activity font
     * @param labelFontName          override the default label font
     * @param generateDefaultDiagram true if a default diagram should be generated if there is no graphic info available
     */
    InputStream generateDiagram(BpmnModel bpmnModel, List<String> highLightedActivities, List<String> highLightedFlows, String activityFontName, String labelFontName, String annotationFontName, boolean generateDefaultDiagram);

    /**
     * Generates a diagram of the given process definition, using the diagram interchange information of the process,
     * or the default diagram image, if generateDefaultDiagram param is true.
     *
     * @param bpmnModel                   bpmn model to get diagram for
     * @param highLightedActivities       activities to highlight
     * @param highLightedFlows            flows to highlight
     * @param currentActivities           current activities to highlight
     * @param erroredActivities           errored activities to highlight
     * @param activityFontName            override the default activity font
     * @param labelFontName               override the default label font
     * @param generateDefaultDiagram      true if a default diagram should be generated if there is no graphic info available
     * @param defaultDiagramImageFileName override the default diagram image file name
     */
    InputStream generateDiagram(BpmnModel bpmnModel, List<String> highLightedActivities, List<String> highLightedFlows, List<String> currentActivities,
                                List<String> erroredActivities,
                                String activityFontName,
                                String labelFontName,
                                String annotationFontName,
                                boolean generateDefaultDiagram,
                                String defaultDiagramImageFileName);

    /**
     * Generates a diagram of the given process definition, using the diagram interchange information of the process.
     * If there is no interchange information available, an ActivitiInterchangeInfoNotFoundException is thrown.
     *
     * @param bpmnModel             bpmn model to get diagram for
     * @param highLightedActivities activities to highlight
     * @param highLightedFlows      flows to highlight
     */
    InputStream generateDiagram(BpmnModel bpmnModel, List<String> highLightedActivities, List<String> highLightedFlows);

    /**
     * Generates a diagram of the given process definition, using the diagram interchange information of the process.
     * If there is no interchange information available, an ActivitiInterchangeInfoNotFoundException is thrown.
     *
     * @param bpmnModel             bpmn model to get diagram for
     * @param highLightedActivities activities to highlight
     */
    InputStream generateDiagram(BpmnModel bpmnModel, List<String> highLightedActivities);

    /**
     * Generates a diagram of the given process definition, using the diagram interchange information of the process.
     * If there is no interchange information available, an ActivitiInterchangeInfoNotFoundException is thrown.
     *
     * @param bpmnModel bpmn model to get diagram for
     */
    InputStream generateDiagram(BpmnModel bpmnModel, String activityFontName, String labelFontName, String annotationFontName);

    String getDefaultActivityFontName();

    String getDefaultLabelFontName();

    String getDefaultAnnotationFontName();

    String getDefaultDiagramImageFileName();
}
