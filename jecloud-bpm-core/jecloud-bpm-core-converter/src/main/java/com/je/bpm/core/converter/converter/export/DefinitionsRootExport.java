/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.export;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.ExtensionAttribute;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;

public class DefinitionsRootExport implements BpmnXMLConstants {

    /**
     * default namespaces for definitions
     */
    protected static final Set<String> defaultNamespaces = new HashSet(asList(BPMN2_PREFIX, XSI_PREFIX, XSD_PREFIX, ACTIVITI_EXTENSIONS_PREFIX, BPMNDI_PREFIX, OMGDC_PREFIX, OMGDI_PREFIX));

    protected static final List<ExtensionAttribute> defaultAttributes = asList(new ExtensionAttribute(TYPE_LANGUAGE_ATTRIBUTE),
            new ExtensionAttribute(EXPRESSION_LANGUAGE_ATTRIBUTE),
            new ExtensionAttribute(TARGET_NAMESPACE_ATTRIBUTE));

    /**
     *
     * @param model
     * @param xtw
     * @param encoding
     * @throws Exception
     */
    public static void writeRootElement(BpmnModel model, XMLStreamWriter xtw, String encoding) throws Exception {
        xtw.writeStartDocument(encoding, "1.0");
        // start definitions root element
        xtw.writeStartElement(BPMN2_PREFIX, ELEMENT_DEFINITIONS, BPMN2_NAMESPACE);
        xtw.setDefaultNamespace(BPMN2_NAMESPACE);
        xtw.writeDefaultNamespace(BPMN2_NAMESPACE);

        xtw.writeNamespace(BPMN2_PREFIX, BPMN2_NAMESPACE);
        xtw.writeNamespace(XSI_PREFIX, XSI_NAMESPACE);
        xtw.writeNamespace(XSD_PREFIX, SCHEMA_NAMESPACE);
        xtw.writeNamespace(ACTIVITI_EXTENSIONS_PREFIX, ACTIVITI_EXTENSIONS_NAMESPACE);
        xtw.writeNamespace(BPMNDI_PREFIX, BPMNDI_NAMESPACE);
        xtw.writeNamespace(OMGDC_PREFIX, OMGDC_NAMESPACE);
        xtw.writeNamespace(OMGDI_PREFIX, OMGDI_NAMESPACE);
        for (String prefix : model.getNamespaces().keySet()) {
            if (!defaultNamespaces.contains(prefix) && StringUtils.isNotEmpty(prefix)) {
                xtw.writeNamespace(prefix, model.getNamespaces().get(prefix));
            }
        }
        xtw.writeAttribute(TYPE_LANGUAGE_ATTRIBUTE, SCHEMA_NAMESPACE);
        xtw.writeAttribute(EXPRESSION_LANGUAGE_ATTRIBUTE, XPATH_NAMESPACE);
        if (StringUtils.isNotEmpty(model.getTargetNamespace())) {
            xtw.writeAttribute(TARGET_NAMESPACE_ATTRIBUTE, model.getTargetNamespace());
        } else {
            xtw.writeAttribute(TARGET_NAMESPACE_ATTRIBUTE, PROCESS_NAMESPACE);
        }
        BpmnXMLUtil.writeCustomAttributes(model.getDefinitionsAttributes().values(), xtw, model.getNamespaces(), defaultAttributes);
    }

}
