/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child;

import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FormProperty;
import com.je.bpm.core.model.FormValue;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.task.UserTask;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;

public class FormPropertyParser extends BaseChildElementParser {

    @Override
    public String getElementName() {
        return ELEMENT_FORMPROPERTY;
    }

    @Override
    public boolean accepts(BaseElement element) {
        return ((element instanceof UserTask) || (element instanceof StartEvent));
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        if (!accepts(parentElement)) {
            return;
        }
        FormProperty property = new FormProperty();
        BpmnXMLUtil.addXMLLocation(property, xtr);
        property.setId(xtr.getAttributeValue(null, ATTRIBUTE_FORM_ID));
        property.setName(xtr.getAttributeValue(null, ATTRIBUTE_FORM_NAME));
        property.setType(xtr.getAttributeValue(null, ATTRIBUTE_FORM_TYPE));
        property.setVariable(xtr.getAttributeValue(null, ATTRIBUTE_FORM_VARIABLE));
        property.setExpression(xtr.getAttributeValue(null, ATTRIBUTE_FORM_EXPRESSION));
        property.setDefaultExpression(xtr.getAttributeValue(null, ATTRIBUTE_FORM_DEFAULT));
        property.setDatePattern(xtr.getAttributeValue(null, ATTRIBUTE_FORM_DATEPATTERN));
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_FORM_REQUIRED))) {
            property.setRequired(Boolean.valueOf(xtr.getAttributeValue(null, ATTRIBUTE_FORM_REQUIRED)));
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_FORM_READABLE))) {
            property.setReadable(Boolean.valueOf(xtr.getAttributeValue(null, ATTRIBUTE_FORM_READABLE)));
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_FORM_WRITABLE))) {
            property.setWriteable(Boolean.valueOf(xtr.getAttributeValue(null, ATTRIBUTE_FORM_WRITABLE)));
        }

        boolean readyWithFormProperty = false;
        try {
            while (!readyWithFormProperty && xtr.hasNext()) {
                xtr.next();
                if (xtr.isStartElement() && ELEMENT_VALUE.equalsIgnoreCase(xtr.getLocalName())) {
                    FormValue value = new FormValue();
                    BpmnXMLUtil.addXMLLocation(value,
                            xtr);
                    value.setId(xtr.getAttributeValue(null,
                            ATTRIBUTE_ID));
                    value.setName(xtr.getAttributeValue(null,
                            ATTRIBUTE_NAME));
                    property.getFormValues().add(value);
                } else if (xtr.isEndElement() && getElementName().equalsIgnoreCase(xtr.getLocalName())) {
                    readyWithFormProperty = true;
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Error parsing form properties child elements",
                    e);
        }

        if (parentElement instanceof UserTask) {
            ((UserTask) parentElement).getFormProperties().add(property);
        } else {
            ((StartEvent) parentElement).getFormProperties().add(property);
        }
    }
}
