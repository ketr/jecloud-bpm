/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.export;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.model.config.process.ExtendedConfigurationConfigImpl;
import com.je.bpm.core.model.process.Process;

import javax.xml.stream.XMLStreamWriter;

/**
 * 流程配置导出
 */
public class ProccessExtendedConfigExport implements BpmnXMLConstants {

    public static void writeConfigs(Process process, XMLStreamWriter xtw) throws Exception {
        ExtendedConfigurationConfigImpl processExtendedConfiguration = process.getExtendedConfiguration();
        xtw.writeStartElement(BPMN2_PREFIX, ELEMENT_PROCESS_EXTENDED_CONFIG, BPMN2_NAMESPACE);
        xtw.writeAttribute(ATTRIBUTE_ID, processExtendedConfiguration.getCustomeEventType());
        xtw.writeAttribute(ATTRIBUTE_PROCESS_CANSPONSOR, String.valueOf(processExtendedConfiguration.isCanSponsor()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_CANRETRIEVE, String.valueOf(processExtendedConfiguration.isCanRetrieve()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_CANRETURN, String.valueOf(processExtendedConfiguration.isCanReturn()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_CANURGED, String.valueOf(processExtendedConfiguration.isCanUrged()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_CANCANCEL, String.valueOf(processExtendedConfiguration.isCanCancel()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_CANINVALID, String.valueOf(processExtendedConfiguration.isCanInvalid()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_CANTRANSFER, String.valueOf(processExtendedConfiguration.isCanTransfer()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_CANDELEGATE, String.valueOf(processExtendedConfiguration.isCanDelegate()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_COUNTERSIGNNODE, String.valueOf(processExtendedConfiguration.isCountersignNode()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_EASYLAUNCH, String.valueOf(processExtendedConfiguration.isEasyLaunch()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_SIMPLEAPPROVAL, String.valueOf(processExtendedConfiguration.isSimpleApproval()));
        xtw.writeAttribute(ATTRIBUTE_ENABLE_SIMPLE_COMMENTS, String.valueOf(processExtendedConfiguration.isEnableSimpleComments()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_EXIGENCY, String.valueOf(processExtendedConfiguration.isExigency()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_HIDESTATEINFO, String.valueOf(processExtendedConfiguration.isHideStateInfo()));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_HIDETIMEINFO, String.valueOf(processExtendedConfiguration.isHideTimeInfo()));
        xtw.writeEndElement();
    }
}

