/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.export;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.GraphicInfo;
import com.je.bpm.core.model.MessageFlow;
import com.je.bpm.core.model.process.SubProcess;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamWriter;
import java.util.List;

public class BPMNDIExport implements BpmnXMLConstants {

    public static void writeBPMNDI(BpmnModel model, XMLStreamWriter xtw) throws Exception {
        // BPMN DI information
        xtw.writeStartElement(BPMNDI_PREFIX, ELEMENT_DI_DIAGRAM, BPMNDI_NAMESPACE);

        String processId = null;
        if (!model.getPools().isEmpty()) {
            processId = "Collaboration";
        } else {
            processId = model.getMainProcess().getId();
        }

        xtw.writeAttribute(ATTRIBUTE_ID, "BPMNDiagram_" + processId);

        xtw.writeStartElement(BPMNDI_PREFIX, ELEMENT_DI_PLANE, BPMNDI_NAMESPACE);
        xtw.writeAttribute(ATTRIBUTE_DI_BPMNELEMENT, processId);
        xtw.writeAttribute(ATTRIBUTE_ID, "BPMNPlane_" + processId);

        for (String elementId : model.getLocationMap().keySet()) {

            if (model.getFlowElement(elementId) != null || model.getArtifact(elementId) != null || model.getPool(elementId) != null || model.getLane(elementId) != null) {

                xtw.writeStartElement(BPMNDI_PREFIX, ELEMENT_DI_SHAPE, BPMNDI_NAMESPACE);
                xtw.writeAttribute(ATTRIBUTE_DI_BPMNELEMENT, elementId);
                xtw.writeAttribute(ATTRIBUTE_ID, "BPMNShape_" + elementId);

                GraphicInfo graphicInfo = model.getGraphicInfo(elementId);
                FlowElement flowElement = model.getFlowElement(elementId);
                if (flowElement instanceof SubProcess && graphicInfo.getExpanded() != null) {
                    xtw.writeAttribute(ATTRIBUTE_DI_IS_EXPANDED, String.valueOf(graphicInfo.getExpanded()));
                }

                xtw.writeStartElement(OMGDC_PREFIX, ELEMENT_DI_BOUNDS, OMGDC_NAMESPACE);
                xtw.writeAttribute(ATTRIBUTE_DI_HEIGHT, "" + graphicInfo.getHeight());
                xtw.writeAttribute(ATTRIBUTE_DI_WIDTH, "" + graphicInfo.getWidth());
                xtw.writeAttribute(ATTRIBUTE_DI_X, "" + graphicInfo.getX());
                xtw.writeAttribute(ATTRIBUTE_DI_Y, "" + graphicInfo.getY());
                xtw.writeEndElement();

                xtw.writeEndElement();
            }
        }

        for (String elementId : model.getFlowLocationMap().keySet()) {

            if (model.getFlowElement(elementId) != null || model.getArtifact(elementId) != null || model.getMessageFlow(elementId) != null) {

                xtw.writeStartElement(BPMNDI_PREFIX, ELEMENT_DI_EDGE, BPMNDI_NAMESPACE);
                xtw.writeAttribute(ATTRIBUTE_DI_BPMNELEMENT, elementId);
                xtw.writeAttribute(ATTRIBUTE_ID, "BPMNEdge_" + elementId);

                List<GraphicInfo> graphicInfoList = model.getFlowLocationGraphicInfo(elementId);
                for (GraphicInfo graphicInfo : graphicInfoList) {
                    xtw.writeStartElement(OMGDI_PREFIX, ELEMENT_DI_WAYPOINT, OMGDI_NAMESPACE);
                    xtw.writeAttribute(ATTRIBUTE_DI_X, "" + graphicInfo.getX());
                    xtw.writeAttribute(ATTRIBUTE_DI_Y, "" + graphicInfo.getY());
                    xtw.writeEndElement();
                }

                GraphicInfo labelGraphicInfo = model.getLabelGraphicInfo(elementId);
                FlowElement flowElement = model.getFlowElement(elementId);
                MessageFlow messageFlow = null;
                if (flowElement == null) {
                    messageFlow = model.getMessageFlow(elementId);
                }

                boolean hasName = false;
                if (flowElement != null && StringUtils.isNotEmpty(flowElement.getName())) {
                    hasName = true;

                } else if (messageFlow != null && StringUtils.isNotEmpty(messageFlow.getName())) {
                    hasName = true;
                }

                if (labelGraphicInfo != null && hasName) {
                    xtw.writeStartElement(BPMNDI_PREFIX, ELEMENT_DI_LABEL, BPMNDI_NAMESPACE);
                    xtw.writeStartElement(OMGDC_PREFIX, ELEMENT_DI_BOUNDS, OMGDC_NAMESPACE);
                    xtw.writeAttribute(ATTRIBUTE_DI_HEIGHT, "" + labelGraphicInfo.getHeight());
                    xtw.writeAttribute(ATTRIBUTE_DI_WIDTH, "" + labelGraphicInfo.getWidth());
                    xtw.writeAttribute(ATTRIBUTE_DI_X, "" + labelGraphicInfo.getX());
                    xtw.writeAttribute(ATTRIBUTE_DI_Y, "" + labelGraphicInfo.getY());
                    xtw.writeEndElement();
                    xtw.writeEndElement();
                }

                xtw.writeEndElement();
            }
        }

        // end BPMN DI elements
        xtw.writeEndElement();
        xtw.writeEndElement();
    }
}
