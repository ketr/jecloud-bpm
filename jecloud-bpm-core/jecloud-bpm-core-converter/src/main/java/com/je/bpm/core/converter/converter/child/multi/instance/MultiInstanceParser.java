/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child.multi.instance;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.converter.converter.child.ElementParser;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.Activity;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.MultiInstanceLoopCharacteristics;

import javax.xml.stream.XMLStreamReader;
import java.util.List;

import static java.util.Arrays.asList;

public class MultiInstanceParser extends BaseChildElementParser {

    private final List<ElementParser<MultiInstanceLoopCharacteristics>> multiInstanceElementParsers;

    public MultiInstanceParser() {
        this(asList(new LoopCardinalityParser(),
                new MultiInstanceDataInputParser(),
                new MultiInstanceInputDataItemParser(),
                new MultiInstanceCompletionConditionParser(),
                new LoopDataOutputRefParser(),
                new MultiInstanceOutputDataItemParser(),
                new MultiInstanceAttributesParser()
        ));
    }

    public MultiInstanceParser(List<ElementParser<MultiInstanceLoopCharacteristics>> multiInstanceElementParsers) {
        this.multiInstanceElementParsers = multiInstanceElementParsers;
    }

    @Override
    public String getElementName() {
        return ELEMENT_MULTIINSTANCE;
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        if (!(parentElement instanceof Activity)) {
            return;
        }
        MultiInstanceLoopCharacteristics multiInstanceDef = new MultiInstanceLoopCharacteristics();
        BpmnXMLUtil.addXMLLocation(multiInstanceDef, xtr);

        parseMultiInstanceProperties(xtr, multiInstanceDef);

        ((Activity) parentElement).setLoopCharacteristics(multiInstanceDef);
    }

    private void parseMultiInstanceProperties(XMLStreamReader xtr, MultiInstanceLoopCharacteristics multiInstanceDef) {
        boolean readyWithMultiInstance = false;
        try {
            do {
                ElementParser<MultiInstanceLoopCharacteristics> matchingParser = multiInstanceElementParsers
                        .stream()
                        .filter(elementParser -> elementParser.canParseCurrentElement(xtr))
                        .findFirst()
                        .orElse(null);
                if (matchingParser != null) {
                    matchingParser.setInformation(xtr, multiInstanceDef);
                }
                if (xtr.isEndElement() && getElementName().equalsIgnoreCase(xtr.getLocalName())) {
                    readyWithMultiInstance = true;
                }
                if (xtr.hasNext()) {
                    xtr.next();
                }
            } while (!readyWithMultiInstance && xtr.hasNext());
        } catch (Exception e) {
            LOGGER.warn("Error parsing multi instance definition",
                    e);
        }
    }

}
