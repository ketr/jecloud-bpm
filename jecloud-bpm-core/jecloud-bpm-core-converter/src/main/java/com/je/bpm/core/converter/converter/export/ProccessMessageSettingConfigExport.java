/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.export;

import com.google.common.base.Strings;
import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.model.config.ProcessRemindTypeEnum;
import com.je.bpm.core.model.config.process.MessageSettingConfigImpl;
import com.je.bpm.core.model.config.process.ProcessRemindTemplate;
import com.je.bpm.core.model.process.Process;

import javax.xml.stream.XMLStreamWriter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 流程消息
 */
public class ProccessMessageSettingConfigExport implements BpmnXMLConstants {

    public static void writeConfigs(Process process, XMLStreamWriter xtw) throws Exception {
        MessageSettingConfigImpl messageSettingConfig = process.getMessageSetting();
        List<ProcessRemindTemplate> processRemindTemplates = messageSettingConfig.getMessageDefinitions();
        xtw.writeStartElement(BPMN2_PREFIX, ATTRIBUTE_PROCESS_MESSAGESETTING_CONFIG, BPMN2_NAMESPACE);
        List<ProcessRemindTypeEnum> processRemindTypeEnums = messageSettingConfig.getMessages();
        String messages = processRemindTypeEnums.stream().map(Enum::toString).collect(Collectors.joining(","));
        xtw.writeAttribute(ATTRIBUTE_PROCESS_STARTUPSETTINGS_MESSAGES, messages);
        xtw.writeAttribute(ATTRIBUTE_PROCESS_STARTUPSETTINGS_MESSAGES_DINGTALK_ID, messageSettingConfig.getJeCloudDingTalkId());
        xtw.writeAttribute(ATTRIBUTE_PROCESS_STARTUPSETTINGS_MESSAGES_DINGTALK_NAME, messageSettingConfig.getJeCloudDingTalkName());
        for (ProcessRemindTemplate processRemindTemplate : processRemindTemplates) {
            xtw.writeStartElement(BPMN2_PREFIX, ATTRIBUTE_PROCESS_MESSAGESETTING_DEFINITIONS_CONFIG, BPMN2_NAMESPACE);
            xtw.writeAttribute(ATTRIBUTE_ID, new ProcessRemindTemplate().getConfigType());
            if (processRemindTemplate.getType() == null) {
                xtw.writeAttribute(ATTRIBUTE_PROCESS_MESSAGESETTING_DEFINITIONS_CODE, "");
            } else {
                xtw.writeAttribute(ATTRIBUTE_PROCESS_MESSAGESETTING_DEFINITIONS_CODE, processRemindTemplate.getType().toString());
            }

            if (Strings.isNullOrEmpty(processRemindTemplate.getTemplate())) {
                xtw.writeAttribute(ATTRIBUTE_PROCESS_MESSAGESETTING_DEFINITIONS_TEXT, "");
            } else {
                xtw.writeAttribute(ATTRIBUTE_PROCESS_MESSAGESETTING_DEFINITIONS_TEXT, processRemindTemplate.getTemplate());
            }

            xtw.writeEndElement();
        }
        xtw.writeEndElement();
    }
}

