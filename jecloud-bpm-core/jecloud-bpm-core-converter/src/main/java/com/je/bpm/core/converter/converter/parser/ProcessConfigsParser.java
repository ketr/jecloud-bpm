/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.parser;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.PredefinedTypeEnum;
import com.je.bpm.core.model.config.ProcessDeployTypeEnum;
import com.je.bpm.core.model.config.ProcessRemindTypeEnum;
import com.je.bpm.core.model.config.process.ProcessBasicConfigImpl;
import com.je.bpm.core.model.process.Process;
import org.apache.commons.lang3.StringUtils;
import javax.xml.stream.XMLStreamReader;

/**
 * 流程属性配置
 */
public class ProcessConfigsParser extends BaseChildElementParser {

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        ProcessBasicConfigImpl processConfig = new ProcessBasicConfigImpl();
        BpmnXMLUtil.addXMLLocation(processConfig, xtr);
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_ID))) {
//            System.out.println(xtr.getAttributeValue(null, ATTRIBUTE_ID));
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_ATTACHEDFUNCNAMES))) {
//            System.out.println(xtr.getAttributeValue(null, ATTRIBUTE_ATTACHEDFUNCNAMES));
        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANRETRIEVE))) {
//            processConfig.setCanRetrieve(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANRETRIEVE)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANSPONSOR))) {
//            processConfig.setCanSponsor(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANSPONSOR)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANTRANSFER))) {
//            processConfig.setCanTransfer(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANTRANSFER)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANDELEGATE))) {
//            processConfig.setCanDelegate(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANDELEGATE)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANURGED))) {
//            processConfig.setCanUrged(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANURGED)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANRETURN))) {
//            processConfig.setCanReturn(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANRETURN)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANINVALID))) {
//            processConfig.setCanInvalid(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANINVALID)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANCANCEL))) {
//            processConfig.setCanCancel(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANCANCEL)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_PREDEFINED))) {
//            processConfig.setPredefined(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_PREDEFINED)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANEVERYONESTART))) {
//            processConfig.setCanEveryoneStart(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANEVERYONESTART)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_PREDEFINETYPE))) {
//            processConfig.setPredefineType(PredefinedTypeEnum.getType(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_PREDEFINETYPE)));
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_REMINDTYPES))) {
//            String remaindTypes = xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_REMINDTYPES);
//            String[] types = remaindTypes.split(",");
//            for (String eachType : types) {
//                processConfig.getRemindTypes().add(ProcessRemindTypeEnum.getType(eachType));
//            }
//        }
//        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_DISPLAYTRACEBUTTON))) {
//            processConfig.setDisplayTraceButton(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_DISPLAYTRACEBUTTON)));
//        }

        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_FUNCCODE))) {
            processConfig.setFuncCode(xtr.getAttributeValue(null, ATTRIBUTE_FUNCCODE));
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_FUNCNAME))) {
            processConfig.setFuncCode(xtr.getAttributeValue(null, ATTRIBUTE_FUNCNAME));
        }

        Process parentProcess = (Process) parentElement;
        parentProcess.setProcessConfig(processConfig);
    }

    @Override
    public String getElementName() {
        return ELEMENT_PROCESS_CONFIG;
    }

}
