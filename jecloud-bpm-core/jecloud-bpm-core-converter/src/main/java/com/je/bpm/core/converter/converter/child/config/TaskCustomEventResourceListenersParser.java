/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child.config;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.core.model.task.KaiteBaseUserTask;

import javax.xml.stream.XMLStreamReader;

/**
 * 任务自定义事件解析
 */
public class TaskCustomEventResourceListenersParser extends BaseChildElementParser {

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        KaiteBaseUserTask kaiteUserTask = (KaiteBaseUserTask) parentElement;
        CustomEvent customEvent = new CustomEvent();
        customEvent.setType(CustomEvent.CustomEventEnum.getEventByCode(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CUSTOM_EVENT_TYPECODE)));
        customEvent.setExecutionStrategy(CustomEvent.CustomExecutionStrategyEnum.getExecutionStrategyByCode(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CUSTOM_EVENT_EXECUTIONSTRATEGYNAME)));
        customEvent.setAssignmentFieldConfiguration(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CUSTOM_EVENT_ASSIGNMENTFIELDCONFIGURATION));
        customEvent.setServiceName(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CUSTOM_EVENT_SERVICENAME));
        customEvent.setMethod(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CUSTOM_EVENT_METHOD));
        customEvent.setExistenceParameter(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CUSTOM_EVENT_EXISTENCEPARAMETER)));
        kaiteUserTask.getCustomEventListeners().add(customEvent);
    }

    @Override
    public String getElementName() {
        return ATTRIBUTE_PROCESS_CUSTOM_TASK_EVENT_CODE;
    }

}
