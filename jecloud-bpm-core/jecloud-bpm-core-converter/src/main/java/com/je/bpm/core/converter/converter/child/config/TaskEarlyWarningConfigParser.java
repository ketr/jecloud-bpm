/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child.config;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.task.TaskEarlyWarningAndPostponementConfigImpl;
import com.je.bpm.core.model.task.KaiteBaseUserTask;

import javax.xml.stream.XMLStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * 任务预警配置解析
 */
public class TaskEarlyWarningConfigParser extends BaseChildElementParser {

    private Map<String, BaseChildElementParser> childParserMap = new HashMap();

    public TaskEarlyWarningConfigParser() {
        TaskEarlyWarningStrategyConfigParser taskEarlyWarningStrategyConfigParser = new TaskEarlyWarningStrategyConfigParser();
        childParserMap.put(taskEarlyWarningStrategyConfigParser.getElementName(), taskEarlyWarningStrategyConfigParser);
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        KaiteBaseUserTask kaiteUserTask = (KaiteBaseUserTask) parentElement;
        TaskEarlyWarningAndPostponementConfigImpl taskEarlyWarningConfig = kaiteUserTask.getTaskEarlyWarningAndPostponementConfig();
        BpmnXMLUtil.addXMLLocation(taskEarlyWarningConfig, xtr);
        taskEarlyWarningConfig.setEnabled(getBoolean(xtr.getAttributeValue(null, ATTRIBUTEE_NABLE)));
        taskEarlyWarningConfig.setProcessingTimeLimitDuration(xtr.getAttributeValue(null, PROCESSING_TIME_LIMIT_DURATION));
        taskEarlyWarningConfig.setProcessingTimeLimitUnitName(xtr.getAttributeValue(null, PROCESSING_TIME_LIMIT_UNIT_NAME));
        taskEarlyWarningConfig.setProcessingTimeLimitUnitCode(xtr.getAttributeValue(null, PROCESSING_TIME_LIMIT_UNIT_CODE));
        taskEarlyWarningConfig.setWarningTimeLimitDuration(xtr.getAttributeValue(null, WARNING_TIME_LIMIT_DURATION));
        taskEarlyWarningConfig.setWarningTimeLimitUnitName(xtr.getAttributeValue(null, WARNING_TIME_LIMIT_UNIT_NAME));
        taskEarlyWarningConfig.setWarningTimeLimitUnitCode(xtr.getAttributeValue(null, WARNING_TIME_LIMIT_UNIT_CODE));
        taskEarlyWarningConfig.setReminderFrequencyDuration(xtr.getAttributeValue(null, REMINDER_FREQUENCY_DURATION));
        taskEarlyWarningConfig.setReminderFrequencyUnitName(xtr.getAttributeValue(null, REMINDER_FREQUENCY_UNIT_NAME));
        taskEarlyWarningConfig.setReminderFrequencyUnitCode(xtr.getAttributeValue(null, REMINDER_FREQUENCY_UNIT_CODE));
        parseChildElements(getElementName(), parentElement, childParserMap, model, xtr);
    }

    @Override
    public String getElementName() {
        return PROPERTY_TASK_EARLYWARNINGANDPOSTPONEMENT_CONFIG;
    }

}
