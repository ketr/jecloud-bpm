/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter;

import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.ErrorEventDefinition;
import com.je.bpm.core.model.EventDefinition;
import com.je.bpm.core.model.event.BoundaryEvent;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;


public class BoundaryEventXMLConverter extends BaseBpmnXMLConverter {

    @Override
    public Class<? extends BaseElement> getBpmnElementType() {
        return BoundaryEvent.class;
    }

    @Override
    protected String getXMLElementName() {
        return ELEMENT_EVENT_BOUNDARY;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        BoundaryEvent boundaryEvent = new BoundaryEvent();
        BpmnXMLUtil.addXMLLocation(boundaryEvent, xtr);
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_BOUNDARY_CANCELACTIVITY))) {
            String cancelActivity = xtr.getAttributeValue(null, ATTRIBUTE_BOUNDARY_CANCELACTIVITY);
            if (ATTRIBUTE_VALUE_FALSE.equalsIgnoreCase(cancelActivity)) {
                boundaryEvent.setCancelActivity(false);
            }
        }
        boundaryEvent.setAttachedToRefId(xtr.getAttributeValue(null, ATTRIBUTE_BOUNDARY_ATTACHEDTOREF));
        parseChildElements(getXMLElementName(), boundaryEvent, model, xtr);
        // Explicitly set cancel activity to false for error boundary events
        if (boundaryEvent.getEventDefinitions().size() == 1) {
            EventDefinition eventDef = boundaryEvent.getEventDefinitions().get(0);

            if (eventDef instanceof ErrorEventDefinition) {
                boundaryEvent.setCancelActivity(false);
            }
        }
        return boundaryEvent;
    }

    @Override
    protected void writeAdditionalAttributes(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {
        BoundaryEvent boundaryEvent = (BoundaryEvent) element;
        if (boundaryEvent.getAttachedToRef() != null) {
            writeDefaultAttribute(ATTRIBUTE_BOUNDARY_ATTACHEDTOREF, boundaryEvent.getAttachedToRef().getId(), xtw);
        }
        if (boundaryEvent.getEventDefinitions().size() == 1) {
            EventDefinition eventDef = boundaryEvent.getEventDefinitions().get(0);
            if (eventDef instanceof ErrorEventDefinition == false) {
                writeDefaultAttribute(ATTRIBUTE_BOUNDARY_CANCELACTIVITY, String.valueOf(boundaryEvent.isCancelActivity()).toLowerCase(), xtw);
            }
        }
    }

    @Override
    protected void writeAdditionalChildElements(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {
        BoundaryEvent boundaryEvent = (BoundaryEvent) element;
        writeEventDefinitions(boundaryEvent, boundaryEvent.getEventDefinitions(), model, xtw);
    }
}
