/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child;

import com.google.common.base.Strings;
import com.je.bpm.core.converter.converter.BaseBpmnXMLConverter;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.process.MessageSettingConfigImpl;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.util.HashMap;
import java.util.Map;


public class ProcessMessageConfigParser extends BaseBpmnXMLConverter {

    private static final Map<String, BaseChildElementParser> childParsers = new HashMap<>();

    private ProcessMessageDefinitionConfigParser processMessageDefinitionConfigParser = new ProcessMessageDefinitionConfigParser();

    public ProcessMessageConfigParser() {
        childParsers.put(processMessageDefinitionConfigParser.getElementName(), processMessageDefinitionConfigParser);
    }


    @Override
    protected Class<? extends BaseElement> getBpmnElementType() {
        return MessageSettingConfigImpl.class;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        MessageSettingConfigImpl messageSetting = new MessageSettingConfigImpl();
        BpmnXMLUtil.addXMLLocation(messageSetting, xtr);
        String messages = xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_STARTUPSETTINGS_MESSAGES);
        String jeCloudDingTalkId = xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_STARTUPSETTINGS_MESSAGES_DINGTALK_ID);
        String jeCloudDingTalkName = xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_STARTUPSETTINGS_MESSAGES_DINGTALK_NAME);
        messageSetting.setJeCloudDingTalkId(jeCloudDingTalkId);
        messageSetting.setJeCloudDingTalkName(jeCloudDingTalkName);
        if (!Strings.isNullOrEmpty(messages)) {
            String[] messageArray = messages.split(",");
            for (String message : messageArray) {
                messageSetting.addProcessRemindTypeEnum(message);
            }
        }
        BpmnXMLUtil.parseChildElements(getXMLElementName(), messageSetting, xtr, childParsers, model);
        model.getMainProcess().setMessageSetting(messageSetting);
        return messageSetting;
    }

    @Override
    protected String getXMLElementName() {
        return ATTRIBUTE_PROCESS_MESSAGESETTING_CONFIG;
    }

    @Override
    protected void writeAdditionalAttributes(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {

    }

    @Override
    protected void writeAdditionalChildElements(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {

    }
}
