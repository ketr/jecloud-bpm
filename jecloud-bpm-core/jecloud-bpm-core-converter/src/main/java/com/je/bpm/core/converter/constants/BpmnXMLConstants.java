/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.constants;

public interface BpmnXMLConstants {

    String BPMN2_NAMESPACE = "http://www.omg.org/spec/BPMN/20100524/MODEL";
    String BPMN2_PREFIX = "bpmn2";
    String XSI_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance";
    String XSI_PREFIX = "xsi";
    String SCHEMA_NAMESPACE = "http://www.w3.org/2001/XMLSchema";
    String XSD_PREFIX = "xsd";
    String TYPE_LANGUAGE_ATTRIBUTE = "typeLanguage";
    String XPATH_NAMESPACE = "http://www.w3.org/1999/XPath";
    String EXPRESSION_LANGUAGE_ATTRIBUTE = "expressionLanguage";
    String PROCESS_NAMESPACE = "http://www.jecloud.net/bpmn";
    String TARGET_NAMESPACE_ATTRIBUTE = "targetNamespace";
    String ACTIVITI_EXTENSIONS_NAMESPACE = "http://activiti.org/bpmn";
    String ACTIVITI_EXTENSIONS_PREFIX = "activiti";
    String ACTIVITI_JECLOUD_NAMESPACE = "http://jecloud.com/bpmn";
    String ACTIVITI_JECLOUD_PREFIX = "jecloud";
    String BPMNDI_NAMESPACE = "http://www.omg.org/spec/BPMN/20100524/DI";
    String BPMNDI_PREFIX = "bpmndi";
    String OMGDC_NAMESPACE = "http://www.omg.org/spec/DD/20100524/DC";
    String OMGDC_PREFIX = "omgdc";
    String OMGDI_NAMESPACE = "http://www.omg.org/spec/DD/20100524/DI";
    String OMGDI_PREFIX = "omgdi";

    String ATTRIBUTE_ID = "id";
    String ATTRIBUTE_NAME = "name";
    String ATTRIBUTE_CODE = "code";
    String ATTRIBUTE_TYPE = "type";
    String ATTRIBUTEE_NABLE = "enable";
    String ATTRIBUTE_DEFAULT = "default";
    String ATTRIBUTE_ITEM_REF = "itemRef";
    String ELEMENT_DEFINITIONS = "definitions";
    String ELEMENT_DOCUMENTATION = "documentation";


    String ELEMENT_SIGNAL = "signal";
    String ELEMENT_MESSAGE = "message";
    String ELEMENT_ERROR = "error";
    String ELEMENT_COLLABORATION = "collaboration";
    String ELEMENT_PARTICIPANT = "participant";
    String ELEMENT_MESSAGE_FLOW = "messageFlow";
    String ELEMENT_LANESET = "laneSet";
    String ELEMENT_LANE = "lane";
    String ELEMENT_FLOWNODE_REF = "flowNodeRef";
    String ATTRIBUTE_PROCESS_REF = "processRef";
    String ELEMENT_RESOURCE = "resource";

    String ELEMENT_PROCESS = "process";
    String ATTRIBUTE_PROCESS_EXECUTABLE = "isExecutable";
    String ELEMENT_POTENTIAL_STARTER = "potentialStarter";
    String ATTRIBUTE_PROCESS_CANDIDATE_USERS = "candidateStarterUsers";
    String ATTRIBUTE_PROCESS_CANDIDATE_GROUPS = "candidateStarterGroups";
    String ELEMENT_SUBPROCESS = "subProcess";
    String ATTRIBUTE_TRIGGERED_BY = "triggeredByEvent";
    String ELEMENT_TRANSACTION = "transaction";
    String ELEMENT_ADHOC_SUBPROCESS = "adHocSubProcess";
    String ATTRIBUTE_ORDERING = "ordering";
    String ATTRIBUTE_CANCEL_REMAINING_INSTANCES = "cancelRemainingInstances";
    String ELEMENT_COMPLETION_CONDITION = "completionCondition";
    String ATTRIBUTE_MESSAGE_EXPRESSION = "messageExpression";
    String ATTRIBUTE_MESSAGE_CORRELATION_KEY = "correlationKey";
    String ATTRIBUTE_SIGNAL_EXPRESSION = "signalExpression";

    String ELEMENT_DATA_STATE = "dataState";

    String ELEMENT_EXTENSIONS = "extensionElements";

    String ELEMENT_EXECUTION_LISTENER = "executionListener";
    String ELEMENT_EVENT_LISTENER = "eventListener";
    String ELEMENT_TASK_LISTENER = "taskListener";
    String ATTRIBUTE_LISTENER_EVENT = "event";
    String ATTRIBUTE_LISTENER_EVENTS = "events";
    String ATTRIBUTE_LISTENER_ENTITY_TYPE = "entityType";
    String ATTRIBUTE_LISTENER_CLASS = "class";
    String ATTRIBUTE_LISTENER_EXPRESSION = "expression";
    String ATTRIBUTE_LISTENER_DELEGATEEXPRESSION = "delegateExpression";
    String ATTRIBUTE_LISTENER_THROW_EVENT_TYPE = "throwEvent";
    String ATTRIBUTE_LISTENER_THROW_SIGNAL_EVENT_NAME = "signalName";
    String ATTRIBUTE_LISTENER_THROW_MESSAGE_EVENT_NAME = "messageName";
    String ATTRIBUTE_LISTENER_THROW_ERROR_EVENT_CODE = "errorCode";
    String ATTRIBUTE_LISTENER_ON_TRANSACTION = "onTransaction";
    String ATTRIBUTE_LISTENER_CUSTOM_PROPERTIES_RESOLVER_CLASS = "customPropertiesResolverClass";
    String ATTRIBUTE_LISTENER_CUSTOM_PROPERTIES_RESOLVER_EXPRESSION = "customPropertiesResolverExpression";
    String ATTRIBUTE_LISTENER_CUSTOM_PROPERTIES_RESOLVER_DELEGATEEXPRESSION = "customPropertiesResolverDelegateExpression";

    String ATTRIBUTE_LISTENER_THROW_EVENT_TYPE_SIGNAL = "signal";
    String ATTRIBUTE_LISTENER_THROW_EVENT_TYPE_GLOBAL_SIGNAL = "globalSignal";
    String ATTRIBUTE_LISTENER_THROW_EVENT_TYPE_MESSAGE = "message";
    String ATTRIBUTE_LISTENER_THROW_EVENT_TYPE_ERROR = "error";

    String ATTRIBUTE_VALUE_TRUE = "true";
    String ATTRIBUTE_VALUE_FALSE = "false";

    String ATTRIBUTE_ACTIVITY_ASYNCHRONOUS = "async";
    String ATTRIBUTE_ACTIVITY_EXCLUSIVE = "exclusive";
    String ATTRIBUTE_ACTIVITY_ISFORCOMPENSATION = "isForCompensation";

    String ELEMENT_IMPORT = "import";
    String ATTRIBUTE_IMPORT_TYPE = "importType";
    String ATTRIBUTE_LOCATION = "location";
    String ATTRIBUTE_NAMESPACE = "namespace";

    String ELEMENT_INTERFACE = "interface";
    String ELEMENT_OPERATION = "operation";
    String ATTRIBUTE_IMPLEMENTATION_REF = "implementationRef";
    String ELEMENT_IN_MESSAGE = "inMessageRef";
    String ELEMENT_OUT_MESSAGE = "outMessageRef";

    String ELEMENT_ITEM_DEFINITION = "itemDefinition";
    String ATTRIBUTE_STRUCTURE_REF = "structureRef";
    String ATTRIBUTE_ITEM_KIND = "itemKind";

    String ELEMENT_DATA_STORE = "dataStore";
    String ELEMENT_DATA_STORE_REFERENCE = "dataStoreReference";
    String ATTRIBUTE_ITEM_SUBJECT_REF = "itemSubjectRef";
    String ATTRIBUTE_DATA_STORE_REF = "dataStoreRef";

    String ELEMENT_IOSPECIFICATION = "ioSpecification";
    String ELEMENT_DATA_INPUT = "dataInput";
    String ELEMENT_DATA_OUTPUT = "dataOutput";
    String ELEMENT_DATA_INPUT_REFS = "dataInputRefs";
    String ELEMENT_DATA_OUTPUT_REFS = "dataOutputRefs";

    String ELEMENT_INPUT_ASSOCIATION = "dataInputAssociation";
    String ELEMENT_OUTPUT_ASSOCIATION = "dataOutputAssociation";
    String ELEMENT_SOURCE_REF = "sourceRef";
    String ELEMENT_TARGET_REF = "targetRef";
    String ELEMENT_TRANSFORMATION = "transformation";
    String ELEMENT_ASSIGNMENT = "assignment";
    String ELEMENT_FROM = "from";
    String ELEMENT_TO = "to";

    // fake element for mail task
    String ELEMENT_TASK_MAIL = "mailTask";

    String ELEMENT_TASK = "task";
    String ELEMENT_TASK_BUSINESSRULE = "businessRuleTask";
    String ELEMENT_TASK_MANUAL = "manualTask";
    String ELEMENT_TASK_RECEIVE = "receiveTask";
    String ELEMENT_TASK_SCRIPT = "scriptTask";
    String ELEMENT_TASK_SEND = "sendTask";
    String ELEMENT_TASK_SERVICE = "serviceTask";
    String ELEMENT_TASK_USER = "userTask";

    //-------------------------------自定义节点-------------------------------

    /**
     * 凯特用户任务
     */
    String ELEMENT_KAITE_USERTASK = "kaiteUserTask";
    /**
     * 凯特多人审批任务
     */
    String ELEMENT_KAITE_MULTI_USERTASK = "kaiteMultiUserTask";
    /**
     * 凯特循环审批任务
     */
    String ELEMENT_KAITE_LOOP_USERTASK = "kaiteLoopUserTask";
    /**
     * 凯特固定人审批任务
     */
    String ELEMENT_KAITE_FIXED_USERTASK = "kaiteFixedUserTask";
    /**
     * 凯特随机审批任务
     */
    String ELEMENT_KAITE_RANDOM_USERTASK = "kaiteRandomUserTask";
    /**
     * 凯特判断审批任务
     */
    String ELEMENT_KAITE_DECIDE_USERTASK = "kaiteDecideUserTask";
    /**
     * 凯特会签任务
     */
    String ELEMENT_KAITE_COUNTERSIGN_USERTASK = "kaiteCounterSignUserTask";
    /**
     * 凯特候选人任务
     */
    String ELEMENT_KAITE_CANDIDATE_USERTASK = "kaiteCandidateUserTask";
    /**
     * 候选配置
     */
    String ELEMENT_KAITE_CANDIDATE_USERTASK_CANDIDATE_CONFIG = "candidateConfig";

    //-------------------------------自定义节点-------------------------------

    String ELEMENT_CALL_ACTIVITY = "callActivity";

    String ATTRIBUTE_EVENT_START_INITIATOR = "initiator";
    String ATTRIBUTE_EVENT_START_INTERRUPTING = "isInterrupting";
    String ATTRIBUTE_FORM_FORMKEY = "formKey";

    String ELEMENT_MULTIINSTANCE = "multiInstanceLoopCharacteristics";
    String ELEMENT_MULTIINSTANCE_CARDINALITY = "loopCardinality";
    String ELEMENT_MULTIINSTANCE_DATAINPUT = "loopDataInputRef";
    String ELEMENT_MULTIINSTANCE_DATAITEM = "inputDataItem";
    String ELEMENT_MULTI_INSTANCE_DATA_OUTPUT = "loopDataOutputRef";
    String ELEMENT_MULTI_INSTANCE_OUTPUT_DATA_ITEM = "outputDataItem";
    String ELEMENT_MULTIINSTANCE_CONDITION = "completionCondition";
    String ATTRIBUTE_MULTIINSTANCE_SEQUENTIAL = "isSequential";
    String ATTRIBUTE_MULTIINSTANCE_COLLECTION = "collection";
    String ATTRIBUTE_MULTIINSTANCE_VARIABLE = "elementVariable";
    String ATTRIBUTE_MULTIINSTANCE_INDEX_VARIABLE = "elementIndexVariable";

    String ATTRIBUTE_TASK_IMPLEMENTATION = "implementation";
    String ATTRIBUTE_TASK_OPERATION_REF = "operationRef";

    String ATTRIBUTE_TASK_SCRIPT_TEXT = "script";
    String ATTRIBUTE_TASK_SCRIPT_FORMAT = "scriptFormat";
    String ATTRIBUTE_TASK_SCRIPT_RESULTVARIABLE = "resultVariable";
    String ATTRIBUTE_TASK_SCRIPT_AUTO_STORE_VARIABLE = "autoStoreVariables";

    String ATTRIBUTE_TASK_SERVICE_CLASS = "class";
    String ATTRIBUTE_TASK_SERVICE_EXPRESSION = "expression";
    String ATTRIBUTE_TASK_SERVICE_DELEGATEEXPRESSION = "delegateExpression";
    String ATTRIBUTE_TASK_SERVICE_RESULTVARIABLE = "resultVariableName";
    String ATTRIBUTE_TASK_SERVICE_EXTENSIONID = "extensionId";
    String ATTRIBUTE_TASK_SERVICE_SKIP_EXPRESSION = "skipExpression";

    String ATTRIBUTE_TASK_USER_OWNER = "owner";
    String ATTRIBUTE_TASK_USER_CANDIDATEUSERS = "candidateUsers";
    String ATTRIBUTE_TASK_USER_CANDIDATEGROUPS = "candidateGroups";
    String ATTRIBUTE_TASK_USER_DUEDATE = "dueDate";
    String ATTRIBUTE_TASK_USER_BUSINESS_CALENDAR_NAME = "businessCalendarName";
    String ATTRIBUTE_TASK_USER_CATEGORY = "category";
    String ATTRIBUTE_TASK_USER_PRIORITY = "priority";
    String ATTRIBUTE_TASK_USER_SKIP_EXPRESSION = "skipExpression";

    String ATTRIBUTE_TASK_RULE_VARIABLES_INPUT = "ruleVariablesInput";
    String ATTRIBUTE_TASK_RULE_RESULT_VARIABLE = "resultVariable";
    String ATTRIBUTE_TASK_RULE_RULES = "rules";
    String ATTRIBUTE_TASK_RULE_EXCLUDE = "exclude";
    String ATTRIBUTE_TASK_RULE_CLASS = "class";

    String ATTRIBUTE_CALL_ACTIVITY_CALLEDELEMENT = "calledElement";
    String ATTRIBUTE_CALL_ACTIVITY_BUSINESS_KEY = "businessKey";
    String ATTRIBUTE_CALL_ACTIVITY_INHERIT_BUSINESS_KEY = "inheritBusinessKey";
    String ATTRIBUTE_CALL_ACTIVITY_INHERITVARIABLES = "inheritVariables";
    String ELEMENT_CALL_ACTIVITY_IN_PARAMETERS = "in";
    String ELEMENT_CALL_ACTIVITY_OUT_PARAMETERS = "out";
    String ATTRIBUTE_IOPARAMETER_SOURCE = "source";
    String ATTRIBUTE_IOPARAMETER_SOURCE_EXPRESSION = "sourceExpression";
    String ATTRIBUTE_IOPARAMETER_TARGET = "target";

    //-------------------------------自定义流程配置-------------------------------
    /**
     * 流程配置
     */
    String ELEMENT_PROCESS_CONFIG = "processBasicConfig";
    /**
     * 功能编码
     */
    String ATTRIBUTE_FUNCCODE = "funcCode";
    /**
     * 功能名称
     */
    String ATTRIBUTE_FUNCNAME = "funcName";
    /**
     * 功能名称
     */
    String ATTRIBUTE_TABLECODE = "tableCode";
    /**
     * 附属功能ids
     */
    String ATTRIBUTE_ATTACHEDFUNCIDS = "attachedFuncIds";
    /**
     * 附属功能names
     */
    String ATTRIBUTE_ATTACHEDFUNCNAMES = "attachedFuncNames";
    /**
     * 附属功能code
     */
    String ATTRIBUTE_ATTACHEDFUNCCODES = "attachedFuncCodes";
    /**
     * 内容模板
     */
    String ATTRIBUTE_CONTENTMODULE = "module";
    /**
     * 流程分类code
     */
    String ATTRIBUTE_PROCESS_CLASSIFICATIONCODE = "processClassificationCode";
    /**
     * 流程分类name
     */
    String ATTRIBUTE_PROCESS_CLASSIFICATIONNAME = "processClassificationName";
    /**
     * 流程分类id
     */
    String ATTRIBUTE_PROCESS_CLASSIFICATIONID = "processClassificationId";
    /**
     * 部署环境
     */
    String ATTRIBUTE_DEPLOYMENTENVIRONMENT = "DeploymentEnvironment";

    /**
     * 功能id
     */
    String ATTRIBUTE_FUNCID = "funcId";

    //-------------------------------自定义流程配置-------------------------------


    //-------------------------------自定义流程拓展配置-------------------------------

    /**
     * 流程配置
     */
    String ELEMENT_PROCESS_EXTENDED_CONFIG = "extendedConfiguration";
    /**
     * 是否可取回配置
     */
    String ATTRIBUTE_PROCESS_CANRETRIEVE = "canRetrieve";
    /**
     * 是否可发起配置
     */
    String ATTRIBUTE_PROCESS_CANSPONSOR = "canSponsor";
    /**
     * 是否可转办配置
     */
    String ATTRIBUTE_PROCESS_CANTRANSFER = "canTransfer";
    /**
     * 是否可委托配置
     */
    String ATTRIBUTE_PROCESS_CANDELEGATE = "canDelegate";
    /**
     * 加签节点
     */
    String ATTRIBUTE_PROCESS_COUNTERSIGNNODE = "countersignNode";
    /**
     * 简易发起
     */
    String ATTRIBUTE_PROCESS_EASYLAUNCH = "easyLaunch";
    /**
     * 简易审批
     */
    String ATTRIBUTE_PROCESS_SIMPLEAPPROVAL = "simpleApproval";
    /**
     * 简易审批意见
     */
    String ATTRIBUTE_ENABLE_SIMPLE_COMMENTS = "enableSimpleComments";
    /**
     * 紧急状态
     */
    String ATTRIBUTE_PROCESS_EXIGENCY = "exigency";
    /**
     * 隐藏状态
     */
    String ATTRIBUTE_PROCESS_HIDESTATEINFO = "hideStateInfo";
    /**
     * 审批耗时时间
     */
    String ATTRIBUTE_PROCESS_HIDETIMEINFO = "hideTimeInfo";
    /**
     * 是否可催办配置
     */
    String ATTRIBUTE_PROCESS_CANURGED = "canUrged";
    /**
     * 可作废配置
     */
    String ATTRIBUTE_PROCESS_CANINVALID = "canInvalid";
    /**
     * 可退回
     */
    String ATTRIBUTE_PROCESS_CANRETURN = "canReturn";
    /**
     * 是否可撤销配置
     */
    String ATTRIBUTE_PROCESS_CANCANCEL = "canCancel";

    /**
     * 是否可催办配置
     */
    String ATTRIBUTE_PROCESS_INITIATOR_CANURGED = "initiatorCanUrged";
    /**
     * 可作废配置
     */
    String ATTRIBUTE_PROCESS_INITIATOR_CANINVALID = "initiatorInvalid";
    /**
     * 是否可撤销配置
     */
    String ATTRIBUTE_PROCESS_INITIATOR_CANCANCEL = "initiatorCanCancel";


    //-------------------------------自定义流程拓展配置-------------------------------

    //-------------------------------自定义流程事件配置-------------------------------
    /**
     *
     */
    String ATTRIBUTE_PROCESS_CUSTOM_EVENTLISTENERS_CONFIG = "customEventlisteners";
    /**
     * 事件
     */
    String ATTRIBUTE_PROCESS_CUSTOM_TASK_EVENT_CODE = "taskEvent";
    /**
     * 事件
     */
    String ATTRIBUTE_PROCESS_CUSTOM_PROCESS_EVENT_CODE = "processEvent";
    /**
     * 事件类型
     */
    String ATTRIBUTE_PROCESS_CUSTOM_EVENT_TYPECODE = "typeCode";
    /**
     * 执行策略
     */
    String ATTRIBUTE_PROCESS_CUSTOM_EVENT_EXECUTIONSTRATEGYNAME = "executionStrategy";
    /**
     * 赋值字段配置
     */
    String ATTRIBUTE_PROCESS_CUSTOM_EVENT_ASSIGNMENTFIELDCONFIGURATION = "assignmentFieldConfiguration";
    /**
     * 业务bean
     */
    String ATTRIBUTE_PROCESS_CUSTOM_EVENT_SERVICENAME = "serviceName";
    /**
     * 业务方法
     */
    String ATTRIBUTE_PROCESS_CUSTOM_EVENT_METHOD = "method";
    /**
     * 是否存在参数
     */
    String ATTRIBUTE_PROCESS_CUSTOM_EVENT_EXISTENCEPARAMETER = "existenceParameter";


    //-------------------------------自定义流程事件配置-------------------------------

    //-------------------------------自定义流程启动配置start-------------------------------

    String ATTRIBUTE_PROCESS_STARTUPSETTINGS_CONFIG = "startupSettings";
    String ATTRIBUTE_PROCESS_STARTUPSETTINGS_CANEVERYONESTART = "canEveryoneStart";
    String ATTRIBUTE_PROCESS_STARTUPSETTINGS_CANEVERYONEROLESID = "canEveryoneRolesId";
    String ATTRIBUTE_PROCESS_STARTUPSETTINGS_CANEVERYONEROLESNAME = "canEveryoneRolesName";
    String ATTRIBUTE_PROCESS_STARTUPSETTINGS_STARTEXPRESSION = "startExpression";
    String ATTRIBUTE_PROCESS_STARTUPSETTINGS_STARTEXPRESSIONFN = "startExpressionFn";


    //-------------------------------自定义流程启动配置end-------------------------------

    //-------------------------------自定义流程消息配置start-------------------------------

    String ATTRIBUTE_PROCESS_MESSAGESETTING_CONFIG = "messageSetting";
    String ATTRIBUTE_PROCESS_MESSAGESETTING_DEFINITIONS_CONFIG = "messageDefinition";
    String ATTRIBUTE_PROCESS_MESSAGESETTING_DEFINITIONS_CODE = "code";
    String ATTRIBUTE_PROCESS_MESSAGESETTING_DEFINITIONS_TEXT = "text";
    String ATTRIBUTE_PROCESS_STARTUPSETTINGS_MESSAGES = "messages";
    String ATTRIBUTE_PROCESS_STARTUPSETTINGS_MESSAGES_DINGTALK_ID = "jeDingTalkId";
    String ATTRIBUTE_PROCESS_STARTUPSETTINGS_MESSAGES_DINGTALK_NAME = "jeDingTalkName";


    //-------------------------------自定义流程消息配置end-------------------------------


    /**
     * 任务指派人配置
     */
    String ELEMENT_ASSIGNMENT_CONFIG = "assignmentConfig";

    String ELEMENT_ASSIGNMENT_ASSIGNMENT_CONFIG = "assignment";

    String ELEMENT_ASSIGNMENT_RESOURCE_CONFIG = "resource";

    String ELEMENT_ASSIGNMENT_RESOURCE_ENTRYPATH = "entryPath";

    String ELEMENT_ASSIGNMENT_RESOURCE_PERMISSION_CONFIG = "permission";

    /**
     * 自定义处理人service名称
     */
    String PROPERTY_ASSIGNMENT_CUSTOM_SERVICENAME = "serviceName";

    /**
     * 自定义处理人方法名称
     */
    String PROPERTY_ASSIGNMENT_CUSTOM_METHODNAME = "methodName";

    /**
     * 公司内可见
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_COMPANY = "company";
    /**
     * 公司监管可见
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_COMPANY_SUPERVISION = "companySupervision";
    /**
     * 本部门可见
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_DEPT = "dept";
    /**
     * 部门内可见
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_DEPT_ALL = "deptAll";
    /**
     * 直属领导
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_DIRECT_LEADER = "directLeader";
    /**
     * 部门领导
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_DEPT_LEADER = "deptLeader";
    /**
     * 监管领导
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_SUPERVISION_LEADER = "supervisionLeader";
    /**
     * sql
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_SQL = "sql";
    /**
     * sqlRemarks
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_SQL_REMARKS = "sqlRemarks";


    String ELEMENT_ASSIGNMENT_RESOURCE_NAME = "resourceName";
    String ELEMENT_ASSIGNMENT_RESOURCE_CODE = "resourceCode";
    String ELEMENT_ASSIGNMENT_RESOURCE_ID = "resourceId";

    /**
     * 任务指派人配置路径选择
     */
    String ELEMENT_ASSIGNMENT_REFERTO = "referTo";

    //-------------------------------自定义岗位配置-------------------------------

    /**
     * 用户任务指派人(岗位)配置
     */
    String ELEMENT_POSITION_CONFIG = "positionConfig";

    //-------------------------------自定义角色配置-------------------------------

    /**
     * 用户任务指派人(角色)配置
     */
    String ELEMENT_ROLE_CONFIG = "roleConfig";

    //-------------------------------自定义部门配置-------------------------------

    /**
     * 用户任务指派人(部门)配置
     */
    String ELEMENT_DEPARTMENT_CONFIG = "departmentConfig";

    /**
     * 用户任务指派人(产品部门)配置
     */
    String ELEMENT_PROJECT_DEPARTMENT_CONFIG = "organisationConfig";

    //-------------------------------自定义用户配置-------------------------------

    /**
     * 用户任务指派人(用户)配置
     */
    String ELEMENT_USER_CONFIG = "userConfig";

    //-------------------------------自定义工作组配置-------------------------------

    /**
     * 用户任务指派人(工作组)配置
     */
    String ELEMENT_WORKGROUP_CONFIG = "workGroupConfig";

    /**
     * 用户固定人指派人(工作组)配置
     */
    String ELEMENT_FIXEDUSER_CONFIG = "fixedUserConfig";

    /**
     * 特殊配置
     */
    String ELEMENT_SPECTIAL_CONFIG = "specialConfig";
    /**
     * 特殊配置枚举类型
     */
    String ATTRIBUTE_SPECTIAL_TYPES = "types";

    /**
     * 业务SQL
     */
    String ELEMENT_BUSINESS_SQL_CONFIG = "businessSqlAssignmentConfig";
    /**
     * 业务SQL
     */
    String ATTRIBUTE_SQL = "sql";
    /**
     * 人员矩阵
     */
    String MATRIX_CONFIG = "matrixConfig";
    String MATRIX_NAME = "matrixName";
    String MATRIX_ID = "matrixId";
    String RESULT_FIELD = "resultField";
    String RESULT_FIELD_ID = "resultFieldId";
    String CONDITIONAL_INFO = "conditionalInfo";
    String CONDITION = "condition";
    String CONDITION_VALUE = "conditionValue";
    String CONDITION_TYPE = "type";
    /**
     * Rbac的SQL配置
     */
    String ELEMENT_RBAC_SQL_CONFIG = "rbacSqlAssignmentConfig";
    /**
     * 自定义配置
     */
    String ELEMENT_CUSTOM_CONFIG = "customConfig";
    /**
     * 自定义方法名
     */
    String ATTRIBUTE_CUSTOM_SERVICENAME = "serviceName";
    /**
     * 表单字段配置
     */
    String ELEMENT_FORMFIELD_CONFIG = "formFieldConfig";
    /**
     * 表单字段
     */
    String ATTRIBUTE_FORMFIELD_CODE = "formFieldCode";
    /**
     * 组织结构树配置
     */
    String ELEMENT_ORGTREE_CONFIG = "orgTreeConfig";
    /**
     * 组织异步结构树配置
     */
    String ELEMENT_ORG_ASYNC_TREE_CONFIG = "orgAsyncTreeConfig";

    //-------------------------------自定义用户任务配置-------------------------------

    /**
     * 是否启用通用配置
     */
    String ATTRIBUTE_ENABLE = "enable";
    /**
     * 人员是否全选
     */
    String ATTRIBUTE_USERTASK_BASIC_ALLUSERDEFAULTSELECTED = "allUserDefaultSelected";
    /**
     * 是否是异步树
     */
    String ATTRIBUTE_USERTASK_BASIC_ASYNCTREE = "asyncTree";

    /**
     * 处理时效
     */
    String ATTRIBUTE_USERTASK_BASIC_HANDLE_PERIOD = "handlePeriod";
    /**
     * 处理时效类型
     */
    String ATTRIBUTE_USERTASK_BASIC_HANDLE_PERIODTYPE = "handlePeriodType";
    /**
     * 任务按钮配置
     */
    String ELEMENT_BUTTONS_PROCESS_CONFIG = "processButtonsConfig";
    /**
     * //TODO 添加process 和task 分开
     * 任务按钮配置
     */
    String ELEMENT_BUTTONS_CONFIG = "buttonsConfig";
    /**
     * 按钮配置
     */
    String ELEMENT_PROCESS_BUTTON = "processButton";
    /**
     * 按钮配置
     */
    String ELEMENT_TASK_BUTTON = "taskButton";
    /**
     * 按钮ID配置
     */
    String ATTRIBUTE_BUTTON_ID = "buttonId";
    /**
     * 按钮Name配置
     */
    String ATTRIBUTE_BUTTON_NAME = "buttonName";
    /**
     * 按钮Code配置
     */
    String ATTRIBUTE_BUTTON_CODE = "buttonCode";
    String ATTRIBUTE_BUTTON_OPERATION = "operationId";
    String ATTRIBUTE_BUTTON_APP_LISTENERS = "appListeners";
    String ATTRIBUTE_BUTTON_PC_LISTENERS = "pcListeners";
    String ATTRIBUTE_BUTTON_BACKEND_LISTENERS = "backendListeners";
    /**
     * 按钮自定义名称
     */
    String ATTRIBUTE_BUTTON_CUSTOMIZE_NAME = "customizeName";
    /**
     * 按钮自定义审批意见
     */
    String ATTRIBUTE_BUTTON_CUSTOMIZE_COMMENTS = "customizeComments";


    /**
     * 展示表达式
     */
    String ATTRIBUTE_TASK_BUTTON_DISLAY_EXPRESSION = "displayExpression";
    /**
     * 流程未启动时是否展示
     */
    String ATTRIBUTE_PROCESS_BUTTON_DISLAY_WHEN_NOT_STARTED = "displayWhenNotStarted";

    /**
     * 任务标签意见配置
     */
    String ELEMENT_USERTASK_COMMENTS_CONFIG = "commentsConfig";
    /**
     * 任务标签意见配置
     */
    String ELEMENT_USERTASK_COMMENT = "comment";
    /**
     * 任务标签意见配置
     */
    String ATTRIBUTE_USERTASK_COMMENT_KEY = "key";
    /**
     * 任务标签意见配置
     */
    String ATTRIBUTE_USERTASK_COMMENT_VALUE = "value";

    /**
     * 任务延期配置
     */
    String ELEMENT_USERTASK_DELAY_CONFIG = "delayConfig";
    /**
     * 任务延期事件类型配置
     */
    String ATTRIBUTE_USERTASK_DELAY_TIMETYPE = "delayTimeType";
    /**
     * 任务办理周期
     */
    String ATTRIBUTE_USERTASK_DELAY_PERIOD = "delayPeriod";
    /**
     * 可延期次数
     */
    String ATTRIBUTE_USERTASK_DELAY_TIMES = "delayTimes";
    /**
     * 延期回调类名称
     */
    String ATTRIBUTE_USERTASK_DELAY_CALLBACKSERVICENAME = "delayCallbackServiceName";

    /**
     * 任务委托配置
     */
    String ELEMENT_USERTASK_DELEGATE_CONFIG = "delegateConfig";

    //--------------------------任务驳回配置start-----------------------------
    /**
     * 任务驳回配置
     */
    String PROPERTY_USERTASK_DISMISS_CONFIG = "dismissConfig";
    /**
     * 任务驳回分解配置
     */
    String PROPERTY_USERTASK_DISMISS_COMMIT_BREAKDOWN_CONFIG = "commitBreakdown";
    /**
     * 任务驳回节点
     */
    String PROPERTY_USERTASK_DISMISS_TASKNAME = "dismissTaskNames";
    /**
     * 任务驳回节点
     */
    String PROPERTY_USERTASK_DISMISS_TASKID = "dismissTaskIds";
    /**
     * 驳回后是否启用直送按钮
     */
    String PROPERTY_USERTASK_DISMISS_ENABLEDIRECTSEND = "directSendAfterDismiss";
    /**
     * 被驳回后强制提交 只能提交，不能直送
     */
    String PROPERTY_USERTASK_DISMISS_FORCECOMMITAFTERDISMISS = "forceCommitAfterDismiss";
    /**
     * 被驳回后禁用送交
     */
    String PROPERTY_USERTASK_DISMISS_DISABLESENDAFTERDISMISS = "disableSendAfterDismiss";
    /**
     * 不可退回
     */
    String PROPERTY_USERTASK_DISMISS_NORETURN = "noReturn";
    /**
     * 可直接送交退回人
     */
    String PROPERTY_USERTASK_DISMISS_DIRECTSENDAFTERRETURN = "directSendAfterReturn";

    //--------------------------任务驳回配置end-----------------------------
    /**
     * 任务退回配置
     */
    String ELEMENT_USERTASK_GOBACK_CONFIG = "gobackConfig";
    /**
     * 退回后可直接提交(直送按钮启用)
     */
    String ATTRIBUTE_USERTASK_GOBACK_DIRECTSUBMITAFTERGOBACK = "directSubmitAfterGoback";
    /**
     * 退回后不可提交，只可以直送
     */
    String ATTRIBUTE_USERTASK_GOBACK_DISABLESUBMITAFTERGOBACK = "disableSubmitAfterGoback";
    /**
     * 任务是否可作废配置
     */
    String ELEMENT_USERTASK_INVALID_CONFIG = "invalidConfig";
    /**
     * 任务跳跃配置
     */
    String ELEMENT_USERTASK_JUMP_CONFIG = "jumpConfig";
    /**
     * 可以跳跃到的节点
     */
    String ATTRIBUTE_USERTASK_JUMP_TASKS = "jumpTasks";
    /**
     * 任务标签意见配置
     */
    String ELEMENT_USERTASK_LABELCOMMENT_CONFIG = "labelCommentConfig";
    /**
     * 任务标签意见配置
     */
    String ELEMENT_USERTASK_LABEL_COMMENT = "labelComment";
    /**
     * 任务标签意见配置
     */
    String ATTRIBUTE_USERTASK_COMMENT_LABEL = "label";
    /**
     * 任务标签意见配置
     */
    String ATTRIBUTE_USERTASK_COMMENT_CONTENT = "content";

    /**
     * 任务监听配置
     */
    String ELEMENT_USERTASK_LISTENER_CONFIG = "listenerConfig";
    /**
     * 任务监听配置
     */
    String ELEMENT_USERTASK_LISTENER = "listener";
    /**
     * 任务监听类型
     */
    String ATTRIBUTE_USERTASK_LISTENER_TYPE = "listenerType";
    /**
     * 任务监听策略
     */
    String ATTRIBUTE_USERTASK_LISTENER_STRATEGY = "strategy";
    /**
     * 任务字段值
     */
    String ATTRIBUTE_USERTASK_LISTENER_FIELDVALUES = "fieldValues";
    /**
     * 调用类名称
     */
    String ATTRIBUTE_USERTASK_LISTENER_BEANNAME = "beanName";
    /**
     * 传阅设置
     */
    String ELEMENT_USERTASK_PASSROUND_CONFIG = "passRoundConfig";
    /**
     * 传阅人员设置
     */
    String ELEMENT_USERTASK_PASSROUND_CIRCULATIONRULE = "circulationRule";

    String ELEMENT_USERTASK_PASSROUND_RULE_TYPE = "ruleType";
    String ELEMENT_USERTASK_PASSROUND_ID = "id";
    String ELEMENT_USERTASK_PASSROUND_NAME = "name";
    String ELEMENT_USERTASK_PASSROUND_SERVICE = "service";
    String ELEMENT_USERTASK_PASSROUND_METHOD = "method";

    /**
     * 是否开启自动传阅
     */
    String ATTRIBUTE_USERTASK_PASSROUND_AUTO = "auto";
    /**
     * 预定义配置
     */
    String ELEMENT_USERTASK_PREDEFINED_CONFIG = "predefinedConfig";
    /**
     * 提醒配置
     */
    String ELEMENT_USERTASK_REMIND_CONFIG = "remindConfig";
    /**
     * 提醒类型配置
     */
    String ATTRIBUTE_USERTASK_REMIND_TYPE = "remindType";
    /**
     * 任务取回配置
     */
    String ELEMENT_USERTASK_RETRIEVE_CONFIG = "retrieveConfig";
    /**
     * 任务转办配置
     */
    String ELEMENT_USERTASK_TRANSFER_CONFIG = "transferConfig";
    /**
     * 任务催办配置
     */
    String ELEMENT_USERTASK_URGE_CONFIG = "urgeConfig";
    /**
     * 表单基础配置
     */
    String ELEMENT_USERTASK_FORM_BASIC = "formBasic";
    /**
     * 表单基础配置-是否可编辑
     */
    String ATTRIBUTE_USERTASK_FORM_BASIC_EDITABLE = "editable";
    /**
     * 表单按钮配置
     */
    String ELEMENT_USERTASK_FORM_BUTTONS = "formButtons";
    /**
     * 表单按钮配置，对应的通用属性有name,code,enable
     */
    String ELEMENT_USERTASK_FORM_BUTTON = "formButton";
    /**
     * 表单子功能配置
     */
    String ELEMENT_USERTASK_FORM_CHILDFUNCS = "formChildFuncs";
    /**
     * 表单子功能配置
     */
    String ELEMENT_USERTASK_FORM_CHILDFUNC = "formChildFunc";
    /**
     * 表单子功能是否可编辑
     */
    String ATTRIBUTE_USERTASK_FORM_CHILDFUNC_EDITABLE = "editable";
    /**
     * 表单子功能隐藏
     */
    String ATTRIBUTE_USERTASK_FORM_CHILDFUNC_HIDDEN = "hidden";
    /**
     * 表单子功能展示
     */
    String ATTRIBUTE_USERTASK_FORM_CHILDFUNC_DISPLAY = "display";
    /**
     * 表单字段配置
     */
    String ELEMENT_USERTASK_FORM_FORMFIELDS = "formFields";
    /**
     * 表单字段配置
     */
    String ELEMENT_USERTASK_FORM_FORMFIELD = "formField";
    /**
     * 表单字段是否可编辑
     */
    String ATTRIBUTE_USERTASK_FORM_FORMFIELD_EDITABLE = "editable";
    /**
     * 表单字段是否只读
     */
    String ATTRIBUTE_USERTASK_FORM_FORMFIELD_READONLY = "readOnly";
    /**
     * 表单字段是否隐藏
     */
    String ATTRIBUTE_USERTASK_FORM_FORMFIELD_HIDDEN = "hidden";
    /**
     * 表单字段是否展示
     */
    String ATTRIBUTE_USERTASK_FORM_FORMFIELD_DISPLAY = "display";
    /**
     * 表单字段是否必填
     */
    String ATTRIBUTE_USERTASK_FORM_FORMFIELD_REQUIRED = "required";
    /**
     * 表单字段值配置
     */
    String ELEMENT_USERTASK_FORM_FIELDVALUES = "fieldValues";
    /**
     * 表单字段值配置
     */
    String ELEMENT_USERTASK_FORM_FIELDVALUE = "fieldValue";
    /**
     * 表单字段值配置
     */
    String ATTRIBUTE_USERTASK_FORM_FIELDVALUED = "value";

    //-------------------------------自定义用户任务配置-------------------------------

    //-------------------------------自定义多实例审批任务配置-------------------------------

    /**
     * 审批类型，串行or并行
     */
    String ATTRIBUTE_MULTI_USERTASK_APPROVETYPE = "userTaskApproveType";

    //-------------------------------自定义多实例审批任务配置-------------------------------

    //-------------------------------自定义多人审批任务配置-------------------------------


    //-------------------------------自定义多人审批任务配置-------------------------------

    //-------------------------------自定义循环审批任务配置-------------------------------

    //-------------------------------自定义循环审批任务配置-------------------------------

    //-------------------------------自定义固定人审批任务配置-------------------------------

    //-------------------------------自定义固定人审批任务配置-------------------------------

    //-------------------------------自定义判断审批任务配置-------------------------------

    //-------------------------------自定义判断审批任务配置-------------------------------

    //-------------------------------自定义候选审批任务配置-------------------------------

    //-------------------------------自定义候选审批任务配置-------------------------------

    //-------------------------------自定义会签审批任务配置-------------------------------

    String COUNTERSIGNED_TASK_COUNTER_SIGN_CONFIG = "taskCounterSignConfig";

    //会签类型，默认是比例通过制
    String COUNTERSIGNED_COUNTERSIGNPASSTYPE = "counterSignPassType";
    //是否启用一票否决
    String COUNTERSIGNED_ENABLEONEBALLOTVETO = "enableOneBallotVeto";
    //是否启用一票通过
    String COUNTERSIGNED_ENABLEONEBALLOTPASS = "enableOneBallotPass";
    //是否启用弃权
    String COUNTERSIGNED_ENABLEABSTAIN = "enableAbstain";
    //通过比例
    String COUNTERSIGNED_AMOUNT = "amount";
    //一票否决处理人类型
    String COUNTERSIGNED_ONEBALLOTVETOTYPE = "oneBallotVetoType";
    //一票否决处理人
    String COUNTERSIGNED_ONEBALLOTVETOUSERS = "oneBallotVetoUsers";
    //一票通过处理人类型
    String COUNTERSIGNED_ONEBALLOTPASSTYPE = "oneBallotPassType";
    //一票通过处理人
    String COUNTERSIGNED_ONEBALLOTPASSUSERS = "oneBallotPassUsers";
    //全部投票
    String COUNTERSIGNED_VOTEALL = "voteAll";
    //运行时调整
    String RUNTIME_TUNING = "runtimeTuning";
    //运行前调整
    String ADJUST_BEFORE_RUNNING = "adjustBeforeRunning";
    //取消全选
    String DESELECT_ALL = "deselectAll";


    //-------------------------------自定义会签审批任务配置-------------------------------

    String ELEMENT_SEQUENCE_FLOW = "sequenceFlow";
    String ELEMENT_FLOW_CONDITION = "conditionExpression";
    String ATTRIBUTE_FLOW_SOURCE_REF = "sourceRef";
    String ATTRIBUTE_FLOW_TARGET_REF = "targetRef";
    String ATTRIBUTE_FLOW_SKIP_EXPRESSION = "skipExpression";

    String ELEMENT_TEXT_ANNOTATION = "textAnnotation";
    String ATTRIBUTE_TEXTFORMAT = "textFormat";
    String ELEMENT_TEXT_ANNOTATION_TEXT = "text";

    String ELEMENT_ASSOCIATION = "association";

    String ELEMENT_GATEWAY_EXCLUSIVE = "exclusiveGateway";
    String ELEMENT_GATEWAY_EVENT = "eventBasedGateway";
    String ELEMENT_GATEWAY_INCLUSIVE = "inclusiveGateway";
    String ELEMENT_GATEWAY_PARALLEL = "parallelGateway";
    String ELEMENT_GATEWAY_COMPLEX = "complexGateway";

    String ELEMENT_EVENT_START = "startEvent";
    String ELEMENT_EVENT_END = "endEvent";
    String ELEMENT_EVENT_BOUNDARY = "boundaryEvent";
    String ELEMENT_EVENT_THROW = "intermediateThrowEvent";
    String ELEMENT_EVENT_CATCH = "intermediateCatchEvent";

    String ATTRIBUTE_BOUNDARY_ATTACHEDTOREF = "attachedToRef";
    String ATTRIBUTE_BOUNDARY_CANCELACTIVITY = "cancelActivity";

    String ELEMENT_EVENT_ERRORDEFINITION = "errorEventDefinition";
    String ATTRIBUTE_ERROR_REF = "errorRef";
    String ATTRIBUTE_ERROR_CODE = "errorCode";
    String ELEMENT_EVENT_MESSAGEDEFINITION = "messageEventDefinition";
    String ATTRIBUTE_MESSAGE_REF = "messageRef";
    String ELEMENT_EVENT_SIGNALDEFINITION = "signalEventDefinition";
    String ATTRIBUTE_SIGNAL_REF = "signalRef";
    String ATTRIBUTE_SCOPE = "scope";
    String ELEMENT_EVENT_TIMERDEFINITION = "timerEventDefinition";
    String ATTRIBUTE_CALENDAR_NAME = "businessCalendarName";
    String ATTRIBUTE_TIMER_DATE = "timeDate";
    String ATTRIBUTE_TIMER_CYCLE = "timeCycle";
    String ATTRIBUTE_END_DATE = "endDate";
    String ATTRIBUTE_TIMER_DURATION = "timeDuration";
    String ELEMENT_EVENT_TERMINATEDEFINITION = "terminateEventDefinition";
    String ATTRIBUTE_TERMINATE_ALL = "terminateAll";
    String ATTRIBUTE_TERMINATE_MULTI_INSTANCE = "terminateMultiInstance";
    String ELEMENT_EVENT_CANCELDEFINITION = "cancelEventDefinition";
    String ELEMENT_EVENT_COMPENSATEDEFINITION = "compensateEventDefinition";
    String ATTRIBUTE_COMPENSATE_ACTIVITYREF = "activityRef";
    String ATTRIBUTE_COMPENSATE_WAITFORCOMPLETION = "waitForCompletion";

    String ELEMENT_FORMPROPERTY = "formProperty";
    String ATTRIBUTE_FORM_ID = "id";
    String ATTRIBUTE_FORM_NAME = "name";
    String ATTRIBUTE_FORM_TYPE = "type";
    String ATTRIBUTE_FORM_EXPRESSION = "expression";
    String ATTRIBUTE_FORM_VARIABLE = "variable";
    String ATTRIBUTE_FORM_READABLE = "readable";
    String ATTRIBUTE_FORM_WRITABLE = "writable";
    String ATTRIBUTE_FORM_REQUIRED = "required";
    String ATTRIBUTE_FORM_DEFAULT = "default";
    String ATTRIBUTE_FORM_DATEPATTERN = "datePattern";
    String ELEMENT_VALUE = "value";

    String ELEMENT_FIELD = "field";
    String ATTRIBUTE_FIELD_NAME = "name";
    String ATTRIBUTE_FIELD_STRING = "stringValue";
    String ATTRIBUTE_FIELD_EXPRESSION = "expression";
    String ELEMENT_FIELD_STRING = "string";

    String ALFRESCO_TYPE = "alfrescoScriptType";

    String ELEMENT_DI_DIAGRAM = "BPMNDiagram";
    String ELEMENT_DI_PLANE = "BPMNPlane";
    String ELEMENT_DI_SHAPE = "BPMNShape";
    String ELEMENT_DI_EDGE = "BPMNEdge";
    String ELEMENT_DI_LABEL = "BPMNLabel";
    String ELEMENT_DI_BOUNDS = "Bounds";
    String ELEMENT_DI_WAYPOINT = "waypoint";
    String ATTRIBUTE_DI_BPMNELEMENT = "bpmnElement";
    String ATTRIBUTE_DI_IS_EXPANDED = "isExpanded";
    String ATTRIBUTE_DI_WIDTH = "width";
    String ATTRIBUTE_DI_HEIGHT = "height";
    String ATTRIBUTE_DI_X = "x";
    String ATTRIBUTE_DI_Y = "y";

    String ELEMENT_DATA_OBJECT = "dataObject";
    String ATTRIBUTE_DATA_ID = "id";
    String ATTRIBUTE_DATA_NAME = "name";
    String ATTRIBUTE_DATA_ITEM_REF = "itemSubjectRef";
    // only used by valued data objects
    String ELEMENT_DATA_VALUE = "value";

    String ELEMENT_CUSTOM_RESOURCE = "customResource";
    String ELEMENT_RESOURCE_ASSIGNMENT = "resourceAssignmentExpression";
    String ELEMENT_FORMAL_EXPRESSION = "formalExpression";
    String ELEMENT_RESOURCE_REF = "resourceRef";
    String ATTRIBUTE_ASSOCIATION_DIRECTION = "associationDirection";

    String FAILED_JOB_RETRY_TIME_CYCLE = "failedJobRetryTimeCycle";
    String MAP_EXCEPTION = "mapException";
    String MAP_EXCEPTION_ERRORCODE = "errorCode";
    String MAP_EXCEPTION_ANDCHILDREN = "includeChildExceptions";

    String ELEMENT_GATEWAY_INCOMING = "incoming";
    String ELEMENT_GATEWAY_OUTGOING = "outgoing";

    //---------------------基础----------------
    /**
     * 绑定表单名称
     */
    String PROPERTY_TASK_PROPERTIES_FORMSCHEMENAME = "formSchemeName";
    /**
     * 绑定表单id
     */
    String PROPERTY_TASK_PROPERTIES_FORMSCHEMEID = "formSchemeId";
    /**
     * 列表同步
     */
    String PROPERTY_TASK_PROPERTIES_LISTSYNCHRONIZATION = "listSynchronization";
    /**
     * 取回
     */
    String PROPERTY_TASK_PROPERTIES_RETRIEVE = "canRetrieve";
    /**
     * 催办
     */
    String PROPERTY_TASK_PROPERTIES_URGE = "canUrge";
    /**
     * 作废
     */
    String PROPERTY_TASK_PROPERTIES_INVALID = "canInvalid";
    /**
     * 委托
     */
    String PROPERTY_TASK_PROPERTIES_DELEGATE = "canDelegate";
    /**
     * 转办
     */
    String PROPERTY_TASK_PROPERTIES_TRANSFER = "canTransfer";
    /**
     * 表单可编辑
     */
    String PROPERTY_TASK_PROPERTIES_FORMEDITABLE = "canFormEditable";
    /**
     * 消息不提醒
     */
    String PROPERTY_TASK_PROPERTIES_REMIND = "canRemind";
    /**
     * 节点属性-不简易审批
     */
    String PROPERTY_TASK_PROPERTIES_SIMPLEAPPROVAL = "closeSimpleApproval";
    /**
     * 自动全选
     */
    String PROPERTY_TASK_PROPERTIES_SELECTALL = "canSelectAll";
    /**
     * 异步树
     */
    String PROPERTY_TASK_PROPERTIES_ASYNTREE = "canAsynTree";
    /**
     * 可跳跃
     */
    String PROPERTY_TASK_PROPERTIES_JUMP = "isJump";
    /**
     * 启用逻辑
     */
    String PROPERTY_TASK_PROPERTIES_LOGICALJUDGMENT = "canLogicalJudgment";
    /**
     * 自动判空
     */
    String PROPERTY_TASK_PROPERTIES_JUDGE_EMPTY = "judgeEmpty";
    /**
     * 是否是加签节点
     */
    String TASK_IS_CS = "isCs";
    /**
     * 是否是加签节点
     */
    String TASK_CREATE_CS_USERID = "createCsUserId";
    /**
     * 是否启用密级
     */
    String SECURITY_ENABLE = "securityEnable";
    /**
     * 密级等级
     */
    String SECURITY_CODE = "securityCode";
    /**
     * 是否加签
     */
    String COUNTERSIGN = "countersign";
    /**
     * 合并处理
     */
    String MERGEAPPLICATION = "mergeApplication";
    /**
     * 是否回签
     */
    String SIGN_BACK = "signBack";
    /**
     * 是否强制回签
     */
    String FORCED_SIGNATURE_BACK = "forcedSignatureBack";
    /**
     * 是否启用暂存
     */
    String STAGING = "staging";
    /**
     * 运行时调整
     */
    String BASIC_RUNTIME_TUNING = "basicRuntimeTuning";


//------------------------------------提交分解start------------------------
    /**
     * 提交分解
     */
    String PROPERTY_TASK_COMMIT_BREAKDOWN_CONFIG = "commitBreakdown";
    String PROPERTY_TASK_COMMIT_BREAKDOWN_BUTTON = "button";
    /**
     * 按钮名称
     */
    String PROPERTY_TASK_COMMIT_BREAKDOWN_BUTTON_NAME = "buttonName";
    /**
     * 节点id
     */
    String PROPERTY_TASK_COMMIT_BREAKDOWN_NODE_ID = "nodeId";
    /**
     * 节点name
     */
    String PROPERTY_TASK_COMMIT_BREAKDOWN_NODE_NAME = "nodeName";

    //------------------------------------提交分解end------------------------

    //------------------------------------预警与延期start------------------------

//
//"earlyWarningAndPostponement":{
//        "enable": "0",
//                "":12,
//                "":"小时",
//                "":"XS",
//                "":12,
//                "":"小时",
//                "":"XS",
//                "reminderFrequencyDuration":1,
//                "reminderFrequencyUnitName":"小时",
//                "reminderFrequencyUnitCode":"XS",
//                "resource": [
//        {
//            "type": "",
//                "name": "",
//                "service": "",
//                "method": ""
//        }
//        ]
//    }
    /**
     * 预警与延期
     */
    String PROPERTY_TASK_EARLYWARNINGANDPOSTPONEMENT_CONFIG = "earlyWarningAndPostponement";
    /**
     * 处理时限时间
     */
    String PROCESSING_TIME_LIMIT_DURATION = "processingTimeLimitDuration";
    /**
     * 处理时限单位名称
     */
    String PROCESSING_TIME_LIMIT_UNIT_NAME = "processingTimeLimitUnitName";
    /**
     * 处理时限单位code
     */
    String PROCESSING_TIME_LIMIT_UNIT_CODE = "processingTimeLimitUnitCode";
    /**
     * 预警时限时间
     */
    String WARNING_TIME_LIMIT_DURATION = "warningTimeLimitDuration";
    /**
     * 预警时限时间单位名称
     */
    String WARNING_TIME_LIMIT_UNIT_NAME = "warningTimeLimitUnitName";
    /**
     * 预警时限时间单位code
     */
    String WARNING_TIME_LIMIT_UNIT_CODE = "warningTimeLimitUnitCode";
    /**
     * 提醒频率时间
     */
    String REMINDER_FREQUENCY_DURATION = "reminderFrequencyDuration";
    /**
     * 提醒频率单位名称
     */
    String REMINDER_FREQUENCY_UNIT_NAME = "reminderFrequencyUnitName";
    /**
     * 提醒频率单位code
     */
    String REMINDER_FREQUENCY_UNIT_CODE = "reminderFrequencyUnitCode";

    //------------------------------------预警与延期end------------------------


    //-------------------加签--------------------------
    /**
     * 加签配置
     */
    String PROPERTY_TASK_ADDSIGNATURE_CONFIG = "addSignature";
    /**
     * 无限加签
     */
    String PROPERTY_TASK_ADDSIGNATURE_UNLIMITED = "unlimited";
    /**
     * 不可回签
     */
    String PROPERTY_TASK_ADDSIGNATURE_NOT_COUNTERSIGNED = "notCountersigned";
    /**
     * 强制回签
     */
    String PROPERTY_TASK_ADDSIGNATURE_MANDATORY_COUNTERSIGNATURE = "mandatoryCountersignature";

    //----------------------审批公告-----------------------------
    /**
     * 审批公告
     */
    String PROPERTY_TASK_APPROVALNOTICE_CONFIG = "approvalNotice";
    /**
     * 流程发起人
     */
    String PROPERTY_TASK_APPROVALNOTICE_PROCESSINITIATOR = "processInitiator";
    /**
     * 已审批人员
     */
    String PROPERTY_TASK_APPROVALNOTICE_THISNODEAPPROVED = "thisNodeApproved";
    /**
     * 本节点已审批直属领导
     */
    String PROPERTY_TASK_APPROVALNOTICE_THISNODEAPPROVALDIRECTLYUNDERLEADER = "thisNodeApprovalDirectlyUnderLeader";
    /**
     * 本节点已审批部门领导
     */
    String PROPERTY_TASK_APPROVALNOTICE_THISNODEAPPROVALDEPTLEADER = "thisNodeApprovalDeptLeader";

    //---------------------------随机------------------------
    /**
     * 随机配置
     */
    String PROPERTY_TASK_RANDOM_CONFIG = "randomConfig";
    /**
     * 团队名称
     */
    String PROPERTY_TASK_RANDOM_TEAM_NAME = "teamName";
    /**
     * 轮询派发
     */
    String PROPERTY_TASK_RANDOM_DISTRIBUTEDPOLLING = "distributedPolling";
    /**
     * 提交给团队
     */
    String PROPERTY_TASK_RANDOM_DISTRIBUTERANDOM = "distributeRandom";
    /**
     * 顺序流样式
     */
    String SEQUENCE_FLOW_STYLE_CONFIG = "styleConfig";
    /**
     * 虚线
     */
    String SEQUENCE_FLOW_STYLE_DOTTEDLINE = "dottedLine";
    /**
     * 颜色
     */
    String SEQUENCE_FLOW_STYLE_COLOR = "color";
    /**
     * 字体大小
     */
    String SEQUENCE_FLOW_STYLE_FONTSIZE = "fontSize";
    /**
     * 字体样式
     */
    String SEQUENCE_FLOW_STYLE_FONTSTYLE = "fontStyle";
    /**
     * 字体颜色
     */
    String SEQUENCE_FLOW_STYLE_FONTCOLOR = "fontColor";

    //---------------------------表单资源控制strat------------------------
    /**
     * 字段赋值
     */
    String PROPERTY_USERTASK_FORM_FIELD_ASSIGNMENT_CONFIG = "fieldAssignmentConfig";

    String PROPERTY_USERTASK_FORM_FIELD_ASSIGNMENT = "fieldAssignment";

    String PROPERTY_USERTASK_FORM_FIELD_ASSIGNMENT_CODE = "code";
    String PROPERTY_USERTASK_FORM_FIELD_ASSIGNMENT_VALUE = "value";
    /**
     * 字段配置
     */
    String PROPERTY_USERTASK_FORM_FIELD_CONTROL_CONFIG = "fieldControlConfig";
    String PROPERTY_USERTASK_FORM_FIELD_CONTROL = "field";
    String PROPERTY_USERTASK_FORM_FIELD_CONTROL_CODE = "fieldCode";
    String PROPERTY_USERTASK_FORM_FIELD_CONTROL_EDITABLE = "editable";
    String PROPERTY_USERTASK_FORM_FIELD_CONTROL_READONLY = "readOnly";
    String PROPERTY_USERTASK_FORM_FIELD_CONTROL_HIDDEN = "hidden";
    String PROPERTY_USERTASK_FORM_FIELD_CONTROL_DISPLAY = "display";
    String PROPERTY_USERTASK_FORM_FIELD_CONTROL_REQUIRED = "required";
    /**
     * 按钮
     */
    String PROPERTY_USERTASK_FORM_TASKFORMBUTTON_CONFIG = "taskFormButtonConfig";
    String PROPERTY_USERTASK_FORM_TASKFORMBUTTON = "button";
    String PROPERTY_USERTASK_FORM_TASKFORMBUTTON_CODE = "buttonCode";
    /**
     * 子功能
     */
    String PROPERTY_USERTASK_FORM_TASKCHILDFUNC_CONFIG = "taskChildFuncConfig";

    String PROPERTY_USERTASK_FORM_TASKCHILDFUNC = "taskChildFunc";

    String PROPERTY_USERTASK_FORM_TASKCHILDFUNC_CODE = "code";

    String PROPERTY_USERTASK_FORM_TASKCHILDFUNC_EDITABLE = "editable";

    String PROPERTY_USERTASK_FORM_TASKCHILDFUNC_HIDDEN = "hidden";

    String PROPERTY_USERTASK_FORM_TASKCHILDFUNC_DISPLAY = "display";

    //------------------------表单资源控制end---------------------------------

}
