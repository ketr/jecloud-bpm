/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.export;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.Activity;
import com.je.bpm.core.model.MultiInstanceLoopCharacteristics;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamWriter;

public class MultiInstanceExport implements BpmnXMLConstants {

    public static void writeMultiInstance(Activity activity, XMLStreamWriter xtw) throws Exception {
        if (activity.getLoopCharacteristics() != null) {
            MultiInstanceLoopCharacteristics multiInstanceObject = activity.getLoopCharacteristics();
            if (hasMultiInstanceElements(multiInstanceObject)) {
                xtw.writeStartElement(ELEMENT_MULTIINSTANCE);
                BpmnXMLUtil.writeDefaultAttribute(ATTRIBUTE_MULTIINSTANCE_SEQUENTIAL,
                        String.valueOf(multiInstanceObject.isSequential()).toLowerCase(), xtw);
                if (StringUtils.isNotEmpty(multiInstanceObject.getInputDataItem())) {
                    BpmnXMLUtil.writeQualifiedAttribute(ATTRIBUTE_MULTIINSTANCE_COLLECTION,
                            multiInstanceObject.getInputDataItem(), xtw);
                }
                if (StringUtils.isNotEmpty(multiInstanceObject.getElementVariable())) {
                    BpmnXMLUtil.writeQualifiedAttribute(ATTRIBUTE_MULTIINSTANCE_VARIABLE,
                            multiInstanceObject.getElementVariable(), xtw);
                }
                if (StringUtils.isNotEmpty(multiInstanceObject.getLoopCardinality())) {
                    xtw.writeStartElement(ELEMENT_MULTIINSTANCE_CARDINALITY);
                    xtw.writeCharacters(multiInstanceObject.getLoopCardinality());
                    xtw.writeEndElement();
                }
                if (StringUtils.isNotEmpty(multiInstanceObject.getLoopDataOutputRef())) {
                    xtw.writeStartElement(ELEMENT_MULTI_INSTANCE_DATA_OUTPUT);
                    xtw.writeCharacters(multiInstanceObject.getLoopDataOutputRef());
                    xtw.writeEndElement();
                }
                if (StringUtils.isNotEmpty(multiInstanceObject.getOutputDataItem())) {
                    xtw.writeStartElement(ELEMENT_MULTI_INSTANCE_OUTPUT_DATA_ITEM);
                    xtw.writeAttribute(ATTRIBUTE_NAME, multiInstanceObject.getOutputDataItem());
                    xtw.writeEndElement();
                }
                if (StringUtils.isNotEmpty(multiInstanceObject.getCompletionCondition())) {
                    xtw.writeStartElement(ELEMENT_MULTIINSTANCE_CONDITION);
                    xtw.writeCharacters(multiInstanceObject.getCompletionCondition());
                    xtw.writeEndElement();
                }
                xtw.writeEndElement();
            }
        }
    }

    private static boolean hasMultiInstanceElements(MultiInstanceLoopCharacteristics multiInstanceObject) {
        return StringUtils.isNotEmpty(multiInstanceObject.getLoopCardinality()) ||
                StringUtils.isNotEmpty(multiInstanceObject.getInputDataItem()) ||
                StringUtils.isNotEmpty(multiInstanceObject.getCompletionCondition()) ||
                StringUtils.isNotEmpty(multiInstanceObject.getLoopDataOutputRef()) ||
                StringUtils.isNotEmpty(multiInstanceObject.getOutputDataItem());
    }
}
