/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child;

import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.*;
import com.je.bpm.core.model.task.SendTask;
import com.je.bpm.core.model.task.ServiceTask;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;


public class FieldExtensionParser extends BaseChildElementParser {

    @Override
    public String getElementName() {
        return ELEMENT_FIELD;
    }

    @Override
    public boolean accepts(BaseElement element) {
        return ((element instanceof ActivitiListener)
                || (element instanceof ServiceTask)
                || (element instanceof SendTask)
                || (element instanceof MessageEventDefinition));
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        if (!accepts(parentElement)) {
            return;
        }
        FieldExtension extension = new FieldExtension();
        BpmnXMLUtil.addXMLLocation(extension, xtr);
        extension.setFieldName(xtr.getAttributeValue(null, ATTRIBUTE_FIELD_NAME));

        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_FIELD_STRING))) {
            extension.setStringValue(xtr.getAttributeValue(null, ATTRIBUTE_FIELD_STRING));

        } else if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_FIELD_EXPRESSION))) {
            extension.setExpression(xtr.getAttributeValue(null, ATTRIBUTE_FIELD_EXPRESSION));

        } else {
            boolean readyWithFieldExtension = false;
            try {
                while (readyWithFieldExtension == false && xtr.hasNext()) {
                    xtr.next();
                    if (xtr.isStartElement() && ELEMENT_FIELD_STRING.equalsIgnoreCase(xtr.getLocalName())) {
                        extension.setStringValue(xtr.getElementText().trim());

                    } else if (xtr.isStartElement() && ATTRIBUTE_FIELD_EXPRESSION.equalsIgnoreCase(xtr.getLocalName())) {
                        extension.setExpression(xtr.getElementText().trim());

                    } else if (xtr.isEndElement() && getElementName().equalsIgnoreCase(xtr.getLocalName())) {
                        readyWithFieldExtension = true;
                    }
                }
            } catch (Exception e) {
                LOGGER.warn("Error parsing field extension child elements", e);
            }
        }

        if (parentElement instanceof ActivitiListener) {
            ((ActivitiListener) parentElement).getFieldExtensions().add(extension);
        } else if (parentElement instanceof ServiceTask) {
            ((ServiceTask) parentElement).getFieldExtensions().add(extension);
        } else if (parentElement instanceof SendTask) {
            ((SendTask) parentElement).getFieldExtensions().add(extension);
        } else if (parentElement instanceof MessageEventDefinition) {
            ((MessageEventDefinition) parentElement).getFieldExtensions().add(extension);
        }
    }
}
