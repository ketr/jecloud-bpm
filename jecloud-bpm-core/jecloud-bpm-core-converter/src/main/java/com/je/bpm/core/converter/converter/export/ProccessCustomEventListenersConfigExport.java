/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.export;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.core.model.process.Process;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.util.List;

/**
 * 流程自定义事件
 */
public class ProccessCustomEventListenersConfigExport implements BpmnXMLConstants {

    public static void writeConfigs(Process process, XMLStreamWriter xtw) throws Exception {
        List<CustomEvent> events = process.getCustomEventListeners();
        xtw.writeStartElement(BPMN2_PREFIX, ATTRIBUTE_PROCESS_CUSTOM_EVENTLISTENERS_CONFIG, BPMN2_NAMESPACE);
        writeEvents(events, xtw, ATTRIBUTE_PROCESS_CUSTOM_PROCESS_EVENT_CODE);
        xtw.writeEndElement();
    }

    public static void writeEvents(List<CustomEvent> events, XMLStreamWriter xtw, String type) throws XMLStreamException {
        for (CustomEvent customEvent : events) {
            xtw.writeStartElement(BPMN2_PREFIX, type, BPMN2_NAMESPACE);
            xtw.writeAttribute(ATTRIBUTE_ID, new CustomEvent().getConfigType());
            xtw.writeAttribute(ATTRIBUTE_PROCESS_CUSTOM_EVENT_TYPECODE, customEvent.getCustomeEventType().toString());
            xtw.writeAttribute(ATTRIBUTE_PROCESS_CUSTOM_EVENT_EXECUTIONSTRATEGYNAME, customEvent.getExecutionStrategy().toString());
            xtw.writeAttribute(ATTRIBUTE_PROCESS_CUSTOM_EVENT_ASSIGNMENTFIELDCONFIGURATION, customEvent.getAssignmentFieldConfiguration());
            xtw.writeAttribute(ATTRIBUTE_PROCESS_CUSTOM_EVENT_SERVICENAME, customEvent.getServiceName());
            xtw.writeAttribute(ATTRIBUTE_PROCESS_CUSTOM_EVENT_METHOD, customEvent.getMethod());
            xtw.writeAttribute(ATTRIBUTE_PROCESS_CUSTOM_EVENT_EXISTENCEPARAMETER, String.valueOf(customEvent.getExistenceParameter()));
            xtw.writeEndElement();
        }
    }
}

