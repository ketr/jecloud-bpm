/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child.config.assignment;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.converter.exceptions.XMLException;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.task.assignment.*;
import com.je.bpm.core.model.task.*;

public abstract class AbstractTaskAssignConfigParser extends BaseChildElementParser {

    protected TaskAssigneeConfigImpl getTaskAssigneeConfig(BaseElement parentElement) {
        TaskAssigneeConfigImpl taskAssigneeConfig;
        if (parentElement instanceof KaiteUserTask) {
            taskAssigneeConfig = ((KaiteUserTask) parentElement).getTaskAssigneeConfig();
        } else if (parentElement instanceof KaiteDecideUserTask) {
            taskAssigneeConfig = ((KaiteDecideUserTask) parentElement).getTaskAssigneeConfig();
        } else if (parentElement instanceof KaiteLoopUserTask) {
            taskAssigneeConfig = ((KaiteLoopUserTask) parentElement).getTaskAssigneeConfig();
        } else if (parentElement instanceof KaiteCounterSignUserTask) {
            taskAssigneeConfig = ((KaiteCounterSignUserTask) parentElement).getTaskAssigneeConfig();
        } else if (parentElement instanceof KaiteMultiUserTask) {
            taskAssigneeConfig = ((KaiteMultiUserTask) parentElement).getTaskAssigneeConfig();
        } else if (parentElement instanceof KaiteCandidateUserTask) {
            taskAssigneeConfig = ((KaiteCandidateUserTask) parentElement).getTaskAssigneeConfig();
        } else {
            throw new XMLException("Error parse element assignee config.");
        }
        return taskAssigneeConfig;
    }

    protected BasicAssignmentConfigImpl getAssignmentConfigImplByType(String type) {
        if (type.equalsIgnoreCase(ELEMENT_USER_CONFIG)) {
            return new UserAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ELEMENT_SPECTIAL_CONFIG)) {
            return new SpecialAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ELEMENT_USER_CONFIG)) {
            return new RoleAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ELEMENT_POSITION_CONFIG)) {
            return new PositionAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ELEMENT_ORGTREE_CONFIG)) {
            return new OrgTreeAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ATTRIBUTE_FORMFIELD_CODE)) {
            return new FormFieldAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ELEMENT_DEPARTMENT_CONFIG)) {
            return new DepartmentAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ELEMENT_PROJECT_DEPARTMENT_CONFIG)) {
            return new ProjectDepartmentAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ELEMENT_CUSTOM_CONFIG)) {
            return new CustomAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ELEMENT_BUSINESS_SQL_CONFIG)) {
            return new BusinessSqlAssignmentConfigImpl();
        } else if (type.equalsIgnoreCase(ELEMENT_ORG_ASYNC_TREE_CONFIG)) {
            return new OrgAsyncTreeAssignmentConfigImpl();
        }
        return null;
    }

}
