/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.parser;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.Interface;
import com.je.bpm.core.model.Operation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamReader;

public class InterfaceParser implements BpmnXMLConstants {

    protected static final Logger LOGGER = LoggerFactory.getLogger(InterfaceParser.class.getName());

    public void parse(XMLStreamReader xtr, BpmnModel model) throws Exception {

        Interface interfaceObject = new Interface();
        BpmnXMLUtil.addXMLLocation(interfaceObject, xtr);
        interfaceObject.setId(model.getTargetNamespace() + ":" + xtr.getAttributeValue(null, ATTRIBUTE_ID));
        interfaceObject.setName(xtr.getAttributeValue(null, ATTRIBUTE_NAME));
        interfaceObject.setImplementationRef(parseMessageRef(xtr.getAttributeValue(null, ATTRIBUTE_IMPLEMENTATION_REF), model));

        boolean readyWithInterface = false;
        Operation operation = null;
        try {
            while (!readyWithInterface && xtr.hasNext()) {
                xtr.next();
                if (xtr.isStartElement() && ELEMENT_OPERATION.equals(xtr.getLocalName())) {
                    operation = new Operation();
                    BpmnXMLUtil.addXMLLocation(operation, xtr);
                    operation.setId(model.getTargetNamespace() + ":" + xtr.getAttributeValue(null, ATTRIBUTE_ID));
                    operation.setName(xtr.getAttributeValue(null, ATTRIBUTE_NAME));
                    operation.setImplementationRef(parseMessageRef(xtr.getAttributeValue(null, ATTRIBUTE_IMPLEMENTATION_REF), model));

                } else if (xtr.isStartElement() && ELEMENT_IN_MESSAGE.equals(xtr.getLocalName())) {
                    String inMessageRef = xtr.getElementText();
                    if (operation != null && StringUtils.isNotEmpty(inMessageRef)) {
                        operation.setInMessageRef(parseMessageRef(inMessageRef.trim(), model));
                    }

                } else if (xtr.isStartElement() && ELEMENT_OUT_MESSAGE.equals(xtr.getLocalName())) {
                    String outMessageRef = xtr.getElementText();
                    if (operation != null && StringUtils.isNotEmpty(outMessageRef)) {
                        operation.setOutMessageRef(parseMessageRef(outMessageRef.trim(), model));
                    }

                } else if (xtr.isEndElement() && ELEMENT_OPERATION.equalsIgnoreCase(xtr.getLocalName())) {
                    if (operation != null && StringUtils.isNotEmpty(operation.getImplementationRef())) {
                        interfaceObject.getOperations().add(operation);
                    }

                } else if (xtr.isEndElement() && ELEMENT_INTERFACE.equals(xtr.getLocalName())) {
                    readyWithInterface = true;
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Error parsing interface child elements", e);
        }

        model.getInterfaces().add(interfaceObject);
    }

    protected String parseMessageRef(String messageRef, BpmnModel model) {
        String result = null;
        if (StringUtils.isNotEmpty(messageRef)) {
            int indexOfP = messageRef.indexOf(':');
            if (indexOfP != -1) {
                String prefix = messageRef.substring(0, indexOfP);
                String resolvedNamespace = model.getNamespace(prefix);
                messageRef = messageRef.substring(indexOfP + 1);

                if (resolvedNamespace == null) {
                    // if it's an invalid prefix will consider this is not a
                    // namespace prefix so will be used as part of the
                    // stringReference
                    messageRef = prefix + ":" + messageRef;
                } else if (!resolvedNamespace.equalsIgnoreCase(model.getTargetNamespace())) {
                    // if it's a valid namespace prefix but it's not the
                    // targetNamespace then we'll use it as a valid namespace
                    // (even out editor does not support defining namespaces it
                    // is still a valid xml file)
                    messageRef = resolvedNamespace + ":" + messageRef;
                }
            }
            result = messageRef;
        }
        return result;
    }
}
