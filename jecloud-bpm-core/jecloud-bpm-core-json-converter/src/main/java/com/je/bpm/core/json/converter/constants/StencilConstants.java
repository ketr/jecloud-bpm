/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.json.converter.constants;

public interface StencilConstants {

    // stencil items
    String STENCIL_EVENT_START_NONE = "StartNoneEvent";
    String STENCIL_EVENT_START_KAITE_NONE = "start";
    String STENCIL_EVENT_START_TIMER = "StartTimerEvent";
    String STENCIL_EVENT_START_MESSAGE = "StartMessageEvent";
    String STENCIL_EVENT_START_SIGNAL = "StartSignalEvent";
    String STENCIL_EVENT_START_ERROR = "StartErrorEvent";

    String STENCIL_EVENT_END_NONE = "EndNoneEvent";
    String STENCIL_EVENT_END_KAITE_NONE = "end";
    String STENCIL_EVENT_END_ERROR = "EndErrorEvent";
    String STENCIL_EVENT_END_CANCEL = "EndCancelEvent";
    String STENCIL_EVENT_END_TERMINATE = "EndTerminateEvent";

    String STENCIL_SUB_PROCESS = "SubProcess";
    String STENCIL_EVENT_SUB_PROCESS = "EventSubProcess";
    String STENCIL_CALL_ACTIVITY = "CallActivity";

    String STENCIL_POOL = "Pool";
    String STENCIL_LANE = "Lane";

    String STENCIL_TASK_BUSINESS_RULE = "BusinessRule";
    String STENCIL_TASK_MAIL = "MailTask";
    String STENCIL_TASK_MANUAL = "ManualTask";
    String STENCIL_TASK_RECEIVE = "ReceiveTask";
    String STENCIL_TASK_SCRIPT = "ScriptTask";
    String STENCIL_TASK_SEND = "SendTask";
    String STENCIL_TASK_SERVICE = "ServiceTask";
    String STENCIL_TASK_USER = "UserTask";

    //------------通用属性
    String UNIVERSAL_RESOURCE = "resource";

    String UNIVERSAL_TYPE = "type";

    String UNIVERSAL_NAME = "name";

    String UNIVERSAL_CONFIG = "config";

    String UNIVERSAL_SERVICE = "service";

    String UNIVERSAL_METHOD = "method";

    String UNIVERSAL_ID = "id";

    String UNIVERSAL_RESOURCE_CODE = "resourceCode";

    String UNIVERSAL_RESOURCE_ID = "resourceId";

    String UNIVERSAL_RESOURCE_NAME = "resourceName";

    String UNIVERSAL_CODE = "code";

    String UNIVERSAL_ENABLE = "enable";

    String UNIVERSAL_SQL = "sql";

    /**
     * 用户任务
     */
    String STENCIL_KAITE_USER_TASK = "task";
    /**
     * 多实例用户任务 KaiteMultiUserTask
     */
    String STENCIL_KAITE_MULTI_USER_TASK = "batchtask";
    /**
     * 自由循环任务 KaiteLoopUserTask
     */
    String STENCIL_KAITE_LOOP_USER_TASK = "circular";
    /**
     * 固定人任务 KaiteFixUserTask
     */
    String STENCIL_KAITE_FIXED_USER_TASK = "to_assignee";
    /**
     * 随机任务 KaiteRandomUserTask
     */
    String STENCIL_KAITE_RANDOM_USER_TASK = "random";
    String STENCIL_KAITE_RANDOM_CONFIG = "randomConfig";
    String STENCIL_KAITE_RANDOM_DISTRIBUTEDPOLLING = "distributedPolling";
    String STENCIL_KAITE_RANDOM_DISTRIBUTERANDOM = "distributeRandom";
    String PROPERTY_TASK_RANDOM_TEAM_NAME = "teamName";
    /**
     * 判断用户任务 KaiteDecideUserTask
     */
    String STENCIL_KAITE_DECIDE_USER_TASK = "decision";
    /**
     * 会签用户任务 kaiteCounterSignUserTask
     */
    String STENCIL_KAITE_COUNTERSIGN_USER_TASK = "countersign";
    /**
     * 候选用户任务 kaiteCandidateUserTask
     */
    String STENCIL_KAITE_CANDIDATE_USER_TASK = "joint";
    /**
     * 候选配置
     */
    String STENCIL_KAITE_CANDIDATE_CANDIDATE_CONFIG = "candidateConfig";
    /**
     * 提交给团队
     */
    String STENCIL_KAITE_CANDIDATE_CANDIDATE_SUBMITTOTEAM = "submitToTeam";

    String STENCIL_TASK_CAMEL = "CamelTask";
    String STENCIL_TASK_MULE = "MuleTask";
    String STENCIL_TASK_SHELL = "ShellTask";
    String STENCIL_TASK_DECISION = "DecisionTask";

    String STENCIL_GATEWAY_EXCLUSIVE = "ExclusiveGateway";
    String STENCIL_GATEWAY_PARALLEL = "ParallelGateway";
    String STENCIL_GATEWAY_INCLUSIVE = "InclusiveGateway";
    String STENCIL_GATEWAY_KAITE_FORK__INCLUSIVE = "branchGateway";
    String STENCIL_GATEWAY_KAITE_JOIN_INCLUSIVE = "aggregationGateway";
    String STENCIL_GATEWAY_EVENT = "EventGateway";

    String STENCIL_EVENT_BOUNDARY_TIMER = "BoundaryTimerEvent";
    String STENCIL_EVENT_BOUNDARY_ERROR = "BoundaryErrorEvent";
    String STENCIL_EVENT_BOUNDARY_SIGNAL = "BoundarySignalEvent";
    String STENCIL_EVENT_BOUNDARY_MESSAGE = "BoundaryMessageEvent";
    String STENCIL_EVENT_BOUNDARY_CANCEL = "BoundaryCancelEvent";
    String STENCIL_EVENT_BOUNDARY_COMPENSATION = "BoundaryCompensationEvent";

    String STENCIL_EVENT_CATCH_SIGNAL = "CatchSignalEvent";
    String STENCIL_EVENT_CATCH_TIMER = "CatchTimerEvent";
    String STENCIL_EVENT_CATCH_MESSAGE = "CatchMessageEvent";

    String STENCIL_EVENT_THROW_SIGNAL = "ThrowSignalEvent";
    String STENCIL_EVENT_THROW_NONE = "ThrowNoneEvent";

    String STENCIL_SEQUENCE_FLOW = "SequenceFlow";
    String STENCIL_SEQUENCE_KAITE_FLOW = "line";
    String STENCIL_MESSAGE_FLOW = "MessageFlow";
    String STENCIL_ASSOCIATION = "Association";
    String STENCIL_DATA_ASSOCIATION = "DataAssociation";

    String STENCIL_TEXT_ANNOTATION = "TextAnnotation";
    String STENCIL_DATA_STORE = "DataStore";

    String PROPERTY_VALUE_YES = "Yes";
    String PROPERTY_VALUE_NO = "No";

    // stencil properties
    String PROPERTY_OVERRIDE_ID = "overrideId";
    String PROPERTY_NAME = "name";
    String PROPERTY_DOCUMENTATION = "documentation";

    String PROPERTY_PROCESS_ID = "process_id";
    String PROPERTY_PROCESS_VERSION = "process_version";
    String PROPERTY_PROCESS_AUTHOR = "process_author";
    String PROPERTY_PROCESS_NAMESPACE = "process_namespace";
    String PROPERTY_PROCESS_EXECUTABLE = "process_executable";

    String PROPERTY_TIMER_DURATON = "timerdurationdefinition";
    String PROPERTY_TIMER_DATE = "timerdatedefinition";
    String PROPERTY_TIMER_CYCLE = "timercycledefinition";
    String PROPERTY_TIMER_CYCLE_END_DATE = "timerenddatedefinition";

    String PROPERTY_MESSAGES = "messages";
    String PROPERTY_MESSAGE_ID = "message_id";
    String PROPERTY_MESSAGE_NAME = "message_name";
    String PROPERTY_MESSAGE_ITEM_REF = "message_item_ref";

    String PROPERTY_MESSAGEREF = "messageref";

    String PROPERTY_SIGNALREF = "signalref";

    String PROPERTY_ERRORREF = "errorref";

    String PROPERTY_CANCEL_ACTIVITY = "cancelactivity";

    String PROPERTY_NONE_STARTEVENT_INITIATOR = "initiator";

    String PROPERTY_ASYNCHRONOUS = "asynchronousdefinition";
    String PROPERTY_EXCLUSIVE = "exclusivedefinition";

    String PROPERTY_MULTIINSTANCE_TYPE = "multiinstance_type";
    String PROPERTY_MULTIINSTANCE_CARDINALITY = "multiinstance_cardinality";
    String PROPERTY_MULTIINSTANCE_COLLECTION = "multiinstance_collection";
    String PROPERTY_MULTIINSTANCE_VARIABLE = "multiinstance_variable";
    String PROPERTY_MULTIINSTANCE_CONDITION = "multiinstance_condition";

    //会签config
    String PROPERTY_COUNTERSIGNED_CONFIG = "taskCounterSignConfig";
    //会签类型，默认是比例通过制
    String PROPERTY_COUNTERSIGNED_COUNTERSIGNPASSTYPE = "counterSignPassType";
    //通过比例
    String PROPERTY_COUNTERSIGNED_AMOUNT = "amount";
    //负责人
    String PROPERTY_COUNTERSIGNED_ONEBALLOTUSERID = "oneBallotUserId";
    //负责人
    String PROPERTY_COUNTERSIGNED_ONEBALLOTUSERNAME = "oneBallotUserName";
    //全部投票
    String PROPERTY_COUNTERSIGNED_VOTEALL = "voteAll";
    //运行时调整
    String RUNTIME_TUNING = "runtimeTuning";
    //运行前调整
    String ADJUST_BEFORE_RUNNING = "adjustBeforeRunning";
    //取消全选
    String PROPERTY_COUNTERSIGNED_DESELECT_ALL = "deselectAll";
    //顺序审批
    String PROPERTY_COUNTERSIGNED_SEQUENTIAL = "sequential";


    String PROPERTY_TASK_LISTENERS = "tasklisteners";
    String PROPERTY_EXECUTION_LISTENERS = "executionlisteners";
    String PROPERTY_LISTENER_EVENT = "event";
    String PROPERTY_LISTENER_CLASS_NAME = "className";
    String PROPERTY_LISTENER_EXPRESSION = "expression";
    String PROPERTY_LISTENER_DELEGATE_EXPRESSION = "delegateExpression";
    String PROPERTY_LISTENER_FIELDS = "fields";

    String PROPERTY_EVENT_LISTENERS = "eventlisteners";
    String PROPERTY_EVENTLISTENER_VALUE = "eventListeners";
    String PROPERTY_EVENTLISTENER_EVENTS = "events";
    String PROPERTY_EVENTLISTENER_EVENT = "event";
    String PROPERTY_EVENTLISTENER_IMPLEMENTATION = "implementation";
    String PROPERTY_EVENTLISTENER_RETHROW_EVENT = "rethrowEvent";
    String PROPERTY_EVENTLISTENER_RETHROW_TYPE = "rethrowType";
    String PROPERTY_EVENTLISTENER_CLASS_NAME = "className";
    String PROPERTY_EVENTLISTENER_DELEGATE_EXPRESSION = "delegateExpression";
    String PROPERTY_EVENTLISTENER_ENTITY_TYPE = "entityType";
    String PROPERTY_EVENTLISTENER_ERROR_CODE = "errorcode";
    String PROPERTY_EVENTLISTENER_SIGNAL_NAME = "signalname";
    String PROPERTY_EVENTLISTENER_MESSAGE_NAME = "messagename";

    String PROPERTY_FIELD_NAME = "name";
    String PROPERTY_FIELD_STRING_VALUE = "stringValue";
    String PROPERTY_FIELD_EXPRESSION = "expression";
    String PROPERTY_FIELD_STRING = "string";

    String PROPERTY_FORMKEY = "formkeydefinition";

    String PROPERTY_USERTASK_ASSIGNMENT = "usertaskassignment";
    String PROPERTY_USERTASK_PRIORITY = "prioritydefinition";
    String PROPERTY_USERTASK_DUEDATE = "duedatedefinition";
    String PROPERTY_USERTASK_ASSIGNEE = "assignee";
    String PROPERTY_USERTASK_CANDIDATE_USERS = "candidateUsers";
    String PROPERTY_USERTASK_CANDIDATE_GROUPS = "candidateGroups";
    String PROPERTY_USERTASK_CATEGORY = "categorydefinition";

    //----------------------流程自定义属性----------------------

    String PROPERTY_ID = "id";
    String PROPERTY_CODE = "code";
    String PROPERTY_ENABLE = "enable";
    String PROPERTY_ICON = "icon";
    String PROPERTY_LABEL = "label";
    String PROPERTY_EDITABLE = "editable";

    /**
     * 流程基础配置
     */
    String PROPERTY_PROCESS_BASIC_CONFIG = "processBasicConfig";
    /**
     * 流程拓展配置
     */
    String PROPERTY_PROCESS_EXTENDED_CONFIG = "extendedConfiguration";
    /**
     * 流程启动配置
     */
    String PROPERTY_PROCESS_STARTUP_CONFIG = "startupSettings";
    /**
     * 流程启动配置
     */
    String PROPERTY_PROCESS_CUSTOM_EVENT_LISTENERS_CONFIG = "customEventListeners";
    /**
     * 流程按钮配置
     */
    String PROPERTY_PROCESS_BUTTON_CONFIG = "processButtonConfig";
    /**
     * 功能编码
     */
    String PROPERTY_FUNC_CODE = "funcCode";
    /**
     * 功能名称
     */
    String PROPERTY_FUNC_NAME = "funcName";
    /**
     * 功能ID
     */
    String PROPERTY_FUNC_ID = "funcId";
    /**
     * 表code
     */
    String PROPERTY_FUNC_TABLE_CODE = "tableCode";
    /**
     * 附属功能
     */
    String PROPERTY_FUNC_ATTACHED_FUNC_IDS = "attachedFuncIds";
    /**
     * 附属功能
     */
    String PROPERTY_FUNC_ATTACHED_FUNC_NAMES = "attachedFuncNames";
    /**
     * 附属功能
     */
    String PROPERTY_FUNC_ATTACHED_FUNC_CODES = "attachedFuncCodes";
    /**
     * 内容模板
     */
    String PROPERTY_FUNC_CONTENT_MODULE = "contentModule";
    /**
     * 流程分类
     */
    String PROPERTY_FUNC_PROCESS_CLASSIFICATION_CODE = "processClassificationCode";
    /**
     * 流程分类
     */
    String PROPERTY_FUNC_PROCESS_CLASSIFICATION_NAME = "processClassificationName";
    /**
     * 流程分类
     */
    String PROPERTY_FUNC_PROCESS_CLASSIFICATION_ID = "processClassificationId";
    /**
     * 运行模式配置,部署环境
     */
    String PROPERTY_PROCESS_MODE = "deploymentEnvironment";
    /**
     * 是否可取回配置
     */
    String PROPERTY_PROCESS_CANRETRIEVE = "canRetrieve";
    /**
     * 是否可发起配置
     */
    String PROPERTY_PROCESS_CANSPONSOR = "canSponsor";
    /**
     * 是否可转办配置
     */
    String PROPERTY_PROCESS_CANTRANSFER = "canTransfer";
    /**
     * 是否可委托配置
     */
    String PROPERTY_PROCESS_CANDELEGATE = "canDelegate";
    /**
     * 加签节点
     */
    String PROPERTY_PROCESS_COUNTERSIGNNODE = "countersignNode";
    /**
     * 简易发起
     */
    String PROPERTY_PROCESS_EASY_LAUNCH = "easyLaunch";
    /**
     * 简易审批
     */
    String PROPERTY_PROCESS_SIMPLE_APPROVAL = "simpleApproval";
    String PROPERTY_ENABLE_SIMPLE_COMMENTS = "enableSimpleComments";
    /**
     * 紧急状态
     */
    String ATTRIBUTE_PROCESS_EXIGENCY = "exigency";
    /**
     * 隐藏状态
     */
    String PROPERTY_PROCESS_HIDE_STATE_INFO = "hideStateInfo";
    /**
     * 审批耗时时间
     */
    String PROPERTY_PROCESS_HIDE_TIME_INFO = "hideTimeInfo";
    /**
     * 是否可催办配置
     */
    String PROPERTY_PROCESS_CANURGED = "canUrged";
    /**
     * 可作废配置
     */
    String PROPERTY_PROCESS_CANINVALID = "canInvalid";
    /**
     * 是否可撤销配置
     */
    String PROPERTY_PROCESS_CANCANCEL = "canCancel";
    /**
     * 是否可催办配置
     */
    String PROPERTY_PROCESS_INITIATOR_CANURGED = "initiatorCanUrged";
    /**
     * 可作废配置
     */
    String PROPERTY_PROCESS_INITIATOR_CANINVALID = "initiatorInvalid";
    /**
     * 是否可撤销配置
     */
    String PROPERTY_PROCESS_INITIATOR_CANCANCEL = "initiatorCanCancel";
    /**
     * 是否启用退回
     */
    String PROPERTY_PROCESS_RETURN = "canReturn";
    /**
     * 是否任何人可以启动流程
     */
    String PROPERTY_PROCESS_CANEVERYONESTART = "canEveryoneStart";
    /**
     * 可启动角色id
     */
    String PROPERTY_PROCESS_CANEVERYONE_ROLES_ID = "canEveryoneRolesId";
    /**
     * 可启动角色name
     */
    String PROPERTY_PROCESS_CANEVERYONE_ROLES_NAME = "canEveryoneRolesName";
    /**
     * 启动表达式
     */
    String PROPERTY_PROCESS_START_EXPRESSION = "startExpression";
    /**
     * 启动表达式fn
     */
    String PROPERTY_PROCESS_START_EXPRESSION_FN = "startExpressionFn";
    /**
     * 流程提醒配置
     */
    String PROPERTY_PROCESS_REMINDTYPES = "remindTypes";
    /**
     * 是否展示流程追踪
     */
    String PROPERTY_PROCESS_DISPLAYTRACEBUTTON = "displayTraceButton";


    //----------------------流程消息----------------------
    /**
     * 流程消息
     */
    String PROPERTY_PROCESS_MESSAGE_SETTING = "messageSetting";
    /**
     * 消息模板信息
     */
    String PROPERTY_PROCESS_MESSAGE_DEFINITIONS = "messageDefinitions";
    /**
     * 消息勾选类型
     */
    String PROPERTY_PROCESS_MESSAGE_MESSAGES = "messages";
    String PROPERTY_PROCESS_MESSAGE_MESSAGES_DINGTALK_ID = "jeDingTalkId";
    String PROPERTY_PROCESS_MESSAGE_MESSAGES_DINGTALK_NAME = "jeDingTalkName";

    //----------------------事件自定义----------------------
    /**
     * 事件类型
     */
    String CUSTOM_EVENT_TYPE_NAME = "typeName";
    /**
     * 事件类型
     */
    String CUSTOM_EVENT_TYPE_CODE = "typeCode";
    /**
     * 执行策略
     */
    String CUSTOM_EVENT_EXECUTION_STRATEGY_NAME = "executionStrategyName";
    /**
     * 执行策略
     */
    String CUSTOM_EVENT_EXECUTION_STRATEGY_CODE = "executionStrategyCode";
    /**
     * 赋值字段配置
     */
    String CUSTOM_EVENT_ASSIGNMENT_FIELD_CONFIGURATION = "assignmentFieldConfiguration";
    /**
     * 业务bean
     */
    String CUSTOM_EVENT_SERVICE_NAME = "serviceName";
    /**
     * 业务方法
     */
    String CUSTOM_EVENT_METHOD = "method";
    /**
     * 是否存在参数
     */
    String CUSTOM_EVENT_EXISTENCEPARAMETER = "existenceParameter";


    //----------------------按钮自定义属性----------------------
    /**
     * 任务按钮配置
     */
    String PROPERTY_BUTTONS_CONFIG = "buttonsConfig";
    /**
     * 按钮ID配置
     */
    String PROPERTY_BUTTON_ID = "buttonId";
    /**
     * 按钮Name配置
     */
    String PROPERTY_BUTTON_NAME = "buttonName";
    /**
     * 按钮自定义名称
     */
    String PROPERTY_BUTTON_CUSTOMIZE_NAME = "customizeName";
    /**
     * 按钮自定义审批意见
     */
    String PROPERTY_BUTTON_CUSTOMIZE_COMMENTS = "customizeComments";
    /**
     * 按钮Code配置
     */
    String PROPERTY_BUTTON_CODE = "buttonCode";
    String PROPERTY_BUTTON_OPERATION = "operationId";
    String PROPERTY_BUTTON_APP_LISTENERS = "appListeners";
    String PROPERTY_BUTTON_PC_LISTENERS = "pcListeners";
    String PROPERTY_BUTTON_BACKEND_LISTENERS = "backendListeners";
    /**
     * 展示表达式
     */
    String PROPERTY_TASK_BUTTON_DISLAY_EXPRESSION = "displayExpression";
    /**
     * 流程未启动时是否展示
     */
    String PROPERTY_PROCESS_BUTTON_DISLAY_WHEN_NOT_STARTED = "displayWhenNotStarted";

    //----------------------任务自定义属性----------------------


    /**
     * 任务指派人配置
     */
    String PROPERTY_ASSIGNMENT_CONFIG = "assignmentConfig";
    /**
     * 人员参照
     */
    String PROPERTY_ASSIGNMENT_REFERTONAME_CONFIG = "referToName";
    /**
     * 人员参照
     */
    String PROPERTY_ASSIGNMENT_REFERTOCODE_CONFIG = "referToCode";
    /**
     * 权限
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_CONFIG = "permission";
    /**
     * 进入路径
     */
    String PROPERTY_ASSIGNMENT_ENTRY_PATH_CONFIG = "entryPath";

    /**
     * 自定义处理人service名称
     */
    String PROPERTY_ASSIGNMENT_CUSTOM_SERVICENAME = "serviceName";

    /**
     * 自定义处理人方法名称
     */
    String PROPERTY_ASSIGNMENT_CUSTOM_METHODNAME = "methodName";

    /**
     * 公司内可见
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_COMPANY = "company";
    /**
     * 公司监管可见
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_COMPANY_SUPERVISION = "companySupervision";
    /**
     * 本部门可见
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_DEPT = "dept";
    /**
     * 部门内可见
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_DEPT_ALL = "deptAll";
    /**
     * 直属领导
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_DIRECT_LEADER = "directLeader";
    /**
     * 部门领导
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_DEPT_LEADER = "deptLeader";
    /**
     * 监管领导
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_SUPERVISION_LEADER = "supervisionLeader";
    /**
     * sql
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_SQL = "sql";
    /**
     * sqlRemarks
     */
    String PROPERTY_ASSIGNMENT_PERMISSION_SQL_REMARKS = "sqlRemarks";


    //-------------------------------自定义岗位配置-------------------------------
    /**
     * 用户任务指派人(岗位)配置
     */
    String PROPERTY_POSITION_CONFIG = "positionConfig";
    //-------------------------------自定义角色配置-------------------------------
    /**
     * 用户任务指派人(角色)配置
     */
    String PROPERTY_ROLE_CONFIG = "roleConfig";
    //-------------------------------自定义部门配置-------------------------------
    /**
     * 用户任务指派人(部门)配置
     */
    String PROPERTY_DEPARTMENT_CONFIG = "departmentConfig";
    //-------------------------------自定义用户配置-------------------------------
    /**
     * 用户任务指派人(用户)配置
     */
    String PROPERTY_USER_CONFIG = "userConfig";
    //-------------------------------自定义工作组配置-------------------------------
    /**
     * 用户任务指派人(工作组)配置
     */
    String PROPERTY_WORKGROUP_CONFIG = "workGroupConfig";
    /**
     * 用户固定人指派人(工作组)配置
     */
    String PROPERTY_FIXEDUSER_CONFIG = "fixedUserConfig";
    /**
     * 特殊配置
     */
    String PROPERTY_SPECTIAL_CONFIG = "spectialConfig";
    /**
     * 特殊配置枚举类型
     */
    String PROPERTY_SPECTIAL_TYPES = "types";
    /**
     * 业务SQL
     */
    String PROPERTY_BUSINESS_SQL_CONFIG = "businessSqlConfig";
    /**
     * 业务SQL
     */
    String PROPERTY_SQL = "sql";
    /**
     * Rbac的SQL配置
     */
    String PROPERTY_RBAC_SQL_CONFIG = "rbacSqlConfig";
    /**
     * 自定义配置
     */
    String PROPERTY_CUSTOM_CONFIG = "customConfig";
    /**
     * 自定义方法名
     */
    String PROPERTY_CUSTOM_SERVICENAME = "serviceName";
    /**
     * 表单字段配置
     */
    String PROPERTY_FORMFIELD_CONFIG = "formFieldConfig";
    /**
     * 表单字段
     */
    String PROPERTY_FORMFIELD_CODE = "formFieldCode";
    /**
     * 组织结构树配置
     */
    String PROPERTY_ORGTREE_CONFIG = "orgTreeConfig";
    /**
     * 组织结构树配置
     */
    String PROPERTY_ORG_ASYNC_TREE_CONFIG = "orgAsyncTreeConfig";
    /**
     * 用户任务基础配置
     */
    String PROPERTY_USERTASK_BASIC_CONFIG = "basicConfig";
    /**
     * 人员是否全选
     */
    String PROPERTY_USERTASK_BASIC_ALLUSERDEFAULTSELECTED = "allUserDefaultSelected";
    /**
     * 是否是异步树
     */
    String PROPERTY_USERTASK_BASIC_ASYNCTREE = "asyncTree";
    /**
     * 处理时效
     */
    String PROPERTY_USERTASK_BASIC_HANDLE_PERIOD = "handlePeriod";
    /**
     * 处理时效类型
     */
    String PROPERTY_USERTASK_BASIC_HANDLE_PERIODTYPE = "handlePeriodType";
    /**
     * 任务标签意见配置
     */
    String PROPERTY_USERTASK_COMMENTS_CONFIG = "commentsConfig";
    /**
     * 任务标签意见配置
     */
    String PROPERTY_USERTASK_COMMENT_KEY = "key";
    /**
     * 任务标签意见配置
     */
    String PROPERTY_USERTASK_COMMENT_VALUE = "value";
    /**
     * 任务延期配置
     */
    String PROPERTY_USERTASK_DELAY_CONFIG = "delayConfig";
    /**
     * 任务延期事件类型配置
     */
    String PROPERTY_USERTASK_DELAY_TIMETYPE = "delayTimeType";
    /**
     * 任务办理周期
     */
    String PROPERTY_USERTASK_DELAY_PERIOD = "delayPeriod";
    /**
     * 可延期次数
     */
    String PROPERTY_USERTASK_DELAY_TIMES = "delayTimes";

    /**
     * 任务委托配置
     */
    String PROPERTY_USERTASK_DELEGATE_CONFIG = "delegateConfig";
    /**
     * 任务驳回配置
     */
    String PROPERTY_USERTASK_DISMISS_CONFIG = "dismissConfig";
    /**
     * 任务驳回分解配置
     */
    String PROPERTY_USERTASK_DISMISS_COMMIT_BREAKDOWN_CONFIG = "commitBreakdown";
    /**
     * 任务驳回节点
     */
    String PROPERTY_USERTASK_DISMISS_TASKNAME = "dismissTaskNames";
    /**
     * 任务驳回节点
     */
    String PROPERTY_USERTASK_DISMISS_TASKID = "dismissTaskIds";
    /**
     * 驳回后是否启用直送按钮
     */
    String PROPERTY_USERTASK_DISMISS_ENABLEDIRECTSEND = "directSendAfterDismiss";
    /**
     * 被驳回后强制提交 只能提交，不能直送
     */
    String PROPERTY_USERTASK_DISMISS_FORCECOMMITAFTERDISMISS = "forceCommitAfterDismiss";
    /**
     * 被驳回后禁用送交
     */
    String PROPERTY_USERTASK_DISMISS_DISABLESENDAFTERDISMISS = "disableSendAfterDismiss";
    /**
     * 不可退回
     */
    String PROPERTY_USERTASK_DISMISS_NORETURN = "noReturn";
    /**
     * 可直接送交退回人
     */
    String PROPERTY_USERTASK_DISMISS_DIRECTSENDAFTERRETURN = "directSendAfterReturn";
    /**
     * 退回后不可提交，只可以直送
     */
    String PROPERTY_USERTASK_DISMISS_DISABLESUBMIT = "disableSubmitAfterDismiss";
    /**
     * 任务预警配置
     */
    String PROPERTY_USERTASK_EARLYWARNING_CONFIG = "earlyWarningConfig";
    /**
     * 是否工作日提醒
     */
    String PROPERTY_USERTASK_EARLYWARNING_WORKDAY = "workday";
    /**
     * 提前周期
     */
    String PROPERTY_USERTASK_EARLYWARNING_BEFORE_PERIOD = "beforePeriod";
    /**
     * 提前策略类型
     */
    String PROPERTY_USERTASK_EARLYWARNING_BEFORE_STRATEGY_TYPE = "beforeStrategyType";
    /**
     * 任务预警策略配置
     */
    String PROPERTY_USERTASK_EARLYWARNING_STRATEGY = "earlyWarningStrategy";
    /**
     * 任务预警策略类型配置
     */
    String PROPERTY_USERTASK_EARLYWARNING_STRATEGY_TYPE = "strategyType";
    /**
     * 任务预警策略周期配置
     */
    String PROPERTY_USERTASK_EARLYWARNING_STRATEGY_PERIOD = "strategyPeriod";
    /**
     * 任务预警策略扩展类名称
     */
    String PROPERTY_USERTASK_EARLYWARNING_STRATEGY_SERVICENAME = "strategyServiceName";
    /**
     * 任务退回配置
     */
    String PROPERTY_USERTASK_GOBACK_CONFIG = "gobackConfig";
    /**
     * 退回后可直接提交(直送按钮启用)
     */
    String PROPERTY_USERTASK_GOBACK_DIRECTSUBMITAFTERGOBACK = "directSubmitAfterGoback";
    /**
     * 退回后不可提交，只可以直送
     */
    String PROPERTY_USERTASK_GOBACK_DISABLESUBMITAFTERGOBACK = "disableSubmitAfterGoback";
    /**
     * 任务是否可作废配置
     */
    String PROPERTY_USERTASK_INVALID_CONFIG = "invalidConfig";
    /**
     * 任务跳跃配置
     */
    String PROPERTY_USERTASK_JUMP_CONFIG = "jumpConfig";
    /**
     * 可以跳跃到的节点
     */
    String PROPERTY_USERTASK_JUMP_TASKS = "jumpTasks";
    /**
     * 任务标签意见配置
     */
    String PROPERTY_USERTASK_LABELCOMMENT_CONFIG = "labelCommentConfig";
    /**
     * 任务标签意见配置
     */
    String PROPERTY_USERTASK_COMMENT_LABEL = "label";
    /**
     * 任务标签意见配置
     */
    String PROPERTY_USERTASK_COMMENT_CONTENT = "content";

    /**
     * 任务监听配置
     */
    String PROPERTY_USERTASK_LISTENER_CONFIG = "listenerConfig";
    /**
     * 任务监听类型
     */
    String PROPERTY_USERTASK_LISTENER_TYPE = "listenerType";
    /**
     * 任务监听策略
     */
    String PROPERTY_USERTASK_LISTENER_STRATEGY = "strategy";
    /**
     * 任务字段值
     */
    String PROPERTY_USERTASK_LISTENER_FIELDVALUES = "fieldValues";
    /**
     * 调用类名称
     */
    String PROPERTY_USERTASK_LISTENER_BEANNAME = "beanName";
    /**
     * 传阅设置
     */
    String PROPERTY_USERTASK_PASSROUND_CONFIG = "passRoundConfig";
    /**
     * 传阅人员
     */
    String PROPERTY_USERTASK_PASSROUND_CIRCULATIONRULES_CONFIG = "circulationRules";
    /**
     * 传阅资源信息名称ID
     */
    String PASS_ROUND_ID = "passRoundId";
    /**
     * 传阅资源信息名称
     */
    String PROPERTY_USERTASK_PASSROUND_CIRCULATIONRULES_NAME = "ruleName";
    /**
     * 传阅资源信息CODE
     */
    String PROPERTY_USERTASK_PASSROUND_CIRCULATIONRULES_CODE = "ruleCode";
    /**
     * 是否开启自动传阅
     */
    String PROPERTY_USERTASK_PASSROUND_AUTO = "auto";
    /**
     * 预定义配置
     */
    String PROPERTY_USERTASK_PREDEFINED_CONFIG = "predefinedConfig";
    /**
     * 提醒配置
     */
    String PROPERTY_USERTASK_REMIND_CONFIG = "remindConfig";
    /**
     * 提醒类型配置
     */
    String PROPERTY_USERTASK_REMIND_TYPE = "remindType";
    /**
     * 任务取回配置
     */
    String PROPERTY_USERTASK_RETRIEVE_CONFIG = "retrieveConfig";
    /**
     * 任务转办配置
     */
    String PROPERTY_USERTASK_TRANSFER_CONFIG = "transferConfig";
    /**
     * 任务催办配置
     */
    String PROPERTY_USERTASK_URGE_CONFIG = "urgeConfig";
    /**
     * 任务表单配置
     */
    String PROPERTY_USERTASK_FORM_CONFIG = "formConfig";
    /**
     * 字段赋值
     */
    String PROPERTY_USERTASK_FORM_FIELD_ASSIGNMENT_CONFIG = "fieldAssignment";
    /**
     * 字段配置
     */
    String PROPERTY_USERTASK_FORM_FIELD_CONTROL_CONFIG = "fieldControl";
    /**
     * 按钮
     */
    String PROPERTY_USERTASK_FORM_TASKFORMBUTTON_CONFIG = "taskFormButton";
    /**
     * 子功能
     */
    String PROPERTY_USERTASK_FORM_TASKCHILDFUNC_CONFIG = "taskChildFunc";

    /**
     * 表单基础配置
     */
    String PROPERTY_USERTASK_FORM_BASIC = "formBasic";
    /**
     * 表单基础配置-是否可编辑
     */
    String PROPERTY_USERTASK_FORM_BASIC_EDITABLE = "editable";
    /**
     * 表单按钮配置
     */
    String PROPERTY_USERTASK_FORM_BUTTONS = "formButtons";
    /**
     * 表单子功能配置
     */
    String PROPERTY_USERTASK_FORM_CHILDFUNCS = "formChildFuncs";
    /**
     * 表单子功能是否可编辑
     */
    String PROPERTY_USERTASK_FORM_CHILDFUNC_EDITABLE = "editable";
    /**
     * 表单子功能隐藏
     */
    String PROPERTY_USERTASK_FORM_CHILDFUNC_HIDDEN = "hidden";
    /**
     * 表单子功能展示
     */
    String PROPERTY_USERTASK_FORM_CHILDFUNC_DISPLAY = "display";
    /**
     * 表单字段配置
     */
    String PROPERTY_USERTASK_FORM_FORMFIELDS = "formFields";
    /**
     * 表单字段是否可编辑
     */
    String PROPERTY_USERTASK_FORM_FORMFIELD_EDITABLE = "editable";
    /**
     * 表单字段是否只读
     */
    String PROPERTY_USERTASK_FORM_FORMFIELD_READONLY = "readOnly";
    String PROPERTY_USERTASK_FORM_FORMFIELD_READONLY_DOWN = "readonly";
    /**
     * 表单字段是否隐藏
     */
    String PROPERTY_USERTASK_FORM_FORMFIELD_HIDDEN = "hidden";
    /**
     * 表单字段是否展示
     */
    String PROPERTY_USERTASK_FORM_FORMFIELD_DISPLAY = "display";
    /**
     * 表单字段是否必填
     */
    String PROPERTY_USERTASK_FORM_FORMFIELD_REQUIRED = "required";
    /**
     * 表单字段值配置
     */
    String PROPERTY_USERTASK_FORM_FIELDVALUES = "fieldValues";
    /**
     * 表单字段值配置
     */
    String PROPERTY_USERTASK_FORM_FIELDVALUED = "value";

    //-------------------------------自定义多实例审批任务配置-------------------------------
    /**
     * 审批类型，串行or并行
     */
    String PROPERTY_MULTI_USERTASK_APPROVETYPE = "userTaskApproveType";

    //-------------------------------自定义多实例审批任务配置-------------------------------

    //-------------------------------自定义多人审批任务配置-------------------------------


    //-------------------------------自定义多人审批任务配置-------------------------------

    //-------------------------------自定义循环审批任务配置-------------------------------

    //-------------------------------自定义循环审批任务配置-------------------------------

    //-------------------------------自定义固定人审批任务配置-------------------------------

    //-------------------------------自定义固定人审批任务配置-------------------------------

    //-------------------------------自定义判断审批任务配置-------------------------------

    //-------------------------------自定义判断审批任务配置-------------------------------

    //-------------------------------自定义候选审批任务配置-------------------------------

    //-------------------------------自定义候选审批任务配置-------------------------------

    //-------------------------------自定义会签审批任务配置-------------------------------

    //-------------------------------自定义会签审批任务配置-------------------------------

    String PROPERTY_SERVICETASK_CLASS = "servicetaskclass";
    String PROPERTY_SERVICETASK_EXPRESSION = "servicetaskexpression";
    String PROPERTY_SERVICETASK_DELEGATE_EXPRESSION = "servicetaskdelegateexpression";
    String PROPERTY_SERVICETASK_RESULT_VARIABLE = "servicetaskresultvariable";
    String PROPERTY_SERVICETASK_FIELDS = "servicetaskfields";
    String PROPERTY_SERVICETASK_FIELD_NAME = "name";
    String PROPERTY_SERVICETASK_FIELD_STRING_VALUE = "stringValue";
    String PROPERTY_SERVICETASK_FIELD_STRING = "string";
    String PROPERTY_SERVICETASK_FIELD_EXPRESSION = "expression";

    String PROPERTY_FORM_PROPERTIES = "formproperties";
    String PROPERTY_FORM_ID = "id";
    String PROPERTY_FORM_NAME = "name";
    String PROPERTY_FORM_TYPE = "type";
    String PROPERTY_FORM_EXPRESSION = "expression";
    String PROPERTY_FORM_VARIABLE = "variable";
    String PROPERTY_FORM_DATE_PATTERN = "datePattern";
    String PROPERTY_FORM_REQUIRED = "required";
    String PROPERTY_FORM_READABLE = "readable";
    String PROPERTY_FORM_WRITABLE = "writable";
    String PROPERTY_FORM_ENUM_VALUES = "enumValues";
    String PROPERTY_FORM_ENUM_VALUES_NAME = "name";
    String PROPERTY_FORM_ENUM_VALUES_ID = "id";

    String PROPERTY_DATA_PROPERTIES = "dataproperties";
    String PROPERTY_DATA_ID = "dataproperty_id";
    String PROPERTY_DATA_NAME = "dataproperty_name";
    String PROPERTY_DATA_TYPE = "dataproperty_type";
    String PROPERTY_DATA_VALUE = "dataproperty_value";

    String PROPERTY_SCRIPT_FORMAT = "scriptformat";
    String PROPERTY_SCRIPT_TEXT = "scripttext";

    String PROPERTY_RULETASK_CLASS = "ruletask_class";
    String PROPERTY_RULETASK_VARIABLES_INPUT = "ruletask_variables_input";
    String PROPERTY_RULETASK_RESULT = "ruletask_result";
    String PROPERTY_RULETASK_RULES = "ruletask_rules";
    String PROPERTY_RULETASK_EXCLUDE = "ruletask_exclude";

    String PROPERTY_MAILTASK_TO = "mailtaskto";
    String PROPERTY_MAILTASK_FROM = "mailtaskfrom";
    String PROPERTY_MAILTASK_SUBJECT = "mailtasksubject";
    String PROPERTY_MAILTASK_CC = "mailtaskcc";
    String PROPERTY_MAILTASK_BCC = "mailtaskbcc";
    String PROPERTY_MAILTASK_TEXT = "mailtasktext";
    String PROPERTY_MAILTASK_HTML = "mailtaskhtml";
    String PROPERTY_MAILTASK_CHARSET = "mailtaskcharset";

    String PROPERTY_CALLACTIVITY_CALLEDELEMENT = "callactivitycalledelement";
    String PROPERTY_CALLACTIVITY_IN = "callactivityinparameters";
    String PROPERTY_CALLACTIVITY_OUT = "callactivityoutparameters";
    String PROPERTY_IOPARAMETER_SOURCE = "source";
    String PROPERTY_IOPARAMETER_SOURCE_EXPRESSION = "sourceExpression";
    String PROPERTY_IOPARAMETER_TARGET = "target";

    String PROPERTY_CAMELTASK_CAMELCONTEXT = "cameltaskcamelcontext";

    String PROPERTY_MULETASK_ENDPOINT_URL = "muletaskendpointurl";
    String PROPERTY_MULETASK_LANGUAGE = "muletasklanguage";
    String PROPERTY_MULETASK_PAYLOAD_EXPRESSION = "muletaskpayloadexpression";
    String PROPERTY_MULETASK_RESULT_VARIABLE = "muletaskresultvariable";

    String PROPERTY_SEQUENCEFLOW_DEFAULT = "defaultflow";
    String PROPERTY_SEQUENCEFLOW_CONDITION = "conditionsequenceflow";
    String PROPERTY_SEQUENCEFLOW_ORDER = "sequencefloworder";
    String PROPERTY_FORM_REFERENCE = "formreference";

    String PROPERTY_MESSAGE_DEFINITIONS = "messagedefinitions";
    String PROPERTY_MESSAGE_DEFINITION_ID = "id";
    String PROPERTY_MESSAGE_DEFINITION_NAME = "name";
    String PROPERTY_MESSAGE_DEFINITION_ITEM_REF = "message_item_ref";

    String PROPERTY_SIGNAL_DEFINITIONS = "signaldefinitions";
    String PROPERTY_SIGNAL_DEFINITION_ID = "id";
    String PROPERTY_SIGNAL_DEFINITION_NAME = "name";
    String PROPERTY_SIGNAL_DEFINITION_SCOPE = "scope";

    String PROPERTY_TERMINATE_ALL = "terminateall";
    String PROPERTY_TERMINATE_MULTI_INSTANCE = "terminateMultiInstance";

    String PROPERTY_DECISIONTABLE_REFERENCE = "decisiontaskdecisiontablereference";
    String PROPERTY_DECISIONTABLE_REFERENCE_ID = "decisiontablereferenceid";
    String PROPERTY_DECISIONTABLE_REFERENCE_NAME = "decisiontablereferencename";
    String PROPERTY_DECISIONTABLE_REFERENCE_KEY = "decisionTableReferenceKey";

    /**
     * 绑定表单名称
     */
    String PROPERTY_TASK_PROPERTIES_FORMSCHEMENAME = "formSchemeName";
    /**
     * 绑定表单id
     */
    String PROPERTY_TASK_PROPERTIES_FORMSCHEMEID = "formSchemeId";
    /**
     * 列表同步
     */
    String PROPERTY_TASK_PROPERTIES_LISTSYNCHRONIZATION = "listSynchronization";
    /**
     * 取回
     */
    String PROPERTY_TASK_PROPERTIES_RETRIEVE = "retrieve";
    /**
     * 催办
     */
    String PROPERTY_TASK_PROPERTIES_URGE = "urge";
    /**
     * 作废
     */
    String PROPERTY_TASK_PROPERTIES_INVALID = "invalid";
    /**
     * 委托
     */
    String PROPERTY_TASK_PROPERTIES_DELEGATE = "delegate";
    /**
     * 转办
     */
    String PROPERTY_TASK_PROPERTIES_TRANSFER = "transfer";
    /**
     * 表单可编辑
     */
    String PROPERTY_TASK_PROPERTIES_FORMEDITABLE = "formEditable";
    /**
     * 消息不提醒
     */
    String PROPERTY_TASK_PROPERTIES_REMIND = "remind";
    /**
     * 不简易审批
     */
    String PROPERTY_TASK_PROPERTIES_SIMPLEAPPROVAL = "closeSimpleApproval";
    /**
     * 自动全选
     */
    String PROPERTY_TASK_PROPERTIES_SELECTALL = "selectAll";
    /**
     * 异步树
     */
    String PROPERTY_TASK_PROPERTIES_ASYNTREE = "asynTree";
    /**
     * 可跳跃
     */
    String PROPERTY_TASK_PROPERTIES_JUMP = "isJump";
    /**
     * 是否启用密级
     */
    String SECURITY_ENABLE = "securityEnable";
    /**
     * 密级等级
     */
    String SECURITY_CODE = "securityCode";
    /**
     * 是否加签
     */
    String COUNTERSIGN = "countersign";
    /**
     * 是否回签
     */
    String SIGN_BACK = "signBack";
    /**
     * 是否合并处理
     */
    String MERGEAPPLICATION = "mergeApplication";
    /**
     * 是否强制回签
     */
    String FORCED_SIGNATURE_BACK = "forcedSignatureBack";
    /**
     * 是否启用暂存
     */
    String STAGING = "staging";
    /**
     * 运行时调整
     */
    String BASIC_RUNTIME_TUNING = "basicRuntimeTuning";
    /**
     * 是否是加签节点
     */
    String TASK_IS_CS = "isCs";
    /**
     * 是否是加签节点
     */
    String TASK_CREATE_CS_USERID = "createCsUserId";
    /**
     * 启用逻辑
     */
    String PROPERTY_TASK_PROPERTIES_LOGICALJUDGMENT = "logicalJudgment";
    /**
     * 自动判空
     */
    String PROPERTY_TASK_PROPERTIES_JUDGE_EMPTY = "judgeEmpty";
    /**
     * 提交分解
     */
    String PROPERTY_TASK_COMMIT_BREAKDOWN_CONFIG = "commitBreakdown";
    /**
     * 按钮名称
     */
    String PROPERTY_TASK_COMMIT_BREAKDOWN_BUTTON_NAME = "buttonName";
    /**
     * 节点id
     */
    String PROPERTY_TASK_COMMIT_BREAKDOWN_NODE_ID = "nodeId";
    /**
     * 节点name
     */
    String PROPERTY_TASK_COMMIT_BREAKDOWN_NODE_NAME = "nodeName";

    /**
     * 预警与延期
     */
    String PROPERTY_TASK_EARLYWARNINGANDPOSTPONEMENT_CONFIG = "earlyWarningAndPostponement";
    /**
     * 处理时限时长
     */
    String PROCESSING_TIME_LIMIT_DURATION = "processingTimeLimitDuration";
    /**
     * 处理时限单位名称
     */
    String PROCESSING_TIME_LIMIT_UNITNAME = "processingTimeLimitUnitName";
    /**
     * 处理时限单位code
     */
    String PROCESSING_TIME_LIMIT_UNITCODE = "processingTimeLimitUnitCode";
    /**
     * 预警时限时长
     */
    String WARNING_TIME_LIMIT_DURATION = "warningTimeLimitDuration";
    /**
     * 预警时限单位名称
     */
    String WARNING_TIME_LIMIT_UNITNAME = "warningTimeLimitUnitName";
    /**
     * 预警时限单位code
     */
    String WARNING_TIME_LIMIT_UNITCODE = "warningTimeLimitUnitCode";
    /**
     * 提醒频率时长
     */
    String REMINDER_FREQUENCY_DURATION = "reminderFrequencyDuration";
    /**
     * 提醒频率单位名称
     */
    String REMINDER_FREQUENCY_UNITNAME = "reminderFrequencyUnitName";
    /**
     * 提醒频率单位code
     */
    String REMINDER_FREQUENCY_UNITCODE = "reminderFrequencyUnitCode";

    //-------------------加签--------------------------
    /**
     * 加签配置
     */
    String PROPERTY_TASK_ADDSIGNATURE_CONFIG = "addSignature";
    /**
     * 无限加签
     */
    String PROPERTY_TASK_ADDSIGNATURE_UNLIMITED = "unlimited";
    /**
     * 不可回签
     */
    String PROPERTY_TASK_ADDSIGNATURE_NOT_COUNTERSIGNED = "notCountersigned";
    /**
     * 强制回签
     */
    String PROPERTY_TASK_ADDSIGNATURE_MANDATORY_COUNTERSIGNATURE = "mandatoryCountersignature";

    //----------------------审批公告-----------------------------
    /**
     * 审批公告
     */
    String PROPERTY_TASK_APPROVALNOTICE_CONFIG = "approvalNotice";
    /**
     * 流程发起人
     */
    String PROPERTY_TASK_APPROVALNOTICE_PROCESSINITIATOR = "processInitiator";
    /**
     * 已审批人员
     */
    String PROPERTY_TASK_APPROVALNOTICE_THISNODEAPPROVED = "thisNodeApproved";
    /**
     * 本节点已审批直属领导
     */
    String PROPERTY_TASK_APPROVALNOTICE_THISNODEAPPROVALDIRECTLYUNDERLEADER = "thisNodeApprovalDirectlyUnderLeader";
    /**
     * 本节点已审批部门领导
     */
    String PROPERTY_TASK_APPROVALNOTICE_THISNODEAPPROVALDEPTLEADER = "thisNodeApprovalDeptLeader";

    /**
     * 顺序流样式
     */
    String SEQUENCE_FLOW_STYLE_CONFIG = "styleConfig";
    /**
     * 虚线
     */
    String SEQUENCE_FLOW_STYLE_DOTTEDLINE = "dottedLine";
    /**
     * 颜色
     */
    String SEQUENCE_FLOW_STYLE_COLOR = "color";
    /**
     * 字体大小
     */
    String SEQUENCE_FLOW_STYLE_FONTSIZE = "fontSize";
    /**
     * 字体样式
     */
    String SEQUENCE_FLOW_STYLE_FONTSTYLE = "fontStyle";
    /**
     * 字体颜色
     */
    String SEQUENCE_FLOW_STYLE_FONTCOLOR = "fontColor";


}
