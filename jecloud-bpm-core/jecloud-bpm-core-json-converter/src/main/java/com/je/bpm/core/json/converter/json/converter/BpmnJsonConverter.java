/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.json.converter.json.converter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Strings;
import com.je.bpm.core.json.converter.constants.EditorJsonConstants;
import com.je.bpm.core.json.converter.constants.StencilConstants;
import com.je.bpm.core.json.converter.json.converter.util.CollectionUtils;
import com.je.bpm.core.json.converter.json.converter.util.JsonConverterUtil;
import com.je.bpm.core.json.converter.model.ModelInfo;
import com.je.bpm.core.model.*;
import com.je.bpm.core.model.artifact.Artifact;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.ProcessButton;
import com.je.bpm.core.model.event.BoundaryEvent;
import com.je.bpm.core.model.event.Event;
import com.je.bpm.core.model.gateway.Gateway;
import com.je.bpm.core.model.lane.Lane;
import com.je.bpm.core.model.lane.Pool;
import com.je.bpm.core.model.message.Message;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.process.SubProcess;
import com.je.bpm.core.model.signal.Signal;
import math.geom2d.Point2D;
import math.geom2d.conic.Circle2D;
import math.geom2d.curve.AbstractContinuousCurve2D;
import math.geom2d.line.Line2D;
import math.geom2d.polygon.Polyline2D;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class BpmnJsonConverter implements EditorJsonConstants, StencilConstants, ActivityProcessor {

    protected static final Logger LOGGER = LoggerFactory.getLogger(BpmnJsonConverter.class);

    protected ObjectMapper objectMapper = new ObjectMapper();

    protected static Map<Class<? extends BaseElement>, Class<? extends BaseBpmnJsonConverter>> convertersToJsonMap = new HashMap();
    protected static Map<String, Class<? extends BaseBpmnJsonConverter>> convertersToBpmnMap = new HashMap<String, Class<? extends BaseBpmnJsonConverter>>();

    public final static String MODELER_NAMESPACE = "http://activiti.com/modeler";
    protected final static DateFormat defaultFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    protected final static DateFormat entFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    static {
        // start and end events
        StartEventJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        EndEventJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);

        // connectors
        SequenceFlowJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        MessageFlowJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        AssociationJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);

        // task types
        BusinessRuleTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        MailTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        ManualTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        ReceiveTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        ScriptTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        ServiceTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        UserTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);

        //自定义节点
        KaiteUserTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        KaiteLoopUserTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        KaiteCounterSignUserTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        KaiteFixedUserTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        KaiteDecideUserTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        KaiteCandidateUserTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        KaiteMultiUserTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        KaiteRandomUserTaskJsonConverter.fillTypes(convertersToBpmnMap,convertersToJsonMap);

        CallActivityJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        CamelTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        MuleTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        SendTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        DecisionTaskJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);

        // gateways
        ExclusiveGatewayJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        InclusiveGatewayJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        ParallelGatewayJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        EventGatewayJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);

        // scope constructs
        SubProcessJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        EventSubProcessJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);

        // catch events
        CatchEventJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);

        // throw events
        ThrowEventJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);

        // boundary events
        BoundaryEventJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);

        // artifacts
        TextAnnotationJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
        DataStoreJsonConverter.fillTypes(convertersToBpmnMap, convertersToJsonMap);
    }

    private static final List<String> DI_CIRCLES = new ArrayList();
    private static final List<String> DI_RECTANGLES = new ArrayList();
    private static final List<String> DI_GATEWAY = new ArrayList();

    static {
        DI_CIRCLES.add(STENCIL_EVENT_START_ERROR);
        DI_CIRCLES.add(STENCIL_EVENT_START_MESSAGE);
        DI_CIRCLES.add(STENCIL_EVENT_START_NONE);
        DI_CIRCLES.add(STENCIL_EVENT_START_KAITE_NONE);
        DI_CIRCLES.add(STENCIL_EVENT_START_TIMER);
        DI_CIRCLES.add(STENCIL_EVENT_START_SIGNAL);

        DI_CIRCLES.add(STENCIL_EVENT_BOUNDARY_ERROR);
        DI_CIRCLES.add(STENCIL_EVENT_BOUNDARY_SIGNAL);
        DI_CIRCLES.add(STENCIL_EVENT_BOUNDARY_TIMER);
        DI_CIRCLES.add(STENCIL_EVENT_BOUNDARY_MESSAGE);
        DI_CIRCLES.add(STENCIL_EVENT_BOUNDARY_CANCEL);
        DI_CIRCLES.add(STENCIL_EVENT_BOUNDARY_COMPENSATION);

        DI_CIRCLES.add(STENCIL_EVENT_CATCH_MESSAGE);
        DI_CIRCLES.add(STENCIL_EVENT_CATCH_SIGNAL);
        DI_CIRCLES.add(STENCIL_EVENT_CATCH_TIMER);

        DI_CIRCLES.add(STENCIL_EVENT_THROW_NONE);
        DI_CIRCLES.add(STENCIL_EVENT_THROW_SIGNAL);

        DI_CIRCLES.add(STENCIL_EVENT_END_NONE);
        DI_CIRCLES.add(STENCIL_EVENT_END_ERROR);
        DI_CIRCLES.add(STENCIL_EVENT_END_CANCEL);
        DI_CIRCLES.add(STENCIL_EVENT_END_TERMINATE);

        DI_RECTANGLES.add(STENCIL_CALL_ACTIVITY);
        DI_RECTANGLES.add(STENCIL_SUB_PROCESS);
        DI_RECTANGLES.add(STENCIL_EVENT_SUB_PROCESS);
        DI_RECTANGLES.add(STENCIL_TASK_BUSINESS_RULE);
        DI_RECTANGLES.add(STENCIL_TASK_MAIL);
        DI_RECTANGLES.add(STENCIL_TASK_MANUAL);
        DI_RECTANGLES.add(STENCIL_TASK_RECEIVE);
        DI_RECTANGLES.add(STENCIL_TASK_SCRIPT);
        DI_RECTANGLES.add(STENCIL_TASK_SEND);
        DI_RECTANGLES.add(STENCIL_TASK_SERVICE);
        DI_RECTANGLES.add(STENCIL_TASK_USER);

        //-----------------------------自定义任务节点-----------------------------
        DI_RECTANGLES.add(STENCIL_KAITE_USER_TASK);
        DI_RECTANGLES.add(STENCIL_KAITE_MULTI_USER_TASK);
        DI_RECTANGLES.add(STENCIL_KAITE_CANDIDATE_USER_TASK);
        DI_RECTANGLES.add(STENCIL_KAITE_COUNTERSIGN_USER_TASK);
        DI_RECTANGLES.add(STENCIL_KAITE_DECIDE_USER_TASK);
        DI_RECTANGLES.add(STENCIL_KAITE_FIXED_USER_TASK);
        DI_RECTANGLES.add(STENCIL_KAITE_LOOP_USER_TASK);
        DI_RECTANGLES.add(STENCIL_KAITE_RANDOM_USER_TASK);

        DI_RECTANGLES.add(STENCIL_TASK_CAMEL);
        DI_RECTANGLES.add(STENCIL_TASK_MULE);
        DI_RECTANGLES.add(STENCIL_TASK_DECISION);
        DI_RECTANGLES.add(STENCIL_TEXT_ANNOTATION);

        DI_GATEWAY.add(STENCIL_GATEWAY_EVENT);
        DI_GATEWAY.add(STENCIL_GATEWAY_EXCLUSIVE);
        DI_GATEWAY.add(STENCIL_GATEWAY_INCLUSIVE);
        DI_GATEWAY.add(STENCIL_GATEWAY_PARALLEL);
    }

    public ObjectNode convertToJson(BpmnModel model) {
        return convertToJson(model, null, null);
    }

    public ObjectNode convertToJson(BpmnModel model, Map<String, ModelInfo> formKeyMap, Map<String, ModelInfo> decisionTableKeyMap) {
        ObjectNode modelNode = objectMapper.createObjectNode();

        ObjectNode stencilNode = objectMapper.createObjectNode();
        stencilNode.put("id", "JEBPM");
        stencilNode.put("url", "http://jepaas.com");
        modelNode.set("jebpm", stencilNode);

        double maxX = 0.0;
        double maxY = 0.0;
        for (GraphicInfo flowInfo : model.getLocationMap().values()) {
            if ((flowInfo.getX() + flowInfo.getWidth()) > maxX) {
                maxX = flowInfo.getX() + flowInfo.getWidth();
            }
            if ((flowInfo.getY() + flowInfo.getHeight()) > maxY) {
                maxY = flowInfo.getY() + flowInfo.getHeight();
            }
        }
        maxX += 50;
        maxY += 50;

        if (maxX < 1485) {
            maxX = 1485;
        }

        if (maxY < 700) {
            maxY = 700;
        }

        //写入画布边界信息，左上，右下
        modelNode.set("bounds", BpmnJsonConverterUtil.createBoundsNode(maxX, maxY, 0, 0));
        modelNode.put("resourceId", "canvas");

        Process mainProcess;
        if (model.getPools().size() > 0) {
            mainProcess = model.getProcess(model.getPools().get(0).getId());
        } else {
            mainProcess = model.getMainProcess();
        }

        //写入流程基本信息
        ObjectNode propertiesNode = objectMapper.createObjectNode();
        if (StringUtils.isNotEmpty(mainProcess.getId())) {
            propertiesNode.put(PROPERTY_PROCESS_ID, mainProcess.getId());
        }
        if (StringUtils.isNotEmpty(mainProcess.getName())) {
            propertiesNode.put(PROPERTY_NAME, mainProcess.getName());
        }

        //写入流程按钮配置
        List<ProcessButton> buttons = model.getMainProcess().getButtons().getButtons();
        //写入流程按钮
        BpmnJsonConverterUtil.convertProcessButtonsToJson(buttons, propertiesNode);
        //写入流程基础配置
        BpmnJsonConverterUtil.convertProcessBasicConfigToJson(model.getMainProcess().getProcessConfig(), propertiesNode);
        //写入流程拓展配置
        BpmnJsonConverterUtil.convertProcessExtendedConfigToJson(model.getMainProcess().getExtendedConfiguration(), propertiesNode);
        //启动设置
        BpmnJsonConverterUtil.convertProcessStartUpConfigToJson(model.getMainProcess().getStartupSettings(), propertiesNode);
        //自定义事件
        BpmnJsonConverterUtil.convertProcessCustomEventListenersConfigToJson(model.getMainProcess().getCustomEventListeners(), propertiesNode);
        //转换消息定义
        BpmnJsonConverterUtil.convertMessagesToJson(model.getMainProcess().getMessageSetting(), propertiesNode);
        //转换执行监听
//        BpmnJsonConverterUtil.convertListenersToJson(mainProcess.getExecutionListeners(), true, propertiesNode);
        //转换事件监听
//        BpmnJsonConverterUtil.convertEventListenersToJson(mainProcess.getEventListeners(), propertiesNode);
        //转换信号定义
//        BpmnJsonConverterUtil.convertSignalDefinitionsToJson(model, propertiesNode);
        //转换数据对象
//        if (CollectionUtils.isNotEmpty(mainProcess.getDataObjects())) {
//            BpmnJsonConverterUtil.convertDataPropertiesToJson(mainProcess.getDataObjects(), propertiesNode);
//        }
        modelNode.set(EDITOR_SHAPE_PROPERTIES, propertiesNode);

        //是否包含包含pool位置信息
        boolean poolHasDI = false;
        if (model.getPools().size() > 0) {
            for (Pool pool : model.getPools()) {
                GraphicInfo graphicInfo = model.getGraphicInfo(pool.getId());
                if (graphicInfo != null) {
                    poolHasDI = true;
                    break;
                }
            }
        }

        ArrayNode shapesArrayNode = objectMapper.createArrayNode();
        if (model.getPools().size() > 0 && poolHasDI) {
            for (Pool pool : model.getPools()) {
                GraphicInfo poolGraphicInfo = model.getGraphicInfo(pool.getId());
                if (poolGraphicInfo == null) {
                    continue;
                }

                //泳
                ObjectNode poolNode = BpmnJsonConverterUtil.createChildShape(pool.getId(),
                        STENCIL_POOL,
                        poolGraphicInfo.getX() + poolGraphicInfo.getWidth(),
                        poolGraphicInfo.getY() + poolGraphicInfo.getHeight(),
                        poolGraphicInfo.getX(),
                        poolGraphicInfo.getY());
                shapesArrayNode.add(poolNode);

                ObjectNode poolPropertiesNode = objectMapper.createObjectNode();
                poolPropertiesNode.put(PROPERTY_OVERRIDE_ID, pool.getId());
                poolPropertiesNode.put(PROPERTY_PROCESS_ID, pool.getProcessRef());
                if (!pool.isExecutable()) {
                    poolPropertiesNode.put(PROPERTY_PROCESS_EXECUTABLE, PROPERTY_VALUE_NO);
                }
                if (StringUtils.isNotEmpty(pool.getName())) {
                    poolPropertiesNode.put(PROPERTY_NAME, pool.getName());
                }
                poolNode.set(EDITOR_SHAPE_PROPERTIES, poolPropertiesNode);

                //道
                ArrayNode laneShapesArrayNode = objectMapper.createArrayNode();
                poolNode.set(EDITOR_CHILD_SHAPES, laneShapesArrayNode);
                ArrayNode outgoingArrayNode = objectMapper.createArrayNode();
                poolNode.set("outgoing", outgoingArrayNode);
                Process process = model.getProcess(pool.getId());
                if (process != null) {
                    Map<String, ArrayNode> laneMap = new HashMap();
                    for (Lane lane : process.getLanes()) {
                        GraphicInfo laneGraphicInfo = model.getGraphicInfo(lane.getId());
                        if (laneGraphicInfo == null) {
                            continue;
                        }
                        ObjectNode laneNode = BpmnJsonConverterUtil.createChildShape(lane.getId(),
                                STENCIL_LANE,
                                laneGraphicInfo.getX() + laneGraphicInfo.getWidth() - poolGraphicInfo.getX(),
                                laneGraphicInfo.getY() + laneGraphicInfo.getHeight() - poolGraphicInfo.getY(),
                                laneGraphicInfo.getX() - poolGraphicInfo.getX(),
                                laneGraphicInfo.getY() - poolGraphicInfo.getY());
                        laneShapesArrayNode.add(laneNode);
                        ObjectNode lanePropertiesNode = objectMapper.createObjectNode();
                        lanePropertiesNode.put(PROPERTY_OVERRIDE_ID, lane.getId());
                        if (StringUtils.isNotEmpty(lane.getName())) {
                            lanePropertiesNode.put(PROPERTY_NAME, lane.getName());
                        }
                        laneNode.set(EDITOR_SHAPE_PROPERTIES, lanePropertiesNode);
                        ArrayNode elementShapesArrayNode = objectMapper.createArrayNode();
                        laneNode.set(EDITOR_CHILD_SHAPES, elementShapesArrayNode);
                        laneNode.set("outgoing", objectMapper.createArrayNode());
                        laneMap.put(lane.getId(), elementShapesArrayNode);
                    }

                    for (FlowElement flowElement : process.getFlowElements()) {
                        Lane laneForElement = null;
                        GraphicInfo laneGraphicInfo = null;
                        FlowElement lookForElement = null;
                        if (flowElement instanceof SequenceFlow) {
                            SequenceFlow sequenceFlow = (SequenceFlow) flowElement;
                            lookForElement = model.getFlowElement(sequenceFlow.getSourceRef());
                        } else {
                            lookForElement = flowElement;
                        }

                        for (Lane lane : process.getLanes()) {
                            if (lane.getFlowReferences().contains(lookForElement.getId())) {
                                laneGraphicInfo = model.getGraphicInfo(lane.getId());
                                if (laneGraphicInfo != null) {
                                    laneForElement = lane;
                                }
                                break;
                            }
                        }

                        if (flowElement instanceof SequenceFlow || laneForElement != null) {
                            processFlowElement(flowElement,
                                    process,
                                    model,
                                    laneMap.get(laneForElement.getId()),
                                    formKeyMap,
                                    decisionTableKeyMap,
                                    laneGraphicInfo.getX(),
                                    laneGraphicInfo.getY());
                        }
                    }
                    processArtifacts(process, model, shapesArrayNode, 0.0, 0.0);
                }

                for (MessageFlow messageFlow : model.getMessageFlows().values()) {
                    if (messageFlow.getSourceRef().equals(pool.getId())) {
                        outgoingArrayNode.add(BpmnJsonConverterUtil.createResourceNode(messageFlow.getId()));
                    }
                }
            }
            processMessageFlows(model, shapesArrayNode);
        } else {
            processFlowElements(model.getMainProcess(), model, shapesArrayNode, formKeyMap, decisionTableKeyMap, 0.0, 0.0);
            processMessageFlows(model, shapesArrayNode);
        }
        modelNode.set(EDITOR_CHILD_SHAPES, shapesArrayNode);
        return modelNode;
    }

    @Override
    public void processFlowElements(FlowElementsContainer container, BpmnModel model, ArrayNode shapesArrayNode, Map<String, ModelInfo> formKeyMap,
                                    Map<String, ModelInfo> decisionTableKeyMap, double subProcessX, double subProcessY) {
        for (FlowElement flowElement : container.getFlowElements()) {
            processFlowElement(flowElement, container, model, shapesArrayNode, formKeyMap, decisionTableKeyMap, subProcessX, subProcessY);
        }
        processArtifacts(container, model, shapesArrayNode, subProcessX, subProcessY);
    }

    protected void processFlowElement(FlowElement flowElement, FlowElementsContainer container, BpmnModel model, ArrayNode shapesArrayNode,
                                      Map<String, ModelInfo> formKeyMap, Map<String, ModelInfo> decisionTableKeyMap,
                                      double containerX, double containerY) {
        Class<? extends BaseBpmnJsonConverter> converter = convertersToJsonMap.get(flowElement.getClass());
        if (converter != null) {
            try {
                BaseBpmnJsonConverter converterInstance = converter.getDeclaredConstructor().newInstance();
                if (converterInstance instanceof FormKeyAwareConverter) {
                    ((FormKeyAwareConverter) converterInstance).setFormKeyMap(formKeyMap);
                }
                if (converterInstance instanceof DecisionTableKeyAwareConverter) {
                    ((DecisionTableKeyAwareConverter) converterInstance).setDecisionTableKeyMap(decisionTableKeyMap);
                }

                converterInstance.convertToJson(flowElement,
                        this,
                        model,
                        container,
                        shapesArrayNode,
                        containerX,
                        containerY);
            } catch (Exception e) {
                LOGGER.error("Error converting {}", flowElement, e);
            }
        }
    }

    protected void processArtifacts(FlowElementsContainer container, BpmnModel model, ArrayNode shapesArrayNode, double containerX, double containerY) {
        for (Artifact artifact : container.getArtifacts()) {
            Class<? extends BaseBpmnJsonConverter> converter = convertersToJsonMap.get(artifact.getClass());
            if (converter != null) {
                try {
                    converter.getDeclaredConstructor().newInstance().convertToJson(artifact,
                            this,
                            model,
                            container,
                            shapesArrayNode,
                            containerX,
                            containerY);
                } catch (Exception e) {
                    LOGGER.error("Error converting {}", artifact, e);
                }
            }
        }
    }

    protected void processMessageFlows(BpmnModel model, ArrayNode shapesArrayNode) {
        for (MessageFlow messageFlow : model.getMessageFlows().values()) {
            MessageFlowJsonConverter jsonConverter = new MessageFlowJsonConverter();
            jsonConverter.convertToJson(messageFlow,
                    this,
                    model,
                    null,
                    shapesArrayNode,
                    0.0,
                    0.0);
        }
    }

    public BpmnModel convertToBpmnModel(JsonNode modelNode) {
        return convertToBpmnModel(modelNode, null, null);
    }

    public BpmnModel convertToBpmnModel(JsonNode modelNode, Map<String, String> formKeyMap, Map<String, String> decisionTableKeyMap) {
        BpmnModel bpmnModel = new BpmnModel();
        bpmnModel.setTargetNamespace("http://activiti.org/test");
        //图形元素，用于存储childShape的
        Map<String, JsonNode> shapeMap = new HashMap();
        Map<String, JsonNode> sourceRefMap = new HashMap();
        Map<String, JsonNode> edgeMap = new HashMap();
        Map<String, List<JsonNode>> sourceAndTargetMap = new HashMap();
        //读取flow元素graph信息
        readShapeDI(modelNode, 0, 0, shapeMap, sourceRefMap, bpmnModel);
        //读取sequenceflow和association信息，并封装source和target
        filterAllEdges(modelNode, edgeMap, sourceAndTargetMap, shapeMap, sourceRefMap);
        //读取sequenceflow和association的graph信息
        readEdgeDI(edgeMap, sourceAndTargetMap, bpmnModel);

        ArrayNode shapesArrayNode = (ArrayNode) modelNode.get(EDITOR_CHILD_SHAPES);
        if (shapesArrayNode == null || shapesArrayNode.size() == 0) {
            return bpmnModel;
        }
        boolean nonEmptyPoolFound = false;
        Map<String, Lane> elementInLaneMap = new HashMap();
        // first create the pool structure
        for (JsonNode shapeNode : shapesArrayNode) {
            String stencilId = BpmnJsonConverterUtil.getStencilId(shapeNode);
            if (STENCIL_POOL.equals(stencilId)) {
                Pool pool = new Pool();
                pool.setId(BpmnJsonConverterUtil.getElementId(shapeNode));
                pool.setName(JsonConverterUtil.getPropertyValueAsString(PROPERTY_NAME, shapeNode));
                pool.setProcessRef(JsonConverterUtil.getPropertyValueAsString(PROPERTY_PROCESS_ID, shapeNode));
                pool.setExecutable(JsonConverterUtil.getPropertyValueAsBoolean(PROPERTY_PROCESS_EXECUTABLE, shapeNode, true));
                bpmnModel.getPools().add(pool);

                Process process = new Process();
                process.setId(pool.getProcessRef());
                process.setName(pool.getName());
                process.setExecutable(pool.isExecutable());
                bpmnModel.addProcess(process);

                ArrayNode laneArrayNode = (ArrayNode) shapeNode.get(EDITOR_CHILD_SHAPES);
                for (JsonNode laneNode : laneArrayNode) {
                    // should be a lane, but just check to be certain
                    String laneStencilId = BpmnJsonConverterUtil.getStencilId(laneNode);
                    if (STENCIL_LANE.equals(laneStencilId)) {
                        nonEmptyPoolFound = true;
                        Lane lane = new Lane();
                        lane.setId(BpmnJsonConverterUtil.getElementId(laneNode));
                        lane.setName(JsonConverterUtil.getPropertyValueAsString(PROPERTY_NAME, laneNode));
                        lane.setParentProcess(process);
                        process.getLanes().add(lane);
                        processJsonElements(laneNode.get(EDITOR_CHILD_SHAPES), modelNode, lane, shapeMap, formKeyMap, decisionTableKeyMap, bpmnModel);
                        if (CollectionUtils.isNotEmpty(lane.getFlowReferences())) {
                            for (String elementRef : lane.getFlowReferences()) {
                                elementInLaneMap.put(elementRef, lane);
                            }
                        }
                    }
                }
            }
        }

        // Signal Definitions exist on the root level，在根节点上的信号定义
        JsonNode signalDefinitionNode = BpmnJsonConverterUtil.getProperty(PROPERTY_SIGNAL_DEFINITIONS, modelNode);
        signalDefinitionNode = BpmnJsonConverterUtil.validateIfNodeIsTextual(signalDefinitionNode);
        signalDefinitionNode = BpmnJsonConverterUtil.validateIfNodeIsTextual(signalDefinitionNode); // no idea why this needs to be done twice ..
        if (signalDefinitionNode != null) {
            if (signalDefinitionNode instanceof ArrayNode) {
                ArrayNode signalDefinitionArrayNode = (ArrayNode) signalDefinitionNode;
                Iterator<JsonNode> signalDefinitionIterator = signalDefinitionArrayNode.iterator();
                while (signalDefinitionIterator.hasNext()) {
                    JsonNode signalDefinitionJsonNode = signalDefinitionIterator.next();
                    String signalId = signalDefinitionJsonNode.get(PROPERTY_SIGNAL_DEFINITION_ID).asText();
                    String signalName = signalDefinitionJsonNode.get(PROPERTY_SIGNAL_DEFINITION_NAME).asText();
                    String signalScope = signalDefinitionJsonNode.get(PROPERTY_SIGNAL_DEFINITION_SCOPE).asText();

                    if (StringUtils.isNotEmpty(signalId) && StringUtils.isNotEmpty(signalName)) {
                        Signal signal = new Signal();
                        signal.setId(signalId);
                        signal.setName(signalName);
                        signal.setScope((signalScope.toLowerCase().equals("processinstance")) ? Signal.SCOPE_PROCESS_INSTANCE : Signal.SCOPE_GLOBAL);
                        bpmnModel.addSignal(signal);
                    }
                }
            }
        }

        if (!nonEmptyPoolFound) {
            Process process = new Process();
            bpmnModel.getProcesses().add(process);
            if (Strings.isNullOrEmpty(BpmnJsonConverterUtil.getPropertyValueAsString(PROPERTY_PROCESS_ID, modelNode))) {
                process.setId(UUID.randomUUID().toString());
            } else {
                process.setId(BpmnJsonConverterUtil.getPropertyValueAsString(PROPERTY_PROCESS_ID, modelNode));
            }
            process.setName(BpmnJsonConverterUtil.getPropertyValueAsString(PROPERTY_NAME, modelNode));
            String namespace = BpmnJsonConverterUtil.getPropertyValueAsString(PROPERTY_PROCESS_NAMESPACE, modelNode);
            if (StringUtils.isNotEmpty(namespace)) {
                bpmnModel.setTargetNamespace(namespace);
            }
            process.setDocumentation(BpmnJsonConverterUtil.getPropertyValueAsString(PROPERTY_DOCUMENTATION, modelNode));
            JsonNode processExecutableNode = JsonConverterUtil.getProperty(PROPERTY_PROCESS_EXECUTABLE, modelNode);
            if (processExecutableNode != null && StringUtils.isNotEmpty(processExecutableNode.asText())) {
                process.setExecutable(JsonConverterUtil.getPropertyValueAsBoolean(PROPERTY_PROCESS_EXECUTABLE, modelNode));
            }

            //解析消息
            BpmnJsonConverterUtil.convertJsonToMessages(modelNode, bpmnModel);
            //解析事件
            BpmnJsonConverterUtil.convertJsonToListeners(modelNode, process);

            //解析事件监听器
            JsonNode eventListenersNode = BpmnJsonConverterUtil.getProperty(PROPERTY_EVENT_LISTENERS, modelNode);
            if (eventListenersNode != null) {
                eventListenersNode = BpmnJsonConverterUtil.validateIfNodeIsTextual(eventListenersNode);
                BpmnJsonConverterUtil.parseEventListeners(eventListenersNode.get(PROPERTY_EVENTLISTENER_VALUE), process);
            }

            //解析data属性
            JsonNode processDataPropertiesNode = modelNode.get(EDITOR_SHAPE_PROPERTIES).get(PROPERTY_DATA_PROPERTIES);
            if (processDataPropertiesNode != null) {
                List<ValuedDataObject> dataObjects = BpmnJsonConverterUtil.convertJsonToDataProperties(processDataPropertiesNode, process);
                process.setDataObjects(dataObjects);
                process.getFlowElements().addAll(dataObjects);
            }
            //添加按钮配置，主页设置了节点的按钮，需要赋值
            shapeMap.put(STENCIL_SUB_PROCESS, modelNode);
            processJsonElements(shapesArrayNode, modelNode, process, shapeMap, formKeyMap, decisionTableKeyMap, bpmnModel);
        } else {
            // sequence flows are on root level so need additional parsing for pools
            // 序列流在根节点上，所以需要对pool进行额外的解析
            for (JsonNode shapeNode : shapesArrayNode) {
                if (STENCIL_SEQUENCE_FLOW.equalsIgnoreCase(BpmnJsonConverterUtil.getStencilId(shapeNode)) || STENCIL_ASSOCIATION.equalsIgnoreCase(BpmnJsonConverterUtil.getStencilId(shapeNode))) {
                    String sourceRef = BpmnJsonConverterUtil.lookForSourceRef(shapeNode.get(EDITOR_SHAPE_ID).asText(), modelNode.get(EDITOR_CHILD_SHAPES));
                    if (sourceRef != null) {
                        Lane lane = elementInLaneMap.get(sourceRef);
                        SequenceFlowJsonConverter flowConverter = new SequenceFlowJsonConverter();
                        if (lane != null) {
                            flowConverter.convertToBpmnModel(shapeNode, modelNode, this, lane, shapeMap, bpmnModel);
                        } else {
                            flowConverter.convertToBpmnModel(shapeNode, modelNode, this, bpmnModel.getProcesses().get(0), shapeMap, bpmnModel);
                        }
                    }
                }
            }
        }

        // sequence flows are now all on root level
        Map<String, SubProcess> subShapesMap = new HashMap<String, SubProcess>();
        for (Process process : bpmnModel.getProcesses()) {
            for (FlowElement flowElement : process.findFlowElementsOfType(SubProcess.class)) {
                SubProcess subProcess = (SubProcess) flowElement;
                fillSubShapes(subShapesMap, subProcess);
            }

            if (subShapesMap.size() > 0) {
                List<String> removeSubFlowsList = new ArrayList<String>();
                for (FlowElement flowElement : process.findFlowElementsOfType(SequenceFlow.class)) {
                    SequenceFlow sequenceFlow = (SequenceFlow) flowElement;
                    if (subShapesMap.containsKey(sequenceFlow.getSourceRef())) {
                        SubProcess subProcess = subShapesMap.get(sequenceFlow.getSourceRef());
                        if (subProcess.getFlowElement(sequenceFlow.getId()) == null) {
                            subProcess.addFlowElement(sequenceFlow);
                            removeSubFlowsList.add(sequenceFlow.getId());
                        }
                    }
                }
                for (String flowId : removeSubFlowsList) {
                    process.removeFlowElement(flowId);
                }
            }
        }

        Map<String, FlowWithContainer> allFlowMap = new HashMap();
        List<Gateway> gatewayWithOrderList = new ArrayList();
        // post handling of process elements
        for (Process process : bpmnModel.getProcesses()) {
            postProcessElements(process, process.getFlowElements(), edgeMap, bpmnModel, allFlowMap, gatewayWithOrderList);
        }

        // sort the sequence flows
        for (Gateway gateway : gatewayWithOrderList) {
            List<ExtensionElement> orderList = gateway.getExtensionElements().get("EDITOR_FLOW_ORDER");
            if (CollectionUtils.isNotEmpty(orderList)) {
                for (ExtensionElement orderElement : orderList) {
                    String flowValue = orderElement.getElementText();
                    if (StringUtils.isNotEmpty(flowValue)) {
                        if (allFlowMap.containsKey(flowValue)) {
                            FlowWithContainer flowWithContainer = allFlowMap.get(flowValue);
                            flowWithContainer.getFlowContainer().removeFlowElement(flowWithContainer.getSequenceFlow().getId());
                            flowWithContainer.getFlowContainer().addFlowElement(flowWithContainer.getSequenceFlow());
                        }
                    }
                }
            }
            gateway.getExtensionElements().remove("EDITOR_FLOW_ORDER");
        }

        Process mainProcess = bpmnModel.getMainProcess();
        //解析流程按钮
        BpmnJsonConverterUtil.convertJsonToProcessButtons(modelNode, mainProcess);
        //解析流程基础配置
        BpmnJsonConverterUtil.convertJsonToProcessBasic(modelNode, mainProcess);

        return bpmnModel;
    }

    @Override
    public void processJsonElements(JsonNode shapesArrayNode, JsonNode modelNode, BaseElement parentElement, Map<String, JsonNode> shapeMap,
                                    Map<String, String> formMap, Map<String, String> decisionTableMap, BpmnModel bpmnModel) {
        for (JsonNode shapeNode : shapesArrayNode) {
            String stencilId = BpmnJsonConverterUtil.getStencilId(shapeNode);
            stencilId = BpmnJsonConverterUtil.getKaiteStencilId(stencilId);
            Class<? extends BaseBpmnJsonConverter> converter = convertersToBpmnMap.get(stencilId);
            try {
                BaseBpmnJsonConverter converterInstance = converter.getDeclaredConstructor().newInstance();
                if (converterInstance instanceof DecisionTableAwareConverter) {
                    ((DecisionTableAwareConverter) converterInstance).setDecisionTableMap(decisionTableMap);
                }
                if (converterInstance instanceof FormAwareConverter) {
                    ((FormAwareConverter) converterInstance).setFormMap(formMap);
                }
                converterInstance.convertToBpmnModel(shapeNode, modelNode, this, parentElement, shapeMap, bpmnModel);
            } catch (Exception e) {
                LOGGER.error("Error converting {}", BpmnJsonConverterUtil.getStencilId(shapeNode), e);
            }
        }
    }

    private void fillSubShapes(Map<String, SubProcess> subShapesMap, SubProcess subProcess) {
        for (FlowElement flowElement : subProcess.getFlowElements()) {
            if (flowElement instanceof SubProcess) {
                SubProcess childSubProcess = (SubProcess) flowElement;
                subShapesMap.put(childSubProcess.getId(), subProcess);
                fillSubShapes(subShapesMap, childSubProcess);
            } else {
                subShapesMap.put(flowElement.getId(), subProcess);
            }
        }
    }

    private Activity retrieveAttachedRefObject(String attachedToRefId, Collection<FlowElement> flowElementList) {
        Activity activity = null;
        if (StringUtils.isNotEmpty(attachedToRefId)) {
            for (FlowElement flowElement : flowElementList) {
                if (attachedToRefId.equals(flowElement.getId())) {
                    activity = (Activity) flowElement;
                    break;
                } else if (flowElement instanceof SubProcess) {
                    SubProcess subProcess = (SubProcess) flowElement;
                    Activity retrievedActivity = retrieveAttachedRefObject(attachedToRefId, subProcess.getFlowElements());
                    if (retrievedActivity != null) {
                        activity = retrievedActivity;
                        break;
                    }
                }
            }
        }
        return activity;
    }

    private void postProcessElements(FlowElementsContainer parentContainer, Collection<FlowElement> flowElementList, Map<String, JsonNode> edgeMap,
                                     BpmnModel bpmnModel, Map<String, FlowWithContainer> allFlowMap, List<Gateway> gatewayWithOrderList) {
        for (FlowElement flowElement : flowElementList) {
            if (flowElement instanceof Event) {
                Event event = (Event) flowElement;
                if (CollectionUtils.isNotEmpty(event.getEventDefinitions())) {
                    EventDefinition eventDef = event.getEventDefinitions().get(0);
                    if (eventDef instanceof SignalEventDefinition) {
                        SignalEventDefinition signalEventDef = (SignalEventDefinition) eventDef;
                        if (StringUtils.isNotEmpty(signalEventDef.getSignalRef())) {
                            if (bpmnModel.getSignal(signalEventDef.getSignalRef()) == null) {
                                bpmnModel.addSignal(new Signal(signalEventDef.getSignalRef(), signalEventDef.getSignalRef()));
                            }
                        }
                    } else if (eventDef instanceof MessageEventDefinition) {
                        MessageEventDefinition messageEventDef = (MessageEventDefinition) eventDef;
                        if (StringUtils.isNotEmpty(messageEventDef.getMessageRef())) {
                            if (bpmnModel.getMessage(messageEventDef.getMessageRef()) == null) {
                                bpmnModel.addMessage(new Message(messageEventDef.getMessageRef(), messageEventDef.getMessageRef(), null));
                            }
                        }
                    }
                }
            }

            if (flowElement instanceof BoundaryEvent) {
                BoundaryEvent boundaryEvent = (BoundaryEvent) flowElement;
                Activity activity = retrieveAttachedRefObject(boundaryEvent.getAttachedToRefId(), parentContainer.getFlowElements());

                if (activity == null) {
                    LOGGER.warn("Boundary event " + boundaryEvent.getId() + " is not attached to any activity");
                } else {
                    boundaryEvent.setAttachedToRef(activity);
                    activity.getBoundaryEvents().add(boundaryEvent);
                }
            } else if (flowElement instanceof Gateway) {
                if (flowElement.getExtensionElements().containsKey("EDITOR_FLOW_ORDER")) {
                    gatewayWithOrderList.add((Gateway) flowElement);
                }
            } else if (flowElement instanceof SubProcess) {
                SubProcess subProcess = (SubProcess) flowElement;
                postProcessElements(subProcess,
                        subProcess.getFlowElements(),
                        edgeMap,
                        bpmnModel,
                        allFlowMap,
                        gatewayWithOrderList);
            } else if (flowElement instanceof SequenceFlow) {
                SequenceFlow sequenceFlow = (SequenceFlow) flowElement;
                FlowElement sourceFlowElement = parentContainer.getFlowElement(sequenceFlow.getSourceRef());
                if (sourceFlowElement != null && sourceFlowElement instanceof FlowNode) {
                    FlowWithContainer flowWithContainer = new FlowWithContainer(sequenceFlow, parentContainer);
                    if (sequenceFlow.getExtensionElements().get("EDITOR_RESOURCEID") != null && sequenceFlow.getExtensionElements().get("EDITOR_RESOURCEID").size() > 0) {
                        allFlowMap.put(sequenceFlow.getExtensionElements().get("EDITOR_RESOURCEID").get(0).getElementText(), flowWithContainer);
                        sequenceFlow.getExtensionElements().remove("EDITOR_RESOURCEID");
                    }

                    ((FlowNode) sourceFlowElement).getOutgoingFlows().add(sequenceFlow);
                    JsonNode edgeNode = edgeMap.get(sequenceFlow.getId());
                    if (edgeNode != null) {
                        boolean isDefault = JsonConverterUtil.getPropertyValueAsBoolean(PROPERTY_SEQUENCEFLOW_DEFAULT, edgeNode);
                        if (isDefault) {
                            if (sourceFlowElement instanceof Activity) {
                                ((Activity) sourceFlowElement).setDefaultFlow(sequenceFlow.getId());
                            } else if (sourceFlowElement instanceof Gateway) {
                                ((Gateway) sourceFlowElement).setDefaultFlow(sequenceFlow.getId());
                            }
                        }
                    }
                }
                FlowElement targetFlowElement = parentContainer.getFlowElement(sequenceFlow.getTargetRef());
                if (targetFlowElement != null && targetFlowElement instanceof FlowNode) {
                    ((FlowNode) targetFlowElement).getIncomingFlows().add(sequenceFlow);
                }
            }
        }
    }

    private void readShapeDI(JsonNode objectNode, double parentX, double parentY, Map<String, JsonNode> shapeMap,
                             Map<String, JsonNode> sourceRefMap, BpmnModel bpmnModel) {
        if (objectNode.get(EDITOR_CHILD_SHAPES) != null) {
            for (JsonNode jsonChildNode : objectNode.get(EDITOR_CHILD_SHAPES)) {
                String stencilId = BpmnJsonConverterUtil.getStencilId(jsonChildNode);
                if (!STENCIL_SEQUENCE_FLOW.equals(stencilId) && !STENCIL_SEQUENCE_KAITE_FLOW.equals(stencilId)) {
                    GraphicInfo graphicInfo = new GraphicInfo();
                    JsonNode boundsNode = jsonChildNode.get(EDITOR_BOUNDS);
//                    ObjectNode upperLeftNode = (ObjectNode) boundsNode.get(EDITOR_BOUNDS_UPPER_LEFT);
//                    graphicInfo.setX(upperLeftNode.get(EDITOR_BOUNDS_X).asDouble() + parentX);
//                    graphicInfo.setY(upperLeftNode.get(EDITOR_BOUNDS_Y).asDouble() + parentY);
//
//                    ObjectNode lowerRightNode = (ObjectNode) boundsNode.get(EDITOR_BOUNDS_LOWER_RIGHT);
//                    graphicInfo.setWidth(lowerRightNode.get(EDITOR_BOUNDS_X).asDouble() - graphicInfo.getX() + parentX);
//                    graphicInfo.setHeight(lowerRightNode.get(EDITOR_BOUNDS_Y).asDouble() - graphicInfo.getY() + parentY);

                    String childShapeId = jsonChildNode.get(EDITOR_SHAPE_ID).asText();
                    bpmnModel.addGraphicInfo(BpmnJsonConverterUtil.getElementId(jsonChildNode), graphicInfo);
                    shapeMap.put(childShapeId, jsonChildNode);

                    ArrayNode outgoingNode = (ArrayNode) jsonChildNode.get("outgoing");
                    if (outgoingNode != null && outgoingNode.size() > 0) {
                        for (JsonNode outgoingChildNode : outgoingNode) {
                            JsonNode resourceNode = outgoingChildNode.get(EDITOR_SHAPE_ID);
                            if (resourceNode != null) {
                                sourceRefMap.put(resourceNode.asText(), jsonChildNode);
                            }
                        }
                    }
                    readShapeDI(jsonChildNode, graphicInfo.getX(), graphicInfo.getY(), shapeMap, sourceRefMap, bpmnModel);
                }
            }
        }
    }

    private void filterAllEdges(JsonNode objectNode, Map<String, JsonNode> edgeMap, Map<String,
            List<JsonNode>> sourceAndTargetMap, Map<String, JsonNode> shapeMap, Map<String, JsonNode> sourceRefMap) {
        if (objectNode.get(EDITOR_CHILD_SHAPES) != null) {
            for (JsonNode jsonChildNode : objectNode.get(EDITOR_CHILD_SHAPES)) {
                ObjectNode childNode = (ObjectNode) jsonChildNode;
                String stencilId = BpmnJsonConverterUtil.getStencilId(childNode);
                if (STENCIL_SUB_PROCESS.equals(stencilId) || STENCIL_POOL.equals(stencilId) || STENCIL_LANE.equals(stencilId)) {
                    filterAllEdges(childNode, edgeMap, sourceAndTargetMap, shapeMap, sourceRefMap);
                } else if (STENCIL_SEQUENCE_FLOW.equals(stencilId) || STENCIL_ASSOCIATION.equals(stencilId)) {
                    String childEdgeId = BpmnJsonConverterUtil.getElementId(childNode);
                    JsonNode targetNode = childNode.get("target");
                    if (targetNode != null && !targetNode.isNull()) {
                        String targetRefId = targetNode.get(EDITOR_SHAPE_ID).asText();
                        List<JsonNode> sourceAndTargetList = new ArrayList();
                        sourceAndTargetList.add(sourceRefMap.get(childNode.get(EDITOR_SHAPE_ID).asText()));
                        sourceAndTargetList.add(shapeMap.get(targetRefId));
                        sourceAndTargetMap.put(childEdgeId, sourceAndTargetList);
                    }
                    edgeMap.put(childEdgeId, childNode);
                }
            }
        }
    }

    private void readEdgeDI(Map<String, JsonNode> edgeMap, Map<String, List<JsonNode>> sourceAndTargetMap, BpmnModel bpmnModel) {
        for (String edgeId : edgeMap.keySet()) {
            JsonNode edgeNode = edgeMap.get(edgeId);
            List<JsonNode> sourceAndTargetList = sourceAndTargetMap.get(edgeId);

            JsonNode sourceRefNode = null;
            JsonNode targetRefNode = null;

            if (sourceAndTargetList != null && sourceAndTargetList.size() > 1) {
                sourceRefNode = sourceAndTargetList.get(0);
                targetRefNode = sourceAndTargetList.get(1);
            }

            if (sourceRefNode == null) {
                LOGGER.info("Skipping edge {} because source ref is null", edgeId);
                continue;
            }

            if (targetRefNode == null) {
                LOGGER.info("Skipping edge {} because target ref is null", edgeId);
                continue;
            }

            JsonNode dockersNode = edgeNode.get(EDITOR_DOCKERS);
            double sourceDockersX = dockersNode.get(0).get(EDITOR_BOUNDS_X).asDouble();
            double sourceDockersY = dockersNode.get(0).get(EDITOR_BOUNDS_Y).asDouble();

            GraphicInfo sourceInfo = bpmnModel.getGraphicInfo(BpmnJsonConverterUtil.getElementId(sourceRefNode));
            GraphicInfo targetInfo = bpmnModel.getGraphicInfo(BpmnJsonConverterUtil.getElementId(targetRefNode));

            double sourceRefLineX = sourceInfo.getX() + sourceDockersX;
            double sourceRefLineY = sourceInfo.getY() + sourceDockersY;

            double nextPointInLineX;
            double nextPointInLineY;

            nextPointInLineX = dockersNode.get(1).get(EDITOR_BOUNDS_X).asDouble();
            nextPointInLineY = dockersNode.get(1).get(EDITOR_BOUNDS_Y).asDouble();
            if (dockersNode.size() == 2) {
                nextPointInLineX += targetInfo.getX();
                nextPointInLineY += targetInfo.getY();
            }

            Line2D firstLine = new Line2D(sourceRefLineX, sourceRefLineY, nextPointInLineX, nextPointInLineY);
            String sourceRefStencilId = BpmnJsonConverterUtil.getStencilId(sourceRefNode);
            String targetRefStencilId = BpmnJsonConverterUtil.getStencilId(targetRefNode);
            List<GraphicInfo> graphicInfoList = new ArrayList<GraphicInfo>();
            AbstractContinuousCurve2D source2D = null;
            if (DI_CIRCLES.contains(sourceRefStencilId)) {
                source2D = new Circle2D(sourceInfo.getX() + sourceDockersX, sourceInfo.getY() + sourceDockersY, sourceDockersX);
            } else if (DI_RECTANGLES.contains(sourceRefStencilId)) {
                source2D = createRectangle(sourceInfo);
            } else if (DI_GATEWAY.contains(sourceRefStencilId)) {
                source2D = createGateway(sourceInfo);
            }

            if (source2D != null) {
                Collection<Point2D> intersections = source2D.intersections(firstLine);
                if (intersections != null && intersections.size() > 0) {
                    Point2D intersection = intersections.iterator().next();
                    graphicInfoList.add(createGraphicInfo(intersection.x(), intersection.y()));
                } else {
                    graphicInfoList.add(createGraphicInfo(sourceRefLineX, sourceRefLineY));
                }
            }

            Line2D lastLine = null;
            if (dockersNode.size() > 2) {
                for (int i = 1; i < dockersNode.size() - 1; i++) {
                    double x = dockersNode.get(i).get(EDITOR_BOUNDS_X).asDouble();
                    double y = dockersNode.get(i).get(EDITOR_BOUNDS_Y).asDouble();
                    graphicInfoList.add(createGraphicInfo(x, y));
                }

                double startLastLineX = dockersNode.get(dockersNode.size() - 2).get(EDITOR_BOUNDS_X).asDouble();
                double startLastLineY = dockersNode.get(dockersNode.size() - 2).get(EDITOR_BOUNDS_Y).asDouble();

                double endLastLineX = dockersNode.get(dockersNode.size() - 1).get(EDITOR_BOUNDS_X).asDouble();
                double endLastLineY = dockersNode.get(dockersNode.size() - 1).get(EDITOR_BOUNDS_Y).asDouble();

                endLastLineX += targetInfo.getX();
                endLastLineY += targetInfo.getY();

                lastLine = new Line2D(startLastLineX, startLastLineY, endLastLineX, endLastLineY);
            } else {
                lastLine = firstLine;
            }

            AbstractContinuousCurve2D target2D = null;
            if (DI_RECTANGLES.contains(targetRefStencilId)) {
                target2D = createRectangle(targetInfo);
            } else if (DI_CIRCLES.contains(targetRefStencilId)) {

                double targetDockersX = dockersNode.get(dockersNode.size() - 1).get(EDITOR_BOUNDS_X).asDouble();
                double targetDockersY = dockersNode.get(dockersNode.size() - 1).get(EDITOR_BOUNDS_Y).asDouble();

                target2D = new Circle2D(targetInfo.getX() + targetDockersX,
                        targetInfo.getY() + targetDockersY,
                        targetDockersX);
            } else if (DI_GATEWAY.contains(targetRefStencilId)) {
                target2D = createGateway(targetInfo);
            }

            if (target2D != null) {
                Collection<Point2D> intersections = target2D.intersections(lastLine);
                if (intersections != null && intersections.size() > 0) {
                    Point2D intersection = intersections.iterator().next();
                    graphicInfoList.add(createGraphicInfo(intersection.x(), intersection.y()));
                } else {
                    graphicInfoList.add(createGraphicInfo(lastLine.getPoint2().x(), lastLine.getPoint2().y()));
                }
            }

            bpmnModel.addFlowGraphicInfoList(edgeId, graphicInfoList);
        }
    }

    private Polyline2D createRectangle(GraphicInfo graphicInfo) {
        Polyline2D rectangle = new Polyline2D(
                new Point2D(graphicInfo.getX(), graphicInfo.getY()),
                new Point2D(graphicInfo.getX() + graphicInfo.getWidth(), graphicInfo.getY()),
                new Point2D(graphicInfo.getX() + graphicInfo.getWidth(), graphicInfo.getY() + graphicInfo.getHeight()),
                new Point2D(graphicInfo.getX(), graphicInfo.getY() + graphicInfo.getHeight()),
                new Point2D(graphicInfo.getX(), graphicInfo.getY()));
        return rectangle;
    }

    private Polyline2D createGateway(GraphicInfo graphicInfo) {
        double middleX = graphicInfo.getX() + (graphicInfo.getWidth() / 2);
        double middleY = graphicInfo.getY() + (graphicInfo.getHeight() / 2);

        Polyline2D gatewayRectangle = new Polyline2D(
                new Point2D(graphicInfo.getX(), middleY),
                new Point2D(middleX, graphicInfo.getY()),
                new Point2D(graphicInfo.getX() + graphicInfo.getWidth(), middleY),
                new Point2D(middleX, graphicInfo.getY() + graphicInfo.getHeight()),
                new Point2D(graphicInfo.getX(), middleY));
        return gatewayRectangle;
    }

    private GraphicInfo createGraphicInfo(double x, double y) {
        GraphicInfo graphicInfo = new GraphicInfo();
        graphicInfo.setX(x);
        graphicInfo.setY(y);
        return graphicInfo;
    }

    class FlowWithContainer {

        protected SequenceFlow sequenceFlow;
        protected FlowElementsContainer flowContainer;

        public FlowWithContainer(SequenceFlow sequenceFlow, FlowElementsContainer flowContainer) {
            this.sequenceFlow = sequenceFlow;
            this.flowContainer = flowContainer;
        }

        public SequenceFlow getSequenceFlow() {
            return sequenceFlow;
        }

        public void setSequenceFlow(SequenceFlow sequenceFlow) {
            this.sequenceFlow = sequenceFlow;
        }

        public FlowElementsContainer getFlowContainer() {
            return flowContainer;
        }

        public void setFlowContainer(FlowElementsContainer flowContainer) {
            this.flowContainer = flowContainer;
        }

    }
}
