/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.json.converter.json.converter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.config.CounterSignPassTypeEnum;
import com.je.bpm.core.model.config.task.TaskCounterSignConfigImpl;
import com.je.bpm.core.model.task.KaiteCounterSignUserTask;
import com.je.bpm.core.model.task.KaiteUserTask;

import java.util.Map;

/**
 * 用户会签任务转换器
 */
public class KaiteCounterSignUserTaskJsonConverter extends AbstractKaiteAssignmentUserTaskJsonConverter {

    public static void fillTypes(Map<String, Class<? extends BaseBpmnJsonConverter>> convertersToBpmnMap,
                                 Map<Class<? extends BaseElement>, Class<? extends BaseBpmnJsonConverter>> convertersToJsonMap) {
        fillJsonTypes(convertersToBpmnMap);
        fillBpmnTypes(convertersToJsonMap);
    }

    public static void fillJsonTypes(Map<String, Class<? extends BaseBpmnJsonConverter>> convertersToBpmnMap) {
        convertersToBpmnMap.put(STENCIL_KAITE_COUNTERSIGN_USER_TASK, KaiteCounterSignUserTaskJsonConverter.class);
    }

    public static void fillBpmnTypes(Map<Class<? extends BaseElement>, Class<? extends BaseBpmnJsonConverter>> convertersToJsonMap) {
        convertersToJsonMap.put(KaiteCounterSignUserTask.class, KaiteCounterSignUserTaskJsonConverter.class);
    }

    @Override
    protected String getStencilId(BaseElement baseElement) {
        return STENCIL_KAITE_COUNTERSIGN_USER_TASK;
    }

    @Override
    protected void convertElementToJson(ObjectNode propertiesNode, BaseElement baseElement, ObjectNode flowElementNode) {
        KaiteCounterSignUserTask userTask = (KaiteCounterSignUserTask) baseElement;
        fillUserTaskAssigneeConfigToJson(userTask.getTaskAssigneeConfig(), flowElementNode);
        TaskCounterSignConfigImpl taskCounterSignConfig = userTask.getCounterSignConfig();
        convertCountersignedToJson(taskCounterSignConfig, flowElementNode, userTask.getTaskBasicConfig().getSequential());
        super.convertElementToJson(propertiesNode, baseElement, flowElementNode);
    }

    private void convertCountersignedToJson(TaskCounterSignConfigImpl taskCounterSignConfig, ObjectNode flowElementNode, Boolean sequential) {
        ObjectNode taskCountersignedConfig = objectMapper.createObjectNode();
        taskCountersignedConfig.put(PROPERTY_COUNTERSIGNED_COUNTERSIGNPASSTYPE, taskCounterSignConfig.getCounterSignPassType().toString());
        taskCountersignedConfig.put(PROPERTY_COUNTERSIGNED_AMOUNT, taskCounterSignConfig.getAmount());
        taskCountersignedConfig.put(PROPERTY_COUNTERSIGNED_ONEBALLOTUSERID, taskCounterSignConfig.getOneBallotUserId());
        taskCountersignedConfig.put(PROPERTY_COUNTERSIGNED_ONEBALLOTUSERNAME, taskCounterSignConfig.getOneBallotUserName());
        taskCountersignedConfig.put(PROPERTY_COUNTERSIGNED_VOTEALL, convertBooleanToIntJson(taskCounterSignConfig.isVoteAll()));
        taskCountersignedConfig.put(RUNTIME_TUNING, convertBooleanToIntJson(taskCounterSignConfig.isRuntimeTuning()));
        taskCountersignedConfig.put(ADJUST_BEFORE_RUNNING, convertBooleanToIntJson(taskCounterSignConfig.isAdjustBeforeRunning()));
        taskCountersignedConfig.put(PROPERTY_COUNTERSIGNED_DESELECT_ALL, convertBooleanToIntJson(taskCounterSignConfig.isDeselectAll()));
        taskCountersignedConfig.put(PROPERTY_COUNTERSIGNED_SEQUENTIAL, sequential);
        flowElementNode.set(PROPERTY_COUNTERSIGNED_CONFIG, taskCountersignedConfig);
    }

    @Override
    protected FlowElement convertJsonToElement(JsonNode elementNode, JsonNode modelNode, Map<String, JsonNode> shapeMap) {
        Boolean isSequential = getValueAsBoolean(PROPERTY_COUNTERSIGNED_SEQUENTIAL, elementNode.get(EDITOR_SHAPE_PROPERTIES));
        TaskCounterSignConfigImpl taskCounterSignConfig = new TaskCounterSignConfigImpl();
        //会签
        JsonNode taskDismissNode = elementNode.get(PROPERTY_COUNTERSIGNED_CONFIG);
        isSequential = getValueAsBoolean(PROPERTY_COUNTERSIGNED_SEQUENTIAL, taskDismissNode);
        if (getValueAsString(PROPERTY_COUNTERSIGNED_COUNTERSIGNPASSTYPE, taskDismissNode).equals(CounterSignPassTypeEnum.PASS_PRINCIPAL.toString())) {
            taskCounterSignConfig.setCounterSignPassType(CounterSignPassTypeEnum.PASS_PRINCIPAL);
        } else {
            taskCounterSignConfig.setCounterSignPassType(CounterSignPassTypeEnum.PASS_PERSENT);
        }
        if (getValueAsInteger(PROPERTY_COUNTERSIGNED_AMOUNT, taskDismissNode) != null) {
            taskCounterSignConfig.setAmount(getValueAsInteger(PROPERTY_COUNTERSIGNED_AMOUNT, taskDismissNode));
        }
        taskCounterSignConfig.setVoteAll(getValueAsBoolean(PROPERTY_COUNTERSIGNED_VOTEALL, taskDismissNode));
        taskCounterSignConfig.setRuntimeTuning(getValueAsBoolean(RUNTIME_TUNING, taskDismissNode));
        taskCounterSignConfig.setAdjustBeforeRunning(getValueAsBoolean(ADJUST_BEFORE_RUNNING, taskDismissNode));
        taskCounterSignConfig.setOneBallotUserId(getValueAsString(PROPERTY_COUNTERSIGNED_ONEBALLOTUSERID, taskDismissNode));
        taskCounterSignConfig.setOneBallotUserName(getValueAsString(PROPERTY_COUNTERSIGNED_ONEBALLOTUSERNAME, taskDismissNode));
        taskCounterSignConfig.setDeselectAll(getValueAsBoolean(PROPERTY_COUNTERSIGNED_DESELECT_ALL, taskDismissNode));
        KaiteCounterSignUserTask task = new KaiteCounterSignUserTask(taskCounterSignConfig, isSequential);
        convertJsonToElement(elementNode, task, shapeMap);
        return task;
    }

    protected void convertJsonToElement(JsonNode elementNode, KaiteCounterSignUserTask userTask, Map<String, JsonNode> shapeMap) {
        parseUserTaskJsonToAssigneeConfig(elementNode, userTask);
        super.convertJsonToElement(elementNode, userTask, shapeMap);
    }


}
