/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.process;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.ProcessDeployTypeEnum;

/**
 * 流程定义配置
 */
public class ProcessBasicConfigImpl extends AbstractProcessConfig {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "processBasicConfig";
    /**
     * 功能编码
     */
    private String funcCode;
    /**
     * 功能名称
     */
    private String funcName;
    /**
     * 表code
     */
    private String tableCode;
    /**
     * 功能名称
     */
    private String funcId;
    /**
     * 附属功能ids
     */
    private String attachedFuncIds;
    /**
     * 附属功能names
     */
    private String attachedFuncNames;
    /**
     * 附属功能codes
     */
    private String attachedFuncCodes;
    /**
     * 内容模板
     */
    private String contentModule;
    /**
     * 流程分类Code
     */
    private String processClassificationId;
    /**
     * 流程分类Code
     */
    private String processClassificationName;
    /**
     * 流程分类Code
     */
    private String processClassificationCode;
    /**
     * 默认生产模式
     */
    private ProcessDeployTypeEnum deploymentEnvironment = ProcessDeployTypeEnum.PRODUCT;

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getFuncName() {
        return funcName;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public ProcessBasicConfigImpl() {
        super(CONFIG_TYPE);
    }

    public String getFuncId() {
        return funcId;
    }

    public void setFuncId(String funcId) {
        this.funcId = funcId;
    }

    public String getAttachedFuncIds() {
        return attachedFuncIds;
    }

    public void setAttachedFuncIds(String attachedFuncIds) {
        this.attachedFuncIds = attachedFuncIds;
    }

    public String getAttachedFuncNames() {
        return attachedFuncNames;
    }

    public void setAttachedFuncNames(String attachedFuncNames) {
        this.attachedFuncNames = attachedFuncNames;
    }

    public String getAttachedFuncCodes() {
        return attachedFuncCodes;
    }

    public void setAttachedFuncCodes(String attachedFuncCodes) {
        this.attachedFuncCodes = attachedFuncCodes;
    }

    public String getContentModule() {
        return contentModule;
    }

    public void setContentModule(String contentModule) {
        this.contentModule = contentModule;
    }

    public String getProcessClassificationId() {
        return processClassificationId;
    }

    public void setProcessClassificationId(String processClassificationId) {
        this.processClassificationId = processClassificationId;
    }

    public String getProcessClassificationName() {
        return processClassificationName;
    }

    public void setProcessClassificationName(String processClassificationName) {
        this.processClassificationName = processClassificationName;
    }

    public String getProcessClassificationCode() {
        return processClassificationCode;
    }

    public void setProcessClassificationCode(String processClassificationCode) {
        this.processClassificationCode = processClassificationCode;
    }

    public ProcessDeployTypeEnum getDeploymentEnvironment() {
        return deploymentEnvironment;
    }

    public void setDeploymentEnvironment(ProcessDeployTypeEnum deploymentEnvironment) {
        this.deploymentEnvironment = deploymentEnvironment;
    }

    @Override
    public BaseElement clone() {
        ProcessBasicConfigImpl config = new ProcessBasicConfigImpl();
        config.setDefinitionId(getDefinitionId());
        config.setDefinitionKey(getDefinitionKey());
        config.setFuncCode(getFuncCode());
        config.setFuncId(getFuncId());
        config.setFuncName(getFuncName());
        config.setAttachedFuncIds(getAttachedFuncIds());
        config.setAttachedFuncCodes(getAttachedFuncCodes());
        config.setAttachedFuncNames(getAttachedFuncNames());
        config.setContentModule(getContentModule());
        config.setProcessClassificationId(getProcessClassificationId());
        config.setProcessClassificationName(getProcessClassificationName());
        config.setProcessClassificationCode(getProcessClassificationCode());
        config.setDeploymentEnvironment(getDeploymentEnvironment());
        return config;
    }

}
