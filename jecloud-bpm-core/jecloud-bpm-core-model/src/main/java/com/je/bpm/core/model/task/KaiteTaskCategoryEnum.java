/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.task;

public enum KaiteTaskCategoryEnum {

    KAITE_USERTASK("kaiteUserTask", "任务节点"),
    KAITE_FIXED_USERTASK("kaiteFixedUserTask", "固定人节点"),
    KAITE_RANDOM_USERTASK("kaiteRandomUserTask", "随机节点"),
    KAITE_DECIDE_USERTASK("kaiteDecideUserTask", "判断节点"),
    KAITE_COUNTERSIGN_USERTASK("kaiteCounterSignUserTask", "会签节点"),
    KAITE_CANDIDATE_USERTASK("kaiteCandidateUserTask", "候选节点"),
    KAITE_LOOP_USERTASK("kaiteLoopUserTask", "自由节点"),
    KAITE_MULTI_USERTASK("kaiteMultiUserTask", "多人节点"),
    INCLUSIVE_GATEWAY("inclusiveGateway", "聚合网关"),
    /**
     * JSON转换时的类型，和数据字典中的类型一致
     */
    JSON_KAITE_USERTASK("task", "任务"),
    JSON_KAITE_FIXED_USERTASK("to_assignee", "固定人"),
    JSON_KAITE_RANDOM_USERTASK("random", "随机"),
    JSON_KAITE_DECIDE_USERTASK("decision", "判断任务"),
    JSON_KAITE_COUNTERSIGN_USERTASK("countersign", "会签任务"),
    JSON_KAITE_CANDIDATE_USERTASK("joint", "候选任务"),
    JSON_KAITE_LOOP_USERTASK("circular", "循环任务"),
    JSON_KAITE_MULTI_USERTASK("batchtask", "多人任务"),
    JSON_END("end", "结束节点"),
    JSON_START("start", "开始");

    private String type;

    private String name;


    public String getType() {
        return type;
    }

    KaiteTaskCategoryEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static String getName(String type) {
        if (type.equals(KAITE_USERTASK.getType())) {
            return KAITE_USERTASK.getName();
        } else if (type.equals(KAITE_FIXED_USERTASK.getType())) {
            return KAITE_FIXED_USERTASK.getName();
        } else if (type.equals(KAITE_RANDOM_USERTASK.getType())) {
            return KAITE_RANDOM_USERTASK.getName();
        } else if (type.equals(KAITE_DECIDE_USERTASK.getType())) {
            return KAITE_DECIDE_USERTASK.getName();
        } else if (type.equals(KAITE_COUNTERSIGN_USERTASK.getType())) {
            return KAITE_COUNTERSIGN_USERTASK.getName();
        } else if (type.equals(KAITE_CANDIDATE_USERTASK.getType())) {
            return KAITE_CANDIDATE_USERTASK.getName();
        } else if (type.equals(KAITE_LOOP_USERTASK.getType())) {
            return KAITE_LOOP_USERTASK.getName();
        } else if (type.equals(KAITE_MULTI_USERTASK.getType())) {
            return KAITE_MULTI_USERTASK.getName();
        } else if (type.equals(INCLUSIVE_GATEWAY.getType())) {
            return INCLUSIVE_GATEWAY.getName();
        }
        return "";
    }

}
