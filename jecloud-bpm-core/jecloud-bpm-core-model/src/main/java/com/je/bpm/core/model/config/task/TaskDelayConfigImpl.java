/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;
import java.util.Calendar;
import java.util.Date;

/**
 * 任务延期配置
 */
public class TaskDelayConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "taskDelayWarningConfig";

    /**
     * 默认不启动
     */
    private boolean enable = false;

    /**
     * 可延期次数，默认为2次，用户可自己配置
     */
    private int delayTimes = 2;

    private HandlePeriodTypeEnum delayTimesType = HandlePeriodTypeEnum.DAY;

    /**
     * 延迟窗口
     */
    private int period;

    /**
     * 回调类
     */
    private String callbackServiceName;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getDelayTimes() {
        return delayTimes;
    }

    public void setDelayTimes(int delayTimes) {
        this.delayTimes = delayTimes;
    }

    public HandlePeriodTypeEnum getDelayTimesType() {
        return delayTimesType;
    }

    public void setDelayTimesType(HandlePeriodTypeEnum delayTimesType) {
        this.delayTimesType = delayTimesType;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getCallbackServiceName() {
        return callbackServiceName;
    }

    public void setCallbackServiceName(String callbackServiceName) {
        this.callbackServiceName = callbackServiceName;
    }

    public TaskDelayConfigImpl() {
        super(CONFIG_TYPE);
    }

    @Override
    public BaseElement clone() {
        TaskDelayConfigImpl taskDelayConfig = new TaskDelayConfigImpl();
        taskDelayConfig.setDelayTimes(getDelayTimes());
        taskDelayConfig.setDelayTimesType(getDelayTimesType());
        taskDelayConfig.setEnable(isEnable());
        taskDelayConfig.setPeriod(getPeriod());
        taskDelayConfig.setTaskName(getTaskName());
        taskDelayConfig.setCallbackServiceName(getCallbackServiceName());
        return taskDelayConfig;
    }

    /**
     * 计算显示延期按钮的时间，延期次数暂时不适用此设置
     *
     * @param baseDate
     * @param taskDelayConfig
     * @return
     */
    public Date calculateDelayTime(Date baseDate,TaskDelayConfigImpl taskDelayConfig) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(baseDate);
        if (HandlePeriodTypeEnum.MINUTE.equals(taskDelayConfig.getDelayTimesType())) {
            calendar.add(Calendar.MINUTE, taskDelayConfig.getPeriod());
        } else if (HandlePeriodTypeEnum.HOUR.equals(taskDelayConfig.getDelayTimesType())) {
            calendar.add(Calendar.HOUR, taskDelayConfig.getPeriod());
        } else if (HandlePeriodTypeEnum.DAY.equals(taskDelayConfig.getDelayTimesType())) {
            calendar.add(Calendar.DAY_OF_MONTH, taskDelayConfig.getPeriod());
        } else if (HandlePeriodTypeEnum.MONTH.equals(taskDelayConfig.getDelayTimesType())) {
            calendar.add(Calendar.MONTH, taskDelayConfig.getPeriod());
        } else {
            return null;
        }
        return calendar.getTime();
    }

}
