/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.button.factory;

import com.je.bpm.core.model.button.*;

import java.util.HashMap;
import java.util.Map;

/**
 * ProcessEngineConfigurationImpl -> 里面执行初始化，添加所有的流程启动后按钮
 */

public class TaskButtonFactory {

    public TaskButtonFactory() {
        init();
    }

    private static final Map<String, TaskButton> map = new HashMap<>();

    private static void addButton(String code, TaskButton taskButton) {
        map.put(code, taskButton);
    }

    public static TaskButton getButton(String code) {
        return map.get(code);
    }

    /**
     * 获取按钮，并赋值展示表达式 暂时没用
     *
     * @param code
     * @param displayExpression
     * @return
     */
    public static TaskButton getButton(String code, String displayExpression) {
        return map.get(code);
    }

    private void init() {
        addButton(ButtonEnum.CANCEL_DELEGATE_BTN.getCode(), new TaskCancelDelegateButton());
        addButton(ButtonEnum.ADD_SIGNATURE_NODE.getCode(), new TaskAddSignatureNodeButton());
        addButton(ButtonEnum.DEL_SIGNATURE_NODE.getCode(), new TaskDelSignatureNodeButton());
        addButton(ButtonEnum.CHANGE_ASSIGNEE_BTN.getCode(), new TaskChangeAssigneeButton());
        addButton(ButtonEnum.RECEIVE_BTN.getCode(), new TaskClaimButton());
        addButton(ButtonEnum.DELEGATE_BTN.getCode(), new TaskDelegateButton());
        addButton(ButtonEnum.DIRECT_SEND_BTN.getCode(), new TaskDirectSendButton());
        addButton(ButtonEnum.DISMISS_BTN.getCode(), new TaskDismissButton());
        addButton(ButtonEnum.GOBACK_BTN.getCode(), new TaskGobackButton());
        addButton(ButtonEnum.PASSROUND_BTN.getCode(), new TaskPassroundButton());
        addButton(ButtonEnum.PASSROUND_READ_BTN.getCode(), new TaskPassroundReadButton());
        addButton(ButtonEnum.RETRIEVE_BTN.getCode(), new TaskRetrieveButton());
        addButton(ButtonEnum.SUBMIT_BTN.getCode(), new TaskSubmitButton());
        addButton(ButtonEnum.TRANSFER_BTN.getCode(), new TaskTransferButton());
        addButton(ButtonEnum.URGE_BTN.getCode(), new TaskUrgeButton());
        addButton(ButtonEnum.COUNTERSIGNED_ABSTAIN_BTN.getCode(), new TaskAbstainButton());
        addButton(ButtonEnum.COUNTERSIGNED_PASS_BTN.getCode(), new TaskPassButton());
        addButton(ButtonEnum.COUNTERSIGNED_VETO_BTN.getCode(), new TaskVetoButton());
        addButton(ButtonEnum.COUNTERSIGNED_REBOOK_REDUCTION_BTN.getCode(), new TaskCountersignedRebookReductionButton());
        addButton(ButtonEnum.COUNTERSIGNED_ADD_SIGNATURE_BTN.getCode(), new TaskCountersignedAddSignatureButton());
        addButton(ButtonEnum.COUNTERSIGN_BTN.getCode(), new TaskCountersignButton());
        addButton(ButtonEnum.SIGN_BACK_BTN.getCode(), new TaskSignBackButton());
        addButton(ButtonEnum.STAGING_BTN.getCode(), new TaskStagingButton());
        addButton(ButtonEnum.WITHDRAW_BTN.getCode(), new TaskWithdrawButton());
    }

}
