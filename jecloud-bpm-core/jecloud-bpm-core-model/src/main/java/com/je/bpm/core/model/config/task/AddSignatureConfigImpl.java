/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

import java.util.ArrayList;
import java.util.List;

public class AddSignatureConfigImpl extends AbstractTaskConfigImpl {
    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "taskDismiss";
    /**
     * 运行加签
     */
    private Boolean enable = false;
    /**
     * 无限加签
     */
    private Boolean unlimited = false;
    /**
     * 不可回签
     */
    private Boolean notCountersigned = false;
    /**
     * 强制回签
     */
    private Boolean mandatoryCountersignature = false;
    /**
     * 加签处理人
     */
    private List<PassRoundResource> circulationRules = new ArrayList<>();

    public AddSignatureConfigImpl() {
        super(CONFIG_TYPE);
    }

    public  String getConfigType() {
        return CONFIG_TYPE;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Boolean getUnlimited() {
        return unlimited;
    }

    public void setUnlimited(Boolean unlimited) {
        this.unlimited = unlimited;
    }

    public Boolean getNotCountersigned() {
        return notCountersigned;
    }

    public void setNotCountersigned(Boolean notCountersigned) {
        this.notCountersigned = notCountersigned;
    }

    public Boolean getMandatoryCountersignature() {
        return mandatoryCountersignature;
    }

    public void setMandatoryCountersignature(Boolean mandatoryCountersignature) {
        this.mandatoryCountersignature = mandatoryCountersignature;
    }

    public List<PassRoundResource> getCirculationRules() {
        return circulationRules;
    }

    public void setCirculationRules(List<PassRoundResource> circulationRules) {
        this.circulationRules = circulationRules;
    }

    public void addCirculationRule(PassRoundResource passRoundResource) {
        this.circulationRules.add(passRoundResource);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AddSignatureConfigImpl{");
        sb.append("enable=").append(enable);
        sb.append(", unlimited=").append(unlimited);
        sb.append(", notCountersigned=").append(notCountersigned);
        sb.append(", mandatoryCountersignature=").append(mandatoryCountersignature);
        sb.append(", circulationRules=").append(circulationRules);
        for(PassRoundResource passRoundResource :circulationRules){
            sb.append(", passRoundResource=").append(passRoundResource.toString());
        }
        sb.append('}');
        return sb.toString();
    }

    @Override
    public BaseElement clone() {
        return null;
    }
}
