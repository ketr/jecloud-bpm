/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

/**
 * 定时器事件定义
 * <p>
 *     定时器事件，是由定义的定时器触发的事件。可以用于开始事件 start event，中间事件 intermediate event，或边界事件 boundary event。定时器事件的行为，取决于所使用的业务日历（business calendar）。
 *     定时器事件有默认的业务日历，但也可以为每个定时器事件定义，定义业务日历。
 * </p>
 */
public class TimerEventDefinition extends EventDefinition {

  protected String timeDate;
  protected String timeDuration;
  protected String timeCycle;
  protected String endDate;
  protected String calendarName;

  public String getTimeDate() {
    return timeDate;
  }

  public void setTimeDate(String timeDate) {
    this.timeDate = timeDate;
  }

  public String getTimeDuration() {
    return timeDuration;
  }

  public void setTimeDuration(String timeDuration) {
    this.timeDuration = timeDuration;
  }

  public String getTimeCycle() {
    return timeCycle;
  }

  public void setTimeCycle(String timeCycle) {
    this.timeCycle = timeCycle;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public String getCalendarName() {
    return calendarName;
  }

  public void setCalendarName(String calendarName) {
    this.calendarName = calendarName;
  }

  @Override
  public TimerEventDefinition clone() {
    TimerEventDefinition clone = new TimerEventDefinition();
    clone.setValues(this);
    return clone;
  }

  public void setValues(TimerEventDefinition otherDefinition) {
    super.setValues(otherDefinition);
    setTimeDate(otherDefinition.getTimeDate());
    setTimeDuration(otherDefinition.getTimeDuration());
    setTimeCycle(otherDefinition.getTimeCycle());
    setEndDate(otherDefinition.getEndDate());
    setCalendarName(otherDefinition.getCalendarName());
  }
}
