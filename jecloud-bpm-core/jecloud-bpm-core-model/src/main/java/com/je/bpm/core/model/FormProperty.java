/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 表单属性
 */
public class FormProperty extends BaseElement {

    /**
     * 属性名称
     */
    protected String name;
    /**
     * 属性表达式
     */
    protected String expression;
    /**
     * 属性变量
     */
    protected String variable;
    /**
     * 属性类型
     */
    protected String type;
    /**
     * 默认表达式
     */
    protected String defaultExpression;
    /**
     * 日期格式
     */
    protected String datePattern;
    /**
     * 是否可读
     */
    protected boolean readable = true;
    /**
     * 是否可写
     */
    protected boolean writeable = true;
    /**
     * 是否必填
     */
    protected boolean required;
    /**
     * 值-集合
     */
    protected List<FormValue> formValues = new ArrayList<FormValue>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getType() {
        return type;
    }

    public String getDefaultExpression() {
        return defaultExpression;
    }

    public void setDefaultExpression(String defaultExpression) {
        this.defaultExpression = defaultExpression;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public boolean isReadable() {
        return readable;
    }

    public void setReadable(boolean readable) {
        this.readable = readable;
    }

    public boolean isWriteable() {
        return writeable;
    }

    public void setWriteable(boolean writeable) {
        this.writeable = writeable;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public List<FormValue> getFormValues() {
        return formValues;
    }

    public void setFormValues(List<FormValue> formValues) {
        this.formValues = formValues;
    }

    @Override
    public FormProperty clone() {
        FormProperty clone = new FormProperty();
        clone.setValues(this);
        return clone;
    }

    public void setValues(FormProperty otherProperty) {
        super.setValues(otherProperty);
        setName(otherProperty.getName());
        setExpression(otherProperty.getExpression());
        setVariable(otherProperty.getVariable());
        setType(otherProperty.getType());
        setDefaultExpression(otherProperty.getDefaultExpression());
        setDatePattern(otherProperty.getDatePattern());
        setReadable(otherProperty.isReadable());
        setWriteable(otherProperty.isWriteable());
        setRequired(otherProperty.isRequired());

        formValues = new ArrayList<FormValue>();
        if (otherProperty.getFormValues() != null && !otherProperty.getFormValues().isEmpty()) {
            for (FormValue formValue : otherProperty.getFormValues()) {
                formValues.add(formValue.clone());
            }
        }
    }
}
