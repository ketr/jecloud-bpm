/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.task.assignment.AssignmentPermission;

public class PassRoundResource extends AbstractTaskConfigImpl {

    private static final String CONFIG_TYPE = "circulationRule";

    public PassRoundResource() {
        super(CONFIG_TYPE);
    }

    @Override
    public BaseElement clone() {
        return null;
    }

    public enum PassRoundTypeEnum {
        customerDepartmentConfig("本部门传阅"),
        departmentConfig("按部门传阅"),
        roleConfig("按角色传阅"),
        userConfig("按人员传阅"),
        formFieldConfig("按表单字段传阅"),
        starterUser("流程启动人"),
        customConfig("自定义");

        private String name;

        public String getName() {
            return name;
        }

        PassRoundTypeEnum(String name) {
            this.name = name;
        }

        public static PassRoundTypeEnum getType(String type) {
            if (customerDepartmentConfig.toString().equalsIgnoreCase(type)) {
                return customerDepartmentConfig;
            } else if (starterUser.toString().equalsIgnoreCase(type)) {
                return starterUser;
            } else if (departmentConfig.toString().equalsIgnoreCase(type)) {
                return departmentConfig;
            } else if (roleConfig.toString().equalsIgnoreCase(type)) {
                return roleConfig;
            } else if (userConfig.toString().equalsIgnoreCase(type)) {
                return userConfig;
            } else if (formFieldConfig.toString().equalsIgnoreCase(type)) {
                return formFieldConfig;
            } else if (customConfig.toString().equalsIgnoreCase(type)) {
                return customConfig;
            }
            return null;
        }

    }

    /**
     * 传阅类型
     */
    private PassRoundTypeEnum passRoundTypeEnum;
    /**
     * 权限
     */
    private AssignmentPermission permission;
    /**
     * 资源id
     */
    private String resourceId = "";

    /**
     * 资源名称
     */
    private String resourceName = "";

    /**
     * 自定义service
     */
    private String service = "";

    /**
     * 自定义方法
     */
    private String method = "";

    public PassRoundTypeEnum getPassRoundTypeEnum() {
        return passRoundTypeEnum;
    }

    public void setPassRoundTypeEnum(PassRoundTypeEnum passRoundTypeEnum) {
        this.passRoundTypeEnum = passRoundTypeEnum;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public AssignmentPermission getPermission() {
        return permission;
    }

    public void setPermission(AssignmentPermission permission) {
        this.permission = permission;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PassRoundResource{");
        sb.append("passRoundTypeEnum=").append(passRoundTypeEnum);
        sb.append(", resourceId='").append(resourceId).append('\'');
        sb.append(", resourceName='").append(resourceName).append('\'');
        sb.append(", service='").append(service).append('\'');
        sb.append(", method='").append(method).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
