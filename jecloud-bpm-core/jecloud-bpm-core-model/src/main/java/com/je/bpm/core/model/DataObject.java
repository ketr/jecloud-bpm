/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

/**
 * 数据对象，可以在流程定义或子流程中定义数据对象，数据对象定义会自动转换为流程变量，名称与 ‘name’ 属性对应。
 * 在BPMN中文件中，可以使用DataObject元素来定义流程参数。当流程启动后，这些参数将会自动被设置到流程实例中，用户可以使用
 * RuntimeService、TaskService的方法来查询这些参数。
 * @author opensource
 * @date 2021-10-22
 */
public class DataObject extends FlowElement {

    /**
     * 项定义
     */
    protected ItemDefinition itemSubjectRef;

    public ItemDefinition getItemSubjectRef() {
        return itemSubjectRef;
    }

    public void setItemSubjectRef(ItemDefinition itemSubjectRef) {
        this.itemSubjectRef = itemSubjectRef;
    }

    @Override
    public DataObject clone() {
        DataObject clone = new DataObject();
        clone.setValues(this);
        return clone;
    }

    public void setValues(DataObject otherElement) {
        super.setValues(otherElement);
        setId(otherElement.getId());
        setName(otherElement.getName());
        setItemSubjectRef(otherElement.getItemSubjectRef());
    }
}
