/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.CounterSignPassTypeEnum;

/**
 * 会签配置
 */
public class TaskCounterSignConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "taskCounterSignConfig";
    /**
     * 会签类型，默认是比例通过制
     */
    private CounterSignPassTypeEnum counterSignPassType = CounterSignPassTypeEnum.PASS_PERSENT;
    /**
     * 数值
     * 按比例时，为百分比
     * 按数量时，为数量值
     */
    private int amount;
    /**
     * 一票通过处理人
     */
    private String oneBallotUserId;
    /**
     * 一票通过处理人
     */
    private String oneBallotUserName;
    /**
     * 全部投票
     */
    private boolean voteAll = false;
    /**
     * 运行时调整
     */
    private boolean runtimeTuning = false;
    /**
     * 运行前调整
     */
    private boolean adjustBeforeRunning = false;
    /**
     * 取消全选
     */
    private boolean deselectAll = false;

    public CounterSignPassTypeEnum getCounterSignPassType() {
        return counterSignPassType;
    }

    public void setCounterSignPassType(CounterSignPassTypeEnum counterSignPassType) {
        this.counterSignPassType = counterSignPassType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getOneBallotUserId() {
        return oneBallotUserId;
    }

    public String getOneBallotUserName() {
        return oneBallotUserName;
    }

    public void setOneBallotUserName(String oneBallotUserName) {
        this.oneBallotUserName = oneBallotUserName;
    }

    public void setOneBallotUserId(String oneBallotUserId) {
        this.oneBallotUserId = oneBallotUserId;
    }

    public TaskCounterSignConfigImpl() {
        super(CONFIG_TYPE);
    }

    public boolean isVoteAll() {
        return voteAll;
    }

    public void setVoteAll(boolean voteAll) {
        this.voteAll = voteAll;
    }

    public boolean isRuntimeTuning() {
        return runtimeTuning;
    }

    public void setRuntimeTuning(boolean runtimeTuning) {
        this.runtimeTuning = runtimeTuning;
    }

    public boolean isAdjustBeforeRunning() {
        return adjustBeforeRunning;
    }

    public void setAdjustBeforeRunning(boolean adjustBeforeRunning) {
        this.adjustBeforeRunning = adjustBeforeRunning;
    }

    public boolean isDeselectAll() {
        return deselectAll;
    }

    public void setDeselectAll(boolean deselectAll) {
        this.deselectAll = deselectAll;
    }

    @Override
    public BaseElement clone() {
        TaskCounterSignConfigImpl taskCounterSignConfig = new TaskCounterSignConfigImpl();
        taskCounterSignConfig.setCounterSignPassType(getCounterSignPassType());
        taskCounterSignConfig.setAmount(getAmount());
        taskCounterSignConfig.setOneBallotUserId(getOneBallotUserId());
        taskCounterSignConfig.setVoteAll(isVoteAll());
        taskCounterSignConfig.setRuntimeTuning(isRuntimeTuning());
        taskCounterSignConfig.setAdjustBeforeRunning(isAdjustBeforeRunning());
        taskCounterSignConfig.setDeselectAll(isDeselectAll());
        return taskCounterSignConfig;
    }


}
