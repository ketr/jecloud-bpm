/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.button;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.button.factory.ButtonEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 按钮定义
 */
public class Button extends BaseElement {

    private static final String FORM_VAR_NAME = "form";
    private static final String BUTTON_VAR_NAME = "buttons";
    private static final String CURRENT = "current";
    private static final String PREVIOUS = "previous";
    private static final String PREV_ASSIGNEE = "prevAssignee";
    /**
     * 自定义提交
     */
    public static final String SUBMIT = "submit";
    /**
     * 自定义驳回
     */
    public static final String TURN_DOWN = "turnDown";
    /**
     * 自定义按钮
     */
    public static final String CUSTOMER_BUTTON_VAR_NAME = "customerButtons";

    /**
     * 名称
     */
    private String name;
    /**
     * 自定义名称
     */
    public String customizeName = "";
    /**
     * 自定义审批意见
     */
    public String customizeComments = "";
    /**
     * 编码
     */
    private String code;
    /**
     * pc端事件
     */
    private String pcListeners = "";
    /**
     * 移动端事件
     */
    private String appListeners = "";
    /**
     * 后端事件
     */
    private String backendListeners = "";
    /**
     * 操作标识
     */
    private String operationId = "";
    /**
     * 如果是启动后展示按钮，展示条件
     */
    private String displayExpressionWhenStarted;
    /**
     * 展示条件fn
     */
    private String displayExpressionWhenStartedFn;

    public Button() {
    }

    public Button(String id, String operationId, String name, String code) {
        this.id = id;
        this.operationId = operationId;
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPcListeners() {
        return pcListeners;
    }

    public void setPcListeners(String pcListeners) {
        this.pcListeners = pcListeners;
    }

    public String getAppListeners() {
        return appListeners;
    }

    public void setAppListeners(String appListeners) {
        this.appListeners = appListeners;
    }

    public String getBackendListeners() {
        return backendListeners;
    }

    public void setBackendListeners(String backendListeners) {
        this.backendListeners = backendListeners;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getCustomizeName() {
        return customizeName;
    }

    public void setCustomizeName(String customizeName) {
        this.customizeName = customizeName;
    }

    public String getCustomizeComments() {
        return customizeComments;
    }

    public void setCustomizeComments(String customizeComments) {
        this.customizeComments = customizeComments;
    }

    @Override
    public BaseElement clone() {
        Button newButton = new Button(getId(), getOperationId(), getName(), getCode());
        newButton.setAppListeners(getAppListeners());
        newButton.setBackendListeners(getBackendListeners());
        newButton.setPcListeners(getPcListeners());
        return newButton;
    }

    /**
     * 解析json拿到真实的按钮buttonCode
     *
     * @param text
     * @return
     */
    public static List<String> buildTaskButton(String text) {
        List<String> buttonCodes = new ArrayList<>();
        if (Strings.isNullOrEmpty(text)) {
            return buttonCodes;
        }
        JSONObject buttonJson = JSONObject.parseObject(text).getJSONObject(FORM_VAR_NAME).getJSONObject(BUTTON_VAR_NAME);
        String currentButtons = buttonJson.getString(CURRENT);
        if (Strings.isNullOrEmpty(currentButtons)) {
            return buttonCodes;
        }
        String[] buttonArray = currentButtons.split(",");
        buttonCodes = Arrays.stream(buttonArray).collect(Collectors.toList());
        return buttonCodes;
    }


    /**
     * 解析json拿到真实的按钮buttonCode
     *
     * @param text
     * @return
     */
    public static List<Button> buildCustomerButtons(String text) {
        List<Button> button = new ArrayList<>();
        if (Strings.isNullOrEmpty(text)) {
            return null;
        }
        JSONObject buttonJson = JSONObject.parseObject(text).getJSONObject(FORM_VAR_NAME).getJSONObject(CUSTOMER_BUTTON_VAR_NAME);
        if (buttonJson == null) {
            return null;
        }
        Object submit = buttonJson.get(SUBMIT);
        if (submit == null) {
            return null;
        }
        List<Map<String, String>> submitMap = (List<Map<String, String>>) submit;
        for (Map<String, String> map : submitMap) {
            String nodeName = map.get("nodeName");
            String nodeId = map.get("nodeId");
            TaskCustomSubmitButton taskCustomSubmitButton = new TaskCustomSubmitButton(nodeName, nodeId);
            button.add(taskCustomSubmitButton);
        }
        return button;
    }

    /**
     * 解析json拿到真实的按钮buttonCode
     *
     * @param text
     * @return
     */
    public static List<String> buildBeforeTaskButton(String text, String logUserId, Boolean isEntrust) {
        List<String> buttonCodes = new ArrayList<>();
        if (Strings.isNullOrEmpty(text)) {
            return buttonCodes;
        }
        String prevAssignee = JSONObject.parseObject(text).getJSONObject(FORM_VAR_NAME).getString(PREV_ASSIGNEE);
        if (Strings.isNullOrEmpty(prevAssignee) || !prevAssignee.equals(logUserId)) {
            return buttonCodes;
        }
        JSONObject buttonJson = JSONObject.parseObject(text).getJSONObject(FORM_VAR_NAME).getJSONObject(BUTTON_VAR_NAME);
        String currentButtons = buttonJson.getString(PREVIOUS);
        if (Strings.isNullOrEmpty(currentButtons)) {
            return buttonCodes;
        }
        String[] buttonArray = currentButtons.split(",");
        List<String> buttonList = new ArrayList<>();
        for(String buttonCode : buttonArray){
            if(!Strings.isNullOrEmpty(buttonCode)){
                buttonList.add(buttonCode);
            }
        }
        if (isEntrust) {
            if (buttonList.contains(ButtonEnum.RETRIEVE_BTN.getCode())) {
                buttonList.remove(ButtonEnum.RETRIEVE_BTN.getCode());
            }
        }
        return buttonList;
    }

    public String getDisplayExpressionWhenStarted() {
        return displayExpressionWhenStarted;
    }

    public void setDisplayExpressionWhenStarted(String displayExpressionWhenStarted) {
        this.displayExpressionWhenStarted = displayExpressionWhenStarted;
    }

    public String getDisplayExpressionWhenStartedFn() {
        return displayExpressionWhenStartedFn;
    }

    public void setDisplayExpressionWhenStartedFn(String displayExpressionWhenStartedFn) {
        this.displayExpressionWhenStartedFn = displayExpressionWhenStartedFn;
    }

}
