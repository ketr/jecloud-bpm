/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

import java.util.ArrayList;
import java.util.List;

/**
 * 表单字段配置
 */
public class TaskFormFieldConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "fieldControl";

    /**
     * 表单字段配置
     */
    private List<FormFieldConfig> formFields = new ArrayList<>();

    public TaskFormFieldConfigImpl() {
        super(CONFIG_TYPE);
    }

    public void add(FormFieldConfig formField) {
        formFields.add(formField);
    }

    public void remove(FormFieldConfig formField) {
        formFields.remove(formField);
    }

    public List<FormFieldConfig> getFormFields() {
        return formFields;
    }

    /**
     * 表单字段
     */
    public static class FormFieldConfig {
        /**
         * 字段
         */
        private String code;
        /**
         * 字段
         */
        private String name;
        /**
         * 是否可编辑
         */
        private Boolean editable;
        /**
         * 是否只读
         */
        private Boolean readonly;
        /**
         * 是否隐藏
         */
        private Boolean hidden;
        /**
         * 是否展示
         */
        private Boolean display;
        /**
         * 是否必填
         */
        private Boolean required;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getEditable() {
            return editable;
        }

        public void setEditable(Boolean editable) {
            this.editable = editable;
        }

        public Boolean getReadonly() {
            return readonly;
        }

        public void setReadonly(Boolean readonly) {
            this.readonly = readonly;
        }

        public Boolean getHidden() {
            return hidden;
        }

        public void setHidden(Boolean hidden) {
            this.hidden = hidden;
        }

        public Boolean getDisplay() {
            return display;
        }

        public void setDisplay(Boolean display) {
            this.display = display;
        }

        public Boolean getRequired() {
            return required;
        }

        public void setRequired(Boolean required) {
            this.required = required;
        }

    }

    @Override
    public BaseElement clone() {
        TaskFormFieldConfigImpl taskFormFieldConfig = new TaskFormFieldConfigImpl();
        FormFieldConfig formFieldConfig;
        for (FormFieldConfig each : getFormFields()) {
            formFieldConfig = new FormFieldConfig();
            formFieldConfig.setCode(each.getCode());
            formFieldConfig.setName(each.getName());
            formFieldConfig.setDisplay(each.getDisplay());
            formFieldConfig.setEditable(each.getEditable());
            formFieldConfig.setHidden(each.getHidden());
            formFieldConfig.setReadonly(each.getReadonly());
            formFieldConfig.setRequired(each.getRequired());
            taskFormFieldConfig.add(formFieldConfig);
        }
        return taskFormFieldConfig;
    }

}
