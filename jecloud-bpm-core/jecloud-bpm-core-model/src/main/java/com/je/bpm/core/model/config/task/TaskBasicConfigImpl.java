/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

import java.util.Calendar;
import java.util.Date;

/**
 * 任务类型基础配置
 */
public class TaskBasicConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "properties";
    /**
     * 任务名称
     */
    private String name;

    /**
     * 绑定表单名称
     */
    private String formSchemeName = "";

    /**
     * 绑定表单id
     */
    private String formSchemeId = "";

    /**
     * 列表同步
     */
    private Boolean listSynchronization = false;

    /**
     * 取回
     */
    private Boolean retrieve = false;

    /**
     * 催办
     */
    private Boolean urge = false;

    /**
     * 作废
     */
    private Boolean invalid = false;
    /**
     * 发起人是否可催办
     */
    private boolean initiatorCanUrged = false;
    /**
     * 发起人是否可撤回
     */
    private boolean initiatorCanCancel = false;
    /**
     * 发起人可作废
     */
    private boolean initiatorCanInvalid = false;
    /**
     * 委托
     */
    private Boolean delegate = false;

    /**
     * 转办
     */
    private Boolean transfer = false;

    /**
     * 表单可编辑
     */
    private Boolean formEditable = false;

    /**
     * 消息不提醒
     */
    private Boolean remind = false;

    /**
     * 简易审批
     */
    private Boolean closeSimpleApproval = false;

    /**
     * 自动全选
     */
    private Boolean selectAll = false;
    /**
     * 异步树
     */
    private Boolean asynTree = false;
    /**
     * 可跳跃
     */
    private Boolean jump = false;

    /**
     * 启用逻辑
     */
    private Boolean logicalJudgment = false;
    /**
     * 自动判空
     */
    private Boolean judgeEmpty = false;
    /**
     * 顺序审批
     */
    private Boolean sequential = false;
    /**
     * 是否启用密级
     */
    private Boolean securityEnable = false;
    /**
     * 密级等级
     */
    private String securityCode;
    /**
     * 是否加签
     */
    private Boolean countersign = false;
    /**
     * 是否回签
     */
    private Boolean signBack = false;
    /**
     * 是否强制回签
     */
    private Boolean forcedSignatureBack = false;
    /**
     * 是否启用暂存
     */
    private Boolean staging = false;
    /**
     * 运行时调整
     */
    private boolean basicRuntimeTuning = false;
    /**
     * 是否是加签节点
     */
    private boolean isCs = false;
    /**
     * 加签人员id
     */
    private String createCsUserId;
    /**
     * 合并处理
     */
    private boolean mergeApplication;
    /**
     * 节点加签
     */
    private boolean countersignNode;


    private HandlePeriodTypeEnum handlePeriodType;

    public TaskBasicConfigImpl() {
        super(CONFIG_TYPE);
    }

    public String getFormSchemeName() {
        return formSchemeName;
    }

    public void setFormSchemeName(String formSchemeName) {
        this.formSchemeName = formSchemeName;
    }

    public String getFormSchemeId() {
        return formSchemeId;
    }

    public void setFormSchemeId(String formSchemeId) {
        this.formSchemeId = formSchemeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HandlePeriodTypeEnum getHandlePeriodType() {
        return handlePeriodType;
    }

    public Boolean getAsynTree() {
        return asynTree;
    }

    public void setAsynTree(Boolean asynTree) {
        this.asynTree = asynTree;
    }

    public void setHandlePeriodType(HandlePeriodTypeEnum handlePeriodType) {
        this.handlePeriodType = handlePeriodType;
    }

    public Boolean getListSynchronization() {
        return listSynchronization;
    }

    public void setListSynchronization(Boolean listSynchronization) {
        this.listSynchronization = listSynchronization;
    }

    public Boolean getRetrieve() {
        return retrieve;
    }

    public void setRetrieve(Boolean retrieve) {
        this.retrieve = retrieve;
    }

    public Boolean getUrge() {
        return urge;
    }

    public void setUrge(Boolean urge) {
        this.urge = urge;
    }

    public Boolean getInvalid() {
        return invalid;
    }

    public void setInvalid(Boolean invalid) {
        this.invalid = invalid;
    }

    public Boolean getDelegate() {
        return delegate;
    }

    public void setDelegate(Boolean delegate) {
        this.delegate = delegate;
    }

    public Boolean getTransfer() {
        return transfer;
    }

    public void setTransfer(Boolean transfer) {
        this.transfer = transfer;
    }

    public Boolean getFormEditable() {
        return formEditable;
    }

    public void setFormEditable(Boolean formEditable) {
        this.formEditable = formEditable;
    }

    public Boolean getRemind() {
        return remind;
    }

    public void setRemind(Boolean remind) {
        this.remind = remind;
    }

    public Boolean getCloseSimpleApproval() {
        return closeSimpleApproval;
    }

    public void setCloseSimpleApproval(Boolean closeSimpleApproval) {
        this.closeSimpleApproval = closeSimpleApproval;
    }

    public Boolean getSelectAll() {
        return selectAll;
    }

    public void setSelectAll(Boolean selectAll) {
        this.selectAll = selectAll;
    }

    public Boolean getJump() {
        return jump;
    }

    public void setJump(Boolean jump) {
        this.jump = jump;
    }

    public Boolean getLogicalJudgment() {
        return logicalJudgment;
    }

    public void setLogicalJudgment(Boolean logicalJudgment) {
        this.logicalJudgment = logicalJudgment;
    }

    public Boolean getSequential() {
        return sequential;
    }

    public void setSequential(Boolean sequential) {
        this.sequential = sequential;
    }

    public Boolean getSecurityEnable() {
        return securityEnable;
    }

    public void setSecurityEnable(Boolean securityEnable) {
        this.securityEnable = securityEnable;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public Boolean getCountersign() {
        return countersign;
    }

    public void setCountersign(Boolean countersign) {
        this.countersign = countersign;
    }

    public Boolean getSignBack() {
        return signBack;
    }

    public void setSignBack(Boolean signBack) {
        this.signBack = signBack;
    }

    public Boolean getForcedSignatureBack() {
        return forcedSignatureBack;
    }

    public void setForcedSignatureBack(Boolean forcedSignatureBack) {
        this.forcedSignatureBack = forcedSignatureBack;
    }

    public Boolean getStaging() {
        return staging;
    }

    public void setStaging(Boolean staging) {
        this.staging = staging;
    }

    public boolean getBasicRuntimeTuning() {
        return basicRuntimeTuning;
    }

    public void setBasicRuntimeTuning(boolean basicRuntimeTuning) {
        this.basicRuntimeTuning = basicRuntimeTuning;
    }

    public boolean isInitiatorCanUrged() {
        return initiatorCanUrged;
    }

    public void setInitiatorCanUrged(boolean initiatorCanUrged) {
        this.initiatorCanUrged = initiatorCanUrged;
    }

    public boolean isInitiatorCanCancel() {
        return initiatorCanCancel;
    }

    public void setInitiatorCanCancel(boolean initiatorCanCancel) {
        this.initiatorCanCancel = initiatorCanCancel;
    }

    public boolean isInitiatorCanInvalid() {
        return initiatorCanInvalid;
    }

    public void setInitiatorCanInvalid(boolean initiatorCanInvalid) {
        this.initiatorCanInvalid = initiatorCanInvalid;
    }

    public boolean isCs() {
        return isCs;
    }

    public void setCs(boolean cs) {
        isCs = cs;
    }

    public String getCreateCsUserId() {
        return createCsUserId;
    }

    public void setCreateCsUserId(String createCsUserId) {
        this.createCsUserId = createCsUserId;
    }

    public boolean isMergeApplication() {
        return mergeApplication;
    }

    public void setMergeApplication(boolean mergeApplication) {
        this.mergeApplication = mergeApplication;
    }

    public Boolean getJudgeEmpty() {
        return judgeEmpty;
    }

    public void setJudgeEmpty(Boolean judgeEmpty) {
        this.judgeEmpty = judgeEmpty;
    }

    public boolean isCountersignNode() {
        return countersignNode;
    }

    public void setCountersignNode(boolean countersignNode) {
        this.countersignNode = countersignNode;
    }

    @Override
    public BaseElement clone() {
        TaskBasicConfigImpl taskBasicConfig = new TaskBasicConfigImpl();
        taskBasicConfig.setName(getName());
        taskBasicConfig.setListSynchronization(getListSynchronization());
        taskBasicConfig.setRetrieve(getRetrieve());
        taskBasicConfig.setUrge(getUrge());
        taskBasicConfig.setInvalid(getInvalid());
        taskBasicConfig.setDelegate(getDelegate());
        taskBasicConfig.setTransfer(getTransfer());
        taskBasicConfig.setFormEditable(getFormEditable());
        taskBasicConfig.setRemind(getRemind());
        taskBasicConfig.setCloseSimpleApproval(getCloseSimpleApproval());
        taskBasicConfig.setSelectAll(getSelectAll());
        taskBasicConfig.setJump(getJump());
        taskBasicConfig.setLogicalJudgment(getLogicalJudgment());
        return taskBasicConfig;
    }

    public Date calculateDueTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getTime();
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskBasicConfigImpl{");
        sb.append("name='").append(name).append('\'');
        sb.append(", formSchemeName='").append(formSchemeName).append('\'');
        sb.append(", formSchemeId='").append(formSchemeId).append('\'');
        sb.append(", listSynchronization=").append(listSynchronization);
        sb.append(", retrieve=").append(retrieve);
        sb.append(", urge=").append(urge);
        sb.append(", invalid=").append(invalid);
        sb.append(", delegate=").append(delegate);
        sb.append(", transfer=").append(transfer);
        sb.append(", formEditable=").append(formEditable);
        sb.append(", remind=").append(remind);
        sb.append(", simpleApproval=").append(closeSimpleApproval);
        sb.append(", selectAll=").append(selectAll);
        sb.append(", asynTree=").append(asynTree);
        sb.append(", jump=").append(jump);
        sb.append(", logicalJudgment=").append(logicalJudgment);
        sb.append(", sequential=").append(sequential);
        sb.append(", handlePeriodType=").append(handlePeriodType);
        sb.append('}');
        return sb.toString();
    }
}
