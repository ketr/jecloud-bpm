/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

import java.util.ArrayList;
import java.util.List;

public class TaskFormChildFuncConfigImpl extends AbstractTaskConfigImpl {
    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "taskChildFunc";
    /**
     * 子功能配置列表
     */
    private List<ChildFuncConfig> childFuncConfigs = new ArrayList<>();

    public TaskFormChildFuncConfigImpl() {
        super(CONFIG_TYPE);
    }

    public void add(ChildFuncConfig childFuncConfig) {
        childFuncConfigs.add(childFuncConfig);
    }

    public void remove(ChildFuncConfig childFuncConfig) {
        childFuncConfigs.remove(childFuncConfig);
    }

    public List<ChildFuncConfig> getChildFuncConfigs() {
        return childFuncConfigs;
    }

    public static class ChildFuncConfig {
        /**
         * 编码
         */
        private String code;
        /**
         * 名称
         */
        private String name;
        /**
         * 是否可编辑
         */
        private Boolean editable;
        /**
         * 是否隐藏
         */
        private Boolean hidden;
        /**
         * 是否隐藏
         */
        private Boolean display;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getEditable() {
            return editable;
        }

        public void setEditable(Boolean editable) {
            this.editable = editable;
        }

        public Boolean getHidden() {
            return hidden;
        }

        public void setHidden(Boolean hidden) {
            this.hidden = hidden;
        }

        public Boolean getDisplay() {
            return display;
        }

        public void setDisplay(Boolean display) {
            this.display = display;
        }

    }

    @Override
    public BaseElement clone() {
        TaskFormChildFuncConfigImpl taskFormChildFuncConfig = new TaskFormChildFuncConfigImpl();
        ChildFuncConfig childFuncConfig;
        for (ChildFuncConfig each : getChildFuncConfigs()) {
            childFuncConfig = new ChildFuncConfig();
            childFuncConfig.setName(each.getName());
            childFuncConfig.setCode(each.getCode());
            childFuncConfig.setEditable(each.getEditable());
            childFuncConfig.setHidden(each.getHidden());
            childFuncConfig.setDisplay(each.getDisplay());
            taskFormChildFuncConfig.add(childFuncConfig);
        }
        return taskFormChildFuncConfig;
    }

}
