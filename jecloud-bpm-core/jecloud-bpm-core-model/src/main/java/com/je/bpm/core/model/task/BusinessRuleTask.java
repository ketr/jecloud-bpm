/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.task;

import java.util.ArrayList;
import java.util.List;

/**
 * 业务规则任务
 * <p>
 *     业务规则任务用于同步执行一条或多条规则。Activiti使用名为Drools Expert的Drools规则引擎执行业务规则。
 *     目前，业务规则中包含的.drl文件，必须与定义了业务规则服务并执行规则的流程定义，一起部署。这意味着流程中使用的所有.drl文件都需要打包在流程BAR文件中，与任务表单类似。要了解为Drools Expert创建业务规则的更多信息，请访问位于JBoss Drools的Drools文档。
 *     如果想要插入自己的规则任务实现，例如，希望通过不同方法使用Drools，或者想使用完全不同的规则引擎，则可以使用BusinessRuleTask的class或expression属性。
 *     这样它会与服务任务的行为完全相同。
 * </p>
 */
public class BusinessRuleTask extends Task {

    protected String resultVariableName;
    protected boolean exclude;
    protected List<String> ruleNames = new ArrayList<String>();
    protected List<String> inputVariables = new ArrayList<String>();
    protected String className;

    public boolean isExclude() {
        return exclude;
    }

    public void setExclude(boolean exclude) {
        this.exclude = exclude;
    }

    public String getResultVariableName() {
        return resultVariableName;
    }

    public void setResultVariableName(String resultVariableName) {
        this.resultVariableName = resultVariableName;
    }

    public List<String> getRuleNames() {
        return ruleNames;
    }

    public void setRuleNames(List<String> ruleNames) {
        this.ruleNames = ruleNames;
    }

    public List<String> getInputVariables() {
        return inputVariables;
    }

    public void setInputVariables(List<String> inputVariables) {
        this.inputVariables = inputVariables;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public BusinessRuleTask clone() {
        BusinessRuleTask clone = new BusinessRuleTask();
        clone.setValues(this);
        return clone;
    }

    public void setValues(BusinessRuleTask otherElement) {
        super.setValues(otherElement);
        setResultVariableName(otherElement.getResultVariableName());
        setExclude(otherElement.isExclude());
        setClassName(otherElement.getClassName());
        ruleNames = new ArrayList<String>(otherElement.getRuleNames());
        inputVariables = new ArrayList<String>(otherElement.getInputVariables());
    }
}
