/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.task;

import com.je.bpm.core.model.ActivitiListener;
import com.je.bpm.core.model.config.task.TaskBasicConfigImpl;
import com.je.bpm.core.model.config.task.TaskListenerConfigImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义Task
 */
public abstract class KaiteTask extends Task {

    /**
     * 任务种类
     */
    protected String category;
    private String extensionId;
    /**
     * 任务基本配置
     */
    protected TaskBasicConfigImpl taskBasicConfig = new TaskBasicConfigImpl();
    /**
     * 任务监听配置
     */
    private TaskListenerConfigImpl taskListenerConfig = new TaskListenerConfigImpl();
    /**
     * 任务监听
     */
    protected List<ActivitiListener> taskListeners = new ArrayList<ActivitiListener>();

    public String getCategory() {
        return category;
    }

    protected abstract void setCategory(String category);

    public TaskListenerConfigImpl getTaskListenerConfig() {
        return taskListenerConfig;
    }

    public void setTaskListenerConfig(TaskListenerConfigImpl taskListenerConfig) {
        this.taskListenerConfig = taskListenerConfig;
    }

    public String getExtensionId() {
        return extensionId;
    }

    public void setExtensionId(String extensionId) {
        this.extensionId = extensionId;
    }

    public List<ActivitiListener> getTaskListeners() {
        return taskListeners;
    }

    public void setTaskListeners(List<ActivitiListener> taskListeners) {
        this.taskListeners = taskListeners;
    }

    public TaskBasicConfigImpl getTaskBasicConfig() {
        return taskBasicConfig;
    }

    public void setTaskBasicConfig(TaskBasicConfigImpl taskBasicConfig) {
        this.taskBasicConfig = taskBasicConfig;
    }

    public void setValues(KaiteTask otherElement) {
        super.setValues(otherElement);
        setCategory(otherElement.getCategory());
        setTaskBasicConfig(getTaskBasicConfig());
        setTaskListeners(getTaskListeners());
    }

}
