/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.process;

import com.je.bpm.core.model.*;
import com.je.bpm.core.model.artifact.Artifact;
import org.apache.commons.lang3.StringUtils;
import java.util.*;

/**
 * 子流程
 * 子流程是包含其他的活动、网关、事件等的活动。其本身构成一个流程，并作为更大流程的一部分。子流程完全在父流程中定义（这就是为什么经常被称作嵌入式子流程）。
 * 子流程有两个主要的使用场景：
 * 子流程可以分层建模。很多建模工具都可以折叠子流程，隐藏子流程的所有细节，而只显示业务流程的高层端到端总览。
 * 子流程创建了新的事件范围。在子流程执行中抛出的事件，可以通过子流程边界上的边界事件捕获。因此为该事件创建了限制在子流程内的范围。
 * 使用子流程也要注意以下几点：
 * 子流程只能有一个空启动事件，而不允许有其他类型的启动事件。请注意BPMN 2.0规范允许省略子流程的启动与结束事件，然而当前的Activiti实现并不支持省略。
 * 顺序流不能跨越子流程边界。
 */
public class SubProcess extends Activity implements FlowElementsContainer {

  protected Map<String, FlowElement> flowElementMap = new LinkedHashMap<String, FlowElement>();
  protected List<FlowElement> flowElementList = new ArrayList<FlowElement>();
  protected List<Artifact> artifactList = new ArrayList<Artifact>();
  protected List<ValuedDataObject> dataObjects = new ArrayList<ValuedDataObject>();

  @Override
  public FlowElement getFlowElement(String id) {
    FlowElement foundElement = null;
    if (StringUtils.isNotEmpty(id)) {
      foundElement = flowElementMap.get(id);
    }
    return foundElement;
  }

  @Override
  public Collection<FlowElement> getFlowElements() {
    return flowElementList;
  }

  @Override
  public void addFlowElement(FlowElement element) {
    flowElementList.add(element);
    element.setParentContainer(this);
    if(element instanceof FlowElementsContainer){
      flowElementMap.putAll(((FlowElementsContainer) element).getFlowElementMap());
    }
    if (StringUtils.isNotEmpty(element.getId())) {
      flowElementMap.put(element.getId(), element);
      if (getParentContainer() != null) {
        getParentContainer().addFlowElementToMap(element);
      }
    }
  }

  @Override
  public void addFlowElementToMap(FlowElement element) {
    if (element != null && StringUtils.isNotEmpty(element.getId())) {
      flowElementMap.put(element.getId(), element);
      if (getParentContainer() != null) {
        getParentContainer().addFlowElementToMap(element);
      }
    }
  }

  @Override
  public void removeFlowElement(String elementId) {
    FlowElement element = getFlowElement(elementId);
    if (element != null) {
      flowElementList.remove(element);
      flowElementMap.remove(elementId);
      if (element.getParentContainer() != null) {
        element.getParentContainer().removeFlowElementFromMap(elementId);
      }
    }
  }

  @Override
  public void removeFlowElementFromMap(String elementId) {
    if (StringUtils.isNotEmpty(elementId)) {
      flowElementMap.remove(elementId);
    }
  }

  @Override
  public Map<String, FlowElement> getFlowElementMap() {
    return flowElementMap;
  }

  public void setFlowElementMap(Map<String, FlowElement> flowElementMap) {
    this.flowElementMap = flowElementMap;
  }

  public boolean containsFlowElementId(String id) {
    return flowElementMap.containsKey(id);
  }

  @Override
  public Artifact getArtifact(String id) {
    Artifact foundArtifact = null;
    for (Artifact artifact : artifactList) {
      if (id.equals(artifact.getId())) {
        foundArtifact = artifact;
        break;
      }
    }
    return foundArtifact;
  }

  @Override
  public Collection<Artifact> getArtifacts() {
    return artifactList;
  }

  @Override
  public void addArtifact(Artifact artifact) {
    artifactList.add(artifact);
  }

  @Override
  public void removeArtifact(String artifactId) {
    Artifact artifact = getArtifact(artifactId);
    if (artifact != null) {
      artifactList.remove(artifact);
    }
  }

  @Override
  public SubProcess clone() {
    SubProcess clone = new SubProcess();
    clone.setValues(this);
    return clone;
  }

  public void setValues(SubProcess otherElement) {
    super.setValues(otherElement);

    /*
     * This is required because data objects in Designer have no DI info and are added as properties, not flow elements
     *
     * Determine the differences between the 2 elements' data object
     */
    for (ValuedDataObject thisObject : getDataObjects()) {
      boolean exists = false;
      for (ValuedDataObject otherObject : otherElement.getDataObjects()) {
        if (thisObject.getId().equals(otherObject.getId())) {
          exists = true;
        }
      }
      if (!exists) {
        // missing object
        removeFlowElement(thisObject.getId());
      }
    }

    dataObjects = new ArrayList<ValuedDataObject>();
    if (otherElement.getDataObjects() != null && !otherElement.getDataObjects().isEmpty()) {
      for (ValuedDataObject dataObject : otherElement.getDataObjects()) {
        ValuedDataObject clone = dataObject.clone();
        dataObjects.add(clone);
        // add it to the list of FlowElements
        // if it is already there, remove it first so order is same as
        // data object list
        removeFlowElement(clone.getId());
        addFlowElement(clone);
      }
    }

    flowElementList.clear();
    for (FlowElement flowElement : otherElement.getFlowElements()) {
      addFlowElement(flowElement);
    }

    artifactList.clear();
    for (Artifact artifact : otherElement.getArtifacts()) {
      addArtifact(artifact);
    }
  }

  public List<ValuedDataObject> getDataObjects() {
    return dataObjects;
  }

  public void setDataObjects(List<ValuedDataObject> dataObjects) {
    this.dataObjects = dataObjects;
  }

}
