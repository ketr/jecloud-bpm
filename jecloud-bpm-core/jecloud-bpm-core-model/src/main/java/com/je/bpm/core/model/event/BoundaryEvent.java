/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.je.bpm.core.model.Activity;

/**
 * 边界事件
 * <p>
 *     边界事件是捕获（型）事件，依附在活动（activity）上（边界事件永远不会抛出）。这意味着当活动运行时，事件在监听特定类型的触发器。
 *     当事件捕获时，活动会被终止，并沿该事件的出口顺序流继续。
 * </p>
 */
public class BoundaryEvent extends Event {

    @JsonIgnore
    protected Activity attachedToRef;
    protected String attachedToRefId;
    protected boolean cancelActivity = true;

    public Activity getAttachedToRef() {
        return attachedToRef;
    }

    public void setAttachedToRef(Activity attachedToRef) {
        this.attachedToRef = attachedToRef;
    }

    public String getAttachedToRefId() {
        return attachedToRefId;
    }

    public void setAttachedToRefId(String attachedToRefId) {
        this.attachedToRefId = attachedToRefId;
    }

    public boolean isCancelActivity() {
        return cancelActivity;
    }

    public void setCancelActivity(boolean cancelActivity) {
        this.cancelActivity = cancelActivity;
    }

    @Override
    public BoundaryEvent clone() {
        BoundaryEvent clone = new BoundaryEvent();
        clone.setValues(this);
        return clone;
    }

    public void setValues(BoundaryEvent otherEvent) {
        super.setValues(otherEvent);
        setAttachedToRefId(otherEvent.getAttachedToRefId());
        setAttachedToRef(otherEvent.getAttachedToRef());
        setCancelActivity(otherEvent.isCancelActivity());
    }
}
