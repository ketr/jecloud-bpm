/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.parse;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.GraphicInfo;

/**
 * 错误问题
 */
public class Problem {

  protected String errorMessage;
  protected String resource;
  protected int line;
  protected int column;

  public Problem(String errorMessage, String localName, int lineNumber, int columnNumber) {
    this.errorMessage = errorMessage;
    this.resource = localName;
    this.line = lineNumber;
    this.column = columnNumber;
  }

  public Problem(String errorMessage, BaseElement element) {
    this.errorMessage = errorMessage;
    this.resource = element.getId();
    this.line = element.getXmlRowNumber();
    this.column = element.getXmlColumnNumber();
  }

  public Problem(String errorMessage, GraphicInfo graphicInfo) {
    this.errorMessage = errorMessage;
    this.line = graphicInfo.getXmlRowNumber();
    this.column = graphicInfo.getXmlColumnNumber();
  }

  @Override
  public String toString() {
    return errorMessage + (resource != null ? " | " + resource : "") + " | line " + line + " | column " + column;
  }

}
