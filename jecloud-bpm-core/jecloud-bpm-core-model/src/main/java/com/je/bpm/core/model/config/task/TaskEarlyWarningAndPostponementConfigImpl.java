/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 任务预警与延期配置
 */
public class TaskEarlyWarningAndPostponementConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "earlyWarningAndPostponement";
    /**
     * 是否启用
     */
    private boolean enabled = false;
    /**
     * 处理时限时间
     */
    private String processingTimeLimitDuration;
    /**
     * 处理时限单位名称
     */
    private String processingTimeLimitUnitName;
    /**
     * 处理时限单位code
     */
    private String processingTimeLimitUnitCode;
    /**
     * 预警时限时间
     */
    private String warningTimeLimitDuration;
    /**
     * 预警时限时间单位名称
     */
    private String warningTimeLimitUnitName;
    /**
     * 预警时限时间单位code
     */
    private String warningTimeLimitUnitCode;
    /**
     * 提醒频率时间
     */
    private String reminderFrequencyDuration;
    /**
     * 提醒频率单位名称
     */
    private String reminderFrequencyUnitName;
    /**
     * 提醒频率单位code
     */
    private String reminderFrequencyUnitCode;
    /**
     * 资源
     */
    List<EarlyWarningAndPostponementSource> source = new ArrayList<>();

    public TaskEarlyWarningAndPostponementConfigImpl() {
        super(CONFIG_TYPE);
    }

    public String getConfigType() {
        return CONFIG_TYPE;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<EarlyWarningAndPostponementSource> getSource() {
        return source;
    }

    public void setSource(List<EarlyWarningAndPostponementSource> source) {
        this.source = source;
    }

    @Override
    public BaseElement clone() {
        TaskEarlyWarningAndPostponementConfigImpl taskEarlyWarningConfig = new TaskEarlyWarningAndPostponementConfigImpl();
        return taskEarlyWarningConfig;
    }

    public Date getStartDate(Date date, int beforePeriod, HandlePeriodTypeEnum earlyWarningType) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (beforePeriod > 0) {
            beforePeriod *= -1;
        }
        if (earlyWarningType.equals(HandlePeriodTypeEnum.HOUR)) {
            calendar.add(Calendar.HOUR, beforePeriod);
        } else if (earlyWarningType.equals(HandlePeriodTypeEnum.DAY)) {
            calendar.add(Calendar.DAY_OF_YEAR, beforePeriod);
        } else if (earlyWarningType.equals(HandlePeriodTypeEnum.WEEK)) {
            calendar.add(Calendar.WEEK_OF_YEAR, beforePeriod);
        } else {
            return null;
        }
        return calendar.getTime();
    }

    public String getProcessingTimeLimitDuration() {
        return processingTimeLimitDuration;
    }

    public void setProcessingTimeLimitDuration(String processingTimeLimitDuration) {
        this.processingTimeLimitDuration = processingTimeLimitDuration;
    }

    public String getProcessingTimeLimitUnitName() {
        return processingTimeLimitUnitName;
    }

    public void setProcessingTimeLimitUnitName(String processingTimeLimitUnitName) {
        this.processingTimeLimitUnitName = processingTimeLimitUnitName;
    }

    public String getProcessingTimeLimitUnitCode() {
        return processingTimeLimitUnitCode;
    }

    public void setProcessingTimeLimitUnitCode(String processingTimeLimitUnitCode) {
        this.processingTimeLimitUnitCode = processingTimeLimitUnitCode;
    }

    public String getWarningTimeLimitDuration() {
        return warningTimeLimitDuration;
    }

    public void setWarningTimeLimitDuration(String warningTimeLimitDuration) {
        this.warningTimeLimitDuration = warningTimeLimitDuration;
    }

    public String getWarningTimeLimitUnitName() {
        return warningTimeLimitUnitName;
    }

    public void setWarningTimeLimitUnitName(String warningTimeLimitUnitName) {
        this.warningTimeLimitUnitName = warningTimeLimitUnitName;
    }

    public String getWarningTimeLimitUnitCode() {
        return warningTimeLimitUnitCode;
    }

    public void setWarningTimeLimitUnitCode(String warningTimeLimitUnitCode) {
        this.warningTimeLimitUnitCode = warningTimeLimitUnitCode;
    }

    public String getReminderFrequencyDuration() {
        return reminderFrequencyDuration;
    }

    public void setReminderFrequencyDuration(String reminderFrequencyDuration) {
        this.reminderFrequencyDuration = reminderFrequencyDuration;
    }

    public String getReminderFrequencyUnitName() {
        return reminderFrequencyUnitName;
    }

    public void setReminderFrequencyUnitName(String reminderFrequencyUnitName) {
        this.reminderFrequencyUnitName = reminderFrequencyUnitName;
    }

    public String getReminderFrequencyUnitCode() {
        return reminderFrequencyUnitCode;
    }

    public void setReminderFrequencyUnitCode(String reminderFrequencyUnitCode) {
        this.reminderFrequencyUnitCode = reminderFrequencyUnitCode;
    }
}
