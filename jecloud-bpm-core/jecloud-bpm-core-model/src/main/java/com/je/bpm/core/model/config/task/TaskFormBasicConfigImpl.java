/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

import java.util.HashMap;
import java.util.Map;

/**
 * 任务表单基本配置
 */
public class TaskFormBasicConfigImpl extends AbstractTaskConfigImpl {
    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "formConfig";

    /**
     * 表单字段设置值配置
     */
    private Map<String, String> taskFormFieldSetValueConfig = new HashMap<>();
    /**
     * 表单字段配置
     */
    private TaskFormFieldConfigImpl taskFormFieldConfig = new TaskFormFieldConfigImpl();
    /**
     * 表单按钮配置
     */
    private TaskFormButtonConfigImpl taskFormButtonConfig = new TaskFormButtonConfigImpl();
    /**
     * 表单子功能配置
     */
    private TaskFormChildFuncConfigImpl taskFormChildFuncConfig = new TaskFormChildFuncConfigImpl();

    public  String getConfigType() {
        return CONFIG_TYPE;
    }

    public Map<String, String> getTaskFormFieldSetValueConfig() {
        return taskFormFieldSetValueConfig;
    }

    public void setTaskFormFieldSetValueConfig(Map<String, String> taskFormFieldSetValueConfig) {
        this.taskFormFieldSetValueConfig = taskFormFieldSetValueConfig;
    }

    public TaskFormFieldConfigImpl getTaskFormFieldConfig() {
        return taskFormFieldConfig;
    }

    public void setTaskFormFieldConfig(TaskFormFieldConfigImpl taskFormFieldConfig) {
        this.taskFormFieldConfig = taskFormFieldConfig;
    }

    public TaskFormButtonConfigImpl getTaskFormButtonConfig() {
        return taskFormButtonConfig;
    }

    public void setTaskFormButtonConfig(TaskFormButtonConfigImpl taskFormButtonConfig) {
        this.taskFormButtonConfig = taskFormButtonConfig;
    }

    public TaskFormChildFuncConfigImpl getTaskFormChildFuncConfig() {
        return taskFormChildFuncConfig;
    }

    public void setTaskFormChildFuncConfig(TaskFormChildFuncConfigImpl taskFormChildFuncConfig) {
        this.taskFormChildFuncConfig = taskFormChildFuncConfig;
    }

    public TaskFormBasicConfigImpl() {
        super(CONFIG_TYPE);
    }

    @Override
    public BaseElement clone() {
        TaskFormBasicConfigImpl taskFormBasicConfig = new TaskFormBasicConfigImpl();
        return taskFormBasicConfig;
    }

}
