/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.ProcessRemindTypeEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * 任务提醒配置
 */
public class TaskRemindConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "taskRemindConfig";

    /**
     * 是否启用提醒，默认启用，这样的话流程级别为最大开关
     */
    private boolean enable = true;
    /**
     * 消息提醒类型，此处加这个，是因为任务可以局部设置，而不沿用流程级别的配置。
     */
    private List<ProcessRemindTypeEnum> remindTypes = new ArrayList<>();

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public List<ProcessRemindTypeEnum> getRemindTypes() {
        return remindTypes;
    }

    public void setRemindTypes(List<ProcessRemindTypeEnum> remindTypes) {
        this.remindTypes = remindTypes;
    }

    public TaskRemindConfigImpl() {
        super(CONFIG_TYPE);
    }

    public void addRemindType(ProcessRemindTypeEnum processRemindTypeEnum) {
        this.remindTypes.add(processRemindTypeEnum);
    }

    public void removeRemindType(ProcessRemindTypeEnum processRemindTypeEnum) {
        this.remindTypes.remove(processRemindTypeEnum);
    }

    @Override
    public BaseElement clone() {
        TaskRemindConfigImpl taskRemindConfig = new TaskRemindConfigImpl();
        taskRemindConfig.setEnable(isEnable());
        taskRemindConfig.setRemindTypes(getRemindTypes());
        return taskRemindConfig;
    }

}
