/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.button.TaskDismissBreakdownButton;

import java.util.ArrayList;
import java.util.List;

/**
 * 任务驳回配置
 */
public class TaskDismissConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "taskDismiss";

    /**
     * 是否可驳回，默认不可驳回
     */
    private boolean enable = false;
    /**
     * 驳回节点Id
     */
    private String dismissTaskId = "";
    /**
     * 驳回分解
     */
    List<TaskDismissBreakdownButton> commitBreakdown = new ArrayList<>();

    /**
     * 驳回后可直接提交驳回人（直送按钮启用），针对原配置"可直接提交驳回人"
     */
    private boolean directSendAfterDismiss = false;
    /**
     * 被驳回后，强制直送
     */
    private boolean forceCommitAfterDismiss = false;
    /**
     * 被驳回后禁用送交,不能提交和直送，只能作废
     */
    private Boolean disableSendAfterDismiss = false;
    /**
     * 不可退回，本节点无法执行退回操作
     */
    private Boolean noReturn = false;
    /**
     * 可直接送交退回人
     */
    private Boolean directSendAfterReturn = false;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isDirectSendAfterDismiss() {
        return directSendAfterDismiss;
    }

    public void setDirectSendAfterDismiss(boolean directSendAfterDismiss) {
        this.directSendAfterDismiss = directSendAfterDismiss;
    }

    public String getDismissTaskId() {
        return dismissTaskId;
    }

    public void setDismissTaskId(String dismissTaskId) {
        this.dismissTaskId = dismissTaskId;
    }

    public TaskDismissConfigImpl() {
        super(CONFIG_TYPE);
    }

    public String getConfigType() {
        return CONFIG_TYPE;
    }

    public List<TaskDismissBreakdownButton> getCommitBreakdown() {
        return commitBreakdown;
    }

    public void setCommitBreakdown(List<TaskDismissBreakdownButton> commitBreakdown) {
        this.commitBreakdown = commitBreakdown;
    }

    public boolean isForceCommitAfterDismiss() {
        return forceCommitAfterDismiss;
    }

    public void setForceCommitAfterDismiss(boolean forceCommitAfterDismiss) {
        this.forceCommitAfterDismiss = forceCommitAfterDismiss;
    }

    public Boolean getDisableSendAfterDismiss() {
        return disableSendAfterDismiss;
    }

    public void setDisableSendAfterDismiss(Boolean disableSendAfterDismiss) {
        this.disableSendAfterDismiss = disableSendAfterDismiss;
    }

    public Boolean getNoReturn() {
        return noReturn;
    }

    public void setNoReturn(Boolean noReturn) {
        this.noReturn = noReturn;
    }

    public Boolean getDirectSendAfterReturn() {
        return directSendAfterReturn;
    }

    public void setDirectSendAfterReturn(Boolean directSendAfterReturn) {
        this.directSendAfterReturn = directSendAfterReturn;
    }

    @Override
    public BaseElement clone() {
        TaskDismissConfigImpl taskDismissConfig = new TaskDismissConfigImpl();
        taskDismissConfig.setDirectSendAfterDismiss(isDirectSendAfterDismiss());
        taskDismissConfig.setEnable(isEnable());
        taskDismissConfig.setDismissTaskId(getDismissTaskId());
        return taskDismissConfig;
    }

}
