/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.gateway;

/**
 * 并行网关
 * 并行网关的功能，基于其入口与出口顺序流：
 *
 * 1. 分支：所有的出口顺序流都并行执行，为每一条顺序流创建一个并行执行。
 * 2. 合并：所有到达并行网关的并行执行，都在网关处等待，直到每一条入口顺序流都有一个执行到达。然后流程经过该合并网关继续。
 *
 * 请注意，如果并行网关同时具有多条入口与出口顺序流，可以同时具有分支与合并的行为。在这种情况下，网关首先合并所有入口顺序流，然后分裂为多条并行执行路径。
 */
public class ParallelGateway extends Gateway {

    @Override
    public ParallelGateway clone() {
        ParallelGateway clone = new ParallelGateway();
        clone.setValues(this);
        return clone;
    }

    public void setValues(ParallelGateway otherElement) {
        super.setValues(otherElement);
    }

}
