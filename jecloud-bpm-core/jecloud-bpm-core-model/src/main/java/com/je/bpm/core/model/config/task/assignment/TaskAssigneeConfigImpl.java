/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task.assignment;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.task.AbstractTaskConfigImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * 任务处理人配置
 */
public class TaskAssigneeConfigImpl extends AbstractTaskConfigImpl {

    private static final String CONFIG_TYPE = "taskAssignConfig";

    public enum ReferToEnum {
        LOGUSER("登录用户"),
        STARTUSER("流程启动用户");
        private String name;

        ReferToEnum(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static ReferToEnum getReferToEnumCode(String type) {
            if (LOGUSER.toString().equalsIgnoreCase(type)) {
                return LOGUSER;
            } else if (STARTUSER.toString().equalsIgnoreCase(type)) {
                return STARTUSER;
            }
            return null;
        }


    }

    public TaskAssigneeConfigImpl() {
        super(CONFIG_TYPE);
    }

    /**
     * 人员参照
     */
    private ReferToEnum referTo;
    /**
     * 人员选择
     */
    List<BasicAssignmentConfigImpl> resource = new ArrayList<>();

    public String getConfigType() {
        return CONFIG_TYPE;
    }

    public ReferToEnum getReferTo() {
        return referTo;
    }

    public void setReferTo(ReferToEnum referTo) {
        this.referTo = referTo;
    }

    public List<BasicAssignmentConfigImpl> getResource() {
        return resource;
    }

    public void addResourceConfig(BasicAssignmentConfigImpl assignmentConfig) {
        this.resource.add(assignmentConfig);
    }

    public void removeResourceConfig(BasicAssignmentConfigImpl assignmentConfig) {
        this.resource.remove(assignmentConfig);
    }

    public void setResource(List<BasicAssignmentConfigImpl> resource) {
        this.resource = resource;
    }

    @Override
    public BaseElement clone() {
        TaskAssigneeConfigImpl taskAssigneeConfig = new TaskAssigneeConfigImpl();
        taskAssigneeConfig.setResource(getResource());
        taskAssigneeConfig.setReferTo(getReferTo());
        return taskAssigneeConfig;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskAssigneeConfigImpl{");
        sb.append("referTo=").append(referTo);
        sb.append("\r\n");
        sb.append(", resource=").append(resource);
        for (BasicAssignmentConfigImpl basicAssignmentConfig : resource) {
            sb.append("\r\n");
            sb.append(", basicAssignmentConfig=").append(basicAssignmentConfig.toString());
        }
        sb.append("\r\n");
        sb.append('}');
        return sb.toString();
    }
}
