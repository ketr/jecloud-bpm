/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 调用活动
 * <p>
 *     BPMN 2.0区分一般的子流程，通常也称作嵌入式子流程，与调用活动，尽管它们看起来很像。从概念上说，两者都在流程执行到达该活动时，调用一个子流程。
 *     区别在于，调用活动引用一个流程定义外部的流程，而subprocess嵌入在原有流程定义内。调用活动的主要使用场景，是它有一个可重复使用的流程定义，可以在多个其他流程定义中调用。
 *     当流程执行到达call activity时，会创建一个新的执行，作为到达调用活动的执行的子执行。这个子执行之后用于执行子流程，潜在地创建了类似普通流程的并行子执行。
 *     父执行将等待子流程完成，之后沿原流程继续执行。
 * </p>
 */
public class CallActivity extends Activity {

    protected String calledElement;
    protected boolean inheritVariables;
    protected List<IOParameter> inParameters = new ArrayList<IOParameter>();
    protected List<IOParameter> outParameters = new ArrayList<IOParameter>();
    protected String businessKey;
    protected boolean inheritBusinessKey;

    public String getCalledElement() {
        return calledElement;
    }

    public void setCalledElement(String calledElement) {
        this.calledElement = calledElement;
    }

    public boolean isInheritVariables() {
        return inheritVariables;
    }

    public void setInheritVariables(boolean inheritVariables) {
        this.inheritVariables = inheritVariables;
    }

    public List<IOParameter> getInParameters() {
        return inParameters;
    }

    public void setInParameters(List<IOParameter> inParameters) {
        this.inParameters = inParameters;
    }

    public List<IOParameter> getOutParameters() {
        return outParameters;
    }

    public void setOutParameters(List<IOParameter> outParameters) {
        this.outParameters = outParameters;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public boolean isInheritBusinessKey() {
        return inheritBusinessKey;
    }

    public void setInheritBusinessKey(boolean inheritBusinessKey) {
        this.inheritBusinessKey = inheritBusinessKey;
    }

    @Override
    public CallActivity clone() {
        CallActivity clone = new CallActivity();
        clone.setValues(this);
        return clone;
    }

    public void setValues(CallActivity otherElement) {
        super.setValues(otherElement);
        setCalledElement(otherElement.getCalledElement());
        setBusinessKey(otherElement.getBusinessKey());
        setInheritBusinessKey(otherElement.isInheritBusinessKey());

        inParameters = new ArrayList<IOParameter>();
        if (otherElement.getInParameters() != null && !otherElement.getInParameters().isEmpty()) {
            for (IOParameter parameter : otherElement.getInParameters()) {
                inParameters.add(parameter.clone());
            }
        }

        outParameters = new ArrayList<IOParameter>();
        if (otherElement.getOutParameters() != null && !otherElement.getOutParameters().isEmpty()) {
            for (IOParameter parameter : otherElement.getOutParameters()) {
                outParameters.add(parameter.clone());
            }
        }
    }

}
