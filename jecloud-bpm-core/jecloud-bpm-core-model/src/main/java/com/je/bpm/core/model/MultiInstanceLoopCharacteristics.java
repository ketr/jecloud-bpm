/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

/**
 * 多实例循环特性
 */
public class MultiInstanceLoopCharacteristics extends BaseElement {

    /**
     * 输入数据项
     */
    private String inputDataItem;
    /**
     * 循环基数
     */
    private String loopCardinality;
    /**
     * 完成条件
     */
    private String completionCondition;
    /**
     * 元素变量
     */
    private String elementVariable;
    /**
     * 元素索引变量
     */
    private String elementIndexVariable;
    /**
     * 是否顺序执行
     */
    private boolean sequential;
    /**
     * 循环输出数据对象引用
     */
    private String loopDataOutputRef;
    /**
     * 输出数据项
     */
    private String outputDataItem;

    public String getInputDataItem() {
        return inputDataItem;
    }

    public void setInputDataItem(String inputDataItem) {
        this.inputDataItem = inputDataItem;
    }

    public String getLoopCardinality() {
        return loopCardinality;
    }

    public void setLoopCardinality(String loopCardinality) {
        this.loopCardinality = loopCardinality;
    }

    public String getCompletionCondition() {
        return completionCondition;
    }

    public void setCompletionCondition(String completionCondition) {
        this.completionCondition = completionCondition;
    }

    public String getElementVariable() {
        return elementVariable;
    }

    public void setElementVariable(String elementVariable) {
        this.elementVariable = elementVariable;
    }

    public String getElementIndexVariable() {
        return elementIndexVariable;
    }

    public void setElementIndexVariable(String elementIndexVariable) {
        this.elementIndexVariable = elementIndexVariable;
    }

    public boolean isSequential() {
        return sequential;
    }

    public void setSequential(boolean sequential) {
        this.sequential = sequential;
    }

    public String getLoopDataOutputRef() {
        return loopDataOutputRef;
    }

    public void setLoopDataOutputRef(String loopDataOutputRef) {
        this.loopDataOutputRef = loopDataOutputRef;
    }

    public String getOutputDataItem() {
        return outputDataItem;
    }

    public void setOutputDataItem(String outputDataItem) {
        this.outputDataItem = outputDataItem;
    }

    @Override
    public MultiInstanceLoopCharacteristics clone() {
        MultiInstanceLoopCharacteristics clone = new MultiInstanceLoopCharacteristics();
        clone.setValues(this);
        return clone;
    }

    public void setValues(MultiInstanceLoopCharacteristics otherLoopCharacteristics) {
        setInputDataItem(otherLoopCharacteristics.getInputDataItem());
        setLoopCardinality(otherLoopCharacteristics.getLoopCardinality());
        setCompletionCondition(otherLoopCharacteristics.getCompletionCondition());
        setElementVariable(otherLoopCharacteristics.getElementVariable());
        setElementIndexVariable(otherLoopCharacteristics.getElementIndexVariable());
        setSequential(otherLoopCharacteristics.isSequential());
        setLoopDataOutputRef(otherLoopCharacteristics.getLoopDataOutputRef());
        setOutputDataItem(otherLoopCharacteristics.getOutputDataItem());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MultiInstanceLoopCharacteristics{");
        sb.append("inputDataItem='").append(inputDataItem).append('\'');
        sb.append(", loopCardinality='").append(loopCardinality).append('\'');
        sb.append(", completionCondition='").append(completionCondition).append('\'');
        sb.append(", elementVariable='").append(elementVariable).append('\'');
        sb.append(", elementIndexVariable='").append(elementIndexVariable).append('\'');
        sb.append(", sequential=").append(sequential);
        sb.append(", loopDataOutputRef='").append(loopDataOutputRef).append('\'');
        sb.append(", outputDataItem='").append(outputDataItem).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
