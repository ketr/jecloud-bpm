/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

/**
 * 审批告知
 */
public class TaskApprovalNoticeConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "taskApprovalNoticeConfig";

    /**
     * 启动人
     */
    private boolean startUser = false;
    /**
     * 已审批人
     */
    private boolean approvedPerson = false;
    /**
     * 审批人直属领导
     */
    private boolean approverDirectLeader = false;
    /**
     * 审批人部门领导
     */
    private boolean approverDeptLeader = false;

    public TaskApprovalNoticeConfigImpl() {
        super(CONFIG_TYPE);
    }

    public  String getConfigType() {
        return CONFIG_TYPE;
    }

    public boolean isStartUser() {
        return startUser;
    }

    public void setStartUser(boolean startUser) {
        this.startUser = startUser;
    }

    public boolean isApprovedPerson() {
        return approvedPerson;
    }

    public void setApprovedPerson(boolean approvedPerson) {
        this.approvedPerson = approvedPerson;
    }

    public boolean isApproverDirectLeader() {
        return approverDirectLeader;
    }

    public void setApproverDirectLeader(boolean approverDirectLeader) {
        this.approverDirectLeader = approverDirectLeader;
    }

    public boolean isApproverDeptLeader() {
        return approverDeptLeader;
    }

    public void setApproverDeptLeader(boolean approverDeptLeader) {
        this.approverDeptLeader = approverDeptLeader;
    }

    @Override
    public BaseElement clone() {
        TaskApprovalNoticeConfigImpl taskRemindConfig = new TaskApprovalNoticeConfigImpl();
        return taskRemindConfig;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskApprovalNoticeConfigImpl{");
        sb.append("startUser=").append(startUser);
        sb.append(", approvedPerson=").append(approvedPerson);
        sb.append(", approverDirectLeader=").append(approverDirectLeader);
        sb.append(", approverDeptLeader=").append(approverDeptLeader);
        sb.append('}');
        return sb.toString();
    }
}
