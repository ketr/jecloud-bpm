/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.process;

import com.je.bpm.core.model.config.ProcessCompletedEventEnum;
import com.je.bpm.core.model.config.ProcessExecutionStrategyEnum;

/**
 * 流程事件
 */
public class ProcessCompletedEvent {
    /**
     * 事件类型
     */
    private ProcessCompletedEventEnum type;
    /**
     * 执行策略
     */
    private ProcessExecutionStrategyEnum executionStrategy;
    /**
     * 赋值字段配置
     */
    private String assignmentFieldConfiguration;
    /**
     * 业务bean
     */
    private String serviceName;
    /**
     * 业务方法
     */
    private String method;
    /**
     * 是否存在参数
     */
    private Boolean existenceParameter;

    public ProcessCompletedEventEnum getType() {
        return type;
    }

    public void setType(ProcessCompletedEventEnum type) {
        this.type = type;
    }


    public ProcessExecutionStrategyEnum getExecutionStrategy() {
        return executionStrategy;
    }

    public void setExecutionStrategy(ProcessExecutionStrategyEnum executionStrategy) {
        this.executionStrategy = executionStrategy;
    }

    public String getAssignmentFieldConfiguration() {
        return assignmentFieldConfiguration;
    }

    public void setAssignmentFieldConfiguration(String assignmentFieldConfiguration) {
        this.assignmentFieldConfiguration = assignmentFieldConfiguration;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Boolean getExistenceParameter() {
        return existenceParameter;
    }

    public void setExistenceParameter(Boolean existenceParameter) {
        this.existenceParameter = existenceParameter;
    }
}
