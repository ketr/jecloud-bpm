/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.task;

import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.MultiInstanceLoopCharacteristics;
import com.je.bpm.core.model.config.task.TaskCounterSignConfigImpl;
import com.je.bpm.core.model.config.task.assignment.TaskAssigneeConfigImpl;
import com.je.bpm.core.model.exceptions.ModelException;

import java.util.ArrayList;
import java.util.List;

/**
 * 会签节点
 */
public class KaiteCounterSignUserTask extends KaiteBaseUserTask {

    @Override
    public String getType() {
        return KaiteTaskCategoryEnum.JSON_KAITE_COUNTERSIGN_USERTASK.getType();
    }

    private static final String LOOP_INPUT_DATA_ITEM = "${datas}";

    /**
     * 通过制，默认为比例通过
     */
    private TaskCounterSignConfigImpl counterSignConfig = new TaskCounterSignConfigImpl();
    /**
     * 指派人配置
     */
    protected TaskAssigneeConfigImpl taskAssigneeConfig = new TaskAssigneeConfigImpl();
    /**
     * 会签人数集合
     */
    private List<String> users = new ArrayList<>();

    public TaskCounterSignConfigImpl getCounterSignConfig() {
        return counterSignConfig;
    }

    private void setCounterSignConfig(TaskCounterSignConfigImpl counterSignConfig) {
        this.counterSignConfig = counterSignConfig;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    @Override
    protected void setCategory(String category) {
        throw new ModelException("非法调用！平台已禁用此方法的调用");
    }

    public TaskAssigneeConfigImpl getTaskAssigneeConfig() {
        return taskAssigneeConfig;
    }

    public void setTaskAssigneeConfig(TaskAssigneeConfigImpl taskAssigneeConfig) {
        this.taskAssigneeConfig = taskAssigneeConfig;
    }

    public KaiteCounterSignUserTask(TaskCounterSignConfigImpl counterSignConfig, Boolean isSequential) {

//        loopCharacteristics.setLoopCardinality(multiInstanceCardinality);
        if (counterSignConfig != null) {
            this.counterSignConfig = counterSignConfig;
        }
        this.category = KaiteTaskCategoryEnum.KAITE_COUNTERSIGN_USERTASK.getType();
        this.loopCharacteristics = new MultiInstanceLoopCharacteristics();
        this.loopCharacteristics.setElementVariable("eachLoopUser");
        this.loopCharacteristics.setSequential(isSequential);
        this.loopCharacteristics.setElementIndexVariable("loopIndex");
        this.loopCharacteristics.setInputDataItem(LOOP_INPUT_DATA_ITEM);
        this.loopCharacteristics.setOutputDataItem("counterSignResult");
        this.taskBasicConfig.setSequential(isSequential);
    }

    @Override
    public boolean hasMultiInstanceLoopCharacteristics() {
        return true;
    }

    @Override
    public FlowElement clone() {
        KaiteCounterSignUserTask clone = new KaiteCounterSignUserTask(getCounterSignConfig(), false);
        clone.setValues(this);
        return clone;
    }

    public void setValues(KaiteCounterSignUserTask otherElement) {
        super.setValues(otherElement);
        setCounterSignConfig(otherElement.getCounterSignConfig());
        setTaskAssigneeConfig(otherElement.getTaskAssigneeConfig());
    }

}
