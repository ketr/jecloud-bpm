/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.process.validation.validator.impl;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.signal.Signal;
import com.je.bpm.core.process.validation.ValidationError;
import com.je.bpm.core.process.validation.validator.Problems;
import com.je.bpm.core.process.validation.validator.ValidatorImpl;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.List;

/**
 *
 */
public class SignalValidator extends ValidatorImpl {

    @Override
    public void validate(BpmnModel bpmnModel, List<ValidationError> errors) {
        Collection<Signal> signals = bpmnModel.getSignals();
        if (signals != null && !signals.isEmpty()) {
            for (Signal signal : signals) {
                if (StringUtils.isEmpty(signal.getId())) {
                    addError(errors, Problems.SIGNAL_MISSING_ID, signal, "Signal must have an id");
                }

                if (StringUtils.isEmpty(signal.getName())) {
                    addError(errors, Problems.SIGNAL_MISSING_NAME, signal, "Signal must have a name");
                }

                if (!StringUtils.isEmpty(signal.getName()) && duplicateName(signals, signal.getId(), signal.getName())) {
                    addError(errors, Problems.SIGNAL_DUPLICATE_NAME, signal, "Duplicate signal name found");
                }

                if (signal.getScope() != null && !signal.getScope().equals(Signal.SCOPE_GLOBAL) && !signal.getScope().equals(Signal.SCOPE_PROCESS_INSTANCE)) {
                    addError(errors, Problems.SIGNAL_INVALID_SCOPE, signal, "Invalid value for 'scope'. Only values 'global' and 'processInstance' are supported");
                }
            }

        }
    }

    protected boolean duplicateName(Collection<Signal> signals, String id, String name) {
        for (Signal signal : signals) {
            if (id != null && signal.getId() != null) {
                if (name.equals(signal.getName()) && !id.equals(signal.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

}
