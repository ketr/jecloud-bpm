/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.process.validation.validator.impl;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.gateway.ExclusiveGateway;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.process.validation.ValidationError;
import com.je.bpm.core.process.validation.validator.Problems;
import com.je.bpm.core.process.validation.validator.ProcessLevelValidator;
import org.apache.commons.lang3.StringUtils;
import com.je.bpm.core.model.process.Process;
import java.util.ArrayList;
import java.util.List;

/**

 */
public class ExclusiveGatewayValidator extends ProcessLevelValidator {

  @Override
  protected void executeValidation(BpmnModel bpmnModel, Process process, List<ValidationError> errors) {
    List<ExclusiveGateway> gateways = process.findFlowElementsOfType(ExclusiveGateway.class);
    for (ExclusiveGateway gateway : gateways) {
      validateExclusiveGateway(process, gateway, errors);
    }
  }

  public void validateExclusiveGateway(Process process, ExclusiveGateway exclusiveGateway, List<ValidationError> errors) {
    if (exclusiveGateway.getOutgoingFlows().isEmpty()) {
      addError(errors, Problems.EXCLUSIVE_GATEWAY_NO_OUTGOING_SEQ_FLOW, process, exclusiveGateway, "Exclusive gateway has no outgoing sequence flow");
    } else if (exclusiveGateway.getOutgoingFlows().size() == 1) {
      SequenceFlow sequenceFlow = exclusiveGateway.getOutgoingFlows().get(0);
      if (StringUtils.isNotEmpty(sequenceFlow.getConditionExpression())) {
        addError(errors, Problems.EXCLUSIVE_GATEWAY_CONDITION_NOT_ALLOWED_ON_SINGLE_SEQ_FLOW, process, exclusiveGateway,
            "Exclusive gateway has only one outgoing sequence flow. This is not allowed to have a condition.");
      }
    } else {
      String defaultSequenceFlow = exclusiveGateway.getDefaultFlow();

      List<SequenceFlow> flowsWithoutCondition = new ArrayList<SequenceFlow>();
      for (SequenceFlow flow : exclusiveGateway.getOutgoingFlows()) {
        String condition = flow.getConditionExpression();
        boolean isDefaultFlow = flow.getId() != null && flow.getId().equals(defaultSequenceFlow);
        boolean hasConditon = StringUtils.isNotEmpty(condition);

        if (!hasConditon && !isDefaultFlow) {
          flowsWithoutCondition.add(flow);
        }
        if (hasConditon && isDefaultFlow) {
          addError(errors, Problems.EXCLUSIVE_GATEWAY_CONDITION_ON_DEFAULT_SEQ_FLOW, process, exclusiveGateway, "Default sequenceflow has a condition, which is not allowed");
        }
      }

      if (!flowsWithoutCondition.isEmpty()) {
        addWarning(errors, Problems.EXCLUSIVE_GATEWAY_SEQ_FLOW_WITHOUT_CONDITIONS, process, exclusiveGateway,
            "Exclusive gateway has at least one outgoing sequence flow without a condition (which isn't the default one)");
      }

    }
  }

}
