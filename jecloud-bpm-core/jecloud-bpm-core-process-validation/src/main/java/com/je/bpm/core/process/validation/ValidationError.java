/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.process.validation;

public class ValidationError {

    protected String validatorSetName;

    protected String problem;

    // Default description in english.
    // Other languages can map the validatorSetName/validatorName to the
    // translated version.
    protected String defaultDescription;

    protected String processDefinitionId;

    protected String processDefinitionName;

    protected int xmlLineNumber;

    protected int xmlColumnNumber;

    protected String activityId;

    protected String activityName;

    protected boolean isWarning;

    public String getValidatorSetName() {
        return validatorSetName;
    }

    public void setValidatorSetName(String validatorSetName) {
        this.validatorSetName = validatorSetName;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getDefaultDescription() {
        return defaultDescription;
    }

    public void setDefaultDescription(String defaultDescription) {
        this.defaultDescription = defaultDescription;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

    public int getXmlLineNumber() {
        return xmlLineNumber;
    }

    public void setXmlLineNumber(int xmlLineNumber) {
        this.xmlLineNumber = xmlLineNumber;
    }

    public int getXmlColumnNumber() {
        return xmlColumnNumber;
    }

    public void setXmlColumnNumber(int xmlColumnNumber) {
        this.xmlColumnNumber = xmlColumnNumber;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public boolean isWarning() {
        return isWarning;
    }

    public void setWarning(boolean isWarning) {
        this.isWarning = isWarning;
    }

    @Override
    public String toString() {
        StringBuilder strb = new StringBuilder();
        strb.append("[Validation set: '" + validatorSetName + "' | Problem: '" + problem + "'] : ");
        strb.append(defaultDescription);
        strb.append(" - [Extra info : ");
        boolean extraInfoAlreadyPresent = false;
        if (processDefinitionId != null) {
            strb.append("processDefinitionId = " + processDefinitionId);
            extraInfoAlreadyPresent = true;
        }
        if (processDefinitionName != null) {
            if (extraInfoAlreadyPresent) {
                strb.append(" | ");
            }
            strb.append("processDefinitionName = " + processDefinitionName + " | ");
            extraInfoAlreadyPresent = true;
        }
        if (activityId != null) {
            if (extraInfoAlreadyPresent) {
                strb.append(" | ");
            }
            strb.append("id = " + activityId + " | ");
            extraInfoAlreadyPresent = true;
        }
        if (activityName != null) {
            if (extraInfoAlreadyPresent) {
                strb.append(" | ");
            }
            strb.append("activityName = " + activityName + " | ");
            extraInfoAlreadyPresent = true;
        }
        strb.append("]");
        if (xmlLineNumber > 0 && xmlColumnNumber > 0) {
            strb.append(" ( line: " + xmlLineNumber + ", column: " + xmlColumnNumber + ")");
        }
        return strb.toString();
    }

}
