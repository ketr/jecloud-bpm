/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.connector;

import com.je.bpm.core.model.task.ServiceTask;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.impl.context.ExecutionContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.integration.IntegrationContextEntity;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.api.runtime.process.impl.ExtensionsVariablesMappingProvider;
import com.je.bpm.model.process.model.IntegrationContext;
import com.je.bpm.model.process.model.impl.IntegrationContextImpl;
import java.util.Objects;

public class IntegrationContextBuilder {

    private ExtensionsVariablesMappingProvider inboundVariablesProvider;

    public IntegrationContextBuilder(ExtensionsVariablesMappingProvider inboundVariablesProvider) {
        this.inboundVariablesProvider = inboundVariablesProvider;
    }

    public IntegrationContext from(IntegrationContextEntity integrationContextEntity, DelegateExecution execution) {
        IntegrationContextImpl integrationContext = buildFromExecution(execution);
        integrationContext.setId(integrationContextEntity.getId());
        return integrationContext;
    }

    public IntegrationContext from(DelegateExecution execution) {
        return buildFromExecution(execution);
    }

    private IntegrationContextImpl buildFromExecution(DelegateExecution execution) {
        IntegrationContextImpl integrationContext = new IntegrationContextImpl();
        integrationContext.setRootProcessInstanceId(execution.getRootProcessInstanceId());
        integrationContext.setProcessInstanceId(execution.getProcessInstanceId());
        integrationContext.setProcessDefinitionId(execution.getProcessDefinitionId());
        integrationContext.setBusinessKey(execution.getProcessInstanceBusinessKey());
        integrationContext.setClientId(execution.getCurrentActivityId());
        integrationContext.setExecutionId(execution.getId());

        if (ExecutionEntity.class.isInstance(execution)) {
            ExecutionContext executionContext = new ExecutionContext(ExecutionEntity.class.cast(execution));
            ExecutionEntity processInstance = executionContext.getProcessInstance();
            if (processInstance != null) {
                integrationContext.setParentProcessInstanceId(processInstance.getParentProcessInstanceId());
                integrationContext.setAppVersion(Objects.toString(processInstance.getAppVersion(), "1"));
            }
            // Let's try to resolve process definition attributes
            ProcessDefinition processDefinition = executionContext.getProcessDefinition();
            if (processDefinition != null) {
                integrationContext.setProcessDefinitionKey(processDefinition.getKey());
                integrationContext.setProcessDefinitionVersion(processDefinition.getVersion());
            }
        }

        ServiceTask serviceTask = (ServiceTask) execution.getCurrentFlowElement();
        if (serviceTask != null) {
            integrationContext.setConnectorType(serviceTask.getImplementation());
            integrationContext.setClientName(serviceTask.getName());
            integrationContext.setClientType(ServiceTask.class.getSimpleName());
        }

        integrationContext.addInBoundVariables(inboundVariablesProvider.calculateInputVariables(execution));

        return integrationContext;
    }

}
