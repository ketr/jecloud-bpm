/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.impl.APIProcessConfigConverter;
import com.je.bpm.api.runtime.process.impl.APIProcessDefinitionConverter;
import com.je.bpm.api.runtime.process.impl.ApiProcessButtonConverter;
import com.je.bpm.api.runtime.process.operator.desc.ProcessInvalidParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessInvalidOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.model.process.payloads.ProcessInstanceInvalidPayload;
import com.je.bpm.model.process.results.ProcessDefinitionResult;
import com.je.bpm.runtime.process.operator.ProcessInstanceInvalidOperator;
import com.je.bpm.runtime.shared.RemoteCallServeManager;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.util.Map;

/**
 * 流程作废操作
 */
public class ProcessInstanceInvalidOperatorImpl extends AbstractOperator<ProcessInstanceInvalidPayload, ProcessDefinitionResult> implements ProcessInstanceInvalidOperator {

    private APIProcessDefinitionConverter processDefinitionConverter;
    private APIProcessConfigConverter processConfigConverter;
    private ApiProcessButtonConverter processButtonConverter;
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;
    private OperatorPayloadParamsValidator<ProcessInstanceInvalidPayload> validator;
    private RemoteCallServeManager remoteCallServeManager;

    public ProcessInstanceInvalidOperatorImpl(APIProcessDefinitionConverter processDefinitionConverter, APIProcessConfigConverter processConfigConverter,
                                              ApiProcessButtonConverter processButtonConverter, RepositoryService repositoryService,
                                              RuntimeService runtimeService, RemoteCallServeManager remoteCallServeManager) {
        this.validator = new ProcessInvalidOperatorValidator();
        this.processDefinitionConverter = processDefinitionConverter;
        this.processConfigConverter = processConfigConverter;
        this.processButtonConverter = processButtonConverter;
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.remoteCallServeManager = remoteCallServeManager;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_INVALID_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_INVALID_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessInvalidParamDesc();
    }

    @Override
    public ProcessDefinitionResult execute(ProcessInstanceInvalidPayload processInvalidPayload) {
        runtimeService.deleteProcessInstance(processInvalidPayload.getPiid(), processInvalidPayload.getComment(),
                processInvalidPayload.getBeanId(), processInvalidPayload.getProd(), processInvalidPayload.getBean());
        return null;
    }

    @Override
    public ProcessInstanceInvalidPayload paramsParse(Map<String, Object> params) {
        ProcessInstanceInvalidPayload processInvalidPayload = new ProcessInstanceInvalidPayload(
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("pdid")),
                formatString(params.get("piid")),
                formatString(params.get("beanId"))
        );
        processInvalidPayload.setComment(formatString(params.get("comment")));
        processInvalidPayload.setTableCode(formatString(params.get("tableCode")));
        processInvalidPayload.setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN));
        return processInvalidPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessInstanceInvalidPayload> getValidator() {
        return validator;
    }
}
