/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.operator.desc.ProcessGetNextElementParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessGetCurrentElementPayloadValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.TaskService;
import com.je.bpm.model.process.model.ProcessCirculatedInfo;
import com.je.bpm.model.process.model.impl.ProcessCirculatedInfoImpl;
import com.je.bpm.model.process.results.ProcessCirculatedElementResult;
import com.je.bpm.runtime.process.operator.ProcessGetCirculatedInfoOperator;
import com.je.bpm.runtime.process.payloads.ProcessGetCirculatedInfoPayload;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.util.Map;

/**
 * 获取传阅人员信息
 */
public class ProcessGetCirculatedInfoOperatorImpl extends AbstractOperator<ProcessGetCirculatedInfoPayload, ProcessCirculatedElementResult>
        implements ProcessGetCirculatedInfoOperator {

    private OperatorPayloadParamsValidator<ProcessGetCirculatedInfoPayload> validator;
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;
    private TaskService taskService;

    public ProcessGetCirculatedInfoOperatorImpl(RepositoryService repositoryService, RuntimeService runtimeService, TaskService taskService) {
        this.validator = new ProcessGetCurrentElementPayloadValidator();
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.taskService = taskService;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_GET_CIRCULATED_INFO_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_GET_CIRCULATED_INFO_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessGetNextElementParamDesc();
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessGetCirculatedInfoPayload> getValidator() {
        return validator;
    }

    @Override
    public ProcessGetCirculatedInfoPayload paramsParse(Map<String, Object> params) {
        String taskId = (String) params.get("taskId");
        String beanId = String.valueOf(params.get("beanId"));
        String prod = (String) params.get(AbstractOperator.PROD);
        Map<String, Object> bean = formatMap(params.get(AbstractOperator.BEAN));
        ProcessGetCirculatedInfoPayload processGetCirculatedInfoPayload = new ProcessGetCirculatedInfoPayload();
        processGetCirculatedInfoPayload.setBean(bean);
        processGetCirculatedInfoPayload.setTaskId(taskId);
        processGetCirculatedInfoPayload.setProd(prod);
        processGetCirculatedInfoPayload.setBeanId(beanId);
        return processGetCirculatedInfoPayload;
    }

    @Override
    public ProcessCirculatedElementResult execute(ProcessGetCirculatedInfoPayload processGetCirculatedInfoPayload) {
        String taskId = processGetCirculatedInfoPayload.getTaskId();
        Object allUserInfo = buildUsersInfo(taskId, processGetCirculatedInfoPayload.getProd(), processGetCirculatedInfoPayload.getBean());
        ProcessCirculatedInfo processCirculatedInfo = new ProcessCirculatedInfoImpl(allUserInfo);
        return new ProcessCirculatedElementResult(processGetCirculatedInfoPayload, processCirculatedInfo);
    }

    private Object buildUsersInfo(String taskId, String prod, Map<String, Object> bean) {
        return taskService.getCirculatedAssignment(taskId, prod, bean);
    }


}

