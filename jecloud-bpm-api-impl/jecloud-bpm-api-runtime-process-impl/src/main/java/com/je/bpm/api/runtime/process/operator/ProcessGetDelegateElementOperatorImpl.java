/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.operator.desc.ProcessGetNextElementParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessGetDelegateElementPayloadValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.Task;
import com.je.bpm.model.process.model.ProcessNextNodeInfo;
import com.je.bpm.model.process.model.impl.ProcessNextNodeInfoImpl;
import com.je.bpm.model.process.results.ProcessDelegateElementResult;
import com.je.bpm.runtime.process.operator.ProcessGetDelegateElementOperator;
import com.je.bpm.runtime.process.payloads.ProcessGetDelegateElementPayload;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 获取委托节点信息
 */
public class ProcessGetDelegateElementOperatorImpl extends AbstractOperator<ProcessGetDelegateElementPayload, ProcessDelegateElementResult>
        implements ProcessGetDelegateElementOperator {

    private OperatorPayloadParamsValidator<ProcessGetDelegateElementPayload> validator = new ProcessGetDelegateElementPayloadValidator();
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;
    private TaskService taskService;

    public ProcessGetDelegateElementOperatorImpl(RepositoryService repositoryService, RuntimeService runtimeService, TaskService taskService) {
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.taskService = taskService;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_GET_DELEGATE_ELEMENT_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_GET_DELEGATE_ELEMENT_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessGetNextElementParamDesc();
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessGetDelegateElementPayload> getValidator() {
        return validator;
    }

    @Override
    public ProcessGetDelegateElementPayload paramsParse(Map<String, Object> params) {
        String executionId = (String) params.get("piid");
        String taskId = (String) params.get("taskId");
        ProcessGetDelegateElementPayload processGetNextElementPayload = new ProcessGetDelegateElementPayload();
        processGetNextElementPayload.setPiid(executionId);
        processGetNextElementPayload.setTaskId(taskId);
        return processGetNextElementPayload;
    }

    @Override
    public ProcessDelegateElementResult execute(ProcessGetDelegateElementPayload processGetDelegateElementPayload) {
        ProcessNextNodeInfo processNextNodeInfo = new ProcessNextNodeInfoImpl();
        String taskId = processGetDelegateElementPayload.getTaskId();
        //获取当前节点信息
        Set<String> taskIds = new HashSet<>();
        taskIds.add(taskId);
        List<Task> tasks = taskService.createTaskQuery().taskId(taskId).list();
        if (tasks.size() == 0) {
            throw new ActivitiException("not find task");
        }
        TaskEntity task = (TaskEntity) tasks.get(0);
        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId(), task.getProcessInstanceId(),task.getBusinessKey());
        //设置节点信息
        //设置处理人信息
        FlowElement nextFlowElement = bpmnModel.getFlowElement(task.getTaskDefinitionKey());
        processNextNodeInfo.setNodeName(nextFlowElement.getName());
        processNextNodeInfo.setNodeId(nextFlowElement.getId());
        return new ProcessDelegateElementResult(this, processNextNodeInfo);
    }

}

