/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.impl.APIProcessConfigConverter;
import com.je.bpm.api.runtime.process.impl.APIProcessDefinitionConverter;
import com.je.bpm.api.runtime.process.impl.ApiProcessButtonConverter;
import com.je.bpm.api.runtime.process.operator.desc.ProcessDefinitionHangParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessDefinitionHangOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.button.ProcessButton;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.model.process.payloads.ProcessDefinitionHangPayload;
import com.je.bpm.model.process.results.ProcessDefinitionResult;
import com.je.bpm.runtime.process.operator.ProcessDefinitionHangOperator;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.util.List;
import java.util.Map;

/**
 * 流程定义挂载
 */
public class ProcessDefinitionHangOperatorImpl extends AbstractOperator<ProcessDefinitionHangPayload, ProcessDefinitionResult> implements ProcessDefinitionHangOperator {

    private OperatorPayloadParamsValidator<ProcessDefinitionHangPayload> validator = new ProcessDefinitionHangOperatorValidator();
    private APIProcessDefinitionConverter processDefinitionConverter;
    private APIProcessConfigConverter processConfigConverter;
    private ApiProcessButtonConverter processButtonConverter;
    private RepositoryService repositoryService;

    public ProcessDefinitionHangOperatorImpl(APIProcessDefinitionConverter processDefinitionConverter,
                                             APIProcessConfigConverter processConfigConverter,
                                             ApiProcessButtonConverter processButtonConverter,
                                             RepositoryService repositoryService) {
        this.processDefinitionConverter = processDefinitionConverter;
        this.processConfigConverter = processConfigConverter;
        this.processButtonConverter = processButtonConverter;
        this.repositoryService = repositoryService;
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessDefinitionHangPayload> getValidator() {
        return validator;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_DEFINITION_HANG_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_DEFINITION_HANG_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessDefinitionHangParamDesc();
    }

    @Override
    public ProcessDefinitionHangPayload paramsParse(Map<String, Object> params) {
        ProcessDefinitionHangPayload payload = new ProcessDefinitionHangPayload(
                formatString(params.get("pdid")),
                formatBoolean(params.get("cascade"))
        );
        return payload;
    }

    @Override
    public ProcessDefinitionResult execute(ProcessDefinitionHangPayload payload) {
        repositoryService.suspendProcessDefinitionById(payload.getPdid(),payload.isCascade(),null);
        ProcessDefinition processDefinition = repositoryService.getProcessDefinition(payload.getPdid());
        ProcessDefinitionResult processDefinitionResult = new ProcessDefinitionResult(payload,processDefinitionConverter.from(processDefinition));
        //设置流程基础配置
        BpmnModel bpmnModel = repositoryService.getBpmnModel(payload.getPdid(),"","");
        processDefinitionResult.setProcessBasicConfig(processConfigConverter.from(bpmnModel.getMainProcess().getProcessConfig()));
        //设置流程按钮
        processDefinitionResult.setButtons((List<com.je.bpm.model.process.model.ProcessButton>) processButtonConverter.from((ProcessButton) bpmnModel.getMainProcess().getButtons().getButtons()));
        return processDefinitionResult;
    }

}
