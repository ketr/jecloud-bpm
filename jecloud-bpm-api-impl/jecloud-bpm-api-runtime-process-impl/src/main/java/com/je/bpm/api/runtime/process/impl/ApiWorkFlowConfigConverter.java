/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.impl;

import com.google.common.base.Strings;
import com.je.bpm.api.runtime.shared.model.impl.ModelConverter;
import com.je.bpm.core.model.config.task.TaskBasicConfigImpl;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.core.model.task.KaiteCounterSignUserTask;
import com.je.bpm.engine.task.GetTakeNodeNameUtil;
import com.je.bpm.model.shared.model.WorkFlowConfig;
import com.je.bpm.model.shared.model.impl.WorkFlowConfigImpl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ApiWorkFlowConfigConverter implements ModelConverter<KaiteBaseUserTask, Map<String, Object>, WorkFlowConfig> {

    @Override
    public WorkFlowConfig from(KaiteBaseUserTask kaiteUserTask) {
        return null;
    }

    @Override
    public WorkFlowConfig from(KaiteBaseUserTask kaiteUserTask, Map<String, Object> params) {
        TaskBasicConfigImpl basicConfig = kaiteUserTask.getTaskBasicConfig();
        WorkFlowConfigImpl workFlowConfig = new WorkFlowConfigImpl();
        workFlowConfig.setCurrentNodeId(kaiteUserTask.getId());
        if (Strings.isNullOrEmpty(kaiteUserTask.getName())) {
            String nodeName = GetTakeNodeNameUtil.builder().getTakeNodeName(kaiteUserTask);
            workFlowConfig.setCurrentNodeName(nodeName);
        } else {
            workFlowConfig.setCurrentNodeName(kaiteUserTask.getName());
        }
        workFlowConfig.setCurrentTarget(String.valueOf(params.get("currentTarget")));
        workFlowConfig.setAsynTree(basicConfig.getAsynTree() == true ? "1" : "0");
        workFlowConfig.setSelectAll(basicConfig.getSelectAll() == true ? "1" : "0");
        workFlowConfig.setPersonnelAdjustments("1");
        if (kaiteUserTask instanceof KaiteCounterSignUserTask) {
            KaiteCounterSignUserTask kaiteCounterSignUserTask = (KaiteCounterSignUserTask) kaiteUserTask;
            if (kaiteCounterSignUserTask.getCounterSignConfig().isDeselectAll() &&
                    kaiteCounterSignUserTask.getCounterSignConfig().isAdjustBeforeRunning()) {
                workFlowConfig.setSelectAll("0");
            } else {
                workFlowConfig.setSelectAll("1");
            }
            if (!kaiteCounterSignUserTask.getCounterSignConfig().isAdjustBeforeRunning()) {
                workFlowConfig.setPersonnelAdjustments("0");
            }
        }

        //紧急状态
        if (params.get("exigency") != null && (boolean) params.get("exigency")) {
            workFlowConfig.setExigency("1");
        } else {
            workFlowConfig.setExigency("0");
        }

        workFlowConfig.setAudFlag(WorkFlowConfigImpl.WorkFlowAudFlag.WAIT);
        workFlowConfig.setListSynchronization(basicConfig.getListSynchronization() == true ? "1" : "0");
        workFlowConfig.setShowSequentialConfig(kaiteUserTask.getLoopCharacteristics() != null ? "1" : "0");
        if (kaiteUserTask.getLoopCharacteristics() != null) {
            workFlowConfig.setSequential(kaiteUserTask.getLoopCharacteristics().isSequential() == true ? "1" : "0");
        } else {
            workFlowConfig.setSequential("0");
        }
        return workFlowConfig;
    }

    @Override
    public List<WorkFlowConfig> from(Collection<KaiteBaseUserTask> sources) {
        return null;
    }

    @Override
    public List<WorkFlowConfig> from(Collection<KaiteBaseUserTask> sources, Map<String, Object> Params) {
        return null;
    }


}
