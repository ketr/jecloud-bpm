/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator.desc;

import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamType;

/**
 * 流程启动操作参数描述
 */
public class ProcessMessageStartParamDesc extends OperationParamDesc {

    private ParamDesc messageParam;
    private ParamDesc prodParam;
    private ParamDesc beanParam;

    public ProcessMessageStartParamDesc() {
        messageParam = new ParamDesc();
        messageParam.setName("messageRef");
        messageParam.setDescription("消息引用。");
        messageParam.setRequired(true);
        messageParam.setType(ParamType.String);

        prodParam = new ParamDesc();
        prodParam.setName(AbstractOperator.PROD);
        prodParam.setDescription("产品编码");
        prodParam.setRequired(true);
        prodParam.setType(ParamType.String);

        beanParam = new ParamDesc();
        beanParam.setName("beanId");
        beanParam.setDescription("数据ID");
        beanParam.setRequired(true);
        beanParam.setType(ParamType.String);

        paramKeys.add(messageParam.getName());
        paramKeys.add(prodParam.getName());
        paramKeys.add(beanParam.getName());
    }

    public ParamDesc getMessageParam() {
        return messageParam;
    }

    public void setMessageParam(ParamDesc messageParam) {
        this.messageParam = messageParam;
    }

    public ParamDesc getProdParam() {
        return prodParam;
    }

    public void setProdParam(ParamDesc prodParam) {
        this.prodParam = prodParam;
    }

    public ParamDesc getBeanParam() {
        return beanParam;
    }

    public void setBeanParam(ParamDesc beanParam) {
        this.beanParam = beanParam;
    }

    @Override
    public String toString() {
        return "ProcessMessageStartParamDesc{" +
                "messageParam=" + messageParam +
                ", prodParam=" + prodParam +
                ", beanParam=" + beanParam +
                ", paramKeys=" + paramKeys +
                '}';
    }
}
