/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator.desc;

import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamType;

/**
 * 获取流程模型操作参数描述
 */
public class ProcessModelGetListParamDesc extends OperationParamDesc {

    private ParamDesc startParam;
    private ParamDesc limitParam;
    private ParamDesc name;
    private ParamDesc key;
    private ParamDesc category;
    private ParamDesc funcCode;
    private ParamDesc funcName;
    private ParamDesc multipleConditionsOrQuery;


    public ProcessModelGetListParamDesc() {
        startParam = new ParamDesc();
        startParam.setName("start");
        startParam.setDescription("起始值。");
        startParam.setRequired(false);
        startParam.setType(ParamType.Integer);

        limitParam = new ParamDesc();
        limitParam.setName("limit");
        limitParam.setDescription("数据条数,空获取全部。");
        limitParam.setRequired(false);
        limitParam.setType(ParamType.Integer);

        name = new ParamDesc();
        name.setName("name");
        name.setDescription("名称。");
        name.setRequired(false);
        name.setType(ParamType.String);

        key = new ParamDesc();
        key.setName("key");
        key.setDescription("key。");
        key.setRequired(false);
        key.setType(ParamType.String);

        category = new ParamDesc();
        category.setName("category");
        category.setDescription("分类。");
        category.setRequired(false);
        category.setType(ParamType.String);

        funcCode = new ParamDesc();
        funcCode.setName("funcCode");
        funcCode.setDescription("功能code。");
        funcCode.setRequired(false);
        funcCode.setType(ParamType.String);

        funcName = new ParamDesc();
        funcName.setName("funcName");
        funcName.setDescription("功能名称。");
        funcName.setRequired(false);
        funcName.setType(ParamType.String);

        multipleConditionsOrQuery = new ParamDesc();
        multipleConditionsOrQuery.setName("multipleConditionsOrQuery");
        multipleConditionsOrQuery.setDescription("是否开启传一个值模糊查询多字段。为空或者0不开启，1开始");
        multipleConditionsOrQuery.setRequired(false);
        multipleConditionsOrQuery.setType(ParamType.String);

        paramKeys.add(startParam.getName());
        paramKeys.add(limitParam.getName());
        paramKeys.add(name.getName());
        paramKeys.add(key.getName());
        paramKeys.add(category.getName());
        paramKeys.add(funcCode.getName());
        paramKeys.add(funcName.getName());
        paramKeys.add(multipleConditionsOrQuery.getName());

    }

    public ParamDesc getStartParam() {
        return startParam;
    }

    public void setStartParam(ParamDesc startParam) {
        this.startParam = startParam;
    }

    public ParamDesc getLimitParam() {
        return limitParam;
    }

    public void setLimitParam(ParamDesc limitParam) {
        this.limitParam = limitParam;
    }

    @Override
    public String toString() {
        return "ProcessModelGetListParamDesc{" +
                "startParam=" + startParam +
                ", limitParam=" + limitParam +
                ", paramKeys=" + paramKeys +
                '}';
    }
}
