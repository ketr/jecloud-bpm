/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.operator.desc.ProcessGetGobackElementParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessGetGobackElementPayloadValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.TaskService;
import com.je.bpm.model.process.model.impl.ProcessGobackNodeInfoImpl;
import com.je.bpm.model.process.results.ProcessGobackElementResult;
import com.je.bpm.runtime.process.operator.ProcessGetGobackElementOperator;
import com.je.bpm.runtime.process.payloads.ProcessGetGobackElementPayload;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.util.List;
import java.util.Map;

/**
 * 获取退回节点信息
 */
public class ProcessGetGobackElementOperatorImpl extends AbstractOperator<ProcessGetGobackElementPayload, ProcessGobackElementResult>
        implements ProcessGetGobackElementOperator {

    private OperatorPayloadParamsValidator<ProcessGetGobackElementPayload> validator = new ProcessGetGobackElementPayloadValidator();
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;
    private TaskService taskService;

    public ProcessGetGobackElementOperatorImpl(RepositoryService repositoryService, RuntimeService runtimeService, TaskService taskService) {
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.taskService = taskService;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_GET_GO_BACK_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_GET_GO_BACK_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessGetGobackElementParamDesc();
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessGetGobackElementPayload> getValidator() {
        return validator;
    }

    @Override
    public ProcessGetGobackElementPayload paramsParse(Map<String, Object> params) {
        String executionId = (String) params.get("piid");
        String taskId = (String) params.get("taskId");
        ProcessGetGobackElementPayload processGetNextElementPayload = new ProcessGetGobackElementPayload();
        processGetNextElementPayload.setPiid(executionId);
        processGetNextElementPayload.setTaskId(taskId);
        return processGetNextElementPayload;
    }

    @Override
    public ProcessGobackElementResult execute(ProcessGetGobackElementPayload processGetGobackElementPayload) {
        String taskId = processGetGobackElementPayload.getTaskId();
        ProcessGobackNodeInfoImpl processGobackNodeInfo = new ProcessGobackNodeInfoImpl();
        String type = processGetGobackElementPayload.getType();
        //在变量里面查找上个节点
        List<Map<String, String>> nodeInfo = taskService.getGobackAndRetrieveNodeInfo(taskId, type);
        if (nodeInfo == null) {
            throw new ActivitiException("无法找到上个节点信息!");
        }
        Map<String, String> map = nodeInfo.get(0);
        if (map.get("nodeName") != null) {
            processGobackNodeInfo.setNodeName(map.get("nodeName"));
            processGobackNodeInfo.setNodeId(map.get("nodeId"));
        }
        if (map.get("directTaskName") != null) {
            processGobackNodeInfo.setNodeName(map.get("directTaskName"));
            processGobackNodeInfo.setNodeId(map.get("directTaskId"));
        }
        return new ProcessGobackElementResult(this, processGobackNodeInfo);
    }

}

