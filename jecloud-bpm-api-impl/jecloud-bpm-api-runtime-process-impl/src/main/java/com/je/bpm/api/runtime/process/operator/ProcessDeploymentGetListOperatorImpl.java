/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.impl.APIDeploymentConverter;
import com.je.bpm.api.runtime.process.operator.desc.ProcessDefinitionGetListParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessDeploymentGetListOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.repository.Deployment;
import com.je.bpm.model.process.payloads.ProcessDeploymentGetListPayload;
import com.je.bpm.model.process.results.ProcessDeploymentListResult;
import com.je.bpm.runtime.process.operator.ProcessDeploymentGetListOperator;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 获取流程定义集合
 */
public class ProcessDeploymentGetListOperatorImpl extends AbstractOperator<ProcessDeploymentGetListPayload, ProcessDeploymentListResult> implements ProcessDeploymentGetListOperator {

    private OperatorPayloadParamsValidator<ProcessDeploymentGetListPayload> validator = new ProcessDeploymentGetListOperatorValidator();
    private APIDeploymentConverter apiDeploymentConverter;
    private RepositoryService repositoryService;

    public ProcessDeploymentGetListOperatorImpl(APIDeploymentConverter apiDeploymentConverter, RepositoryService repositoryService) {
        this.apiDeploymentConverter = apiDeploymentConverter;
        this.repositoryService = repositoryService;
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessDeploymentGetListPayload> getValidator() {
        return validator;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_DEPLOYMENT_LIST_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_DEPLOYMENT_LIST_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessDefinitionGetListParamDesc();
    }

    @Override
    public ProcessDeploymentGetListPayload paramsParse(Map<String, Object> params) {
        ProcessDeploymentGetListPayload payload = new ProcessDeploymentGetListPayload(
                formatString(params.get("processDefinitionKey")),
                formatInteger(params.get("start")),
                formatInteger(params.get("limit"))
        );
        return payload;
    }

    @Override
    public ProcessDeploymentListResult execute(ProcessDeploymentGetListPayload payload) {
        List<Deployment> deployments;
        if (payload.getLimit() == null || payload.getStart() == null) {
            deployments = repositoryService.createDeploymentQuery().processDefinitionKey(payload.getProcessDefinitionKey()).list();
        } else {
            deployments = repositoryService.createDeploymentQuery().processDefinitionKey(payload.getProcessDefinitionKey()).listPage(payload.getStart(), payload.getLimit());
        }
        List<com.je.bpm.model.process.model.Deployment> convertedDeployments = new ArrayList<>();
        for (Deployment each : deployments) {
            convertedDeployments.add(apiDeploymentConverter.from(each));
        }
        return new ProcessDeploymentListResult(payload, convertedDeployments);
    }

}
