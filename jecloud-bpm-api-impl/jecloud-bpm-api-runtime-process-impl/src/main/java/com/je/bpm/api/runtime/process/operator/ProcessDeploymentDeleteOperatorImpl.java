/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.operator.desc.ProcessDeploymentDeleteParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessDeploymentDeleteOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.model.process.payloads.ProcessDeploymentDeletePayload;
import com.je.bpm.model.shared.EmptyResult;
import com.je.bpm.runtime.process.operator.ProcessDeploymentDeleteOperator;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import java.util.Map;

/**
 * 流程发起操作
 */
public class ProcessDeploymentDeleteOperatorImpl extends AbstractOperator<ProcessDeploymentDeletePayload, EmptyResult> implements ProcessDeploymentDeleteOperator {

    private RepositoryService repositoryService;
    private OperatorPayloadParamsValidator<ProcessDeploymentDeletePayload> validator;

    public ProcessDeploymentDeleteOperatorImpl(RepositoryService repositoryService) {
        this.validator = new ProcessDeploymentDeleteOperatorValidator();
        this.repositoryService = repositoryService;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_DEPLOYMENT_DELETE_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_DEPLOYMENT_DELETE_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessDeploymentDeleteParamDesc();
    }

    @Override
    public EmptyResult execute(ProcessDeploymentDeletePayload payload) {
        repositoryService.deleteDeployment(payload.getDeploymentId(),payload.isCascade());
        EmptyResult emptyResult = new EmptyResult(payload);
        return emptyResult;
    }

    @Override
    public ProcessDeploymentDeletePayload paramsParse(Map<String, Object> params) {
        ProcessDeploymentDeletePayload payload = new ProcessDeploymentDeletePayload(
                formatString(params.get("deploymentId")),
                formatBoolean(params.get("cascade"))
        );
        return payload;
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessDeploymentDeletePayload> getValidator() {
        return validator;
    }

}
