/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.impl;

import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.impl.bpmn.behavior.MappingExecutionContext;
import com.je.bpm.engine.impl.bpmn.behavior.VariablesCalculator;
import com.je.bpm.spring.process.ProcessExtensionService;
import com.je.bpm.spring.process.model.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Collections.emptyMap;

/**
 * 扩展变量计算器
 */
public class ExtensionsVariablesMappingProvider implements VariablesCalculator {

    private ProcessExtensionService processExtensionService;

    private ExpressionResolver expressionResolver;

    public ExtensionsVariablesMappingProvider(ProcessExtensionService processExtensionService, ExpressionResolver expressionResolver) {
        this.processExtensionService = processExtensionService;
        this.expressionResolver = expressionResolver;
    }

    /**
     * 计算映射值
     * @param inputMapping 输入mapping
     * @param execution 当前执行流
     * @param extensions 扩展属性
     * @return
     */
    protected Optional<Object> calculateMappedValue(Mapping inputMapping, DelegateExecution execution, Extension extensions) {
        if (inputMapping != null) {
            if (Mapping.SourceMappingType.VALUE.equals(inputMapping.getType())) {
                return Optional.of(inputMapping.getValue());
            }
            if (Mapping.SourceMappingType.VARIABLE.equals(inputMapping.getType())) {
                String name = inputMapping.getValue().toString();
                VariableDefinition processVariableDefinition = extensions.getPropertyByName(name);
                if (processVariableDefinition != null) {
                    return Optional.ofNullable(execution.getVariable(processVariableDefinition.getName()));
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public Map<String, Object> calculateInputVariables(DelegateExecution execution) {
        Extension extensions = processExtensionService.getExtensionsForId(execution.getProcessDefinitionId());
        Map<String, Object> constants = calculateConstants(execution, extensions);
        //判断当前task是否包含mapping
        if (!extensions.hasMapping(execution.getCurrentActivityId())) {
            return constants;
        }

        //包含mapping，需要计算变量
        Map<String, Object> inboundVariables;
        //判断是否需要计算输入变量
        if (extensions.shouldMapAllInputs(execution.getCurrentActivityId())) {
            inboundVariables = execution.getVariables();
        } else {
            inboundVariables = calculateInputVariables(execution, extensions);
        }
        inboundVariables = expressionResolver.resolveExpressionsMap(new VariableScopeExpressionEvaluator(execution), inboundVariables);
        inboundVariables.putAll(constants);
        return inboundVariables;
    }

    /**
     * 获取常量
     * @param execution 当前执行流
     * @param extensions 扩展属性
     * @return
     */
    private Map<String, Object> calculateConstants(DelegateExecution execution, Extension extensions) {
        Map<String, Object> constants = new HashMap<>();
        ProcessConstantsMapping processConstantsMapping = extensions.getConstantForFlowElement(execution.getCurrentActivityId());
        for (Map.Entry<String, ConstantDefinition> mapping : processConstantsMapping.entrySet()) {
            constants.put(mapping.getKey(), mapping.getValue().getValue());
        }
        return constants;
    }

    /**
     * 获取当前任务输入变量
     * @param execution
     * @param extensions
     * @return
     */
    private Map<String, Object> calculateInputVariables(DelegateExecution execution, Extension extensions) {
        Map<String, Object> inboundVariables = new HashMap<>();
        ProcessVariablesMapping processVariablesMapping = extensions.getMappingForFlowElement(execution.getCurrentActivityId());
        Map<String, Mapping> inputMappings = processVariablesMapping.getInputs();
        for (Map.Entry<String, Mapping> mapping : inputMappings.entrySet()) {
            Optional<Object> mappedValue = calculateMappedValue(mapping.getValue(), execution, extensions);
            mappedValue.ifPresent(value -> inboundVariables.put(mapping.getKey(), value));
        }
        return inboundVariables;
    }

    /**
     * 计算输出map值
     * @param mapping
     * @param currentContextVariables
     * @return
     */
    private Optional<Object> calculateOutPutMappedValue(Mapping mapping, Map<String, Object> currentContextVariables) {
        if (mapping != null) {
            if (Mapping.SourceMappingType.VALUE.equals(mapping.getType())) {
                return Optional.of(mapping.getValue());
            } else {
                if (Mapping.SourceMappingType.VARIABLE.equals(mapping.getType())) {
                    String name = mapping.getValue().toString();
                    return currentContextVariables != null ? Optional.ofNullable(currentContextVariables.get(name)) : Optional.empty();
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public Map<String, Object> calculateOutPutVariables(MappingExecutionContext mappingExecutionContext, Map<String, Object> availableVariables) {
        Extension extensions = processExtensionService.getExtensionsForId(mappingExecutionContext.getProcessDefinitionId());
        if (!extensions.hasMapping(mappingExecutionContext.getActivityId())) {
            return emptyMap();
        }
        if (extensions.shouldMapAllOutputs(mappingExecutionContext.getActivityId())) {
            return (availableVariables != null ? new HashMap<>(availableVariables) : emptyMap());
        }
        if (availableVariables != null && !availableVariables.isEmpty()) {
            if (expressionResolver.containsExpression(availableVariables)) {
                throw new ActivitiIllegalArgumentException("Expressions are not allowed as variable values in the output mapping");
            }
            return calculateOutPutVariables(mappingExecutionContext, extensions, availableVariables);
        } else {
            return emptyMap();
        }
    }

    private Map<String, Object> calculateOutPutVariables(MappingExecutionContext mappingExecutionContext, Extension extensions, Map<String, Object> availableVariables) {
        Map<String, Object> outboundVariables = new HashMap<>();
        ProcessVariablesMapping processVariablesMapping = extensions.getMappingForFlowElement(mappingExecutionContext.getActivityId());
        Map<String, Mapping> outputMappings = processVariablesMapping.getOutputs();
        for (Map.Entry<String, Mapping> mapping : outputMappings.entrySet()) {
            String name = mapping.getKey();
            VariableDefinition processVariableDefinition = extensions.getPropertyByName(name);
            if (processVariableDefinition != null) {
                calculateOutPutMappedValue(mapping.getValue(), availableVariables) .ifPresent(value -> outboundVariables.put(name, value));
            }
        }
        return expressionResolver.resolveExpressionsMap(new SimpleMapExpressionEvaluator(availableVariables), outboundVariables);
    }
}
