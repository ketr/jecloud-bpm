/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.google.common.base.Strings;
import com.je.bpm.api.runtime.process.operator.desc.ProcessGetNextElementParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessGetNextElementAssigneePayloadValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.model.process.model.ProcessNextNodeAssigneeInfo;
import com.je.bpm.model.process.model.impl.ProcessNextNodeAssigneeInfoImpl;
import com.je.bpm.model.process.results.ProcessNextElementAssigneeResult;
import com.je.bpm.runtime.process.operator.ProcessGetNextElementAssigneeOperator;
import com.je.bpm.runtime.process.payloads.ProcessGetNextElementAssigneePayload;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.util.Arrays;
import java.util.Map;

/**
 * 获取下提交到的下个节点人员信息操作
 */
public class ProcessGetNextElementAssigneeOperatorImpl extends AbstractOperator<ProcessGetNextElementAssigneePayload,
        ProcessNextElementAssigneeResult> implements ProcessGetNextElementAssigneeOperator {

    public static final String[] CURRENT = new String[]{OperatorEnum.TASK_DELEGATE_OPERATOR.getId(),
            OperatorEnum.TASK_TRANSFER_OPERATOR.getId(), OperatorEnum.TASK_PERSONNEL_ADJUSTMENTS_OPERATOR.getId(),
            OperatorEnum.TASK_CHANGE_ASSIGNEE_OPERATOR.getId(), OperatorEnum.TASK_COUNTERSIGN_OPERATOR.getId()};

    private OperatorPayloadParamsValidator<ProcessGetNextElementAssigneePayload> validator;
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;

    public ProcessGetNextElementAssigneeOperatorImpl(RepositoryService repositoryService, RuntimeService runtimeService) {
        this.validator = new ProcessGetNextElementAssigneePayloadValidator();
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_GET_NEXT_ELEMENT_ASSIGNEE_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_GET_NEXT_ELEMENT_ASSIGNEE_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessGetNextElementParamDesc();
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessGetNextElementAssigneePayload> getValidator() {
        return validator;
    }

    @Override
    public ProcessGetNextElementAssigneePayload paramsParse(Map<String, Object> params) {
        String taskId = (String) params.get("taskId");
        String pdid = (String) params.get("pdid");
        String prod = (String) params.get(AbstractOperator.PROD);
        String target = (String) params.get("target");
        String operationId = (String) params.get("operationId");
        String beanId = String.valueOf(params.get("beanId"));
        ProcessGetNextElementAssigneePayload processGetNextElementAssigneePayload = new ProcessGetNextElementAssigneePayload();
        processGetNextElementAssigneePayload.setTaskId(taskId);
        processGetNextElementAssigneePayload.setPdid(pdid);
        processGetNextElementAssigneePayload.setProd(prod);
        processGetNextElementAssigneePayload.setTarget(target);
        processGetNextElementAssigneePayload.setOperationId(operationId);
        processGetNextElementAssigneePayload.setBusinessKey(beanId);
        return processGetNextElementAssigneePayload;
    }

    @Override
    public ProcessNextElementAssigneeResult execute(ProcessGetNextElementAssigneePayload processGetNextElementAssigneePayload) {
        String taskId = processGetNextElementAssigneePayload.getTaskId();
        ProcessNextNodeAssigneeInfo processNextNodeInfo = new ProcessNextNodeAssigneeInfoImpl();
        //获取可以提交的节点信息人信息
        Object users;
        String operationId = processGetNextElementAssigneePayload.getOperationId();
        Boolean isSubmitTheCurrentNode = runtimeService.isSubmitTheCurrentNode(taskId, processGetNextElementAssigneePayload.getTarget());
        //操作类型不为空，并且是委托，转办，更换负责人，人员调整 或者是提交到当前节点
        if (processGetNextElementAssigneePayload.getAdjust().equals("0") && !Strings.isNullOrEmpty(operationId)
                && (Arrays.asList(CURRENT).contains(operationId) || isSubmitTheCurrentNode)) {
            users = runtimeService.getCurrentElementAssignee(processGetNextElementAssigneePayload.getPdid(),
                    taskId, processGetNextElementAssigneePayload.getTarget(), processGetNextElementAssigneePayload.getBean(),
                    processGetNextElementAssigneePayload.getProd(), operationId, processGetNextElementAssigneePayload.getBusinessKey());
        } else {
            users = runtimeService.getSubmitElementAssignee(processGetNextElementAssigneePayload.getPdid(),
                    taskId, processGetNextElementAssigneePayload.getTarget(), processGetNextElementAssigneePayload.getBean(),
                    processGetNextElementAssigneePayload.getProd(), processGetNextElementAssigneePayload.getAdjust(), operationId, processGetNextElementAssigneePayload.getBusinessKey());
        }

        processNextNodeInfo.setUser(users);
        return new ProcessNextElementAssigneeResult(processGetNextElementAssigneePayload, processNextNodeInfo);
    }

    public static void main(String[] args) {
        String operationId = "taskTransferOperation";
        if (Strings.isNullOrEmpty(operationId) || !Arrays.asList(CURRENT).contains(operationId) && !true) {
            System.out.println(1);
        } else {
            System.out.println(2);
        }
    }
}

