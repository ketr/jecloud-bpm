/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.je.bpm.api.runtime.task.operator.desc.TaskChangeAssigneeParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskAdjustingPersonnelValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.TaskService;
import com.je.bpm.model.task.payloads.TaskAdjustingPersonnelPayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskAdjustingPersonnelOperator;

import java.util.Map;

/**
 * 更换处理人
 */
public class TaskAdjustingPersonnelOperatorImpl extends AbstractOperator<TaskAdjustingPersonnelPayload, TaskResult>
        implements TaskAdjustingPersonnelOperator {

    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskAdjustingPersonnelPayload> validator;

    public TaskAdjustingPersonnelOperatorImpl(TaskService taskService) {
        this.validator = new TaskAdjustingPersonnelValidator();
        this.taskService = taskService;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_ADJUSTING_PERSONNEL_OPERATION.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_ADJUSTING_PERSONNEL_OPERATION.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskChangeAssigneeParamDesc();
    }

    @Override
    public TaskResult execute(TaskAdjustingPersonnelPayload taskAdjustingPersonnelPayload) {
        String userId = getAssigneeUser(taskAdjustingPersonnelPayload.getAssignee());
        taskService.taskAdjustingPersonnel(userId, taskAdjustingPersonnelPayload.getProd(), taskAdjustingPersonnelPayload.getTaskId());
        TaskResult result = new TaskResult(taskAdjustingPersonnelPayload, null);
        return result;
    }

    @Override
    public TaskAdjustingPersonnelPayload paramsParse(Map<String, Object> params) {
        TaskAdjustingPersonnelPayload taskAdjustingPersonnelPayload = new TaskAdjustingPersonnelPayload(
                formatString(params.get("piid")),
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("taskId")),
                formatString(params.get("beanId")),
                formatString(params.get("assignee"))
        );
        taskAdjustingPersonnelPayload.setFiles(formatString(params.get("files")));
        return taskAdjustingPersonnelPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskAdjustingPersonnelPayload> getValidator() {
        return validator;
    }

}
