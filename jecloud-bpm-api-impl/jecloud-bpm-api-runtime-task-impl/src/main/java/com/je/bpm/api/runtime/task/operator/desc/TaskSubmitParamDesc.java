/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator.desc;

import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamType;

/**
 * 任务取回参数描述
 */
public class TaskSubmitParamDesc extends OperationParamDesc {

    private ParamDesc piidParam;
    private ParamDesc prodParam;
    private ParamDesc beanParam;
    private ParamDesc taskParam;
    private ParamDesc assigneeParam;
    private ParamDesc targetParam;

    public TaskSubmitParamDesc() {
        piidParam = new ParamDesc();
        piidParam.setName("piid");
        piidParam.setDescription("流程实例ID。");
        piidParam.setRequired(true);
        piidParam.setType(ParamType.String);

        taskParam = new ParamDesc();
        taskParam.setName("taskId");
        taskParam.setDescription("流程任务ID。");
        taskParam.setRequired(true);
        taskParam.setType(ParamType.String);

        prodParam = new ParamDesc();
        prodParam.setName(AbstractOperator.PROD);
        prodParam.setDescription("产品编码。");
        prodParam.setRequired(true);
        prodParam.setType(ParamType.String);

        beanParam = new ParamDesc();
        beanParam.setName("beanId");
        beanParam.setDescription("数据ID。");
        beanParam.setRequired(true);
        beanParam.setType(ParamType.String);

        assigneeParam = new ParamDesc();
        assigneeParam.setName("assignee");
        assigneeParam.setDescription("下一个节点指派人信息，如果是多个人使用逗号分割。如果不需要指定，则可以为空。");
        assigneeParam.setRequired(true);
        assigneeParam.setType(ParamType.String);

        targetParam = new ParamDesc();
        targetParam.setName("target");
        targetParam.setDescription("目标节点");
        targetParam.setRequired(false);
        targetParam.setType(ParamType.String);

        paramKeys.add(piidParam.getName());
        paramKeys.add(taskParam.getName());
        paramKeys.add(prodParam.getName());
        paramKeys.add(beanParam.getName());
        paramKeys.add(assigneeParam.getName());
        paramKeys.add(targetParam.getName());
    }

    public ParamDesc getPiidParam() {
        return piidParam;
    }

    public void setPiidParam(ParamDesc piidParam) {
        this.piidParam = piidParam;
    }

    public ParamDesc getProdParam() {
        return prodParam;
    }

    public void setProdParam(ParamDesc prodParam) {
        this.prodParam = prodParam;
    }

    public ParamDesc getBeanParam() {
        return beanParam;
    }

    public void setBeanParam(ParamDesc beanParam) {
        this.beanParam = beanParam;
    }

    public ParamDesc getTaskParam() {
        return taskParam;
    }

    public void setTaskParam(ParamDesc taskParam) {
        this.taskParam = taskParam;
    }

    public ParamDesc getAssigneeParam() {
        return assigneeParam;
    }

    public void setAssigneeParam(ParamDesc assigneeParam) {
        this.assigneeParam = assigneeParam;
    }

    public ParamDesc getTargetParam() {
        return targetParam;
    }

    public void setTargetParam(ParamDesc targetParam) {
        this.targetParam = targetParam;
    }

    @Override
    public String toString() {
        return "TaskSubmitParamDesc{" +
                "piidParam=" + piidParam +
                ", prodParam=" + prodParam +
                ", beanParam=" + beanParam +
                ", taskParam=" + taskParam +
                ", assigneeParam=" + assigneeParam +
                ", targetParam=" + targetParam +
                ", paramKeys=" + paramKeys +
                '}';
    }

}
