/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskUrgeParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskUrgeOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.TaskService;
import com.je.bpm.model.task.payloads.TaskUrgePayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskUrgeOperator;

import java.util.Map;

/**
 * 任务催办操作
 */
public class TaskUrgeOperatorImpl extends AbstractOperator<TaskUrgePayload, TaskResult> implements TaskUrgeOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskUrgePayload> validator;

    public TaskUrgeOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService) {
        this.validator = new TaskUrgeOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_URGE_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_URGE_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskUrgeParamDesc();
    }

    @Override
    public TaskResult execute(TaskUrgePayload taskUrgePayload) {
        taskService.urgeTask(taskUrgePayload.getReminderMethod(), taskUrgePayload.getPersonBeingUrged(), taskUrgePayload.getUrgentContent(),
                taskUrgePayload.getCcUser(), taskUrgePayload.getCcContent(), taskUrgePayload.getTaskId(),
                taskUrgePayload.getCurrentNodeId(), taskUrgePayload.getFuncCode());
        TaskResult result = new TaskResult(taskUrgePayload, null);
        return result;
    }

    @Override
    public TaskUrgePayload paramsParse(Map<String, Object> params) {
        TaskUrgePayload taskUrgePayload = TaskUrgePayload.builder(
                formatString(params.get("reminderMethod")),
                formatString(params.get("personBeingUrgedIds")),
                formatString(params.get("personBeingUrgedNames")),
                formatString(params.get("urgentContent")),
                formatString(params.get("ccUserIds")),
                formatString(params.get("ccUserNames")),
                formatString(params.get("ccContent")),
                formatString(params.get("taskId")),
                formatString(params.get("currentNodeId")),
                formatString(params.get("funcCode"))
        );
        taskUrgePayload.setFiles(formatString(params.get("files")));
        return taskUrgePayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskUrgePayload> getValidator() {
        return validator;
    }
}
