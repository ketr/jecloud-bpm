/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.alibaba.fastjson2.JSONObject;
import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskSubmitParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.CountersignOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.core.model.task.KaiteMultiUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.task.Task;
import com.je.bpm.engine.task.TaskQuery;
import com.je.bpm.engine.upcoming.ActivitiUpcomingRun;
import com.je.bpm.model.task.payloads.TaskCountersignPayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskCountersignOperator;

import java.util.List;
import java.util.Map;

/**
 * 任务加签操作
 */
public class TaskCountersignOperatorImpl extends AbstractOperator<TaskCountersignPayload, TaskResult> implements TaskCountersignOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskCountersignPayload> validator;
    private ActivitiUpcomingRun activitiUpcomingRun;
    private RepositoryService repositoryService;

    public TaskCountersignOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService, ActivitiUpcomingRun activitiUpcomingRun, RepositoryService repositoryService) {
        this.validator = new CountersignOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
        this.activitiUpcomingRun = activitiUpcomingRun;
        this.repositoryService = repositoryService;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_COUNTERSIGN_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_COUNTERSIGN_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskSubmitParamDesc();
    }

    @Override
    public TaskCountersignPayload paramsParse(Map<String, Object> params) {
        TaskCountersignPayload payload = TaskCountersignPayload.build()
                .setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN))
                .setBeanId(formatString(params.get("beanId")))
                .setComment(formatString(params.get("comment")))
                .setProd(formatString(params.get(AbstractOperator.PROD)))
                .setTaskId(formatString(params.get("taskId")))
                .setAssignee(formatString(params.get("assignee")))
                .setTarget(formatString(params.get("target")))
                .setNodeId(formatString(params.get("nodeId")))
                .setSignBackId(formatString(params.get("signBackId")));
        payload.setFiles(formatString(params.get("files")));
        return payload;
    }


    @Override
    public TaskResult execute(TaskCountersignPayload payload) {
//        taskService.countersign(payload.getTaskId(),
//                payload.getProd(),
//                payload.getBean(),
//                payload.getComment(),
//                payload.getBeanId(),
//                payload.getAssignee(), payload.getSignBackId());
//        return null;
        //检查流程是否已经提交
        TaskQuery taskQuery = taskService.createTaskQuery().taskId(payload.getTaskId());
        List<Task> list = taskQuery.list();
        if (list.size() == 0) {
            throw new ActivitiException(String.format("Can't find task definiton from the process"));
        }
        Task task = list.get(0);
        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId(), task.getProcessInstanceId(), task.getBusinessKey());
        FlowElement taskFlowElement = bpmnModel.getFlowElement(task.getTaskDefinitionKey());
        if (taskFlowElement == null) {
            throw new ActivitiException(String.format("Can't find task %s definiton from the process.", task.getName()));
        }
        if (taskFlowElement instanceof KaiteMultiUserTask) {
            String taskId = payload.getTaskId();
            String assigneeJson = payload.getAssignee();
            String assignee = JSONObject.parseObject(assigneeJson).getString("assignee");
            taskService.multipleCountersign(assignee, taskId, payload.getComment(), payload.getFiles());
        } else {
            Map<String, Object> histVar = taskService.getVariables(task.getId());
            //节点提交完成操作
            String target = payload.getTarget();
            if (taskFlowElement instanceof KaiteBaseUserTask) {
                KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) taskFlowElement;
                target = kaiteBaseUserTask.getIncomingFlows().get(0).getId();
            }
            taskService.complete(payload.getTaskId(), payload.getAssignee(), target,
                    payload.getProd(), payload.getBean(), payload.getComment(), payload.getSequentials(), payload.getIsJump(),
                    true, false, histVar, payload.getFiles());
        }
        return null;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskCountersignPayload> getValidator() {
        return validator;
    }

}
