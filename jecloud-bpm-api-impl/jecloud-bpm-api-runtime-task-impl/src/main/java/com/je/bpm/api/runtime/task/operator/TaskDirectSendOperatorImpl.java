/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskDirectSendParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskDirectSendOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.upcoming.ActivitiUpcomingRun;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import com.je.bpm.model.task.payloads.TaskDirectSendPayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskDirectSendOperator;

import java.util.Map;

/**
 * 任务直送操作
 */
public class TaskDirectSendOperatorImpl extends AbstractOperator<TaskDirectSendPayload, TaskResult> implements TaskDirectSendOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskDirectSendPayload> validator;
    private ActivitiUpcomingRun activitiUpcomingRun;

    public TaskDirectSendOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService,
                                      ActivitiUpcomingRun activitiUpcomingRun) {
        this.validator = new TaskDirectSendOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
        this.activitiUpcomingRun = activitiUpcomingRun;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_DIRECT_SEND_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_DIRECT_SEND_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskDirectSendParamDesc();
    }

    @Override
    public TaskResult execute(TaskDirectSendPayload taskDirectSendPayload) {
        UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(taskDirectSendPayload.getBean(), taskDirectSendPayload.getComment(),
                SubmitTypeEnum.DIRECTSEND, taskDirectSendPayload.getBeanId(), taskDirectSendPayload.getTaskId(), null);
        activitiUpcomingRun.completeUpcoming(upcomingInfo);
        taskService.directComplete(taskDirectSendPayload.getTaskId(), taskDirectSendPayload.getComment(),
                taskDirectSendPayload.getBean(), taskDirectSendPayload.getProd(),taskDirectSendPayload.getFiles());
        TaskResult result = new TaskResult(taskDirectSendPayload, null);
        return result;
    }

    @Override
    public TaskDirectSendPayload paramsParse(Map<String, Object> params) {
        TaskDirectSendPayload taskDirectSendPayload = new TaskDirectSendPayload(
                formatString(params.get("piid")),
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("taskId")),
                formatString(params.get("beanId"))
        );
        taskDirectSendPayload.setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN));
        taskDirectSendPayload.setComment(formatString(params.get("comment")));
        taskDirectSendPayload.setFiles(formatString(params.get("files")));
        return taskDirectSendPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskDirectSendPayload> getValidator() {
        return validator;
    }

}
