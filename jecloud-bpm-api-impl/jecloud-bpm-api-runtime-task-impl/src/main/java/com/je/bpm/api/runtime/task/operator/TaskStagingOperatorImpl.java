/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.je.bpm.api.runtime.task.operator.desc.TaskClaimParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskStagingOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.TaskService;
import com.je.bpm.model.task.payloads.TaskStagingPayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskStagingOperator;

import java.util.Map;

/**
 * 暂存操作
 */
public class TaskStagingOperatorImpl extends AbstractOperator<TaskStagingPayload, TaskResult> implements TaskStagingOperator {

    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskStagingPayload> validator;

    public TaskStagingOperatorImpl(TaskService taskService) {
        this.validator = new TaskStagingOperatorValidator();
        this.taskService = taskService;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_STAGING_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_STAGING_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskClaimParamDesc();
    }

    @Override
    public TaskResult execute(TaskStagingPayload taskStagingPayload) {
        taskService.staging(taskStagingPayload.getTaskId(), taskStagingPayload.getComment(), taskStagingPayload.getFiles());
        return new TaskResult();
    }

    @Override
    public TaskStagingPayload paramsParse(Map<String, Object> params) {
        String taskId = formatString(params.get("taskId"));
        String comment = formatString(params.get("comment"));
        TaskStagingPayload taskClaimPayload = TaskStagingPayload.builder(taskId, comment);
        taskClaimPayload.setFiles(formatString(params.get("files")));
        return taskClaimPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskStagingPayload> getValidator() {
        return validator;
    }

}
