/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.shared.model.impl;

import com.je.bpm.model.shared.model.WorkFlowConfig;

public class WorkFlowConfigImpl implements WorkFlowConfig {

    /**
     * 节点id
     */
    private String currentNodeId;
    /**
     * 节点名称
     */
    private String currentNodeName;
    /**
     * target
     */
    private String currentTarget;
    /**
     * 简易审批
     */
    private String simpleApproval = "0";
    /**
     * 简易审批意见
     */
    private String enableSimpleComments = "0";
    /**
     * 自动全选
     */
    private String selectAll = "0";
    /**
     * 异步树
     */
    private String asynTree = "0";
    /**
     * 流程状态
     */
    private WorkFlowAudFlag audFlag;
    /**
     * 流程状态
     */
    private String audFlagName;
    /**
     * 是否同步
     */
    private String sequential;
    /**
     * 是否展示同步
     */
    private String showSequentialConfig;
    /**
     * 节点类型
     */
    private String type;
    /**
     * 人员多选
     */
    private String multiple;
    /**
     * 列表同步
     */
    private String listSynchronization;
    /**
     * 是否人员可调整
     */
    private String personnelAdjustments;
    /**
     * 是否开启紧急状态
     */
    private String exigency;
    /**
     * 紧急状态值
     */
    private String exigencyValue;

    public String getCurrentNodeId() {
        return currentNodeId;
    }

    public void setCurrentNodeId(String currentNodeId) {
        this.currentNodeId = currentNodeId;
    }

    public String getCurrentNodeName() {
        return currentNodeName;
    }

    public void setCurrentNodeName(String currentNodeName) {
        this.currentNodeName = currentNodeName;
    }

    public String getCurrentTarget() {
        return currentTarget;
    }

    public void setCurrentTarget(String currentTarget) {
        this.currentTarget = currentTarget;
    }

    @Override
    public WorkFlowAudFlag getAudFlag() {
        return this.audFlag;
    }

    @Override
    public void setAudFlag(WorkFlowAudFlag audFlag) {
        this.audFlag = audFlag;
        this.audFlagName = audFlag.getName();
    }

    public String getAudFlagName() {
        return this.audFlag.getName();
    }

    @Override
    public String getSimpleApproval() {
        return simpleApproval;
    }

    @Override
    public void setSimpleApproval(String simpleApproval) {
        this.simpleApproval = simpleApproval;
    }

    @Override
    public String getSelectAll() {
        return selectAll;
    }

    @Override
    public void setSelectAll(String selectAll) {
        this.selectAll = selectAll;
    }

    @Override
    public String getAsynTree() {
        return asynTree;
    }

    @Override
    public void setAsynTree(String asynTree) {
        this.asynTree = asynTree;
    }

    @Override
    public String getAppVersion() {
        return null;
    }

    public String getSequential() {
        return sequential;
    }

    public void setSequential(String sequential) {
        this.sequential = sequential;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMultiple() {
        return multiple;
    }

    public void setMultiple(String multiple) {
        this.multiple = multiple;
    }

    public String getShowSequentialConfig() {
        return showSequentialConfig;
    }

    public void setShowSequentialConfig(String showSequentialConfig) {
        this.showSequentialConfig = showSequentialConfig;
    }

    public String getListSynchronization() {
        return listSynchronization;
    }

    public void setListSynchronization(String listSynchronization) {
        this.listSynchronization = listSynchronization;
    }

    public String getPersonnelAdjustments() {
        return personnelAdjustments;
    }

    public void setPersonnelAdjustments(String personnelAdjustments) {
        this.personnelAdjustments = personnelAdjustments;
    }

    public String getExigency() {
        return exigency;
    }

    public void setExigency(String exigency) {
        this.exigency = exigency;
    }

    public String getEnableSimpleComments() {
        return enableSimpleComments;
    }

    public void setEnableSimpleComments(String enableSimpleComments) {
        this.enableSimpleComments = enableSimpleComments;
    }

    public String getExigencyValue() {
        return exigencyValue;
    }

    public void setExigencyValue(String exigencyValue) {
        this.exigencyValue = exigencyValue;
    }
}
