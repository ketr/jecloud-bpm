/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.shared.model.impl;

import com.je.bpm.model.shared.model.VariableInstance;

/**
 * 变量实例实现
 *
 * @param <T>
 * @author opensource
 * @date 2021-10-21
 */
public class VariableInstanceImpl<T> implements VariableInstance {

    /**
     * 变量名称
     */
    private String name;

    /**
     * 变量类型
     */
    private String type;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 变量值
     */
    private T value;

    /**
     * 任务ID
     */
    private String taskId;

    public VariableInstanceImpl() {
    }

    public VariableInstanceImpl(String name, String type, T value, String processInstanceId, String taskId) {
        this.name = name;
        this.type = type;
        this.processInstanceId = processInstanceId;
        this.value = value;
        this.taskId = taskId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    @Override
    public boolean isTaskVariable() {
        return taskId != null;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "VariableInstanceImpl{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", processInstanceId='" + processInstanceId + '\'' +
                ", taskId='" + taskId + '\'' +
                ", value='" + value.toString() + '\'' +
                '}';
    }

}
