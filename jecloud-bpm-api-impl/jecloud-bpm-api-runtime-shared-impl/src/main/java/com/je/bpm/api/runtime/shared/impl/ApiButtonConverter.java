/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.shared.impl;

import com.je.bpm.api.runtime.shared.model.impl.ListConverter;
import com.je.bpm.api.runtime.shared.model.impl.ModelConverter;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.ProcessButton;
import com.je.bpm.core.model.button.TaskButton;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * apiProcessButtonConverter link ApiProcessButtonConverter
 * apiTaskButtonConverter link ApiTaskButtonConverter
 */

public class ApiButtonConverter extends ListConverter<List<Button>, Map<String, Object>, List<com.je.bpm.model.shared.model.Button>>
        implements ModelConverter<List<Button>, Map<String, Object>, List<com.je.bpm.model.shared.model.Button>> {

    private ListConverter apiProcessButtonConverter;
    private ListConverter apiTaskButtonConverter;

    public ApiButtonConverter(ListConverter apiProcessButtonConverter, ListConverter apiTaskButtonConverter) {
        this.apiProcessButtonConverter = apiProcessButtonConverter;
        this.apiTaskButtonConverter = apiTaskButtonConverter;
    }

    @Override
    public List<com.je.bpm.model.shared.model.Button> from(List<Button> source, Map<String, Object> params) {
        List<com.je.bpm.model.shared.model.Button> sharedButtons = new ArrayList<>();
        for (Button button : source) {
            if (button instanceof ProcessButton) {
                sharedButtons.add((com.je.bpm.model.shared.model.Button) apiProcessButtonConverter.from(button,params));
            }
            if (button instanceof TaskButton) {
                sharedButtons.add((com.je.bpm.model.shared.model.Button) apiTaskButtonConverter.from(button,params));
            }
        }
        //流程级别按钮、任务级别按钮 按规则排序
        sharedButtons.sort(Comparator.comparing(com.je.bpm.model.shared.model.Button::setSortOrder));
        return sharedButtons;
    }


}
