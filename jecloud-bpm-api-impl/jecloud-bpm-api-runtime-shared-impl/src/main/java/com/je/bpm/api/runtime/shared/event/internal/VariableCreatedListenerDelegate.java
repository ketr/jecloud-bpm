/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.shared.event.internal;

import com.je.bpm.engine.delegate.event.ActivitiEvent;
import com.je.bpm.engine.delegate.event.ActivitiEventListener;
import com.je.bpm.engine.delegate.event.ActivitiVariableEvent;
import com.je.bpm.api.runtime.shared.event.impl.ToVariableCreatedConverter;
import com.je.bpm.model.shared.event.VariableCreatedEvent;
import com.je.bpm.runtime.shared.event.listener.VariableEventListener;
import java.util.List;

/**
 * 变量创建事件监听器代理者
 */
public class VariableCreatedListenerDelegate implements ActivitiEventListener {

    private final List<VariableEventListener<VariableCreatedEvent>> listeners;
    private final ToVariableCreatedConverter converter;
    private final VariableEventFilter variableEventFilter;

    public VariableCreatedListenerDelegate(List<VariableEventListener<VariableCreatedEvent>> listeners,ToVariableCreatedConverter converter,VariableEventFilter variableEventFilter) {
        this.listeners = listeners;
        this.converter = converter;
        this.variableEventFilter = variableEventFilter;
    }

    @Override
    public void onEvent(ActivitiEvent event) {
        if (event instanceof ActivitiVariableEvent) {
            ActivitiVariableEvent internalEvent = (ActivitiVariableEvent) event;
            if (variableEventFilter.shouldEmmitEvent(internalEvent)) {
                converter.from(internalEvent)
                        .ifPresent(convertedEvent -> {
                            if (listeners != null) {
                                for (VariableEventListener<VariableCreatedEvent> listener : listeners) {
                                    listener.onEvent(convertedEvent);
                                }
                            }
                        });
            }
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }

}
