/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.event.impl;

import com.je.bpm.model.process.events.MessageDefinitionEvent;
import com.je.bpm.model.process.events.StartMessageDeployedEvent;
import com.je.bpm.model.process.model.ProcessDefinition;
import com.je.bpm.model.process.model.StartMessageDeploymentDefinition;
import com.je.bpm.model.shared.event.impl.AbstractRuntimeEventImpl;

public class StartMessageDeployedEventImpl extends AbstractRuntimeEventImpl<StartMessageDeploymentDefinition, MessageDefinitionEvent.MessageDefinitionEvents> implements StartMessageDeployedEvent {

    private StartMessageDeployedEventImpl(Builder builder) {
        this(builder.entity);
    }

    StartMessageDeployedEventImpl() {
    }

    public StartMessageDeployedEventImpl(StartMessageDeploymentDefinition startMessageEventSubscription) {
        super(startMessageEventSubscription);
        ProcessDefinition processDefinition = startMessageEventSubscription.getProcessDefinition();
        setProcessDefinitionId(processDefinition.getId());
        setProcessDefinitionKey(processDefinition.getKey());
        setProcessDefinitionVersion(processDefinition.getVersion());
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Creates a builder to build {@link StartMessageDeployedEventImpl} and initialize it with the given object.
     * @param startMessageDeployedEventImpl to initialize the builder with
     * @return created builder
     */
    public static Builder builderFrom(StartMessageDeployedEventImpl startMessageDeployedEventImpl) {
        return new Builder(startMessageDeployedEventImpl);
    }

    /**
     * Builder to build {@link StartMessageDeployedEventImpl}.
     */
    public static final class Builder {

        private StartMessageDeploymentDefinition entity;

        public Builder() {
        }

        private Builder(StartMessageDeployedEventImpl startMessageDeployedEventImpl) {
            this.entity = startMessageDeployedEventImpl.getEntity();
        }

        /**
        * Builder method for entity parameter.
        * @param entity field to set
        * @return builder
        */
        public Builder withEntity(StartMessageDeploymentDefinition entity) {
            this.entity = entity;
            return this;
        }

        /**
        * Builder method of the builder.
        * @return built class
        */
        public StartMessageDeployedEventImpl build() {
            return new StartMessageDeployedEventImpl(this);
        }
    }


}
