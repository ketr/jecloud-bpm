/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.model.impl;

import com.je.bpm.model.process.model.ProcessConfig;
import com.je.bpm.model.shared.model.AbstractConfig;
import java.util.List;

/**
 * 流程基础配置
 */
public class ProcessBasicConfigImpl extends AbstractConfig implements ProcessConfig {

    private String funcCode;
    private String funcName;

    /**
     * 流程定义ID
     */
    private String definitionId;
    /**
     * 流程定义KEY
     */
    private String definitionKey;
    /**
     * 默认生产模式
     */
    private String mode;
    /**
     * 是否可取回
     */
    private boolean canRetrieve = true;
    /**
     * 是否可发起，起效后，流程启动会流转到第二个节点
     */
    private boolean canSponsor = true;
    /**
     * 是否可转办
     */
    private boolean canTransfer = false;
    /**
     * 是否可委托
     */
    private boolean canDelegate = false;
    /**
     * 是否可催办
     */
    private boolean canUrged = true;
    /**
     * 可作废
     */
    private boolean canInvalid = true;
    /**
     * 是否可撤销
     */
    private boolean canCancel = true;
    /**
     * 是否启用预定义
     */
    private boolean predefined = false;
    /**
     * 预定义类型，默认为按步骤设置处理人
     */
    private String predefineType;
    /**
     * 是否任何人可以启动流程
     */
    private boolean canEveryoneStart = false;
    /**
     * 流程提醒配置，默认为推送
     */
    private List<String> remindTypes;
    /**
     * 是否展示流程追踪
     */
    private boolean displayTraceButton = true;

    public ProcessBasicConfigImpl(String type) {
        super(type);
    }

    public ProcessBasicConfigImpl(String type, String definitionId, String definitionKey) {
        super(type);
        this.definitionId = definitionId;
        this.definitionKey = definitionKey;
    }

    @Override
    public String getDefinitionId() {
        return definitionId;
    }

    public void setDefinitionId(String definitionId) {
        this.definitionId = definitionId;
    }

    @Override
    public String getDefinitionKey() {
        return definitionKey;
    }

    public void setDefinitionKey(String definitionKey) {
        this.definitionKey = definitionKey;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public boolean isCanRetrieve() {
        return canRetrieve;
    }

    public void setCanRetrieve(boolean canRetrieve) {
        this.canRetrieve = canRetrieve;
    }

    public boolean isCanSponsor() {
        return canSponsor;
    }

    public void setCanSponsor(boolean canSponsor) {
        this.canSponsor = canSponsor;
    }

    public boolean isCanTransfer() {
        return canTransfer;
    }

    public void setCanTransfer(boolean canTransfer) {
        this.canTransfer = canTransfer;
    }

    public boolean isCanDelegate() {
        return canDelegate;
    }

    public void setCanDelegate(boolean canDelegate) {
        this.canDelegate = canDelegate;
    }

    public boolean isCanUrged() {
        return canUrged;
    }

    public void setCanUrged(boolean canUrged) {
        this.canUrged = canUrged;
    }

    public boolean isCanInvalid() {
        return canInvalid;
    }

    public void setCanInvalid(boolean canInvalid) {
        this.canInvalid = canInvalid;
    }

    public boolean isCanCancel() {
        return canCancel;
    }

    public void setCanCancel(boolean canCancel) {
        this.canCancel = canCancel;
    }

    public boolean isPredefined() {
        return predefined;
    }

    public void setPredefined(boolean predefined) {
        this.predefined = predefined;
    }

    public String getPredefineType() {
        return predefineType;
    }

    public void setPredefineType(String predefineType) {
        this.predefineType = predefineType;
    }

    public boolean isCanEveryoneStart() {
        return canEveryoneStart;
    }

    public void setCanEveryoneStart(boolean canEveryoneStart) {
        this.canEveryoneStart = canEveryoneStart;
    }

    public List<String> getRemindTypes() {
        return remindTypes;
    }

    public void setRemindTypes(List<String> remindTypes) {
        this.remindTypes = remindTypes;
    }

    public boolean isDisplayTraceButton() {
        return displayTraceButton;
    }

    public void setDisplayTraceButton(boolean displayTraceButton) {
        this.displayTraceButton = displayTraceButton;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }
}
