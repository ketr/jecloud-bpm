/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.conf;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.module.SimpleAbstractTypeResolver;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.je.bpm.model.process.ModelOperatorService;
import com.je.bpm.model.process.ModelOperatorServiceImpl;
import com.je.bpm.model.process.converter.*;
import com.je.bpm.model.process.event.impl.StartMessageDeployedEventImpl;
import com.je.bpm.model.process.events.StartMessageDeployedEvent;
import com.je.bpm.model.process.model.*;
import com.je.bpm.model.process.model.impl.*;
import com.je.bpm.model.process.payloads.*;
import com.je.bpm.model.process.results.ProcessInstanceResult;
import com.je.bpm.model.process.serializer.ProcessVariablesMapDeserializer;
import com.je.bpm.model.process.serializer.ProcessVariablesMapSerializer;
import com.je.bpm.runtime.process.operator.ProcessModelDeleteOperator;
import com.je.bpm.runtime.process.operator.ProcessModelDeployOperator;
import com.je.bpm.runtime.process.operator.ProcessModelGetOperator;
import com.je.bpm.runtime.process.operator.ProcessModelSaveOperator;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.convert.ApplicationConversionService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.support.FormattingConversionService;

import java.util.Collections;
import java.util.Set;

@AutoConfigureBefore({JacksonAutoConfiguration.class})
@Configuration
public class ProcessModelAutoConfiguration {

    @Autowired(required = false)
    @ProcessVariableTypeConverter
    @Lazy
    private Set<Converter<?, ?>> converters = Collections.emptySet();

    @Bean
    ModelOperatorService modelOperatorService(ProcessModelSaveOperator processModelSaveOperator, ProcessModelDeployOperator deployOperator,
                                              ProcessModelGetOperator processModelGetOperator, ProcessModelDeleteOperator processModelDeleteOperator) {
        return new ModelOperatorServiceImpl(processModelSaveOperator, deployOperator, processModelGetOperator, processModelDeleteOperator);
    }

    //this bean will be automatically injected inside boot's ObjectMapper
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public Module customizeProcessModelObjectMapper(ObjectProvider<ConversionService> conversionServiceProvider) {
        SimpleModule module = new SimpleModule("mapProcessModelInterfaces", Version.unknownVersion());
        SimpleAbstractTypeResolver resolver = new SimpleAbstractTypeResolver() {
            //this is a workaround for https://github.com/FasterXML/jackson-databind/issues/2019
            //once version 2.9.6 is related we can remove this @override method
            @Override
            public JavaType resolveAbstractType(DeserializationConfig config,
                                                BeanDescription typeDesc) {
                return findTypeMapping(config, typeDesc.getType());
            }
        };

        resolver.addMapping(BPMNActivity.class, BPMNActivityImpl.class);
        resolver.addMapping(ProcessInstance.class, ProcessInstanceImpl.class);
        resolver.addMapping(ProcessDefinition.class, ProcessDefinitionImpl.class);
        resolver.addMapping(BPMNSequenceFlow.class, BPMNSequenceFlowImpl.class);
        resolver.addMapping(IntegrationContext.class, IntegrationContextImpl.class);
        resolver.addMapping(BPMNSignal.class, BPMNSignalImpl.class);
        resolver.addMapping(BPMNTimer.class, BPMNTimerImpl.class);
        resolver.addMapping(BPMNMessage.class, BPMNMessageImpl.class);
        resolver.addMapping(BPMNError.class, BPMNErrorImpl.class);
        resolver.addMapping(MessageSubscription.class, MessageSubscriptionImpl.class);
        resolver.addMapping(StartMessageSubscription.class, StartMessageSubscriptionImpl.class);
        resolver.addMapping(StartMessageDeployedEvent.class, StartMessageDeployedEventImpl.class);
        resolver.addMapping(StartMessageDeploymentDefinition.class, StartMessageDeploymentDefinitionImpl.class);
        module.registerSubtypes(new NamedType(ProcessInstanceResult.class, ProcessInstanceResult.class.getSimpleName()));
        module.registerSubtypes(new NamedType(SignalPayload.class, SignalPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TimerPayload.class, TimerPayload.class.getSimpleName()));

        module.registerSubtypes(new NamedType(ProcessEmptyStartPayload.class, ProcessEmptyStartPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(ProcessEmptySponsorPayload.class, ProcessEmptySponsorPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(ProcessInstanceHangPayload.class, ProcessInstanceHangPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(ProcessInstanceActivePayload.class, ProcessInstanceActivePayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(ProcessInstanceCancelPayload.class, ProcessInstanceCancelPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(ProcessInstanceInvalidPayload.class, ProcessInstanceInvalidPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(ProcessMessageStartPayload.class, ProcessMessageStartPayload.class.getSimpleName()));

        module.registerSubtypes(new NamedType(ProcessMessageStartPayload.class, ProcessMessageStartPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(MessageEventPayload.class, MessageEventPayload.class.getSimpleName()));
        module.setAbstractTypes(resolver);
        ConversionService conversionService = conversionServiceProvider.getIfUnique(this::conversionService);
        module.addSerializer(new ProcessVariablesMapSerializer(conversionService));
        module.addDeserializer(ProcessVariablesMap.class, new ProcessVariablesMapDeserializer(conversionService));
        return module;
    }

    public FormattingConversionService conversionService() {
        ApplicationConversionService conversionService = new ApplicationConversionService();
        converters.forEach(conversionService::addConverter);
        return conversionService;
    }

    @Bean
    public StringToMapConverter stringToMapConverter(@Lazy ObjectMapper objectMapper) {
        return new StringToMapConverter(objectMapper);
    }

    @Bean
    public MapToStringConverter mapToStringConverter(@Lazy ObjectMapper objectMapper) {
        return new MapToStringConverter(objectMapper);
    }

    @Bean
    public StringToJsonNodeConverter stringToJsonNodeConverter(@Lazy ObjectMapper objectMapper) {
        return new StringToJsonNodeConverter(objectMapper);
    }

    @Bean
    public JsonNodeToStringConverter jsonNodeToStringConverter(@Lazy ObjectMapper objectMapper) {
        return new JsonNodeToStringConverter(objectMapper);
    }

    @Bean
    public StringToDateConverter stringToDateConverter() {
        return new StringToDateConverter();
    }

    @Bean
    public DateToStringConverter dateToStringConverter() {
        return new DateToStringConverter();
    }

    @Bean
    public StringToLocalDateTimeConverter stringToLocalDateTimeConverter() {
        return new StringToLocalDateTimeConverter();
    }

    @Bean
    public LocalDateTimeToStringConverter localDateTimeToStringConverter() {
        return new LocalDateTimeToStringConverter();
    }

    @Bean
    public StringToLocalDateConverter stringToLocalDateConverter() {
        return new StringToLocalDateConverter();
    }

    @Bean
    public LocalDateToStringConverter localDateToStringConverter() {
        return new LocalDateToStringConverter();
    }

    @Bean
    public StringToListConverter sringToListConverter(@Lazy ObjectMapper objectMapper) {
        return new StringToListConverter(objectMapper);
    }

    @Bean
    public ListToStringConverter listToStringConverter(@Lazy ObjectMapper objectMapper) {
        return new ListToStringConverter(objectMapper);
    }

    @Bean
    public StringToSetConverter stringToSetConverter(@Lazy ObjectMapper objectMapper) {
        return new StringToSetConverter(objectMapper);
    }

    @Bean
    public SetToStringConverter setToStringConverter(@Lazy ObjectMapper objectMapper) {
        return new SetToStringConverter(objectMapper);
    }

    @Bean
    public StringToObjectValueConverter stringToObjectValueConverter(@Lazy ObjectMapper objectMapper) {
        return new StringToObjectValueConverter(objectMapper);
    }

    @Bean
    public ObjectValueToStringConverter objectValueToStringConverter(@Lazy ObjectMapper objectMapper) {
        return new ObjectValueToStringConverter(objectMapper);
    }

}
