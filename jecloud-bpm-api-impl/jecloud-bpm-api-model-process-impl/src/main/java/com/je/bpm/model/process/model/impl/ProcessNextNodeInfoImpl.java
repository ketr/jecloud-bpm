/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.model.impl;

import com.je.bpm.model.process.model.ProcessNextNodeInfo;

/**
 * 提交节点信息
 */
public class ProcessNextNodeInfoImpl implements ProcessNextNodeInfo {
    /**
     * 提交顺序流id
     */
    private String target;
    /**
     * 提交节点名称
     */
    private String nodeName;
    /**
     * 提交节点名称
     */
    private String nodeId;
    /**
     * 节点类型
     */
    private String type;
    /**
     * 直接提交,如果当前节点是多人节点\会签节点\聚合节点\结束节点,可以直接提交不用选人
     */
    private String submitDirectly;
    /**
     *  是否是跳跃，
     */
    private String isJump;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubmitDirectly() {
        return submitDirectly;
    }

    public void setSubmitDirectly(Boolean submitDirectly) {
        if (submitDirectly == false) {
            this.submitDirectly = "0";
        } else {
            this.submitDirectly = "1";
        }
    }

    public String getIsJump() {
        return isJump;
    }

    public void setIsJump(Boolean isJump) {
        if (isJump == false) {
            this.isJump = "0";
        } else {
            this.isJump = "1";
        }
    }
}
