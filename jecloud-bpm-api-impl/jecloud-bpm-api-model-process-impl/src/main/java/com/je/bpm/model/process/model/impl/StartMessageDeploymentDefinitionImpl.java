/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.model.impl;

import com.je.bpm.model.process.model.ProcessDefinition;
import com.je.bpm.model.process.model.StartMessageDeploymentDefinition;
import com.je.bpm.model.process.model.StartMessageSubscription;

import java.util.Objects;

public class StartMessageDeploymentDefinitionImpl implements StartMessageDeploymentDefinition {

    private StartMessageSubscription messageSubscription;

    private ProcessDefinition processDefinition;

    private StartMessageDeploymentDefinitionImpl(Builder builder) {
        this.messageSubscription = builder.messageSubscription;
        this.processDefinition = builder.processDefinition;
    }

    StartMessageDeploymentDefinitionImpl() { }

    @Override
    public ProcessDefinition getProcessDefinition() {
        return processDefinition;
    }

    @Override
    public StartMessageSubscription getMessageSubscription() {
        return messageSubscription;
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageSubscription, processDefinition);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        StartMessageDeploymentDefinitionImpl other = (StartMessageDeploymentDefinitionImpl) obj;
        return Objects.equals(messageSubscription, other.messageSubscription) &&
                Objects.equals(processDefinition, other.processDefinition);
    }

    @Override
    public String toString() {
        StringBuilder builder2 = new StringBuilder();
        builder2.append("StartMessageDeploymentDefinitionImpl [messageSubscription=")
                .append(messageSubscription)
                .append(", processDefinition=")
                .append(processDefinition)
                .append("]");
        return builder2.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Creates a builder to build {@link StartMessageDeploymentDefinitionImpl} and initialize it with the given object.
     *
     * @param startMessageEventSubscriptionImpl to initialize the builder with
     * @return created builder
     */
    public static Builder builderFrom(StartMessageDeploymentDefinitionImpl startMessageEventSubscriptionImpl) {
        return new Builder(startMessageEventSubscriptionImpl);
    }

    /**
     * Builder to build {@link StartMessageDeploymentDefinitionImpl}.
     */
    public static final class Builder {

        private StartMessageSubscription messageSubscription;
        private ProcessDefinition processDefinition;

        public Builder() {
        }

        private Builder(StartMessageDeploymentDefinitionImpl startMessageEventSubscriptionImpl) {
            this.messageSubscription = startMessageEventSubscriptionImpl.messageSubscription;
            this.processDefinition = startMessageEventSubscriptionImpl.processDefinition;
        }

        /**
         * Builder method for messageEventSubscription parameter.
         *
         * @param messageEventSubscription field to set
         * @return builder
         */
        public Builder withMessageSubscription(StartMessageSubscription messageEventSubscription) {
            this.messageSubscription = messageEventSubscription;
            return this;
        }

        /**
         * Builder method for processDefinition parameter.
         *
         * @param processDefinition field to set
         * @return builder
         */
        public Builder withProcessDefinition(ProcessDefinition processDefinition) {
            this.processDefinition = processDefinition;
            return this;
        }

        /**
         * Builder method of the builder.
         *
         * @return built class
         */
        public StartMessageDeploymentDefinitionImpl build() {
            return new StartMessageDeploymentDefinitionImpl(this);
        }
    }

}
