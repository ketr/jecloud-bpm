/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Lists;
import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.json.converter.json.converter.BpmnJsonConverter;
import com.je.bpm.core.layout.BpmnAutoLayout;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.button.TaskCommitBreakdownButton;
import com.je.bpm.core.model.button.TaskDismissBreakdownButton;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.core.model.config.ProcessDeployTypeEnum;
import com.je.bpm.core.model.config.ProcessRemindTemplateTypeEnum;
import com.je.bpm.core.model.config.ProcessRemindTypeEnum;
import com.je.bpm.core.model.config.process.*;
import com.je.bpm.core.model.config.sequenceflow.SequenceFlowStyleConfig;
import com.je.bpm.core.model.config.task.*;
import com.je.bpm.core.model.config.task.assignment.*;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.task.KaiteMultiUserTask;
import com.je.bpm.core.model.task.KaiteUserTask;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.impl.util.io.ResourceStreamSource;
import com.je.bpm.engine.repository.Deployment;
import com.je.bpm.engine.repository.DeploymentBuilder;
import com.je.bpm.engine.runtime.ProcessInstance;
import com.je.bpm.engine.task.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class KaiteUserTaskTest {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;

    private BpmnModel bpmnModel;

    @Before
    public void setUser() {
        securityUtil.logInAs("system");
        bpmnModel = new BpmnModel();
        Process process = new Process();
        process.setName("测试凯特用户任务");
        process.setId("testKaiteUserTask");
        ProcessBasicConfigImpl processBasicConfig = new ProcessBasicConfigImpl();
        processBasicConfig.setFuncId("funcid");
        processBasicConfig.setFuncName("funcname");
        processBasicConfig.setFuncCode("funccode");
        processBasicConfig.setTableCode("tableCode");
        processBasicConfig.setAttachedFuncCodes("attachedFuncCodes");
        processBasicConfig.setAttachedFuncIds("attachedFuncIds");
        processBasicConfig.setAttachedFuncNames("attachedFuncNames");
        processBasicConfig.setContentModule("module");
        processBasicConfig.setProcessClassificationName("name");
        processBasicConfig.setProcessClassificationId("id");
        processBasicConfig.setProcessClassificationCode("code");
        processBasicConfig.setDeploymentEnvironment(ProcessDeployTypeEnum.PRODUCT);
        process.setProcessConfig(processBasicConfig);

        ExtendedConfigurationConfigImpl extendedConfigurationConfig = new ExtendedConfigurationConfigImpl();
        extendedConfigurationConfig.setCanSponsor(false);
        extendedConfigurationConfig.setCanRetrieve(true);
        extendedConfigurationConfig.setCanReturn(true);
        extendedConfigurationConfig.setCanUrged(true);
        extendedConfigurationConfig.setCanCancel(true);
        extendedConfigurationConfig.setCanInvalid(true);
        extendedConfigurationConfig.setCanTransfer(true);
        extendedConfigurationConfig.setCanDelegate(true);
        extendedConfigurationConfig.setEasyLaunch(true);
        extendedConfigurationConfig.setSimpleApproval(true);
        extendedConfigurationConfig.setHideStateInfo(true);
        extendedConfigurationConfig.setHideTimeInfo(true);
        process.setExtendedConfiguration(extendedConfigurationConfig);

        StartupSettingsConfigImpl startupSettingsConfig = new StartupSettingsConfigImpl();
        startupSettingsConfig.setCanEveryoneStart(true);
        startupSettingsConfig.setCanEveryoneRolesName("系统管理员");
        startupSettingsConfig.setCanEveryoneRolesId("admins");
        startupSettingsConfig.setStartExpression("sss");
        startupSettingsConfig.setStartExpressionFn("sssssfn");
        process.setStartupSettings(startupSettingsConfig);

        List<CustomEvent> customEventListeners = new ArrayList<>();
        CustomEvent customEvent = new CustomEvent();
        customEvent.setType(CustomEvent.CustomEventEnum.PROCESS_START);
        customEvent.setExecutionStrategy(CustomEvent.CustomExecutionStrategyEnum.FIELD_ASSIGNMENT);
        customEvent.setAssignmentFieldConfiguration("赋值字段配置");
        customEvent.setServiceName("service");
        customEvent.setMethod("method");
        customEvent.setExistenceParameter(true);
        customEventListeners.add(customEvent);
        process.setCustomEventListeners(customEventListeners);

        MessageSettingConfigImpl messageSettingConfig = new MessageSettingConfigImpl();
        List<ProcessRemindTemplate> messageDefinitions = new ArrayList<>();
        ProcessRemindTemplate processRemindTemplate = new ProcessRemindTemplate();
        processRemindTemplate.setType(ProcessRemindTemplateTypeEnum.NOTE);
        processRemindTemplate.setTemplate("模板");
        messageDefinitions.add(processRemindTemplate);
        messageSettingConfig.setMessageDefinitions(messageDefinitions);
        List<ProcessRemindTypeEnum> messages = ProcessRemindTypeEnum.getDefaultRemindTypes();
        messages.add(ProcessRemindTypeEnum.NOTE);
        messageSettingConfig.setMessages(messages);
        process.setMessageSetting(messageSettingConfig);

        //开始节点的属性
        StartEvent startEvent = new StartEvent();
        startEvent.setId("start");
        startEvent.setName("start");


        //节点1 判断、普通
        KaiteUserTask kaiteUserTask1 = new KaiteUserTask();
        TaskBasicConfigImpl taskBasicConfig = new TaskBasicConfigImpl();
        taskBasicConfig.setFormSchemeId("FormSchemeId");
        taskBasicConfig.setFormSchemeName("FormSchemeName");
        taskBasicConfig.setListSynchronization(true);
        taskBasicConfig.setRetrieve(true);
        taskBasicConfig.setUrge(true);
        taskBasicConfig.setInvalid(true);
        taskBasicConfig.setDelegate(true);
        taskBasicConfig.setTransfer(true);
        taskBasicConfig.setFormEditable(true);
        taskBasicConfig.setRemind(true);
        taskBasicConfig.setCloseSimpleApproval(true);
        taskBasicConfig.setSelectAll(true);
        taskBasicConfig.setAsynTree(true);
        taskBasicConfig.setJump(true);
        taskBasicConfig.setLogicalJudgment(true);
        kaiteUserTask1.setTaskBasicConfig(taskBasicConfig);
        //人员
        kaiteUserTask1.setTaskAssigneeConfig(addUser());
        //提交分解
        kaiteUserTask1.setCommitBreakdownButtonList(addCommitBreakdownButton());
        //驳回配置
        kaiteUserTask1.setTaskDismissConfig(addTaskDismiss());
        //预警与延期
        kaiteUserTask1.setTaskEarlyWarningAndPostponementConfig(addEarlyWarningAndPost());
        //任务传阅配置
        kaiteUserTask1.setTaskPassRoundConfig(addTaskPassRoundConfig());
        //事件配置
        kaiteUserTask1.setCustomEventListeners(addCustomEventListeners());
        //加签
        kaiteUserTask1.setAddSignatureConfig(addAddSignatureConfig());
        //审批告知
        kaiteUserTask1.setTaskApprovalNoticeConfig(addTaskApprovalNoticeConfig());
        //表单基本配置
        kaiteUserTask1.setTaskFormBasicConfig(addtaskFormBasicConfig());

        kaiteUserTask1.setId("kaiteUserTask1");
        kaiteUserTask1.setName("凯特用户任务1");

        //节点2  候选
        //节点2
        KaiteUserTask kaiteUserTask2 = new KaiteUserTask();
        kaiteUserTask2.setTaskAssigneeConfig(addUser());
        kaiteUserTask2.setId("kaiteUserTask2");
        kaiteUserTask2.setName("凯特会签用户任务");

        //节点3  会签
        KaiteMultiUserTask kaiteUserTask3 = new KaiteMultiUserTask(true);
        kaiteUserTask3.getTaskBasicConfig().setSequential(true);

        kaiteUserTask3.setTaskAssigneeConfig(addUser());
        kaiteUserTask3.setId("kaiteUserTask3");
        kaiteUserTask3.setName("凯特用户任务3");

        //结束节点属性
        EndEvent endEvent = new EndEvent();
        endEvent.setId("endEvent");
        endEvent.setName("endEvent");

        //连线信息
        List<SequenceFlow> sequenceFlows1 = new ArrayList<SequenceFlow>();
        SequenceFlow s1 = new SequenceFlow();
        s1.setId("start_to_kaiteUserTask1");
        s1.setName("start_to_kaiteUserTask1");
        s1.setSourceRef("start");
        s1.setTargetRef("kaiteUserTask1");

        SequenceFlowStyleConfig sequenceFlowStyleConfig = new SequenceFlowStyleConfig();
        sequenceFlowStyleConfig.setDottedLine(true);
        sequenceFlowStyleConfig.setColor("color");
        sequenceFlowStyleConfig.setFontSize("fontsize");
        sequenceFlowStyleConfig.setFontStyle("fontstyle");
        sequenceFlowStyleConfig.setFontColor("fontcolor");
        s1.setStyleConfig(sequenceFlowStyleConfig);

        sequenceFlows1.add(s1);

        List<SequenceFlow> sequenceFlows2 = new ArrayList<SequenceFlow>();
        SequenceFlow s2 = new SequenceFlow();
        s2.setId("kaiteUserTask1_kaiteUserTask2");
        s2.setName("kaiteUserTask1_kaiteUserTask2");
        s2.setSourceRef("kaiteUserTask1");
        s2.setTargetRef("kaiteUserTask2");
        sequenceFlows2.add(s2);

        List<SequenceFlow> sequenceFlows3 = new ArrayList<SequenceFlow>();
        SequenceFlow s3 = new SequenceFlow();
        s3.setId("kaiteUserTask2_kaiteUserTask3");
        s3.setName("kaiteUserTask2_kaiteUserTask3");
        s3.setSourceRef("kaiteUserTask2");
        s3.setTargetRef("kaiteUserTask3");
        sequenceFlows3.add(s3);

        List<SequenceFlow> toEnd = new ArrayList<SequenceFlow>();
        SequenceFlow s4 = new SequenceFlow();
        s4.setId("kaiteUserTask3_endEvent");
        s4.setName("kaiteUserTask3_endEvent");
        s4.setSourceRef("kaiteUserTask3");
        s4.setTargetRef("endEvent");
        toEnd.add(s4);

        startEvent.setOutgoingFlows(sequenceFlows1);
        kaiteUserTask1.setIncomingFlows(sequenceFlows1);
        kaiteUserTask1.setOutgoingFlows(sequenceFlows2);
        kaiteUserTask2.setIncomingFlows(sequenceFlows2);
        kaiteUserTask2.setOutgoingFlows(sequenceFlows3);
        kaiteUserTask3.setIncomingFlows(sequenceFlows3);
        kaiteUserTask3.setOutgoingFlows(toEnd);
        endEvent.setIncomingFlows(toEnd);

        process.addFlowElement(startEvent);
        process.addFlowElement(kaiteUserTask1);
        process.addFlowElement(kaiteUserTask2);
        process.addFlowElement(kaiteUserTask3);
        process.addFlowElement(endEvent);
        process.addFlowElement(s1);
        process.addFlowElement(s2);
        process.addFlowElement(s3);
        process.addFlowElement(s4);

        bpmnModel.addProcess(process);
    }

    private TaskAssigneeConfigImpl addUser() {
        TaskAssigneeConfigImpl taskAssigneeConfig = new TaskAssigneeConfigImpl();
        addUsers(taskAssigneeConfig);
        taskAssigneeConfig.setReferTo(TaskAssigneeConfigImpl.ReferToEnum.LOGUSER);
        return taskAssigneeConfig;
    }

    public void addUsers(TaskAssigneeConfigImpl taskAssigneeConfig) {
        //角色 18612118473 18612118473  UO7MyyTuC1QkHqbg6Hf   f29049fc63d4472e95d089abdcacedf8
        RoleAssignmentConfigImpl roleAssignmentConfig = new RoleAssignmentConfigImpl();
        roleAssignmentConfig.addResource(new AssigneeResource("产品开发人员", "hFDg2zmqdfcEaqlgvzw", "产品开发人员"));
        AssignmentPermission assignmentPermission = new AssignmentPermission();
        assignmentPermission.setCompany(true);
        assignmentPermission.setCompanySupervision(true);
        assignmentPermission.setDept(true);
        assignmentPermission.setDeptAll(true);
        assignmentPermission.setDirectLeader(true);
        assignmentPermission.setDeptLeader(true);
        assignmentPermission.setSupervisionLeader(true);
        assignmentPermission.setSql("sql");
        assignmentPermission.setSqlRemarks("sql备注");
        roleAssignmentConfig.setEntryPath("kaiteUserTask1");
        roleAssignmentConfig.setPermission(assignmentPermission);
        taskAssigneeConfig.addResourceConfig(roleAssignmentConfig);

        //部门
        DepartmentAssignmentConfigImpl departmentAssignmentConfig = new DepartmentAssignmentConfigImpl();
        departmentAssignmentConfig.addResource(new AssigneeResource("系统管理一部", "UO7MyyTuC1QkHqbg6H2", "系统管理一部"));
        departmentAssignmentConfig.setEntryPath("kaiteUserTask1");
        departmentAssignmentConfig.setPermission(assignmentPermission);
        taskAssigneeConfig.addResourceConfig(departmentAssignmentConfig);

        //人员
        UserAssignmentConfigImpl userAssignmentConfig = new UserAssignmentConfigImpl();
        userAssignmentConfig.addResource(new AssigneeResource("刘利军", "f29049fc63d4472e95d089abdcacedf5", "UO7MyyTuC1QkHqbg6Hf"));
        userAssignmentConfig.setEntryPath("kaiteUserTask1");
        taskAssigneeConfig.addResourceConfig(userAssignmentConfig);

        //组织架构
        OrgTreeAssignmentConfigImpl orgTreeAssignmentConfig = new OrgTreeAssignmentConfigImpl();
        orgTreeAssignmentConfig.setEnable(true);
        taskAssigneeConfig.addResourceConfig(orgTreeAssignmentConfig);

        //自定义
        CustomAssignmentConfigImpl customAssignmentConfig = new CustomAssignmentConfigImpl();
        customAssignmentConfig.setServiceName("service");
        customAssignmentConfig.setMethodName("methodName");
        taskAssigneeConfig.addResourceConfig(customAssignmentConfig);

        //特殊处理
        SpecialAssignmentConfigImpl specialAssignmentConfig = new SpecialAssignmentConfigImpl();
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.LOGINED_USER);
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.DIRECT_LEADER);
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.DEPT_LEADER);
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.DEPT_MONITOR_LEADER);
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.STARTER_USER);
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.PREV_ASSIGN_USER);
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.PREV_ASSIGN_USER_DIRECT_LEADER);
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.DEPT_USERS);
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.DEPT_ALL_USERS);
        specialAssignmentConfig.add(SpecialAssignmentConfigImpl.SpectialTypeEnum.DEPT_MONITOR_USERS);
        taskAssigneeConfig.addResourceConfig(specialAssignmentConfig);

    }

    /**
     * 提交分解
     */
    private List<TaskCommitBreakdownButton> addCommitBreakdownButton() {
        List<TaskCommitBreakdownButton> commitBreakdownButtonList = new ArrayList<>();
        TaskCommitBreakdownButton taskCommitBreakdownButton = new TaskCommitBreakdownButton();
        taskCommitBreakdownButton.setName("自定义提交");
        taskCommitBreakdownButton.setNodeId("1");
        taskCommitBreakdownButton.setNodeName("one");
        commitBreakdownButtonList.add(taskCommitBreakdownButton);
        return commitBreakdownButtonList;
    }

    //驳回配置
    private TaskDismissConfigImpl addTaskDismiss() {
        TaskDismissConfigImpl taskDismissConfig = new TaskDismissConfigImpl();
        taskDismissConfig.setEnable(true);
        taskDismissConfig.setDismissTaskId("kaiteUserTask1");
        List<TaskDismissBreakdownButton> commitBreakdown = new ArrayList<>();
        TaskDismissBreakdownButton taskDismissBreakdownButton = new TaskDismissBreakdownButton();
        taskDismissBreakdownButton.setName("自定义提交");
        taskDismissBreakdownButton.setNodeId("1");
        taskDismissBreakdownButton.setNodeName("one");
        commitBreakdown.add(taskDismissBreakdownButton);
        taskDismissConfig.setCommitBreakdown(commitBreakdown);
        taskDismissConfig.setDirectSendAfterDismiss(true);
        taskDismissConfig.setForceCommitAfterDismiss(true);
        taskDismissConfig.setDisableSendAfterDismiss(true);
        taskDismissConfig.setNoReturn(true);
        taskDismissConfig.setDirectSendAfterReturn(true);
        return taskDismissConfig;
    }

    //预警与延期
    private TaskEarlyWarningAndPostponementConfigImpl addEarlyWarningAndPost() {
        TaskEarlyWarningAndPostponementConfigImpl taskEarlyWarningAndPostponementConfig = new TaskEarlyWarningAndPostponementConfigImpl();
        taskEarlyWarningAndPostponementConfig.setEnabled(true);
        List<EarlyWarningAndPostponementSource> source = new ArrayList<>();
        EarlyWarningAndPostponementSource earlyWarningAndPostponementSource = new EarlyWarningAndPostponementSource();
        earlyWarningAndPostponementSource.setSourceType(EarlyWarningAndPostponementSource.EarlyWarningAndPostponementSourceType.EARLY_WARNING);
        source.add(earlyWarningAndPostponementSource);
        taskEarlyWarningAndPostponementConfig.setSource(source);
        return taskEarlyWarningAndPostponementConfig;
    }

    //任务传阅配置
    private TaskPassRoundConfigImpl addTaskPassRoundConfig() {
        TaskPassRoundConfigImpl taskPassRoundConfig = new TaskPassRoundConfigImpl();
        taskPassRoundConfig.setEnable(true);
        taskPassRoundConfig.setAuto(true);
        List<PassRoundResource> passRoundResourceList = new ArrayList<>();
        PassRoundResource passRoundResource = new PassRoundResource();
        passRoundResource.setPassRoundTypeEnum(PassRoundResource.PassRoundTypeEnum.customerDepartmentConfig);
        passRoundResourceList.add(passRoundResource);

        PassRoundResource passRoundResource2 = new PassRoundResource();
        passRoundResource2.setPassRoundTypeEnum(PassRoundResource.PassRoundTypeEnum.roleConfig);
        passRoundResource2.setResourceId("1");
        passRoundResource2.setResourceName("研发部门");
        passRoundResourceList.add(passRoundResource2);

        PassRoundResource passRoundResource3 = new PassRoundResource();
        passRoundResource3.setPassRoundTypeEnum(PassRoundResource.PassRoundTypeEnum.customConfig);
        passRoundResource3.setService("service");
        passRoundResource3.setMethod("method");
        passRoundResourceList.add(passRoundResource3);

        taskPassRoundConfig.setPassRoundResourceList(passRoundResourceList);
        return taskPassRoundConfig;
    }

    //事件配置
    private List<CustomEvent> addCustomEventListeners() {
        List<CustomEvent> customEventListeners = new ArrayList<>();
        CustomEvent customEvent = new CustomEvent();
        customEvent.setType(CustomEvent.CustomEventEnum.PROCESS_END);
        customEvent.setExecutionStrategy(CustomEvent.CustomExecutionStrategyEnum.FIELD_ASSIGNMENT);
        customEvent.setAssignmentFieldConfiguration("字段赋值策略");
        customEvent.setServiceName("service");
        customEvent.setMethod("method");
        customEvent.setExistenceParameter(true);
        customEventListeners.add(customEvent);
        return customEventListeners;
    }

    //加签
    private AddSignatureConfigImpl addAddSignatureConfig() {
        AddSignatureConfigImpl addSignatureConfig = new AddSignatureConfigImpl();
        addSignatureConfig.setEnable(true);
        addSignatureConfig.setUnlimited(true);
        addSignatureConfig.setNotCountersigned(true);
        addSignatureConfig.setMandatoryCountersignature(true);
        PassRoundResource passRoundResource = new PassRoundResource();
        passRoundResource.setPassRoundTypeEnum(PassRoundResource.PassRoundTypeEnum.roleConfig);
        passRoundResource.setResourceId("role");
        passRoundResource.setResourceName("角色");
        passRoundResource.setService("service");
        passRoundResource.setMethod("method");
        addSignatureConfig.addCirculationRule(passRoundResource);
        return addSignatureConfig;
    }

    //审批告知
    private TaskApprovalNoticeConfigImpl addTaskApprovalNoticeConfig() {
        TaskApprovalNoticeConfigImpl taskApprovalNoticeConfig = new TaskApprovalNoticeConfigImpl();
        taskApprovalNoticeConfig.setStartUser(true);
        taskApprovalNoticeConfig.setApprovedPerson(true);
        taskApprovalNoticeConfig.setApproverDeptLeader(true);
        taskApprovalNoticeConfig.setApproverDirectLeader(true);
        return taskApprovalNoticeConfig;
    }

    //表单控制
    private TaskFormBasicConfigImpl addtaskFormBasicConfig() {
        TaskFormBasicConfigImpl taskFormBasicConfig = new TaskFormBasicConfigImpl();
        Map<String, String> taskFormFieldSetValueConfig = new HashMap<>();
        taskFormFieldSetValueConfig.put("1", "one");

        TaskFormFieldConfigImpl taskFormFieldConfig = new TaskFormFieldConfigImpl();
        TaskFormFieldConfigImpl.FormFieldConfig formFieldConfig = new TaskFormFieldConfigImpl.FormFieldConfig();
        formFieldConfig.setCode("code");
        formFieldConfig.setName("name");
        formFieldConfig.setHidden(true);
        formFieldConfig.setEditable(true);
//        formFieldConfig.setReadOnly(true);
        formFieldConfig.setDisplay(true);
        formFieldConfig.setRequired(true);
        taskFormFieldConfig.add(formFieldConfig);

        TaskFormButtonConfigImpl taskFormButtonConfig = new TaskFormButtonConfigImpl();
        TaskFormButtonConfigImpl.ButtonConfig buttonConfig = new TaskFormButtonConfigImpl.ButtonConfig();
        buttonConfig.setCode("button1");
        buttonConfig.setName("按钮1");
        buttonConfig.setEnable(true);
        taskFormButtonConfig.add(buttonConfig);

        TaskFormChildFuncConfigImpl taskFormChildFuncConfig = new TaskFormChildFuncConfigImpl();
        TaskFormChildFuncConfigImpl.ChildFuncConfig childFuncConfig = new TaskFormChildFuncConfigImpl.ChildFuncConfig();
        childFuncConfig.setCode("childFunc1");
        childFuncConfig.setDisplay(true);
        childFuncConfig.setHidden(true);
        childFuncConfig.setEditable(true);
        taskFormChildFuncConfig.add(childFuncConfig);

        taskFormBasicConfig.setTaskFormFieldSetValueConfig(taskFormFieldSetValueConfig);
        taskFormBasicConfig.setTaskFormFieldConfig(taskFormFieldConfig);
        taskFormBasicConfig.setTaskFormButtonConfig(taskFormButtonConfig);
        taskFormBasicConfig.setTaskFormChildFuncConfig(taskFormChildFuncConfig);
        return taskFormBasicConfig;
    }


    @Test
    public void convertToJson() throws IOException {
        new BpmnAutoLayout(bpmnModel).execute();
        BpmnJsonConverter bpmnJsonConverter = new BpmnJsonConverter();
        ObjectNode objectNode = bpmnJsonConverter.convertToJson(bpmnModel);
        String property = objectNode.toPrettyString();
        File f = new File("C:\\Users\\yuchunhui\\Desktop\\json.txt");
        f.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(f);
        PrintStream printStream = new PrintStream(fileOutputStream);
        System.setOut(printStream);
        System.out.println(property);
    }

    @Test
    public void jsonToElement() throws IOException {
        BpmnJsonConverter bpmnJsonConverter = new BpmnJsonConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/decide.bpmn20.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode modelNode = mapper.readTree(resourceStreamSource.getInputStream());
        bpmnModel = bpmnJsonConverter.convertToBpmnModel(modelNode);
        System.out.println(1111);
    }

    @Test
    public void convertToXml() throws IOException {
        BpmnJsonConverter bpmnJsonConverter = new BpmnJsonConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/decide2.bpmn20.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode modelNode = mapper.readTree(resourceStreamSource.getInputStream());
        bpmnModel = bpmnJsonConverter.convertToBpmnModel(modelNode);
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String bytes = new String(convertToXML);
        System.out.println(bytes);
    }

    @Test
    public void xmlToElement() throws IOException {
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/sdf.bpmn20.xml");
        BpmnModel bpmnModel2 = bpmnXMLConverter.convertToBpmnModel(resourceStreamSource, false, false);
        System.out.println(new String(bpmnXMLConverter.convertToXML(bpmnModel2)));
//        for (FlowElement flowElement : bpmnModel.getMainProcess().getFlowElementList()) {
//            for (FlowElement flowElement2 : bpmnModel2.getMainProcess().getFlowElementList()) {
//                if (flowElement instanceof KaiteUserTask && flowElement2 instanceof KaiteUserTask) {
//                    System.out.println("--------------------------one");
//                    KaiteUserTask kaiteUserTask1 = (KaiteUserTask) flowElement;
//                    System.out.println(kaiteUserTask1.toString());
//                    System.out.println("--------------------------two");
//                    KaiteUserTask kaiteUserTask2 = (KaiteUserTask) flowElement2;
//                    System.out.println(kaiteUserTask2.toString());
//                } else if (flowElement instanceof KaiteCandidateUserTask && flowElement2 instanceof KaiteCandidateUserTask) {
//                    KaiteCandidateUserTask kaiteUserTask1 = (KaiteCandidateUserTask) flowElement;
//                    KaiteCandidateUserTask kaiteUserTask2 = (KaiteCandidateUserTask) flowElement2;
//
//                    System.out.println(kaiteUserTask2.toString());
//                } else if (flowElement instanceof KaiteMultiUserTask && flowElement2 instanceof KaiteMultiUserTask) {
//                    KaiteMultiUserTask kaiteUserTask1 = (KaiteMultiUserTask) flowElement;
//                    KaiteMultiUserTask kaiteUserTask2 = (KaiteMultiUserTask) flowElement2;
//
//                    System.out.println(kaiteUserTask2.toString());
//                }
//            }
//        }


//        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
//        FileOutputStream fos = new FileOutputStream(new File("/Users/liulijun/Documents/testconverter.bpmn20.xml"));
//        fos.write(convertToXML);
//        fos.close();
    }

    @Test
    public void deploy() {
        DeploymentBuilder deploymentBuilder = processEngine.getRepositoryService().createDeployment().addClasspathResource("process/testconverter.bpmn20.xml");
        deploymentBuilder.disableBpmnValidation();
        deploymentBuilder.disableSchemaValidation();
        deploymentBuilder.key("testKaiteUserTask");
        deploymentBuilder.name("testKaiteUserTask");
        deploymentBuilder.category("bpmnModel");
        Deployment deployment = deploymentBuilder.deploy();
        System.out.println(deployment.getId());
    }

    @Test
    public void start() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("kaiteUserTask1", "liulj1");
        ProcessInstance processInstance = processEngine.getRuntimeService().startProcessInstanceByKey("testKaiteUserTask", variables);
        System.out.println(processInstance.getName());
        System.out.println(processInstance.getProcessInstanceId());
    }

    @Test
    public void sponsor1() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("kaiteUserTask1", "liulj1");
        variables.put("kaiteUserTask2", "liulj2");
        variables.put("sponsor", true);
        ProcessInstance processInstance = processEngine.getRuntimeService().startProcessInstanceByKey("testKaiteUserTask", variables);
        System.out.println(processInstance.getName());
        System.out.println(processInstance.getProcessInstanceId());
    }

    @Test
    public void sponsor2() {
        ProcessInstance processInstance = processEngine.getRuntimeService().sponsorProcessInstanceByKey("testKaiteUserTask", "");
        System.out.println(processInstance.getName());
        System.out.println(processInstance.getProcessInstanceId());
    }

    @Test
    public void findTask() {
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("7f872dc4-775a-11ec-9442-d64933d16f90").list();
        for (Task each : tasks) {
            Object variables = processEngine.getTaskService().getVariable(each.getId(), "configVariables");
            JSONObject jsonObject = JSONObject.parseObject(variables.toString());
            for (Map.Entry<String, Object> eachEntry : jsonObject.entrySet()) {
                System.out.println("-------" + eachEntry.getKey() + "-------" + eachEntry.getValue());
            }
        }
    }

    @Test
    public void completeTask() {
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("8b232c8a-775b-11ec-a9c4-d64933d16f90").list();
        for (Task each : tasks) {
//            processEngine.getTaskService().complete(each.getId(), "test1");
//            processEngine.getTaskService().complete(each.getId());
        }
    }

    @Test
    public void delegateTask() {
        //测试委托
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("8b232c8a-775b-11ec-a9c4-d64933d16f90").list();
        for (Task each : tasks) {
//            processEngine.getTaskService().delegateTask(each.getId(), "liu1","");
        }
    }

    @Test
    /**
     * 测试解决委托
     */
    public void cancelDelegateTask() {
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("8b232c8a-775b-11ec-a9c4-d64933d16f90").list();
        for (Task each : tasks) {
//            processEngine.getTaskService().cancelDelegateTask(each.getId());
        }
    }

    @Test
    /**
     * 传阅
     */
    public void passRoundTask() {
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("921b4cd7-7449-11ec-85d6-7a99d37ed607").list();
        for (Task each : tasks) {
            processEngine.getTaskService().passRoundTask(each.getId(), Lists.newArrayList("user1", "user2"), "", "", "");
        }
    }

    @Test
    /**
     * 已阅
     */
    public void readPassRound() {
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("921b4cd7-7449-11ec-85d6-7a99d37ed607").list();
        for (Task each : tasks) {
            processEngine.getTaskService().readPassRoundTask(each.getId(), "user2","");
        }
    }

}
