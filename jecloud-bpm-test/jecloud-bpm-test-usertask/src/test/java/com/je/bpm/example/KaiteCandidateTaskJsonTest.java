/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.json.converter.json.converter.BpmnJsonConverter;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.process.ProcessBasicConfigImpl;
import com.je.bpm.core.model.config.task.*;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.task.KaiteCandidateUserTask;
import com.je.bpm.core.model.task.KaiteFixedUserTask;
import com.je.bpm.core.model.task.KaiteUserTask;
import com.je.bpm.engine.ManagementService;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.impl.util.io.ResourceStreamSource;
import com.je.bpm.model.process.model.ProcessRunForm;
import com.je.bpm.model.process.results.ProcessButtonListResult;
import com.je.bpm.model.process.results.ProcessModelResult;
import com.je.bpm.model.shared.Result;
import com.je.bpm.model.shared.model.Button;
import com.je.bpm.model.task.model.TaskButton;
import com.je.bpm.model.task.result.TaskListResult;
import com.je.bpm.runtime.process.ProcessOperatorService;
import com.je.bpm.runtime.shared.operator.Operator;
import com.je.bpm.runtime.shared.operator.OperatorRegistry;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;
import com.je.bpm.runtime.task.TaskOperatorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class KaiteCandidateTaskJsonTest {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;
    @Autowired
    private ProcessOperatorService processOperatorService;
    @Autowired
    private TaskOperatorService taskOperatorService ;
    private BpmnModel bpmnModel;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    ManagementService managementService;

    @Before
    public void setUser() {
        securityUtil.logInAs("zhangsan");
        bpmnModel = new BpmnModel();
        Process process = new Process();
        process.setName("请假申请3");
        process.setId("qingjiashenqing3");

        //开始节点的属性
        StartEvent startEvent = new StartEvent();
        startEvent.setId("start");
        startEvent.setName("start");

        //节点1
        KaiteUserTask kaiteUserTask1 = new KaiteUserTask();
        kaiteUserTask1.setId("kaiteUserTask1");
        kaiteUserTask1.setName("凯特用户任务1");

        // 委托配置
        TaskDelegateConfigImpl taskDelegateConfig = new TaskDelegateConfigImpl();
        taskDelegateConfig.setEnable(true);
        // 驳回配置
        TaskDismissConfigImpl taskDismissConfig = new TaskDismissConfigImpl();
        taskDismissConfig.setEnable(true);
        taskDismissConfig.setDismissTaskId("kaiteUserTask1");
        // 任务退回配置
        TaskGobackConfigImpl taskGobackConfig = new TaskGobackConfigImpl();
        taskGobackConfig.setEnable(true);
        // 流程作废配置
        TaskInvalidConfigImpl taskInvalidConfig = new TaskInvalidConfigImpl();
        taskInvalidConfig.setEnable(true);
        // 任务传阅配置
        TaskPassRoundConfigImpl taskPassRoundConfig = new TaskPassRoundConfigImpl();
        taskPassRoundConfig.setEnable(true);
        // 催办配置
        TaskUrgeConfigImpl taskUrgeConfig = new TaskUrgeConfigImpl();
        taskUrgeConfig.setEnable(true);
        // 转办配置
        TaskTransferConfigImpl taskTransferConfig = new TaskTransferConfigImpl();
        taskTransferConfig.setEnable(true);
        // 取回配置
        TaskRetrieveConfigImpl taskRetrieveConfig = new TaskRetrieveConfigImpl();
        taskRetrieveConfig.setEnable(true);
        kaiteUserTask1.setTaskDismissConfig(taskDismissConfig);
        kaiteUserTask1.setTaskPassRoundConfig(taskPassRoundConfig);

        //节点2
        KaiteCandidateUserTask kaiteUserTask2 = new KaiteCandidateUserTask();
        kaiteUserTask2.setId("kaiteCounterSignUserTask");
        kaiteUserTask2.setName("凯特候选用户任务");

        kaiteUserTask2.setTaskDismissConfig(taskDismissConfig);
        kaiteUserTask2.setTaskPassRoundConfig(taskPassRoundConfig);

        //节点3
        KaiteFixedUserTask kaiteUserTask3 = new KaiteFixedUserTask();
        kaiteUserTask3.setId("kaiteUserTask3");
        kaiteUserTask3.setName("凯特固定人任务3");

        //节点4
        KaiteUserTask kaiteUserTask4 = new KaiteUserTask();
        kaiteUserTask4.setId("kaiteUserTask4");
        kaiteUserTask4.setName("凯特用户任务4");

        //结束节点属性
        EndEvent endEvent = new EndEvent();
        endEvent.setId("endEvent");
        endEvent.setName("endEvent");

        //连线信息
        List<SequenceFlow> sequenceFlows1 = new ArrayList();

        SequenceFlow s1 = new SequenceFlow();
        s1.setId("s1");
        s1.setName("s1");
        s1.setSourceRef("start");
        s1.setTargetRef("kaiteUserTask1");
        sequenceFlows1.add(s1);

        List<SequenceFlow> sequenceFlows2 = new ArrayList<>();
        SequenceFlow s2 = new SequenceFlow();
        s2.setId("s2");
        s2.setName("s2");
        s2.setSourceRef("kaiteUserTask1");
        s2.setTargetRef("kaiteCounterSignUserTask");
        sequenceFlows2.add(s2);

        List<SequenceFlow> sequenceFlows3 = new ArrayList<>();
        SequenceFlow s3 = new SequenceFlow();
        s3.setId("s3");
        s3.setName("s3");
        s3.setSourceRef("kaiteCounterSignUserTask");
        s3.setTargetRef("kaiteUserTask3");
        sequenceFlows3.add(s3);

        List<SequenceFlow> sequenceFlows4 = new ArrayList<>();
        sequenceFlows4.add(s3);

        SequenceFlow s4 = new SequenceFlow();
        s4.setId("s4");
        s4.setName("s4");
        s4.setSourceRef("kaiteUserTask3");
        s4.setTargetRef("kaiteUserTask4");
        sequenceFlows3.add(s4);

        List<SequenceFlow> sequenceFlows5 = new ArrayList<>();
        sequenceFlows5.add(s4);

        List<SequenceFlow> toEnd1 = new ArrayList();
        SequenceFlow s5 = new SequenceFlow();
        s5.setId("s5");
        s5.setName("s5");
        s5.setSourceRef("kaiteUserTask3");
        s5.setTargetRef("endEvent");
        toEnd1.add(s5);

        List<SequenceFlow> toEnd2 = new ArrayList();
        SequenceFlow s6 = new SequenceFlow();
        s6.setId("s6");
        s6.setName("s6");
        s6.setSourceRef("kaiteUserTask4");
        s6.setTargetRef("endEvent");
        toEnd2.add(s6);

        List<SequenceFlow> toEnd = new ArrayList();
        toEnd.add(s5);
        toEnd.add(s6);

        startEvent.setOutgoingFlows(sequenceFlows1);
        kaiteUserTask1.setIncomingFlows(sequenceFlows1);
        kaiteUserTask1.setOutgoingFlows(sequenceFlows2);
        kaiteUserTask2.setIncomingFlows(sequenceFlows2);
        kaiteUserTask2.setOutgoingFlows(sequenceFlows3);

        kaiteUserTask3.setIncomingFlows(sequenceFlows4);
        kaiteUserTask3.setOutgoingFlows(toEnd1);
        kaiteUserTask4.setIncomingFlows(sequenceFlows5);
        kaiteUserTask4.setOutgoingFlows(toEnd2);
        endEvent.setIncomingFlows(toEnd);

        process.addFlowElement(startEvent);
        process.addFlowElement(kaiteUserTask1);
        process.addFlowElement(kaiteUserTask2);
        process.addFlowElement(kaiteUserTask3);
        process.addFlowElement(kaiteUserTask4);
        process.addFlowElement(endEvent);
        process.addFlowElement(s1);
        process.addFlowElement(s2);
        process.addFlowElement(s3);
        process.addFlowElement(s4);
        process.addFlowElement(s5);
        process.addFlowElement(s6);
        ProcessBasicConfigImpl processBasicConfig = new ProcessBasicConfigImpl();
        process.setProcessConfig(processBasicConfig);
        bpmnModel.addProcess(process);
    }

    static String json = "{\"nodeMaps\":{},\"jebpm\":{\"id\":\"JEBPM\",\"url\":\"http://jepaas.com\"},\"bounds\":{\"lowerRight\":{\"x\":1485,\"y\":700},\"upperLeft\":{\"x\":0,\"y\":0}},\"resourceId\":\"canvas\",\"properties\":{\"JE_WORKFLOW_PROCESSINFO_ID\":\"7e98ee135019458191b4c11208eb9a5e\",\"process_id\":\"zOIqwdppmaac1Nr3dCQ\",\"name\":\"测试普通流程\",\"processBasicConfig\":{\"name\":\"测试普通流程\",\"funcCode\":\"TEST_PTLC\",\"funcName\":\"普通流程\",\"funcId\":\"U8ymbZqVoKTdMMdE5Ss\",\"tableCode\":\"TEST_PTLC\",\"SY_PRODUCT_ID\":\"yxljlUCJu9fcxX7tRAi111\",\"SY_PRODUCT_CODE\":\"test\",\"SY_PRODUCT_NAME\":\"测试服务\",\"attachedFuncCodes\":\"\",\"attachedFuncNames\":\"\",\"attachedFuncIds\":\"\",\"contentModule\":\"11111\",\"processClassificationName\":\"12312\",\"processClassificationCode\":\"\",\"processClassificationId\":\"\",\"deploymentEnvironment\":\"YF\"},\"startupSettings\":{\"canEveryoneStart\":\"1\",\"canEveryoneRolesId\":\"\",\"canEveryoneRolesName\":\"\",\"startExpression\":\"\",\"startExpressionFn\":\"\"},\"extendedConfiguration\":{\"canSponsor\":\"0\",\"canRetrieve\":\"1\",\"canReturn\":\"1\",\"canUrged\":\"1\",\"canCancel\":\"1\",\"canInvalid\":\"1\",\"canTransfer\":\"1\",\"canDelegate\":\"1\",\"easyLaunch\":\"1\",\"simpleApproval\":\"1\",\"hideStateInfo\":\"1\",\"hideTimeInfo\":\"1\"},\"customEventListeners\":[],\"messageSetting\":{\"messageDefinitions\":{\"flowBacklog\":\"\",\"title\":\"流程[{@PROCESS_NAME@}]提醒\",\"dwr\":\"流程提醒：由<font color=red>{@USER_NAME@}</font>在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}任务,执行操作：{@SUBMIT_OPERATE@},执行意见：\",\"email\":\"你有一条任务需要审批!\\n由{@USER_NAME@}在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}流程任务,执行动作：{@SUBMIT_OPERATE@},执行意见：{@SUBMIT_COMMENTS@}\",\"note\":\"流程提醒：由{@USER_NAME@}在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}任务，请尽快处理!\",\"thirdparty\":\"你有一条任务需要审批!\\n由{@USER_NAME@}在{@NOW_TIME@}给您{@SUBMIT_OPERATE@}了{@PROCESS_NAME@}流程任务,执行动作：{@SUBMIT_OPERATE@},执行意见：{@SUBMIT_COMMENTS@}\"},\"messages\":[\"PUSH\"]},\"processButtonConfig\":[]},\"childShapes\":[{\"child\":[],\"bounds\":{},\"resourceId\":\"line7\",\"childShapes\":[],\"stencil\":{\"id\":\"line\"},\"target\":[{\"resourceId\":\"task5\"}],\"properties\":{\"resourceId\":\"line7\",\"name\":\"\"},\"styleConfig\":{\"dashed\":\"0\",\"strokeColor\":\"\",\"fontSize\":\"12\",\"fontFamily\":\"宋体\",\"fontColor\":\"\"},\"source\":[{\"resourceId\":\"start4\"}]},{\"child\":[],\"bounds\":{\"x\":-190,\"y\":342,\"width\":48,\"height\":48},\"resourceId\":\"start4\",\"childShapes\":[],\"stencil\":{\"id\":\"start\"},\"target\":[{\"resourceId\":\"line7\"}],\"properties\":{\"resourceId\":\"start4\",\"name\":\"\"}},{\"child\":[],\"bounds\":{},\"resourceId\":\"line8\",\"childShapes\":[],\"stencil\":{\"id\":\"line\"},\"target\":[{\"resourceId\":\"task6\"}],\"properties\":{\"resourceId\":\"line8\",\"name\":\"\"},\"styleConfig\":{\"dashed\":\"0\",\"strokeColor\":\"\",\"fontSize\":\"12\",\"fontFamily\":\"宋体\",\"fontColor\":\"\"},\"source\":[{\"resourceId\":\"task5\"}]},{\"child\":[],\"bounds\":{\"x\":-20,\"y\":350,\"width\":80,\"height\":40},\"resourceId\":\"task5\",\"childShapes\":[],\"stencil\":{\"id\":\"task\"},\"target\":[{\"resourceId\":\"line8\"}],\"properties\":{\"categorydefinition\":\"kaiteUserTask\",\"name\":\"任务\",\"formSchemeName\":\"\",\"formSchemeId\":\"\",\"listSynchronization\":\"0\",\"retrieve\":\"0\",\"Urge\":\"0\",\"invalid\":\"0\",\"transfer\":\"0\",\"delegate\":\"0\",\"formEditable\":\"0\",\"remind\":\"0\",\"simpleApproval\":\"0\",\"selectAll\":\"0\",\"asynTree\":\"0\",\"jump\":\"0\",\"logicalJudgment\":\"0\"},\"assignmentConfig\":{\"resource\":[],\"referToCode\":\"LOGUSER\"},\"commitBreakdown\":[],\"dismissConfig\":{\"enable\":\"0\",\"dismissTaskNames\":\"\",\"dismissTaskIds\":\"\",\"commitBreakdown\":[],\"directSendAfterDismiss\":\"0\",\"forceCommitAfterDismiss\":\"0\",\"disableSendAfterDismiss\":\"0\",\"noReturn\":\"0\",\"directSendAfterReturn\":\"0\"},\"earlyWarningAndPostponement\":{\"enable\":\"0\",\"resource\":[]},\"passRoundConfig\":{\"enable\":\"0\",\"auto\":\"0\",\"circulationRules\":[]},\"buttonsConfig\":[],\"customEventListeners\":[],\"addSignature\":{\"enable\":\"0\",\"unlimited\":\"0\",\"notCountersigned\":\"0\",\"mandatoryCountersignature\":\"0\",\"circulationRules\":[]},\"approvalNotice\":{\"processInitiator\":\"1\",\"thisNodeApproved\":\"0\",\"thisNodeApprovalDirectlyUnderLeader\":\"0\",\"thisNodeApprovalDeptLeader\":\"0\"},\"formConfig\":{\"fieldAssignment\":{},\"fieldControl\":{},\"taskFormButton\":{},\"taskChildFunc\":{}}},{\"child\":[],\"bounds\":{},\"resourceId\":\"line10\",\"childShapes\":[],\"stencil\":{\"id\":\"line\"},\"target\":[{\"resourceId\":\"end9\"}],\"properties\":{\"resourceId\":\"line10\",\"name\":\"\"},\"styleConfig\":{\"dashed\":\"0\",\"strokeColor\":\"\",\"fontSize\":\"12\",\"fontFamily\":\"宋体\",\"fontColor\":\"\"},\"source\":[{\"resourceId\":\"task6\"}]},{\"child\":[],\"bounds\":{\"x\":200,\"y\":350,\"width\":80,\"height\":40},\"resourceId\":\"task6\",\"childShapes\":[],\"stencil\":{\"id\":\"task\"},\"target\":[{\"resourceId\":\"line10\"}],\"properties\":{\"categorydefinition\":\"kaiteUserTask\",\"name\":\"任务\",\"formSchemeName\":\"\",\"formSchemeId\":\"\",\"listSynchronization\":\"1\",\"retrieve\":\"0\",\"Urge\":\"1\",\"invalid\":\"1\",\"transfer\":\"1\",\"delegate\":\"1\",\"formEditable\":\"1\",\"remind\":\"1\",\"simpleApproval\":\"1\",\"selectAll\":\"1\",\"asynTree\":\"1\",\"jump\":\"0\",\"logicalJudgment\":\"1\"},\"assignmentConfig\":{\"resource\":[{\"entryPath\":\"\",\"type\":\"roleConfig\",\"name\":\"按角色处理\",\"resourceName\":\"测试使用权限组角色\",\"resourceCode\":\"role-000022\",\"resourceId\":\"SA5WhCvWx0CnFNWxJnx\",\"id\":\"eyF30T20hEM8AHBssmQ\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_134\",\"resourceIcon\":\"fal fa-user\",\"permission\":{\"company\":\"0\",\"companySupervision\":\"0\",\"dept\":\"0\",\"deptAll\":\"0\",\"directLeader\":\"0\",\"deptLeader\":\"0\",\"supervisionLeader\":\"0\",\"sql\":\"\",\"sqlRemarks\":\"\"},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"departmentConfig\",\"name\":\"按部门处理\",\"resourceName\":\"系统管理部\",\"resourceCode\":\"系统管理部\",\"resourceId\":\"UO7MyyTuC1QkHqbg6Hf\",\"id\":\"OkK1ONNcoatGdbm9IbT\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_135\",\"resourceIcon\":\"fal fa-address-book\",\"permission\":{\"company\":\"0\",\"companySupervision\":\"0\",\"dept\":\"0\",\"deptAll\":\"0\",\"directLeader\":\"0\",\"deptLeader\":\"0\",\"supervisionLeader\":\"0\",\"sql\":\"\",\"sqlRemarks\":\"\"},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"userConfig\",\"name\":\"按人员处理\",\"resourceName\":\"吝志超,刘利军,白梦浩,云凤程,刘利军,测试用户1,白梦浩,赵宝利,刘旭,赵宝利\",\"resourceCode\":\"linzc,admin,baimh,yunfc,admin,test,baimh,zhaobl,liux,zhaobl\",\"resourceId\":\"1f3dce9448864704abf350d78fa2e7a9,1f3f30f816e14fb19e11d8087a2f3916,3685abdc0ebd4a33bbd50ce32114dc57,3a3f82969cac4ddbb7527f1315919ee6,3e156a488cda44f680368e9b30f0fda9,805296a89ebe44e8ad9a8f77a527f44c,c1f57db5ade8440d8ffd70a722e6772d,d7ef93a0cf594394b0f91846bb9d51c7,f4a7622800da48b78682915831016d97,f7779d8432e44d0581f4778311f1a1ac\",\"id\":\"6WPxS6rjZYTgeVCwYpK\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_136\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"specialConfig\",\"name\":\"特殊处理\",\"resourceName\":\"当前登录人,直属领导,本部门领导,本部门监管领导,流程启动人,任务指派人,任务指派人直属领导,前置任务指派人,前置任务指派人直属领导,本部门内人员,本部门内(含子)人员,监管部门内人员,本公司领导,本公司监管领导,所在子公司（展示组织结构）\",\"resourceCode\":\"LOGINED_USER,DIRECT_LEADER,DEPT_LEADER,DEPT_MONITOR_LEADER,STARTER_USER,TASK_ASSGINE,TASK_ASSGINE_HEAD,PREV_ASSIGN_USER,PREV_ASSIGN_USER_DIRECT_LEADER,DEPT_USERS,DEPT_ALL_USERS,DEPT_MONITOR_USERS,COMPANY_LEADERS,COMPANY_MONITOR_LEADERS,SUBSIDIARY\",\"resourceId\":\"25042a25-3eae-4ba3-9a78-b56687ef5381,QalFCpT2ejQ2CzvwrjN,104c9ba4-109e-4360-a945-3446a07a7597,PnBzZEFrF57IeNUuBFt,c17afcb2-e8ff-4920-99ed-78f2b56965ce,dfe426ce-592c-4a8e-a872-19c0d91a711e,f63fe016-9f1d-4efb-a96e-09431072108a,d31cde81-1d6c-4361-a554-534848e0c491,e91560e6-9b7a-486b-8315-e3281a722a5c,A0NpRyDX0tS1TTcVT7E,YEFaWPqhjP7tCtmw7jV,PFrq2u3cl3mkn9FxRUH,19HaFGHWkpkFQsWIZL9,MKfTcSNMfsmQz5PNi8s,hIOYtizTSn7vMivcTwp\",\"id\":\"fz4nvwPsWAn4gA3HML0\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_137\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"formFieldConfig\",\"name\":\"表单字段\",\"resourceName\":\"登记人主键\",\"resourceCode\":\"SY_CREATEUSERID\",\"resourceId\":\"20de6c07e20a470d9361609b7fcc8bab\",\"id\":\"DhpiEe499Hvhqxbbdsp\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_138\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"orgTreeConfig\",\"name\":\"组织架构\",\"resourceName\":\"\",\"resourceCode\":\"\",\"resourceId\":\"\",\"id\":\"feWkvEug0DCNus6lbrz\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_139\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"businessSqlAssignmentConfig\",\"name\":\"用户SQL\",\"resourceName\":\"select * from JE_RBAC_VACCOUNTDEPT\",\"resourceCode\":\"\",\"resourceId\":\"\",\"id\":\"mDnjA5Sl9SecQsY3710\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_140\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"select * from JE_RBAC_VACCOUNTDEPT\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"customConfig\",\"name\":\"自定义\",\"resourceName\":\"testservice\",\"resourceCode\":\"\",\"resourceId\":\"\",\"id\":\"q1GLiaEmZGilJ7ZtIvg\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_141\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"testservice\",\"methodName\":\"aaa\"}],\"referToCode\":\"LOGUSER\"},\"commitBreakdown\":[],\"dismissConfig\":{\"enable\":\"0\",\"dismissTaskNames\":\"\",\"dismissTaskIds\":\"\",\"commitBreakdown\":[],\"directSendAfterDismiss\":\"0\",\"forceCommitAfterDismiss\":\"0\",\"disableSendAfterDismiss\":\"0\",\"noReturn\":\"0\",\"directSendAfterReturn\":\"0\"},\"earlyWarningAndPostponement\":{\"enable\":\"0\",\"resource\":[]},\"passRoundConfig\":{\"enable\":\"0\",\"auto\":\"0\",\"circulationRules\":[]},\"buttonsConfig\":[],\"customEventListeners\":[],\"addSignature\":{\"enable\":\"0\",\"unlimited\":\"0\",\"notCountersigned\":\"0\",\"mandatoryCountersignature\":\"0\",\"circulationRules\":[]},\"approvalNotice\":{\"processInitiator\":\"1\",\"thisNodeApproved\":\"0\",\"thisNodeApprovalDirectlyUnderLeader\":\"0\",\"thisNodeApprovalDeptLeader\":\"0\"},\"formConfig\":{\"fieldAssignment\":{},\"fieldControl\":{},\"taskFormButton\":{},\"taskChildFunc\":{}}},{\"child\":[],\"bounds\":{\"x\":430,\"y\":350,\"width\":48,\"height\":48},\"resourceId\":\"end9\",\"childShapes\":[],\"stencil\":{\"id\":\"end\"},\"target\":[],\"properties\":{\"resourceId\":\"end9\",\"name\":\"\"},\"formConfig\":{\"fieldAssignment\":{},\"fieldControl\":{},\"taskFormButton\":{},\"taskChildFunc\":{}}}]}";

    @Test
    public void convertToJson() throws IOException {
        runtimeService.executeEarlyWarningJob();
    }



    @Test
    public void convertToBpmnModel() throws IOException {
        BpmnJsonConverter bpmnJsonConverter = new BpmnJsonConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/decide.bpmn20.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode modelNode = mapper.readTree(resourceStreamSource.getInputStream());
        bpmnModel = bpmnJsonConverter.convertToBpmnModel(modelNode);
        System.out.println(1111);
    }

    @Test
    public void convertToXml() throws IOException {
        BpmnJsonConverter bpmnJsonConverter = new BpmnJsonConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/decide.bpmn20.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode modelNode = mapper.readTree(resourceStreamSource.getInputStream());
        bpmnModel = bpmnJsonConverter.convertToBpmnModel(modelNode);
//        new BpmnAutoLayout(bpmnModel).execute();
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String bytes = new String(convertToXML);
        System.out.println(bytes);
    }


    @Test
    public void convertToBpmnModel2() {
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/decide.bpmn20.xml");
        bpmnModel = bpmnXMLConverter.convertToBpmnModel(resourceStreamSource, false, false);
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String xml = new String(convertToXML);
        System.out.println(xml);
    }

    /**
     * 修改model
     */
    @Test
    public void updateModel() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_SAVE_OPERATOR.getId());
        operator.getParamDesc();
        Map<String, Object> params = new HashMap<>();
        params.put("modelId", "18897e56-e214-11ec-a5ec-5254004c35e5");
        params.put("editorSourceValueId", "18897e55-e214-11ec-a5ec-5254004c35e5");
        params.put("metaInfo", json);
        params.put("funcCode", "TEST_PTLC");
        params.put("funcName", "请假申请");
        params.put("category", "test");
        params.put("runModeName", "生产环境模式");
        params.put("runModeCode", "PRODUCT");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessModelResult) {
            ProcessModelResult processInstanceResult = (ProcessModelResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        }

//        Operator operator2 = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_DEPLOY_OPERATOR.getId());
//        Map<String, Object> params2 = new HashMap<>();
//        params2.put("modelId", "a8bac1c5-b970-11ec-ba93-1c1b0dcdefb3");
//        Result result2 = operator2.operate(params);
//        Assert.assertNotNull(result2);
    }

    /**
     * 启动model
     */
    @Test
    public void start() throws PayloadValidErrorException {
        String prod = "meta";
        String key = "8MAJv2vaPEVej1oHNfX";
        String beanId = "1";
        processOperatorService.start(prod, key, beanId);
    }

    /**
     * 提交
     */
    @Test
    public void submit() throws PayloadValidErrorException {
        String piid = "792b08e0-d448-11ec-aff2-005056c00001";
        String beanId = "36";
        String taskId = "792b08e7-d448-11ec-aff2-005056c00001";
        String assignee = "zhangsan,lisi,wangwu";
        String prod = "meta";
//        String target = "s2";
        TaskListResult listResult = taskOperatorService.submit(prod, piid, beanId, taskId, assignee, "提交", null);
    }

    /**
     * 领取任务
     */
    @Test
    public void claim() throws PayloadValidErrorException {
        String piid = "c0a74a98-e8bb-11ec-8b97-005056c00001";
        String beanId = "sSn6pl2JLJpj8mZNT80";
        String taskId = "c0ad1709-e8bb-11ec-8b97-005056c00001";
        String prod = "meta";
        taskOperatorService.claim(prod, piid, beanId, taskId);
    }


    /**
     * 提交2
     */
    @Test
    public void submit2() throws PayloadValidErrorException {
        String piid = "37e6f8e8-bed8-11ec-8f40-1c1b0dcdefb3";
        String beanId = "54";
        String taskId = "56908dd2-bed8-11ec-87a6-1c1b0dcdefb3";
        String assignee = "zhangsan";
        String prod = "meta";
        String target = "s3";
        TaskListResult listResult = taskOperatorService.submit(prod, piid, beanId, taskId, assignee, "提交", target);
    }


    /**
     * 根据funcCode，获取流程初始按钮
     */
    @Test
    public void getProcessInitialButton() throws PayloadValidErrorException {
        String funcCode = "qjsq";
        String userId = "system";
        String beanId = "36";
        Map<String, Object> bean = new HashMap<>();
        Result result = processOperatorService.getButton(funcCode, userId, beanId, bean);
        if (result instanceof ProcessButtonListResult) {
            ProcessButtonListResult processButtonListResult = (ProcessButtonListResult) result;
            List<ProcessRunForm> list = processButtonListResult.getEntity();
            for (ProcessRunForm processRunForm : list) {
                List<Button> buttonList = processRunForm.getList();
                for (Button button : buttonList) {
                    if (button instanceof TaskButton) {
                        System.out.println("taskId:" + ((TaskButton) button).getTaskId());
                    }
                    System.out.println("pdid:" + button.getPdid());
                    System.out.println("buttonName:" + button.getName());
                    System.out.println("OperationId:" + button.getOperationId());
                    System.out.println("piid:" + button.getPiid());
                    System.out.println("code:" + button.getCode());
                }
                System.out.println("--------------------------------------------------");
            }
        }
        Assert.assertNotNull(result);
    }


}
