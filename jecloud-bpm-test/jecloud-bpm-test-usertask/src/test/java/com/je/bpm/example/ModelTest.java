/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.task.KaiteTaskCategoryEnum;
import com.je.bpm.engine.HistoryService;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.history.HistoricActivityInstance;
import com.je.bpm.engine.history.HistoricProcessInstance;
import com.je.bpm.engine.history.HistoricTaskInstance;
import com.je.bpm.model.process.ModelOperatorService;
import com.je.bpm.model.process.model.*;
import com.je.bpm.model.process.results.*;
import com.je.bpm.model.shared.Result;
import com.je.bpm.model.shared.model.Button;
import com.je.bpm.model.task.model.TaskButton;
import com.je.bpm.model.task.result.TaskListResult;
import com.je.bpm.runtime.process.ProcessOperatorService;
import com.je.bpm.runtime.shared.operator.Operator;
import com.je.bpm.runtime.shared.operator.OperatorRegistry;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;
import com.je.bpm.runtime.task.TaskOperatorService;
import org.assertj.core.util.Strings;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 控制台test
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ModelTest {
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;
    @Autowired
    private ProcessOperatorService processOperatorService;
    @Autowired
    private TaskOperatorService taskOperatorService;
    @Autowired
    private ModelOperatorService modelOperatorService;
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private RuntimeService runtimeService;

    @Before
    public void setUser() {
        securityUtil.logInAs("7cf7e4b9a8864354b58c813338ca0e1f");
    }

    static String json = "{\"nodeMaps\":{},\"jebpm\":{\"id\":\"JEBPM\",\"url\":\"http://jepaas.com\"},\"bounds\":{\"lowerRight\":{\"x\":1485,\"y\":700},\"upperLeft\":{\"x\":0,\"y\":0}},\"resourceId\":\"canvas\",\"properties\":{\"JE_WORKFLOW_PROCESSINFO_ID\":\"7e98ee135019458191b4c11208eb9a5e\",\"process_id\":\"wo18jgPFMWQN0VTZXjL\",\"name\":\"测试普通流程\",\"processBasicConfig\":{\"name\":\"测试普通流程\",\"funcCode\":\"TEST_PTLC\",\"funcName\":\"普通流程\",\"funcId\":\"U8ymbZqVoKTdMMdE5Ss\",\"tableCode\":\"TEST_PTLC\",\"SY_PRODUCT_ID\":\"yxljlUCJu9fcxX7tRAi111\",\"SY_PRODUCT_CODE\":\"test\",\"SY_PRODUCT_NAME\":\"测试服务\",\"attachedFuncCodes\":\"\",\"attachedFuncNames\":\"\",\"attachedFuncIds\":\"\",\"contentModule\":\"11111\",\"processClassificationName\":\"12312\",\"processClassificationCode\":\"\",\"processClassificationId\":\"\",\"deploymentEnvironment\":\"YF\"},\"startupSettings\":{\"canEveryoneStart\":\"1\",\"canEveryoneRolesId\":\"\",\"canEveryoneRolesName\":\"\",\"startExpression\":\"\",\"startExpressionFn\":\"\"},\"extendedConfiguration\":{\"canSponsor\":\"0\",\"canRetrieve\":\"1\",\"canReturn\":\"1\",\"canUrged\":\"1\",\"canCancel\":\"1\",\"canInvalid\":\"1\",\"canTransfer\":\"1\",\"canDelegate\":\"1\",\"easyLaunch\":\"1\",\"simpleApproval\":\"1\",\"hideStateInfo\":\"1\",\"hideTimeInfo\":\"1\"},\"customEventListeners\":[],\"messageSetting\":{\"messageDefinitions\":{\"flowBacklog\":\"\",\"title\":\"流程[{@PROCESS_NAME@}]提醒\",\"dwr\":\"流程提醒：由<font color=red>{@USER_NAME@}</font>在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}任务,执行操作：{@SUBMIT_OPERATE@},执行意见：\",\"email\":\"你有一条任务需要审批!\\n由{@USER_NAME@}在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}流程任务,执行动作：{@SUBMIT_OPERATE@},执行意见：{@SUBMIT_COMMENTS@}\",\"note\":\"流程提醒：由{@USER_NAME@}在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}任务，请尽快处理!\",\"thirdparty\":\"你有一条任务需要审批!\\n由{@USER_NAME@}在{@NOW_TIME@}给您{@SUBMIT_OPERATE@}了{@PROCESS_NAME@}流程任务,执行动作：{@SUBMIT_OPERATE@},执行意见：{@SUBMIT_COMMENTS@}\"},\"messages\":[\"PUSH\"]},\"processButtonConfig\":[]},\"childShapes\":[{\"child\":[],\"bounds\":{},\"resourceId\":\"line7\",\"childShapes\":[],\"stencil\":{\"id\":\"line\"},\"target\":[{\"resourceId\":\"task5\"}],\"properties\":{\"resourceId\":\"line7\",\"name\":\"\"},\"styleConfig\":{\"dashed\":\"0\",\"strokeColor\":\"\",\"fontSize\":\"12\",\"fontFamily\":\"宋体\",\"fontColor\":\"\"},\"source\":[{\"resourceId\":\"start4\"}]},{\"child\":[],\"bounds\":{\"x\":-190,\"y\":342,\"width\":48,\"height\":48},\"resourceId\":\"start4\",\"childShapes\":[],\"stencil\":{\"id\":\"start\"},\"target\":[{\"resourceId\":\"line7\"}],\"properties\":{\"resourceId\":\"start4\",\"name\":\"\"}},{\"child\":[],\"bounds\":{},\"resourceId\":\"line8\",\"childShapes\":[],\"stencil\":{\"id\":\"line\"},\"target\":[{\"resourceId\":\"task6\"}],\"properties\":{\"resourceId\":\"line8\",\"name\":\"\"},\"styleConfig\":{\"dashed\":\"0\",\"strokeColor\":\"\",\"fontSize\":\"12\",\"fontFamily\":\"宋体\",\"fontColor\":\"\"},\"source\":[{\"resourceId\":\"task5\"}]},{\"child\":[],\"bounds\":{\"x\":-20,\"y\":350,\"width\":80,\"height\":40},\"resourceId\":\"task5\",\"childShapes\":[],\"stencil\":{\"id\":\"task\"},\"target\":[{\"resourceId\":\"line8\"}],\"properties\":{\"categorydefinition\":\"kaiteUserTask\",\"name\":\"任务\",\"formSchemeName\":\"\",\"formSchemeId\":\"\",\"listSynchronization\":\"0\",\"retrieve\":\"0\",\"Urge\":\"0\",\"invalid\":\"0\",\"transfer\":\"0\",\"delegate\":\"0\",\"formEditable\":\"0\",\"remind\":\"0\",\"simpleApproval\":\"0\",\"selectAll\":\"0\",\"asynTree\":\"0\",\"jump\":\"0\",\"logicalJudgment\":\"0\"},\"assignmentConfig\":{\"resource\":[],\"referToCode\":\"LOGUSER\"},\"commitBreakdown\":[],\"dismissConfig\":{\"enable\":\"0\",\"dismissTaskNames\":\"\",\"dismissTaskIds\":\"\",\"commitBreakdown\":[],\"directSendAfterDismiss\":\"0\",\"forceCommitAfterDismiss\":\"0\",\"disableSendAfterDismiss\":\"0\",\"noReturn\":\"0\",\"directSendAfterReturn\":\"0\"},\"earlyWarningAndPostponement\":{\"enable\":\"0\",\"resource\":[]},\"passRoundConfig\":{\"enable\":\"0\",\"auto\":\"0\",\"circulationRules\":[]},\"buttonsConfig\":[],\"customEventListeners\":[],\"addSignature\":{\"enable\":\"0\",\"unlimited\":\"0\",\"notCountersigned\":\"0\",\"mandatoryCountersignature\":\"0\",\"circulationRules\":[]},\"approvalNotice\":{\"processInitiator\":\"1\",\"thisNodeApproved\":\"0\",\"thisNodeApprovalDirectlyUnderLeader\":\"0\",\"thisNodeApprovalDeptLeader\":\"0\"},\"formConfig\":{\"fieldAssignment\":{},\"fieldControl\":{},\"taskFormButton\":{},\"taskChildFunc\":{}}},{\"child\":[],\"bounds\":{},\"resourceId\":\"line10\",\"childShapes\":[],\"stencil\":{\"id\":\"line\"},\"target\":[{\"resourceId\":\"end9\"}],\"properties\":{\"resourceId\":\"line10\",\"name\":\"\"},\"styleConfig\":{\"dashed\":\"0\",\"strokeColor\":\"\",\"fontSize\":\"12\",\"fontFamily\":\"宋体\",\"fontColor\":\"\"},\"source\":[{\"resourceId\":\"task6\"}]},{\"child\":[],\"bounds\":{\"x\":200,\"y\":350,\"width\":80,\"height\":40},\"resourceId\":\"task6\",\"childShapes\":[],\"stencil\":{\"id\":\"task\"},\"target\":[{\"resourceId\":\"line10\"}],\"properties\":{\"categorydefinition\":\"kaiteUserTask\",\"name\":\"任务\",\"formSchemeName\":\"\",\"formSchemeId\":\"\",\"listSynchronization\":\"1\",\"retrieve\":\"0\",\"Urge\":\"1\",\"invalid\":\"1\",\"transfer\":\"1\",\"delegate\":\"1\",\"formEditable\":\"1\",\"remind\":\"1\",\"simpleApproval\":\"1\",\"selectAll\":\"1\",\"asynTree\":\"1\",\"jump\":\"0\",\"logicalJudgment\":\"1\"},\"assignmentConfig\":{\"resource\":[{\"entryPath\":\"\",\"type\":\"roleConfig\",\"name\":\"按角色处理\",\"resourceName\":\"测试使用权限组角色\",\"resourceCode\":\"role-000022\",\"resourceId\":\"SA5WhCvWx0CnFNWxJnx\",\"id\":\"eyF30T20hEM8AHBssmQ\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_134\",\"resourceIcon\":\"fal fa-user\",\"permission\":{\"company\":\"0\",\"companySupervision\":\"0\",\"dept\":\"0\",\"deptAll\":\"0\",\"directLeader\":\"0\",\"deptLeader\":\"0\",\"supervisionLeader\":\"0\",\"sql\":\"\",\"sqlRemarks\":\"\"},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"departmentConfig\",\"name\":\"按部门处理\",\"resourceName\":\"系统管理部\",\"resourceCode\":\"系统管理部\",\"resourceId\":\"UO7MyyTuC1QkHqbg6Hf\",\"id\":\"OkK1ONNcoatGdbm9IbT\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_135\",\"resourceIcon\":\"fal fa-address-book\",\"permission\":{\"company\":\"0\",\"companySupervision\":\"0\",\"dept\":\"0\",\"deptAll\":\"0\",\"directLeader\":\"0\",\"deptLeader\":\"0\",\"supervisionLeader\":\"0\",\"sql\":\"\",\"sqlRemarks\":\"\"},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"userConfig\",\"name\":\"按人员处理\",\"resourceName\":\"吝志超,刘利军,白梦浩,云凤程,刘利军,测试用户1,白梦浩,赵宝利,刘旭,赵宝利\",\"resourceCode\":\"linzc,admin,baimh,yunfc,admin,test,baimh,zhaobl,liux,zhaobl\",\"resourceId\":\"1f3dce9448864704abf350d78fa2e7a9,1f3f30f816e14fb19e11d8087a2f3916,3685abdc0ebd4a33bbd50ce32114dc57,3a3f82969cac4ddbb7527f1315919ee6,3e156a488cda44f680368e9b30f0fda9,805296a89ebe44e8ad9a8f77a527f44c,c1f57db5ade8440d8ffd70a722e6772d,d7ef93a0cf594394b0f91846bb9d51c7,f4a7622800da48b78682915831016d97,f7779d8432e44d0581f4778311f1a1ac\",\"id\":\"6WPxS6rjZYTgeVCwYpK\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_136\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"specialConfig\",\"name\":\"特殊处理\",\"resourceName\":\"当前登录人,直属领导,本部门领导,本部门监管领导,流程启动人,任务指派人,任务指派人直属领导,前置任务指派人,前置任务指派人直属领导,本部门内人员,本部门内(含子)人员,监管部门内人员,本公司领导,本公司监管领导,所在子公司（展示组织结构）\",\"resourceCode\":\"LOGINED_USER,DIRECT_LEADER,DEPT_LEADER,DEPT_MONITOR_LEADER,STARTER_USER,TASK_ASSGINE,TASK_ASSGINE_HEAD,PREV_ASSIGN_USER,PREV_ASSIGN_USER_DIRECT_LEADER,DEPT_USERS,DEPT_ALL_USERS,DEPT_MONITOR_USERS,COMPANY_LEADERS,COMPANY_MONITOR_LEADERS,SUBSIDIARY\",\"resourceId\":\"25042a25-3eae-4ba3-9a78-b56687ef5381,QalFCpT2ejQ2CzvwrjN,104c9ba4-109e-4360-a945-3446a07a7597,PnBzZEFrF57IeNUuBFt,c17afcb2-e8ff-4920-99ed-78f2b56965ce,dfe426ce-592c-4a8e-a872-19c0d91a711e,f63fe016-9f1d-4efb-a96e-09431072108a,d31cde81-1d6c-4361-a554-534848e0c491,e91560e6-9b7a-486b-8315-e3281a722a5c,A0NpRyDX0tS1TTcVT7E,YEFaWPqhjP7tCtmw7jV,PFrq2u3cl3mkn9FxRUH,19HaFGHWkpkFQsWIZL9,MKfTcSNMfsmQz5PNi8s,hIOYtizTSn7vMivcTwp\",\"id\":\"fz4nvwPsWAn4gA3HML0\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_137\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"formFieldConfig\",\"name\":\"表单字段\",\"resourceName\":\"登记人主键\",\"resourceCode\":\"SY_CREATEUSERID\",\"resourceId\":\"20de6c07e20a470d9361609b7fcc8bab\",\"id\":\"DhpiEe499Hvhqxbbdsp\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_138\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"orgTreeConfig\",\"name\":\"组织架构\",\"resourceName\":\"\",\"resourceCode\":\"\",\"resourceId\":\"\",\"id\":\"feWkvEug0DCNus6lbrz\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_139\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"businessSqlAssignmentConfig\",\"name\":\"用户SQL\",\"resourceName\":\"select * from JE_RBAC_VACCOUNTDEPT\",\"resourceCode\":\"\",\"resourceId\":\"\",\"id\":\"mDnjA5Sl9SecQsY3710\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_140\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"select * from JE_RBAC_VACCOUNTDEPT\",\"serviceName\":\"\",\"methodName\":\"\"},{\"entryPath\":\"\",\"type\":\"customConfig\",\"name\":\"自定义\",\"resourceName\":\"testservice\",\"resourceCode\":\"\",\"resourceId\":\"\",\"id\":\"q1GLiaEmZGilJ7ZtIvg\",\"__action__\":\"doInsert\",\"_X_ROW_KEY\":\"row_141\",\"resourceIcon\":\"\",\"permission\":{},\"sql\":\"\",\"serviceName\":\"testservice\",\"methodName\":\"aaa\"}],\"referToCode\":\"LOGUSER\"},\"commitBreakdown\":[],\"dismissConfig\":{\"enable\":\"0\",\"dismissTaskNames\":\"\",\"dismissTaskIds\":\"\",\"commitBreakdown\":[],\"directSendAfterDismiss\":\"0\",\"forceCommitAfterDismiss\":\"0\",\"disableSendAfterDismiss\":\"0\",\"noReturn\":\"0\",\"directSendAfterReturn\":\"0\"},\"earlyWarningAndPostponement\":{\"enable\":\"0\",\"resource\":[]},\"passRoundConfig\":{\"enable\":\"0\",\"auto\":\"0\",\"circulationRules\":[]},\"buttonsConfig\":[],\"customEventListeners\":[],\"addSignature\":{\"enable\":\"0\",\"unlimited\":\"0\",\"notCountersigned\":\"0\",\"mandatoryCountersignature\":\"0\",\"circulationRules\":[]},\"approvalNotice\":{\"processInitiator\":\"1\",\"thisNodeApproved\":\"0\",\"thisNodeApprovalDirectlyUnderLeader\":\"0\",\"thisNodeApprovalDeptLeader\":\"0\"},\"formConfig\":{\"fieldAssignment\":{},\"fieldControl\":{},\"taskFormButton\":{},\"taskChildFunc\":{}}},{\"child\":[],\"bounds\":{\"x\":430,\"y\":350,\"width\":48,\"height\":48},\"resourceId\":\"end9\",\"childShapes\":[],\"stencil\":{\"id\":\"end\"},\"target\":[],\"properties\":{\"resourceId\":\"end9\",\"name\":\"\"},\"formConfig\":{\"fieldAssignment\":{},\"fieldControl\":{},\"taskFormButton\":{},\"taskChildFunc\":{}}}]}";

    @Test
    public void convertToBpmnModel() {
        System.out.println(1);
    }

    /**
     * 获取model payLoad参数
     */
    @Test
    public void getSaveModelPlayLod() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_SAVE_OPERATOR.getId());
        OperationParamDesc operationParamDesc = operator.getParamDesc();
        System.out.println(operationParamDesc.toString());
    }

    /**
     * 保存model
     */
    @Test
    public void saveModel() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_SAVE_OPERATOR.getId());
        Map<String, Object> params = new HashMap<>();
        params.put("metaInfo", json);
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessModelResult) {
            ProcessModelResult processInstanceResult = (ProcessModelResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        }
    }

    /**
     * 获取model
     */
    @Test
    public void getModel() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_GET_OPERATOR.getId());
        Map<String, Object> params = new HashMap<>();
        params.put("modelId", "198ffbff-af25-11ec-9bc9-005056c00001");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessModelResult) {
            ProcessModelResult processInstanceResult = (ProcessModelResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getEditorSourceValueId());
        }
    }

    /**
     * 获取model
     */
    @Test
    public void getLastModelByKey() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_GET_OPERATOR.getId());
        Map<String, Object> params = new HashMap<>();
        params.put("modelId", "198ffbff-af25-11ec-9bc9-005056c00001");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessModelResult) {
            ProcessModelResult processInstanceResult = (ProcessModelResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getEditorSourceValueId());
        }
    }

    /**
     * 修改model
     */
    @Test
    public void updateModel() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_SAVE_OPERATOR.getId());
        operator.getParamDesc();
        Map<String, Object> params = new HashMap<>();
        params.put("modelId", "ab42fec3-e007-11ec-84d2-005056c00001");
        params.put("editorSourceValueId", "ab42d7b2-e007-11ec-84d2-005056c00001");
        params.put("metaInfo", json);
        params.put("funcCode", "WJS_PTB");
        params.put("funcName", "请假申请");
        params.put("category", "test");
        params.put("runModeName", "生产环境模式");
        params.put("runModeCode", "PRODUCT");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessModelResult) {
            ProcessModelResult processInstanceResult = (ProcessModelResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        }
    }

    /**
     * 获取model列表数据
     */
    @Test
    public void getModels() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_LIST_OPERATOR.getId());
        operator.getParamDesc();
        Map<String, Object> params = new HashMap<>();
        params.put("name", "模型名称haha");
        params.put("key", "模型key");
        params.put("multipleConditionsOrQuery", "1");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessModelListResult) {
            ProcessModelListResult processModelListResult = (ProcessModelListResult) result;
            List<com.je.bpm.model.process.model.Model> list = processModelListResult.getEntity();
            for (com.je.bpm.model.process.model.Model model : list) {
                System.out.println(model.getName());
            }
        }
    }


    /**
     * 部署model
     */
    @Test
    public void modelDeploy() throws PayloadValidErrorException {
        modelOperatorService.modelDeploy("18897e56-e214-11ec-a5ec-5254004c35e5");
    }

    /**
     * 根据funcCode，获取流程初始按钮
     */
    @Test
    public void getProcessInitialButton() throws PayloadValidErrorException {
        String funcCode = "TEST_PTLC";
        String userId = "7cf7e4b9a8864354b58c813338ca0e1f";
        String beanId = "25f38bcf79314338b92624124b4feba3";
        Map<String, Object> bean = new HashMap<>();
        Result result = processOperatorService.getButton(funcCode, userId, beanId, bean);
        if (result instanceof ProcessButtonListResult) {
            ProcessButtonListResult processButtonListResult = (ProcessButtonListResult) result;
            List<ProcessRunForm> list = processButtonListResult.getEntity();
            for (ProcessRunForm processRunForm : list) {
                List<Button> buttonList = processRunForm.getList();
                System.out.println(processRunForm.getWorkFlowConfig().getCurrentNodeName());
                for (Button button : buttonList) {
                    if (button instanceof TaskButton) {
                        System.out.println("taskId:" + ((TaskButton) button).getTaskId());
                    }
                    System.out.println("pdid:" + button.getPdid());
                    System.out.println("buttonName:" + button.getName());
                    System.out.println("OperationId:" + button.getOperationId());
                    System.out.println("piid:" + button.getPiid());
                    System.out.println("code:" + button.getCode());
                }
                System.out.println("--------------------------------------------------");
            }
        }
        Assert.assertNotNull(result);
    }

    /**
     * 发起
     */
    @Test
    public void sponsor() throws PayloadValidErrorException {
        String prod = "meta";
        String user = "[{nodeId:\"taskETjeoru33FkDXXdUfTZ\",nodeName:\"财务部\",assignee:\"7cf7e4b9a8864354b58c813338ca0e1f\",assigneeName:\"于春辉\"}]";
        Map<String, Object> bean = new HashMap<>();
        bean.put("JBSQ_JBTS","3");
        String processDefinitionId = "FO7AHYyPz1rzrl4TBct:1:bb9a625b-0989-11ed-96aa-0242f87b94e8";
        String beanId = "54e19cda0e024a8da6b7ad1ec597a5e7";
        String target = "linesWB65d70xcAaPiK6Ui1";
        String comment = "同意";
    }

    /**
     * 启动model
     */
    @Test
    public void start() throws PayloadValidErrorException {
        String prod = "test";
        String pdid = "Process_29ISOgVUe:2:819071df-479c-11ec-9aa8-acde48001122";
        String beanId = "3";
        processOperatorService.start(prod, pdid, beanId);
    }

    /**
     * 撤销model
     */
    @Test
    public void cancel() throws PayloadValidErrorException {
        String prod = "meta";
        String pdid = "zOIqwdppmaac1Nr3dCQ:3:eb520e22-e25a-11ec-b2d5-0242f87b94e8";
        String beanId = "f4ae74635e0d42a5bd54d85dce14d3ee";
        String piid = "627df7bf-e3de-11ec-a4af-005056c00001";
        processOperatorService.cancel(prod, pdid, piid, beanId);
    }

    @Test
    public void submit() throws PayloadValidErrorException {
        String piid = "7d38933c-f460-11ec-be89-5254004c35e5";
        String beanId = "de31e0f2e80e4a2785b5981edf491ba1";
        String taskId = "7d674470-f460-11ec-be89-5254004c35e5";
        String assignee = "[{nodeId:\"tasktvY2eRkqCbSP5pCzq3Y\",nodeName:\"任务3\",assignee:\"1f3dce9448864704abf350d78fa2e7a9\",assigneeName:\"吝志超\"},{nodeId:\"taskGRrUGE8cOv6emLRxwZD\",nodeName:\"任务4\",assignee:\"1f3dce9448864704abf350d78fa2e7a9\",assigneeName:\"吝志超\"},{nodeId:\"taskGQY7SO24xvLV6YLC76J\",nodeName:\"任务5\",assignee:\"1f3dce9448864704abf350d78fa2e7a9\",assigneeName:\"吝志超\"}]";
        String prod = "test";
        String target = "lineEjdcwON4q2NS5FcbCwx";
        TaskListResult listResult = taskOperatorService.submit(prod, piid, beanId, taskId, assignee, "同意", target);
    }

    /**
     * 获取提交的节点和名称
     */
    @Test
    public void getSubmitOutGoingNode() throws PayloadValidErrorException {
        String taskId = "";
        String pdid = "GlQU0aj2e1KaxCWcdO3:1:149ee75e-f61f-11ec-a974-0242f87b94e8";
        Map<String, Object> bean = new HashMap<>();
        String prod = "test";
        ProcessNextElementResult processNextElementResult = processOperatorService.getSubmitOutGoingNode(taskId, pdid, prod, bean,"");
        List<ProcessNextNodeInfo> list = processNextElementResult.getEntity();
        for (ProcessNextNodeInfo processNextNodeInfo : list) {
            System.out.println("target:" + processNextNodeInfo.getTarget());
            System.out.println("nodeId:" + processNextNodeInfo.getNodeId());
            System.out.println("nodeName:" + processNextNodeInfo.getNodeName());
        }
        System.out.println("----------------------------------------------------");
    }

    /**
     * 获取提交人信息
     */
    @Test
    public void getSubmitOutGoingNodeAssignee() throws PayloadValidErrorException {
//        String taskId = "";
        String taskId = "24d57a8d-e7e5-11ec-9331-0242f87b94e8";
        String pdid = "zOIqwdppmaac1Nr3dCQ:27:09a09a7f-e7e5-11ec-9331-0242f87b94e8";
        String target = "lineMQPnBCK0KDsM7YR5BTw";
        Map<String, Object> bean = new HashMap<>();
        String prod = "";
        ProcessNextElementAssigneeResult processNextElementResult = processOperatorService.getSubmitOutGoingNodeAssignee
                (taskId, pdid, prod, target, bean, "taskDelegateOperation","");
        ProcessNextNodeAssigneeInfo processNextNodeAssigneeInfo = processNextElementResult.getEntity();
        System.out.println(processNextNodeAssigneeInfo.getUser());
        System.out.println("----------------------------------------------------");
    }


    /**
     * 获取退回节点名称
     */
    @Test
    public void getGobackName() throws PayloadValidErrorException {
        String piid = "9ac4089f-f467-11ec-8322-0242f87b94e8";
        String taskId = "9fef46b6-f467-11ec-8322-0242f87b94e8";
        ProcessGobackElementResult result = processOperatorService.getGobackNodeName(piid, taskId);
        ProcessGobackNodeInfo processGobackNodeInfo = result.getEntity();
        System.out.println("nodeName:" + processGobackNodeInfo.getNodeName());
        System.out.println("----------------------------------------------------");
    }


    /**
     * 退回
     */
    @Test
    public void goBack() throws PayloadValidErrorException {
        String piid = "";
        String taskId = "4e130076-ed74-11ec-971b-0242f87b94e8";
        String prod = "";
        String beanId = "";
        String comment = "";
        taskOperatorService.goback(prod, piid, beanId, taskId, comment);
    }

    /**
     * 获取取回节点名称
     */
    @Test
    public void getRetrieveName() throws PayloadValidErrorException {
        String piid = "8c9ff1d2-b703-11ec-a4c8-005056c00001";
        String taskId = "5e421142-b71f-11ec-9938-005056c00001";
        ProcessGobackElementResult result = processOperatorService.getRetrieveNodeName(piid, taskId);
        ProcessGobackNodeInfo processGobackNodeInfo = result.getEntity();
        System.out.println("nodeName:" + processGobackNodeInfo.getNodeName());
        System.out.println("----------------------------------------------------");
    }

    /**
     * 取回
     */
    @Test
    public void retrieveBtn() throws PayloadValidErrorException {
        String piid = "5d9640d3-b71f-11ec-9938-005056c00001";
        String taskId = "5e421142-b71f-11ec-9938-005056c00001";
        String prod = "";
        String beanId = "";
        taskOperatorService.retrieve(prod, piid, beanId, taskId);
    }

    /**
     * 作废
     */
    @Test
    public void invalid() throws PayloadValidErrorException {
        String piid = "677ffc56-b017-11ec-9db5-005056c00001";
        String taskId = "e058fbb4-b15f-11ec-a1ba-005056c00001";
        String pdid = "";
        String prod = "";
        String beanId = "";
        processOperatorService.invalid(prod, pdid, piid, beanId);
    }


    /**
     * 获取委托节点信息
     */
    @Test
    public void getDelegateNodeInfo() throws PayloadValidErrorException {
        String piid = "09fa26b1-e7d2-11ec-8fbb-0242f87b94e8";
        String taskId = "679beb7d-e7d3-11ec-8069-005056c00001";
        ProcessDelegateElementResult result = processOperatorService.getDelegateNode(piid, taskId);
        ProcessNextNodeInfo processNextNodeInfo = result.getEntity();
        System.out.println("nodeName:" + processNextNodeInfo.getNodeName());
        System.out.println("----------------------------------------------------");
    }

    /**
     * 委托
     */
    @Test
    public void delegate() throws PayloadValidErrorException {
        String piid = "0d21b243-eddd-11ec-8783-0242f87b94e8";
        String taskId = "0f7195c9-eddd-11ec-8783-0242f87b94e8";
        String prod = "";
        String beanId = "c906be5656714441919bacd5f04f9cbe";
        String assignee = "system1";
        String comment = "委托";
        taskOperatorService.delegate(prod, piid, beanId, taskId, assignee, comment);
    }


    /**
     * 取消委托
     */
    @Test
    public void cancelDelegate() throws PayloadValidErrorException {
        String piid = "6f5c1b6b-afed-11ec-9f77-005056c00001";
        String taskId = "c3d811f8-afed-11ec-8acb-005056c00001";
        String prod = "";
        String beanId = "";
        taskOperatorService.cancelDelegate(prod, piid, beanId, taskId);
    }

    /**
     * 转办
     */
    @Test
    public void transfer() throws PayloadValidErrorException {
        String piid = "6f5c1b6b-afed-11ec-9f77-005056c00001";
        String taskId = "c3d811f8-afed-11ec-8acb-005056c00001";
        String prod = "";
        String beanId = "";
        String assignee = "system1";
        taskOperatorService.transfer(prod, piid, beanId, taskId, assignee);
    }

    /**
     * 催办
     */
    @Test
    public void urge() throws PayloadValidErrorException {
        String piid = "6f5c1b6b-afed-11ec-9f77-005056c00001";
        String taskId = "c3d811f8-afed-11ec-8acb-005056c00001";
        String prod = "";
        String beanId = "";
        List<String> copyUsers = new ArrayList<>();
        copyUsers.add("system");
//        taskOperatorService.urge(prod, piid, beanId, taskId, copyUsers);
    }

    /**
     * 获取驳回信息
     */
    @Test
    public void getDismissOutGoingNode() throws PayloadValidErrorException {
        String piid = "55727532-f36a-11ec-9313-5254004c35e5";
        String taskId = "d43f3f1d-f384-11ec-ba9d-0242f87b94e8";
        String pdid = "h6GzfvgZHijzuaGSB75:3:8c1928b1-f368-11ec-9313-5254004c35e5";
        Map<String, Object> bean = new HashMap<>();
        ProcessDismissElementResult processDismissElementResult = processOperatorService.getDismissOutGoingNode(piid, taskId, pdid, bean);
        List<ProcessDismissNodeInfo> list = processDismissElementResult.getEntity();
        for (ProcessDismissNodeInfo processDismissNodeInfo : list) {
            System.out.println("nodeName:" + processDismissNodeInfo.getNodeName());
            System.out.println("nodeId:" + processDismissNodeInfo.getNodeId());
            System.out.println("----------------------------------------------------");
        }
    }

    /**
     * 驳回
     */
    @Test
    public void dismiss() throws PayloadValidErrorException {
        String piid = "e0372e86-e86b-11ec-b490-0242f87b94e8";
        String taskId = "e1933b3a-e86b-11ec-b490-0242f87b94e8";
        String prod = "meta";
        String beanId = "ECdjCOBVIIgkIaHUG92";
        String targetDefinitionKey = "taskZ2m7WJPXAay0dP0blsH";
        String comment = "驳回";
        taskOperatorService.dismiss(prod, piid, beanId, taskId, targetDefinitionKey, comment);
    }

    /**
     * 直送
     */
    @Test
    public void directSend() throws PayloadValidErrorException {
        String piid = "e0372e86-e86b-11ec-b490-0242f87b94e8";
        String taskId = "042750a9-e86c-11ec-b6b1-005056c00001";
        String prod = "meta";
        String beanId = "ECdjCOBVIIgkIaHUG92";
        taskOperatorService.taskDirectSend(prod, piid, beanId, taskId);
    }

    /**
     * 获取直送节点名称
     */
    @Test
    public void getDirectSendName() throws PayloadValidErrorException {
        String piid = "55727532-f36a-11ec-9313-5254004c35e5";
        String taskId = "d43f3f1d-f384-11ec-ba9d-0242f87b94e8";
        ProcessGobackElementResult result = processOperatorService.getDismissNodeName(piid, taskId);
        ProcessGobackNodeInfo processGobackNodeInfo = result.getEntity();
        System.out.println("nodeName:" + processGobackNodeInfo.getNodeName());
        System.out.println("----------------------------------------------------");
    }


    /**
     * 启动model
     */
    @Test
    public void start2() throws PayloadValidErrorException {
        String prod = "meta";
        String key = "P6Fz6X3IlkeZzrdSNTj:1:f2766fe6-ecb0-11ec-a28a-0242f87b94e8";
        String beanId = "1";
        processOperatorService.start(prod, key, beanId);
    }

    /**
     * 提交2
     */
    @Test
    public void submit2() throws PayloadValidErrorException {
        String piid = "e570ee65-ed51-11ec-9895-005056c00001";
        String taskId = "55b48e74-ed52-11ec-84c6-005056c00001";
        String beanId = "1";
        String assignee = "[{nodeId:\"task6C9u9Du4RIlQrxiZOg3\",nodeName:\"任务\",assignee:\"f4a7622800da48b78682915831016d97\",assigneeName:\"刘旭\"}]";
        String prod = "meta";
        String target = "";
        TaskListResult listResult = taskOperatorService.submit(prod, piid, beanId, taskId, assignee, "提交", target);
    }

    /**
     * 传阅
     */
    @Test
    public void passroun() throws PayloadValidErrorException {
        String piid = "1f1322b2-b558-11ec-90c5-005056c00001";
        String beanId = "17";
        String taskId = "20826a27-b558-11ec-90c5-005056c00001";
        String prod = "meta";
        List<String> assigneeList = new ArrayList<>();
        assigneeList.add("zhangsan");
        taskOperatorService.passround(prod, piid, beanId, taskId, assigneeList);
    }


    /**
     * 审阅
     */
    @Test
    public void passroundRead() throws PayloadValidErrorException {
        String piid = "8825a6fc-b266-11ec-b416-005056c00001";
        String beanId = "16";
        String taskId = "87991b01-b26d-11ec-ba59-005056c00001";
        String prod = "meta";
        List<String> assigneeList = new ArrayList<>();
        String assignee = "zhangsan";
        taskOperatorService.passroundRead(prod, piid, beanId, taskId, assignee);
    }


    /**
     * 发起
     */
    @Test
    public void hang() {
        BpmnModel bpmnModel = processEngine.getRepositoryService().getBpmnModel("qingjiashenqing:7:cc978d67-b002-11ec-812b-005056c00001","","");
        System.out.println(bpmnModel);
//        processEngine.getRuntimeService().suspendProcessInstanceById("64e6113b-aa99-11ec-bf3d-005056c00001");
    }

    /**
     * 清空
     */
    @Test
    public void remove() {
        List<String> keys = new ArrayList<>();
        keys.add("ZqrtSy2vX7fMtSYh4cd");
        keys.add("oYwsfynK06vM3EiqipE");
        for (String key : keys) {
            List<com.je.bpm.engine.repository.Deployment> list = repositoryService.createDeploymentQuery().deploymentKey(key).list();
            //1.根据部署对象ID删除流程定义
            for (com.je.bpm.engine.repository.Deployment deployment : list) {
                String deployId = deployment.getId();
                List<com.je.bpm.engine.repository.ProcessDefinition> processDefinitions = repositoryService.createProcessDefinitionQuery().
                        processDefinitionId(deployId).list();
                for (com.je.bpm.engine.repository.ProcessDefinition processDefinition : processDefinitions) {
                    List<HistoricProcessInstance> histPiid = historyService.createHistoricProcessInstanceQuery().processDefinitionId(processDefinition.getId()).list();
                    for (HistoricProcessInstance historicProcessInstance : histPiid) {
                        //2.删除历史流程实例根据流程实例ID
                        historyService.deleteHistoricProcessInstance(historicProcessInstance.getId());
                        //3.删除正在运行中的流程根据流程实例ID
                    }
                }
                repositoryService.deleteDeployment(deployment.getId(), true);
            }
        }
    }


    /**
     * 获取流程历史
     */
    @Test
    public void getHistList() {

        // 历史流程实例表 "act_hi_procinst"
        // 历史节点表 act_hi_actinst
        // 历史意见表( act_hi_comment )
        // 历史流程人员表( act_ru_identitylink )
        // 历史任务实例表( act_hi_taskinst )
        String beanId = "6a760acf585b4dfea89bba692b496132";
        HistoryService historyService = processEngine.getHistoryService();
        List<HistoricProcessInstance> procinst = historyService.createHistoricProcessInstanceQuery().processInstanceBusinessKey(beanId).list();
        if (procinst.size() == 0) {
            return;
        }
        String piid = procinst.get(0).getId();
        List<HistoricActivityInstance> actinst = historyService.createHistoricActivityInstanceQuery().processInstanceId(piid).
                orderByHistoricActivityInstanceStartTime().asc().list();
        String actId = "";
        for (HistoricActivityInstance historicActivityInstance : actinst) {
            String actType = historicActivityInstance.getActivityType();
            if (actId.equals(historicActivityInstance.getActivityId())) {
                System.out.println("---------------------------");
                continue;
            } else {
                actId = historicActivityInstance.getActivityId();
            }
            if (Strings.isNullOrEmpty(historicActivityInstance.getActivityName())) {
                System.out.println(KaiteTaskCategoryEnum.getName(actType));
            } else {
                System.out.println("actName=" + historicActivityInstance.getActivityName());
            }
            System.out.println("actType=" + actType);
            if (Strings.isNullOrEmpty(KaiteTaskCategoryEnum.getName(actType))) {
                System.out.println("---------------------------");
                continue;
            }
            List<HistoricTaskInstance> tasks = processEngine.getHistoryService().createHistoricTaskInstanceQuery().processInstanceId(piid)
                    .taskDefinitionKey(historicActivityInstance.getActivityId()).orderByTaskCreateTime().asc().list();
            for (HistoricTaskInstance historicTaskInstance : tasks) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dateFormat.setLenient(false);
                System.out.println("处理人：" + historicTaskInstance.getAssignee());
                System.out.println("接收时间" + dateFormat.format(historicTaskInstance.getStartTime()));
                System.out.println("结束时间" + dateFormat.format(historicTaskInstance.getEndTime()));
                System.out.println("耗时" + historicTaskInstance.getDurationInMillis());
            }
            System.out.println("---------------------------");
        }
    }

}
