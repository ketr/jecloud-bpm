/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.task.assignment.RoleAssignmentConfigImpl;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.task.KaiteUserTask;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.impl.util.io.ResourceStreamSource;
import com.je.bpm.engine.repository.Deployment;
import com.je.bpm.engine.repository.DeploymentBuilder;
import com.je.bpm.engine.runtime.ProcessInstance;
import com.je.bpm.engine.task.Task;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class KaiteUserTaskJumpTest {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;

    private BpmnModel bpmnModel;

    @Before
    public void setUser(){
        securityUtil.logInAs("system");
        bpmnModel = new BpmnModel();
        Process process = new Process();
        process.setName("测试跳跃流程");
        process.setId("testJumpFlow");

        //开始节点的属性
        StartEvent startEvent=new StartEvent();
        startEvent.setId("start");
        startEvent.setName("start");

        RoleAssignmentConfigImpl manageRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        RoleAssignmentConfigImpl developRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        //节点1
        KaiteUserTask kaiteUserTask1 = new KaiteUserTask();
        kaiteUserTask1.setId("kaiteUserTask1");
        kaiteUserTask1.setName("凯特用户任务1");

        //节点2
        KaiteUserTask kaiteUserTask2 = new KaiteUserTask();
        kaiteUserTask2.setId("kaiteUserTask2");
        kaiteUserTask2.setName("凯特用户任务2");
        //设置跳跃
        List<String> canJumpList = new ArrayList<>();
        canJumpList.add("kaiteUserTask1");
        canJumpList.add("kaiteUserTask3");

        //节点3
        KaiteUserTask kaiteUserTask3 = new KaiteUserTask();
        kaiteUserTask3.setId("kaiteUserTask3");
        kaiteUserTask3.setName("凯特用户任务3");

        //结束节点属性
        EndEvent endEvent=new EndEvent();
        endEvent.setId("endEvent");
        endEvent.setName("endEvent");

        //连线信息
        List<SequenceFlow> sequenceFlows1=new ArrayList<SequenceFlow>();
        SequenceFlow s1=new SequenceFlow();
        s1.setId("start_to_kaiteUserTask1");
        s1.setName("start_to_kaiteUserTask1");
        s1.setSourceRef("start");
        s1.setTargetRef("kaiteUserTask1");
        sequenceFlows1.add(s1);

        List<SequenceFlow> sequenceFlows2=new ArrayList<SequenceFlow>();
        SequenceFlow s2=new SequenceFlow();
        s2.setId("kaiteUserTask1_kaiteUserTask2");
        s2.setName("kaiteUserTask1_kaiteUserTask2");
        s2.setSourceRef("kaiteUserTask1");
        s2.setTargetRef("kaiteUserTask2");
        sequenceFlows2.add(s2);

        List<SequenceFlow> sequenceFlows3=new ArrayList<SequenceFlow>();
        SequenceFlow s3=new SequenceFlow();
        s3.setId("kaiteUserTask2_kaiteUserTask3");
        s3.setName("kaiteUserTask2_kaiteUserTask3");
        s3.setSourceRef("kaiteUserTask2");
        s3.setTargetRef("kaiteUserTask3");
        sequenceFlows3.add(s3);

        List<SequenceFlow> toEnd=new ArrayList<SequenceFlow>();
        SequenceFlow s4=new SequenceFlow();
        s4.setId("kaiteUserTask3_endEvent");
        s4.setName("kaiteUserTask3_endEvent");
        s4.setSourceRef("kaiteUserTask3");
        s4.setTargetRef("endEvent");
        toEnd.add(s4);

        startEvent.setOutgoingFlows(sequenceFlows1);
        kaiteUserTask1.setIncomingFlows(sequenceFlows1);
        kaiteUserTask1.setOutgoingFlows(sequenceFlows2);
        kaiteUserTask2.setIncomingFlows(sequenceFlows2);
        kaiteUserTask2.setOutgoingFlows(sequenceFlows3);
        kaiteUserTask3.setIncomingFlows(sequenceFlows3);
        kaiteUserTask3.setOutgoingFlows(toEnd);
        endEvent.setIncomingFlows(toEnd);

        process.addFlowElement(startEvent);
        process.addFlowElement(kaiteUserTask1);
        process.addFlowElement(kaiteUserTask2);
        process.addFlowElement(kaiteUserTask3);
        process.addFlowElement(endEvent);
        process.addFlowElement(s1);
        process.addFlowElement(s2);
        process.addFlowElement(s3);
        process.addFlowElement(s4);

        bpmnModel.addProcess(process);
    }

    @Test
    public void convertToXml(){
        BpmnXMLConverter bpmnXMLConverter=new BpmnXMLConverter();
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String bytes=new String(convertToXML);
        System.out.println(bytes);
    }

    @Test
    public void convertToBpmnModel() throws IOException {
        BpmnXMLConverter bpmnXMLConverter=new BpmnXMLConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/jump.bpmn20.xml");
        bpmnModel = bpmnXMLConverter.convertToBpmnModel(resourceStreamSource,false,false);
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String xml = new String(convertToXML);
        System.out.println(xml);
    }

    @Test
    public void deploy(){
        DeploymentBuilder deploymentBuilder = processEngine.getRepositoryService().createDeployment().addClasspathResource("process/jump.bpmn20.xml");
        deploymentBuilder.disableBpmnValidation();
        deploymentBuilder.disableSchemaValidation();
        deploymentBuilder.key("testJump");
        deploymentBuilder.name("测试跳跃流程");
        deploymentBuilder.category("jump");
        Deployment deployment = deploymentBuilder.deploy();
        System.out.println(deployment.getId());
    }

    @Test
    public void sponsor(){
        ProcessInstance processInstance = processEngine.getRuntimeService().sponsorProcessInstanceByKey("testJumpFlow", "");
        System.out.println(processInstance.getName());
        System.out.println(processInstance.getProcessInstanceId());
    }

    @Test
    public void jumpToTask1(){
        Map<String,Object> variables = new HashMap<>();
        variables.put("kaiteUserTask3","jumpUser3");
        processEngine.getTaskService().jump("c7d430d7-7cf0-11ec-9ee5-7e89041dd10b","kaiteUserTask3",variables);
    }

    @Test
    public void retrieveTask(){
//        processEngine.getTaskService().retrieveTask("15a7b8e5-7d03-11ec-bd73-1a999195866b","取回");
    }

    @Test
    public void completeTask(){
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("c7c03399-7cf0-11ec-9ee5-7e89041dd10b").list();
        for (Task each : tasks) {
//            processEngine.getTaskService().complete(each.getId(),"test1");
            processEngine.getTaskService().complete(each.getId());
        }
    }

}
